module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/recommended',
  ],
  overrides: [],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'prefer-arrow-functions'],
  rules: {
    'spaced-comment': ['error', 'always'],
    'multiline-comment-style': ['error', 'separate-lines'],
    'import/no-default-export': 2,
    'import/no-unresolved': 0,
    'import/named': 0,
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    'react/display-name': 'off',
    'react/prop-types': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/jsx-indent-props': ['error', 2],
    'react/function-component-definition': [
      'error',
      { namedComponents: 'arrow-function' },
    ],
    // 'arrow-body-style': ['error', 'as-needed'],
    'prefer-arrow-functions/prefer-arrow-functions': [
      'error',
      {
        classPropertiesAllowed: false,
        disallowPrototype: true,
        returnStyle: 'unchanged',
        singleReturnOnly: true,
      },
    ],
  },
  settings: {
    react: {
      version: '18.0',
    },
  },
};
