#!/bin/bash
export SHELL="/bin/bash"
export PGHOST="db"

#Database Setup
export PGPASSWORD="postgres"
echo "create user pleroma with encrypted password 'pleroma';" | psql -U postgres postgres
echo "alter role pleroma superuser;" | psql -U postgres postgres
echo "create database akkoma with template = template0 owner = pleroma encoding = 'utf8';" | psql -U postgres postgres

export PGPASSWORD="pleroma"
echo "create extension if not exists citext;" | psql -U pleroma pleroma 
echo "create extension if not exists pg_trgm;" | psql -U pleroma pleroma
echo "create extension if not exists \"uuid-ossp\";" |  psql -U pleroma pleroma

su pleroma -s $SHELL -lc "/opt/pleroma/bin/pleroma_ctl migrate"
su pleroma -s $SHELL -lc "/opt/pleroma/bin/pleroma daemon"

echo "Sleeping for 20 seconds so Akkoma starts.........."
sleep 20
echo "Create and Admin User:"
su pleroma -s $SHELL -lc "./bin/pleroma_ctl user new admin admin@akkoma.google --admin --password admin -y"
su pleroma -s $SHELL -lc "./bin/pleroma_ctl frontend install pleroma-fe --ref stable"
su pleroma -s $SHELL -lc "./bin/pleroma_ctl frontend install admin-fe --ref stable"