#!/bin/bash
export CURRENT_UID=$(id -u):$(id -g)

docker network create --gateway 172.16.1.1 --subnet 172.16.1.0/24 cumfe_subnet 
docker-compose -p cumfe down
docker-compose -p cumfe up -d