#!/bin/bash
export SHELL="/bin/bash"
cd /opt/misskey

export PGHOST="db"
export PGUSER="postgres"
export PGPASSWORD="postgres"

echo "create user misskey with encrypted password 'misskey';"| psql -U postgres postgres
echo "alter role misskey superuser;" | psql -U postgres postgres
echo "create database misskey with template = template0 owner = misskey encoding = 'utf8';" | psql -U postgres postgres
echo "grant all privileges on database misskey to misskey;" | psql -U postgres postgres

yarn run init