import Config

config :pleroma, Pleroma.Web.Endpoint,
   url: [host: "pleroma.google", scheme: "https", port: 443],
   http: [ip: {0, 0, 0, 0}, port: 4000],
   secret_key_base: "0jKveRU44GNncPeKLgcdGVm+7rJb2hNxCpCWd2g0A8Lw2cVJmDCypNqa3ljMZcwJ",
   signing_salt: "C+0dBzoS"

config :pleroma, :instance,
  name: "admin",
  email: "admin@pleroma.google",
  notify_email: "admin@pleroma.google",
  limit: 5000,
  registrations_open: true

config :pleroma, :media_proxy,
  enabled: false,
  redirect_on_failure: true

config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "pleroma",
  password: "pleroma",
  database: "pleroma",
  hostname: "db"

config :web_push_encryption, :vapid_details,
  subject: "mailto:admin@pleroma.google",
  public_key: "BFXyq2frSPpiMgueoxcmvoMzp6Bm-vXI-dasDM4fAA-zjP7JgILG06LKpQuU4eLdbjGnYsmurUeubD8l7b103zg",
  private_key: "Ths8KtdsPNEXl2EazZj7MQRY3xxIb0FOCjEvai4eI8Y"

config :pleroma, :database, rum_enabled: false
config :pleroma, :instance, static_dir: "/var/lib/pleroma/static"
config :pleroma, Pleroma.Uploaders.Local, uploads: "/var/lib/pleroma/uploads"

config :joken, default_signer: "6EasmUX4lCJSlAnGScL0rXBhIeoaWjarH7TGhdbe9VLXO/MnSkMY2LaZixW7kgaT"

config :pleroma, configurable_from_database: true
