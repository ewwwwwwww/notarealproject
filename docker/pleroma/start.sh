#!/bin/bash
export SHELL="/bin/bash"
FILE='/configured.txt'

#Start Cron and Postgresql
crond

#Restore latest Database if container is new
if [ ! -f "$FILE" ]
then
bash /setup.sh
touch /configured.txt
fi

echo "starting nginx"

nginx

echo "starting pleroma"

#Start Pleroma
su pleroma -s $SHELL -lc "/opt/pleroma/bin/pleroma daemon"&

tail -f /dev/null