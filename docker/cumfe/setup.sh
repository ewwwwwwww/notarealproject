#!/bin/bash
export SHELL="/bin/bash"
export PGHOST="db"
cd /opt/cumfe

yarn prepare
yarn cmder setup -y
yarn ws @cum/db gm migrate