#!/bin/bash
export SHELL="/bin/bash"
FILE='/configured.txt'

cd /opt/cumfe

yarn install

#Restore latest Database if container is new
if [ ! -f "$FILE" ]
then
bash /setup.sh
touch /configured.txt
fi

echo "starting nginx"

nginx

echo "starting cumfe"

# FIXME: use NODE_EXTRA_CA_CERTS
export NODE_TLS_REJECT_UNAUTHORIZED=0

yarn watch

tail -f /dev/null