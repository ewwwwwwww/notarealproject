// eslint-disable-next-line
/// <reference types="vitest" />
import { defineConfig } from 'vite';

// eslint-disable-next-line
export default defineConfig({
  test: {
    include: [
      '@cum/check/src/plugins/**.ts',
      '**/*.{test,spec}.{ts,mts,cts,jsx,tsx}',
    ],
    exclude: [
      '**/node_modules/**',
      '**/dist/**',
      '**/cypress/**',
      '**/.{idea,git,cache,output,temp}/**',
      '.pnp.cjs',
      '.pnp.loader.mjs',
      '@cum/check/src.old',
    ],
    coverage: {
      skipFull: true,
    },
  },
});
