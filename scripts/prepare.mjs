import concurrently from 'concurrently';

const run = async (commands) => {
  try {
    return concurrently(commands);
  } catch (err) {
    console.log(err);
  }
};

const main = async () => {
  await run([
    {
      command: 'yarn workspace @cum/config build',
      name: 'config',
    },
    {
      command: 'yarn workspace @cum/fixtures build',
      name: 'fixtures',
    },
  ]);

  await run([
    {
      command: 'yarn workspace @cum/check build',
      name: 'check',
    },
    {
      command: 'yarn workspace @cum/cmder build',
      name: 'cmder',
    },
  ]);
};

main();
