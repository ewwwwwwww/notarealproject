import concurrently from 'concurrently';

const run = async (commands) => {
  try {
    return concurrently(commands);
  } catch (err) {
    console.log(err);
  }
};

const main = async () => {
  await run([
    {
      command: 'yarn workspace @cum/config watch',
      name: 'config',
    },
    {
      command: 'yarn workspace @cum/fixtures watch',
      name: 'fixtures',
    },
    {
      command: 'yarn workspace @cum/db watch | npx pino-pretty',
      name: 'db',
    },
    {
      command: 'yarn workspace @cum/check watch',
      name: 'check',
    },
    {
      command: 'yarn workspace @cum/cmder watch',
      name: 'cmder',
    },
    {
      command: 'yarn workspace @cum/fe watch | npx pino-pretty',
      name: 'fe',
    },
    {
      command: 'yarn workspace @cum/be watch | npx pino-pretty',
      name: 'be',
    },
    {
      command: 'yarn workspace @cum/admin watch',
      name: 'admin',
    },
    {
      command: 'yarn workspace @cum/proxy watch',
      name: 'proxy',
    },
  ]);
};

main();
