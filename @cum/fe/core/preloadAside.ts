import { PageContext } from './types.js';
import { Environment, GraphQLTaggedNode } from 'react-relay';
import reactRelay from 'react-relay';

const { loadQuery } = reactRelay;

export const getAside = async (
  type?: string,
): Promise<{ query: GraphQLTaggedNode; Aside: any; AsideBar?: any }> => {
  return import(`aside/${type}.tsx`).then((m) => {
    if (!m.Aside) {
      throw new Error(`aside/${type}.tsx is missing 'Aside' export`);
    }

    return m;
  });
};

export const preloadAside = async (
  pageContext: PageContext,
  environment: Environment,
  variables?: Record<string, unknown>,
) => {
  const { Aside: AsideType, AsideVariables } = pageContext.exports;

  if (!AsideType) return null;

  const vars =
    typeof AsideVariables === 'function'
      ? AsideVariables(variables, pageContext.routeParams)
      : {};

  const { query, Aside, AsideBar } = await getAside(AsideType);

  if (!query) {
    return { Aside, AsideBar };
  }

  const queryRef = loadQuery(environment, query, vars);

  return { Aside, AsideBar, queryRef };
};
