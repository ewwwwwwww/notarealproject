export const isNumber = (n: any): n is number => {
  return typeof n === 'number';
};

export const isString = (s: any): s is string => {
  return typeof s === 'string';
};

export const isUuid = (s: any): s is UUID => {
  return isString(s) && s.length === 36;
};
