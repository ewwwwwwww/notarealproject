import type { I18n } from '@lingui/core';
import { LocaleData } from 'javascript-time-ago';
import { en } from 'make-plural/plurals';

export const locales = ['en', 'psuedo'];
export const localeDefault = 'en';

// anounce which locales we are going to use and connect them to approprite plural rules
export function initTranslation(i18n: I18n): void {
  i18n.loadLocaleData({
    en: { plurals: en },
    pseudo: { plurals: en },
  });
}

// CVE:FIXME: see if locale can be injected with ../
// FIXME: psuedo not loading
export async function loadTranslation(locale: string, isProduction = true) {
  let data;

  if (isProduction) {
    data = await import(`../locales/${locale}/messages.ts`);
  } else {
    data = await import(`../locales/${locale}/messages.ts`);
    // data = await import(
    //   /* @vite-ignore */ `@lingui/loader!../locales/${lcl}/messages.po`
    // );
  }

  return data.messages;
}

// FIXME: see if locale can be injected with ../
export async function loadTranslationTime(locale: string): Promise<LocaleData> {
  // check if file missing the import default
  const data = await import(`../locales/${locale}/timeago.json`);
  //   assert: {
  //     type: 'json',
  //   },
  // };

  return data;
}
