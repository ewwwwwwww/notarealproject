export const getSrcPath = (hash: string) => {
  if (!hash) return null;

  const prefix = hash.slice(0, 2);

  return `/file/${prefix}/${hash}`;
};

export const getUserPath = (user: {
  remote?: boolean | null;
  nameFull?: string | null;
  name?: string | null;
}) => {
  if (!user) return '';

  return `/user/${user.remote ? user.nameFull : user.name}`;
};
