import { lru } from 'tiny-lru';

const userIdLRU = lru<string>();

export const cacheUserId = (name: string, value?: string) => {
  if (value) {
    userIdLRU.set(name, value);

    return value;
  }

  return userIdLRU.get(name);
};
