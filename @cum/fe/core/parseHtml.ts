interface Emoji {
  name: string;
  hash: string;
}

enum TokenType {
  Text,
  Emoji,
  Mention,
  GreenText,
  RedText,
}

interface Token {
  type: TokenType;
  data: boolean;
}

// <p>
//   <span
//     class="mention"
//     data-index="3"
//     data-denotation-char="@"
//     data-id="4"
//     data-value="Jane Sinkspitter"
//   >
//
//     <span contenteditable="false">
//       <span class="ql-mention-denotation-char">@</span>Jane Sinkspitter
//     </span>
//
//   </span>{' '}
// </p>;

const tokenizer = (input: string): Token[] => {
  return [];
};

export const parseHtml = (input: string, emoji: Emoji[]) => {
  return {
    body: input,
    mentions: [],
  };
};
