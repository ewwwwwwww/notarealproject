import { PageContext } from './types.js';
import { Environment } from 'react-relay';
import reactRelay from 'react-relay';

const { loadQuery } = reactRelay;

export const preloadQuery = (
  pageContext: PageContext,
  environment: Environment,
) => {
  const { query, PageVariables } = pageContext.exports;

  if (PageVariables) {
    const { _query, ...variables } = PageVariables(
      pageContext.routeParams ?? {},
    );

    if (_query) {
      return {
        query: _query,
        queryRef: loadQuery(environment, _query, variables),
        variables,
      };
    }

    if (!query) {
      throw new Error('page has query variables but is missing a query');
    }

    return {
      queryRef: loadQuery(environment, query, variables),
      variables,
    };
  }

  if (query) {
    return {
      queryRef: loadQuery(environment, query, {}),
      variables: {},
    };
  }

  return { variables: {} };
};
