import type { PageContextBuiltIn } from 'vite-plugin-ssr';
import { GraphQLTaggedNode, OperationType, RecordSource } from 'relay-runtime';
import { RecordMap } from 'relay-runtime/lib/store/RelayStoreTypes.js';
import { PreloadedQuery } from 'react-relay';
import { AsideType } from 'components/layout/useLayout.js';
// TODO: fix type
// import type { PageContextBuiltInClient } from 'vite-plugin-ssr/client/router'; // When using Client Routing
// import type { PageContextBuiltInClient } from 'vite-plugin-ssr/client' // When using Server Routing

export type Page = (pageProps: PageProps) => React.ReactElement;
export type PageProps = Record<string, unknown>;

export type PageContextCustom = {
  Page: Page;
  userAgent: string;
  pageProps?: PageProps;
  locale: string;
  relayInitData?: any;
  cookiesInit?: string;

  // TODO: types
  translation: any;
  // TODO: types
  translationTime: any;

  exports: {
    documentProps?: {
      title: string;
    };
    query?: GraphQLTaggedNode;
    PageVariables?: PV<OperationType, unknown>;
    AsideVariables?: AV<OperationType, OperationType, unknown>;
    Aside?: AsideType;
    relayMock?: boolean;
  };

  documentProps?: {
    title: string;
  };
};

export type PageContextServer = PageContextBuiltIn<Page> & PageContextCustom;
// type PageContextClient = PageContextBuiltInClient<Page> & PageContextCustom;
export type PageContextClient = PageContextBuiltIn<Page> & PageContextCustom;

export type PageContext = PageContextClient | PageContextServer;

/**
 * Page Variable function type
 *
 * @remarks
 * This can be used to take in page params and transform them into graphql variables
 *
 */
export type PV<
  Query extends OperationType,
  Params = Record<string, unknown>,
> = (params: Params) => Query['variables'] & { _query?: GraphQLTaggedNode };

export type PC<Query extends OperationType> = React.FC<{
  query?: GraphQLTaggedNode;
  queryRef: PreloadedQuery<Query>;
}>;

export type AV<
  Query extends OperationType,
  AsideQuery extends OperationType,
  Params = {},
> = (
  variables: Query['variables'] | undefined,
  params: Params,
) => AsideQuery['variables'];
