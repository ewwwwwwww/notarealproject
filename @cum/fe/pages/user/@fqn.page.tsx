import type { AV, PC, PV } from 'core/types.js';
import type { FqnQuery } from './__generated__/FqnQuery.graphql.js';
import type { InboxAsideQuery } from 'aside/__generated__/InboxAsideQuery.graphql.js';

import { graphql, usePreloadedQuery } from 'react-relay';

import { Section } from 'components/layout/Layout.jsx';
import { UserProfile } from 'components/user/UserProfile.jsx';
import { UserGallery } from 'components/user/UserGallery.jsx';
import { Timeline, TimelineBar } from 'components/timeline/Timeline.jsx';
import { timelineStore } from 'components/timeline/useTimeline.js';
import { useMeFragment } from 'hooks/useMe.js';
import { shortDateStore, useShortDate } from 'components/core/ShortDate.jsx';

export const query = graphql`
  query FqnQuery(
    $condition: NoteCondition!
    $orderBy: NotesOrderBy!
    $name: String!
    $domain: String
  ) {
    instance {
      ...Logo_instance
    }

    me {
      id
      ...Sidebar_user
      ...useMeFragment
    }

    user: userByName(name: $name, domain: $domain) {
      id
      ...UserProfile_user
      ...UserGallery_user
      ...UserCard_user
      ...UserMeta_user
    }

    ...TimelineFragment @arguments(condition: $condition, orderBy: $orderBy)

    now
  }
`;

interface Props {
  fqn: string;
}

export const PageVariables: PV<FqnQuery, Props> = (params) => {
  const { getState } = timelineStore;
  const { reset, setCondition } = getState();
  const { fqn } = params;

  reset();

  const values = fqn.split('@');

  let name;
  let domain;

  if (values.length === 1) {
    name = values[0];

    setCondition('userByName', name);
  } else if (values.length === 2) {
    name = values[0];
    domain = values[1];

    setCondition('userByFqn', name);
  } else {
    throw new Error('invalid username');
  }

  const { orderBy, condition } = getState();

  return {
    name,
    domain,
    orderBy: orderBy,
    condition: condition,
  };
};

export const Aside = 'User';

export const AsideVariables: AV<FqnQuery, InboxAsideQuery, Props> = (vars) => {
  const { name, domain } = vars ?? {};
  return { name, domain };
};

export const Page: PC<FqnQuery> = (props) => {
  const { queryRef } = props;
  const { instance, me, now, ...data } = usePreloadedQuery(query, queryRef);
  const { setOffset } = shortDateStore.getState();

  setOffset(now);
  useMeFragment(me);

  // TODO: 404
  if (!data.user) return <div>user not found</div>;

  return (
    <Section Header={TimelineBar}>
      <Timeline data={data}>
        <UserProfile data={data.user} />
        <UserGallery data={data.user} />
      </Timeline>
    </Section>
  );
};
