/**
 * @generated SignedSource<<5ad1660d7b8e3463a0a40237029de525>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NotesOrderBy = "COUNT_ATTACHMENTS_ASC" | "COUNT_ATTACHMENTS_DESC" | "CTIME_ASC" | "CTIME_DESC" | "DOMAIN_ID_ASC" | "DOMAIN_ID_DESC" | "NATURAL" | "NOTE_ID_ASC" | "NOTE_ID_DESC" | "PRIMARY_KEY_ASC" | "PRIMARY_KEY_DESC" | "REMOTE_ASC" | "REMOTE_DESC" | "REPLY_ID_ASC" | "REPLY_ID_DESC" | "SENSITIVE_ASC" | "SENSITIVE_DESC" | "THREAD_ID_ASC" | "THREAD_ID_DESC" | "USER_ID_ASC" | "USER_ID_DESC" | "WEIGHTED_KEYWORDS_ASC" | "WEIGHTED_KEYWORDS_DESC" | "%future added value";
export type NoteCondition = {
  countAttachments?: number | null;
  ctime?: Datetime | null;
  dateEnd?: string | null;
  dateStart?: string | null;
  domainId?: UUID | null;
  domainName?: string | null;
  fullText?: string | null;
  hasTags?: ReadonlyArray<string> | null;
  hideBlocks?: boolean | null;
  noteId?: UUID | null;
  onlyFollows?: boolean | null;
  remote?: boolean | null;
  replyId?: UUID | null;
  sensitive?: boolean | null;
  threadId?: UUID | null;
  userByFqn?: string | null;
  userByName?: string | null;
  userId?: UUID | null;
  weightedKeywords?: string | null;
};
export type FqnQuery$variables = {
  condition: NoteCondition;
  domain?: string | null;
  name: string;
  orderBy: NotesOrderBy;
};
export type FqnQuery$data = {
  readonly instance: {
    readonly " $fragmentSpreads": FragmentRefs<"Logo_instance">;
  } | null;
  readonly me: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"Sidebar_user" | "useMeFragment">;
  } | null;
  readonly now: Datetime | null;
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserCard_user" | "UserGallery_user" | "UserMeta_user" | "UserProfile_user">;
  } | null;
  readonly " $fragmentSpreads": FragmentRefs<"TimelineFragment">;
};
export type FqnQuery = {
  response: FqnQuery$data;
  variables: FqnQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "condition"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "domain"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "name"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "orderBy"
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v5 = [
  {
    "kind": "Variable",
    "name": "domain",
    "variableName": "domain"
  },
  {
    "kind": "Variable",
    "name": "name",
    "variableName": "name"
  }
],
v6 = {
  "kind": "Variable",
  "name": "condition",
  "variableName": "condition"
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "now",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v9 = [
  (v4/*: any*/),
  (v8/*: any*/)
],
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "userId",
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nick",
  "storageKey": null
},
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "caption",
  "storageKey": null
},
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "countFollowers",
  "storageKey": null
},
v16 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "countFollowing",
  "storageKey": null
},
v17 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "countBlockers",
  "storageKey": null
},
v18 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "countBlocking",
  "storageKey": null
},
v19 = [
  (v4/*: any*/)
],
v20 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "manuallyApprovesFollowers",
  "storageKey": null
},
v21 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "ctime",
  "storageKey": null
},
v22 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v23 = [
  (v4/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "src",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "File",
    "kind": "LinkedField",
    "name": "file",
    "plural": false,
    "selections": [
      (v4/*: any*/),
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "blake3",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
],
v24 = {
  "alias": null,
  "args": null,
  "concreteType": "UserAttachment",
  "kind": "LinkedField",
  "name": "avatar",
  "plural": false,
  "selections": (v23/*: any*/),
  "storageKey": null
},
v25 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "noteId",
  "storageKey": null
},
v26 = {
  "kind": "Literal",
  "name": "first",
  "value": 10
},
v27 = [
  (v6/*: any*/),
  (v26/*: any*/),
  {
    "items": [
      {
        "kind": "Variable",
        "name": "orderBy.0",
        "variableName": "orderBy"
      }
    ],
    "kind": "ListValue",
    "name": "orderBy"
  }
],
v28 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "cursor",
  "storageKey": null
},
v29 = [
  (v26/*: any*/)
],
v30 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v31 = {
  "alias": null,
  "args": null,
  "concreteType": "PageInfo",
  "kind": "LinkedField",
  "name": "pageInfo",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "endCursor",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hasNextPage",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "FqnQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Domain",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Logo_instance"
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v4/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Sidebar_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "useMeFragment"
          }
        ],
        "storageKey": null
      },
      {
        "alias": "user",
        "args": (v5/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "userByName",
        "plural": false,
        "selections": [
          (v4/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserProfile_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserGallery_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserCard_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserMeta_user"
          }
        ],
        "storageKey": null
      },
      {
        "args": [
          (v6/*: any*/),
          {
            "kind": "Variable",
            "name": "orderBy",
            "variableName": "orderBy"
          }
        ],
        "kind": "FragmentSpread",
        "name": "TimelineFragment"
      },
      (v7/*: any*/)
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v3/*: any*/),
      (v2/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "FqnQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Domain",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": (v9/*: any*/),
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v4/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countNotificationsUnread",
            "storageKey": null
          },
          (v10/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "domainId",
            "storageKey": null
          },
          (v8/*: any*/),
          (v11/*: any*/),
          (v12/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "dead",
            "storageKey": null
          },
          (v13/*: any*/),
          (v14/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "avatarId",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "bannerId",
            "storageKey": null
          },
          (v15/*: any*/),
          (v16/*: any*/),
          (v17/*: any*/),
          (v18/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countNotifications",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Domain",
            "kind": "LinkedField",
            "name": "domain",
            "plural": false,
            "selections": (v9/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserAttachment",
            "kind": "LinkedField",
            "name": "avatar",
            "plural": false,
            "selections": (v19/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserAttachment",
            "kind": "LinkedField",
            "name": "banner",
            "plural": false,
            "selections": (v19/*: any*/),
            "storageKey": null
          },
          (v20/*: any*/),
          (v21/*: any*/)
        ],
        "storageKey": null
      },
      {
        "alias": "user",
        "args": (v5/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "userByName",
        "plural": false,
        "selections": [
          (v4/*: any*/),
          (v14/*: any*/),
          (v13/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "FieldsConnection",
            "kind": "LinkedField",
            "name": "fields",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Field",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "label",
                    "storageKey": null
                  },
                  (v4/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v22/*: any*/),
          (v8/*: any*/),
          (v11/*: any*/),
          (v24/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countNotes",
            "storageKey": null
          },
          (v15/*: any*/),
          (v16/*: any*/),
          (v10/*: any*/),
          (v20/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserAttachment",
            "kind": "LinkedField",
            "name": "banner",
            "plural": false,
            "selections": (v23/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "first",
                "value": 8
              }
            ],
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Note",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v4/*: any*/),
                  (v25/*: any*/),
                  {
                    "alias": null,
                    "args": [
                      {
                        "kind": "Literal",
                        "name": "first",
                        "value": 1
                      }
                    ],
                    "concreteType": "NoteAttachmentsConnection",
                    "kind": "LinkedField",
                    "name": "noteAttachments",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "NoteAttachment",
                        "kind": "LinkedField",
                        "name": "nodes",
                        "plural": true,
                        "selections": [
                          (v4/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "attachmentId",
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": "noteAttachments(first:1)"
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notes(first:8)"
          },
          (v12/*: any*/),
          (v21/*: any*/),
          (v17/*: any*/),
          (v18/*: any*/)
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v27/*: any*/),
        "concreteType": "NotesConnection",
        "kind": "LinkedField",
        "name": "notes",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "NotesEdge",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              (v28/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Note",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "threadId",
                    "storageKey": null
                  },
                  (v25/*: any*/),
                  (v21/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "scope",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Domain",
                    "kind": "LinkedField",
                    "name": "domain",
                    "plural": false,
                    "selections": [
                      (v4/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "host",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "user",
                    "plural": false,
                    "selections": [
                      (v4/*: any*/),
                      (v22/*: any*/),
                      (v8/*: any*/),
                      (v11/*: any*/),
                      (v24/*: any*/),
                      (v12/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "replyId",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": (v29/*: any*/),
                    "concreteType": "MentionsConnection",
                    "kind": "LinkedField",
                    "name": "mentions",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "MentionsEdge",
                        "kind": "LinkedField",
                        "name": "edges",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Mention",
                            "kind": "LinkedField",
                            "name": "node",
                            "plural": false,
                            "selections": [
                              (v4/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "User",
                                "kind": "LinkedField",
                                "name": "user",
                                "plural": false,
                                "selections": [
                                  (v4/*: any*/),
                                  (v8/*: any*/),
                                  (v22/*: any*/),
                                  (v11/*: any*/),
                                  (v24/*: any*/)
                                ],
                                "storageKey": null
                              },
                              (v30/*: any*/)
                            ],
                            "storageKey": null
                          },
                          (v28/*: any*/)
                        ],
                        "storageKey": null
                      },
                      (v31/*: any*/)
                    ],
                    "storageKey": "mentions(first:10)"
                  },
                  {
                    "alias": null,
                    "args": (v29/*: any*/),
                    "filters": null,
                    "handle": "connection",
                    "key": "NoteMentions_user_mentions",
                    "kind": "LinkedHandle",
                    "name": "mentions"
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactAggrsConnection",
                    "kind": "LinkedField",
                    "name": "reactAggrs",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ReactAggrsEdge",
                        "kind": "LinkedField",
                        "name": "edges",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ReactAggr",
                            "kind": "LinkedField",
                            "name": "node",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "count",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "countReplies",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "countLikes",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "countBookmarks",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "countReacts",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "countRepeats",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isLikedByYou",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isBookmarkedByYou",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isRepeatedByYou",
                    "storageKey": null
                  },
                  (v30/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v31/*: any*/),
          {
            "kind": "ClientExtension",
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "__id",
                "storageKey": null
              }
            ]
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v27/*: any*/),
        "filters": [
          "condition",
          "orderBy"
        ],
        "handle": "connection",
        "key": "TimelineFragment_notes",
        "kind": "LinkedHandle",
        "name": "notes"
      },
      (v7/*: any*/)
    ]
  },
  "params": {
    "cacheID": "85e0b09ab2501fd457bd44ef1ca9835d",
    "id": null,
    "metadata": {},
    "name": "FqnQuery",
    "operationKind": "query",
    "text": "query FqnQuery(\n  $condition: NoteCondition!\n  $orderBy: NotesOrderBy!\n  $name: String!\n  $domain: String\n) {\n  instance {\n    ...Logo_instance\n    id\n  }\n  me {\n    id\n    ...Sidebar_user\n    ...useMeFragment\n  }\n  user: userByName(name: $name, domain: $domain) {\n    id\n    ...UserProfile_user\n    ...UserGallery_user\n    ...UserCard_user\n    ...UserMeta_user\n  }\n  ...TimelineFragment_2DBGLG\n  now\n}\n\nfragment FollowButton_user on User {\n  id\n  userId\n  countFollowers\n  remote\n  manuallyApprovesFollowers\n  isFollowedByYou\n  isBlockingYou\n  isBlockedByYou\n}\n\nfragment Logo_instance on Domain {\n  id\n  name\n}\n\nfragment NoteActions_note on Note {\n  id\n  noteId\n  countReplies\n  countLikes\n  countBookmarks\n  countReacts\n  countRepeats\n  isLikedByYou\n  isBookmarkedByYou\n  isRepeatedByYou\n}\n\nfragment NoteCard_note on Note {\n  id\n  body\n  threadId\n  noteId\n  ...NoteHeader_note\n  ...NoteMentions_note\n  ...NoteReacts_note\n  ...NoteActions_note\n}\n\nfragment NoteFavicon_domain on Domain {\n  id\n  host\n}\n\nfragment NoteHeader_note on Note {\n  ctime\n  scope\n  domain {\n    ...NoteFavicon_domain\n    id\n  }\n  user {\n    id\n    ...UserAvatar_user\n    ...UserName_user\n  }\n}\n\nfragment NoteMentions_note on Note {\n  replyId\n  mentions(first: 10) {\n    edges {\n      node {\n        id\n        user {\n          id\n          name\n          ...UserAvatar_user\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n  id\n}\n\nfragment NoteReacts_note on Note {\n  reactAggrs {\n    edges {\n      node {\n        count\n      }\n    }\n  }\n}\n\nfragment Sidebar_user on User {\n  id\n  countNotificationsUnread\n  ...UserButton_user\n}\n\nfragment TimelineFragment_2DBGLG on Query {\n  notes(first: 10, condition: $condition, orderBy: [$orderBy]) {\n    edges {\n      cursor\n      node {\n        id\n        ...NoteCard_note\n        __typename\n      }\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n\nfragment UserActionBar_user on User {\n  id\n  isBlockedByYou\n}\n\nfragment UserAvatar_user on User {\n  id\n  ...UserLink\n  avatar {\n    id\n    src\n    file {\n      id\n      blake3\n    }\n  }\n}\n\nfragment UserBanner_user on User {\n  id\n  banner {\n    id\n    src\n    file {\n      id\n      blake3\n    }\n  }\n}\n\nfragment UserButton_user on User {\n  id\n}\n\nfragment UserCardMobile_user on User {\n  id\n  ...UserAvatar_user\n  ...UserCardStats_user\n  ...FollowButton_user\n}\n\nfragment UserCardStats_user on User {\n  countNotes\n  countFollowers\n  countFollowing\n}\n\nfragment UserCard_user on User {\n  id\n  ...UserAvatar_user\n  ...UserName_user\n  ...UserCardStats_user\n  ...FollowButton_user\n}\n\nfragment UserGallery_user on User {\n  id\n  notes(first: 8) {\n    nodes {\n      id\n      noteId\n      noteAttachments(first: 1) {\n        nodes {\n          id\n          attachmentId\n        }\n      }\n    }\n  }\n}\n\nfragment UserLink on User {\n  id\n  remote\n  name\n  nameFull\n}\n\nfragment UserMeta_user on User {\n  id\n  ctime\n  countBlockers\n  countBlocking\n  countNotes\n}\n\nfragment UserName_user on User {\n  id\n  nick\n  name\n  nameFull\n  remote\n}\n\nfragment UserProfile_user on User {\n  caption\n  body\n  fields {\n    nodes {\n      body\n      label\n      id\n    }\n  }\n  ...UserCardMobile_user\n  ...UserActionBar_user\n  ...UserBanner_user\n}\n\nfragment useMeFragment on User {\n  id\n  userId\n  domainId\n  name\n  nameFull\n  nick\n  dead\n  body\n  caption\n  avatarId\n  bannerId\n  countFollowers\n  countFollowing\n  countBlockers\n  countBlocking\n  countNotifications\n  countNotificationsUnread\n  domain {\n    id\n    name\n  }\n  avatar {\n    id\n  }\n  banner {\n    id\n  }\n  manuallyApprovesFollowers\n  ctime\n}\n"
  }
};
})();

(node as any).hash = "676d045cd4fce6a64bd354d2cba6ce13";

export default node;
