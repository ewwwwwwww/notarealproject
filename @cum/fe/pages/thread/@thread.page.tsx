import type { PC, PV } from 'core/types.js';
import type { ThreadQuery } from './__generated__/ThreadQuery.graphql.js';

import { graphql, usePreloadedQuery } from 'react-relay';

import { Section } from 'components/layout/Layout.jsx';
import { Timeline, TimelineBar } from 'components/timeline/Timeline.jsx';
import { timelineStore } from 'components/timeline/useTimeline.js';
import { useMeFragment } from 'hooks/useMe.js';

import shortUUID from 'short-uuid';
import { shortDateStore, useShortDate } from 'components/core/ShortDate.jsx';
const translator = shortUUID();

export const query = graphql`
  query ThreadQuery($condition: NoteCondition!, $orderBy: NotesOrderBy!) {
    instance {
      id
      ...Logo_instance
    }

    me {
      id
      ...Sidebar_user
      ...useMeFragment
    }

    ...TimelineFragment @arguments(condition: $condition, orderBy: $orderBy)
    now
  }
`;

interface Props {
  thread: string;
}

export const PageVariables: PV<ThreadQuery, Props> = (props) => {
  const { getState } = timelineStore;
  const { reset, setCondition } = getState();
  const threadId: UUID = translator.toUUID(props.thread);

  reset();
  setCondition('threadId', threadId);

  const { orderBy, condition } = getState();

  return {
    orderBy: orderBy,
    condition: condition,
  };
};

export const Aside = 'Inbox';

export const Page: PC<ThreadQuery> = (props) => {
  const { queryRef } = props;
  const { instance, me, now, ...data } = usePreloadedQuery(query, queryRef);
  const { setOffset } = shortDateStore.getState();

  setOffset(now);
  useMeFragment(me);

  return (
    <Section Header={TimelineBar} span={'auto'}>
      <Timeline data={data} />
    </Section>
  );
};
