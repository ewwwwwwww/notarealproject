import { PageContext } from 'core/types.js';
import { locales, localeDefault } from 'core/i18n.js';

const extractLocale = (url: string) => {
  const urlPaths = url.split('/');

  let locale: string;
  let urlWithoutLocale: string;

  // We remove the URL locale, for example `/de-DE/about` => `/about`
  const firstPath = urlPaths[1];

  if (
    locales.filter((locale) => locale !== localeDefault).includes(firstPath)
  ) {
    locale = firstPath;
    urlWithoutLocale = '/' + urlPaths.slice(2).join('/');
  } else {
    locale = localeDefault;
    urlWithoutLocale = url;
  }

  return { locale, urlWithoutLocale };
};

export const onBeforeRoute = (pageContext: PageContext) => {
  let urlMod = pageContext.urlOriginal;

  const { urlWithoutLocale, locale } = extractLocale(urlMod);

  return {
    pageContext: {
      // We make `locale` available as `pageContext.locale`.
      // We then use https://vite-plugin-ssr.com/pageContext-anywhere
      // to access `pageContext.locale` in any React/Vue component.
      locale,
      // We overwrite the original URL
      urlOriginal: urlWithoutLocale,
    },
  };
};
