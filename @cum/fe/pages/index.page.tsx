import type { PC, PV } from 'core/types.js';

import { graphql, usePreloadedQuery } from 'react-relay';
import type { pagesIndexQuery } from './__generated__/pagesIndexQuery.graphql.js';

import { Section } from 'components/layout/Layout.jsx';
import { Timeline, TimelineBar } from 'components/timeline/Timeline.jsx';
import { timelineStore } from 'components/timeline/useTimeline.js';
import { useMeFragment } from 'hooks/useMe.js';
import { memo } from 'react';
import { shortDateStore, useShortDate } from 'components/core/ShortDate.jsx';

export const query = graphql`
  query pagesIndexQuery($condition: NoteCondition!, $orderBy: NotesOrderBy!) {
    instance {
      id
      ...Logo_instance
    }

    me {
      id
      ...Sidebar_user
      ...useMeFragment
    }

    ...TimelineFragment @arguments(condition: $condition, orderBy: $orderBy)

    now
  }
`;

interface Props {}

export const PageVariables: PV<pagesIndexQuery, Props> = (props) => {
  const { getState } = timelineStore;
  const { reset } = getState();

  reset();

  const { orderBy, condition } = getState();

  return {
    orderBy: orderBy,
    condition: condition,
  };
};

export const Aside = 'Inbox';

export const Page: PC<pagesIndexQuery> = (props) => {
  const { queryRef } = props;
  const { instance, me, now, ...data } = usePreloadedQuery(query, queryRef);
  const { setOffset } = shortDateStore.getState();

  setOffset(now);
  useMeFragment(me);

  return (
    <Section Header={TimelineBar}>
      <Timeline data={data} />
    </Section>
  );
};
