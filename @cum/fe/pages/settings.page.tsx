import type { PC } from 'core/types.js';

import { graphql, usePreloadedQuery } from 'react-relay';

import { Section } from 'components/layout/Layout.jsx';
import { useMeFragment } from 'hooks/useMe.js';
import { settingsQuery } from './__generated__/settingsQuery.graphql.js';
import { SettingsTheme } from 'components/settings/SettingsTheme.jsx';
import { SettingsSize } from 'components/settings/SettingsSize.jsx';
import { SettingsCard } from 'components/settings/SettingsCard.jsx';
import { SettingsUpload } from 'components/settings/SettingsUpload.jsx';

export const query = graphql`
  query settingsQuery {
    instance {
      id
      ...Logo_instance
    }

    me {
      id
      ...useMeFragment
      ...SettingsUpload
    }
  }
`;

export const Aside = 'Settings';

export const Page: PC<settingsQuery> = (props) => {
  const { queryRef } = props;
  const { instance, me, ...data } = usePreloadedQuery(query, queryRef);

  useMeFragment(me);

  if (!me) return null;

  return (
    <Section>
      <SettingsTheme />
      <SettingsUpload data={me} />
      <SettingsSize />
      <SettingsCard />
    </Section>
  );
};
