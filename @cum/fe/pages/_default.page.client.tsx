import { Root, hydrateRoot, createRoot } from 'react-dom/client';
import { PageShell } from 'components/PageShell.jsx';
import { PageContextClient } from 'core/types.js';
import { Environment } from 'react-relay';
import { useRelayEnvironment } from 'hooks/useRelayEnvironment.js';
import { preloadQuery } from 'core/preloadQuery.js';
import { getPageTitle } from 'core/getPageTitle.js';
import { i18n } from '@lingui/core';
import TimeAgo from 'javascript-time-ago';
import {
  initTranslation,
  loadTranslation,
  loadTranslationTime,
} from 'core/i18n.js';
import { pageStore } from 'hooks/usePage.js';
import { layoutStore } from 'components/layout/useLayout.js';
import { preloadAside } from 'core/preloadAside.js';

// fix relay mock needing global
// eslint-disable-next-line
(window as any).global = window;

let root: Root;
let relay: Environment;

export const clientRouting = true;

export const render = async (pageContext: PageContextClient) => {
  const { Page, exports, locale, relayInitData } = pageContext;
  const { setAside } = layoutStore.getState();
  const { setPage } = pageStore.getState();

  setPage(pageContext);
  setAside(exports.Aside ?? null);

  const translation =
    pageContext.translation ?? (await loadTranslation(locale ?? 'en'));
  const translationTime =
    pageContext.translationTime ?? (await loadTranslationTime(locale ?? 'en'));

  initTranslation(i18n);

  i18n.load(locale ?? 'en', translation);
  i18n.activate(locale ?? 'en');
  TimeAgo.setDefaultLocale(locale);
  TimeAgo.addLocale(translationTime);

  if (!relay) {
    relay = useRelayEnvironment(relayInitData, false, exports.relayMock);
  }

  const { query, queryRef, variables } = preloadQuery(pageContext, relay);
  const aside = await preloadAside(pageContext, relay, variables);

  await queryRef?.source?.toPromise();
  await aside?.queryRef?.source?.toPromise();

  const page = (
    <PageShell relay={relay} i18n={i18n} locale={locale} aside={aside}>
      <Page query={query} queryRef={queryRef} />
    </PageShell>
  );

  const container = document.getElementById('page-view')!;

  // @ts-ignore
  if (pageContext.isHydration) {
    root = hydrateRoot(container, page);
  } else {
    if (!root) root = createRoot(container);

    root.render(page);
  }

  document.title = getPageTitle(pageContext);
};

export const onHydrationEnd = () => {
  console.log('Hydration finished; page is now interactive.');
};

export const onPageTransitionStart = () => {
  console.log('Page transition start');
  document.querySelector('body')!.classList.add('page-is-transitioning');
};

export const onPageTransitionEnd = () => {
  console.log('Page transition end');
  document.querySelector('body')!.classList.remove('page-is-transitioning');
};
