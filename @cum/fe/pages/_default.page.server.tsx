import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { PageContextServer } from 'core/types.js';
import { dangerouslySkipEscape } from 'vite-plugin-ssr';
import { PageShell } from 'components/PageShell.jsx';
import { createStylesServer, ServerStyles } from '@mantine/ssr';
import { useRelayEnvironment } from 'hooks/useRelayEnvironment.js';
import { preloadQuery } from 'core/preloadQuery.js';
import { getPageTitle } from 'core/getPageTitle.js';
import { i18n } from '@lingui/core';
import TimeAgo from 'javascript-time-ago';
import {
  initTranslation,
  loadTranslation,
  loadTranslationTime,
} from 'core/i18n.js';
import { pageStore } from 'hooks/usePage.js';
import { layoutStore } from 'components/layout/useLayout.js';
import { preloadAside } from 'core/preloadAside.js';

export const passToClient = [
  'documentProps',
  'routeParams',
  'relayInitData',
  'locale',
  'translation',
  'translationTime',
];

const stylesServer = createStylesServer();

export const render = async (pageContext: PageContextServer) => {
  const { Page, exports, locale, cookiesInit } = pageContext;
  const { setAside } = layoutStore.getState();
  const { setPage } = pageStore.getState();

  setPage(pageContext);
  setAside(exports.Aside ?? null);

  const translation = await loadTranslation(locale ?? 'en');
  const translationTime = await loadTranslationTime(locale ?? 'en');

  initTranslation(i18n);
  i18n.load(locale ?? 'en', translation);
  i18n.activate(locale ?? 'en');
  TimeAgo.setDefaultLocale(locale ?? 'en');
  TimeAgo.addLocale(translationTime);

  const relay = useRelayEnvironment(
    undefined,
    true,
    exports.relayMock,
    cookiesInit,
  );

  const { query, queryRef, variables } = preloadQuery(pageContext, relay);
  const aside = await preloadAside(pageContext, relay, variables);

  // not needed for streaming ssr
  await Promise.all([
    queryRef?.source?.toPromise(),
    aside?.queryRef?.source?.toPromise(),
  ]);

  const content = renderToString(
    <PageShell relay={relay} i18n={i18n} locale={locale} aside={aside}>
      <Page query={query} queryRef={queryRef} />
    </PageShell>,
  );

  const title = getPageTitle(pageContext);
  const styles = renderToStaticMarkup(
    <ServerStyles html={content} server={stylesServer} />,
  );

  const result = `
    <!DOCTYPE html>
      <head>
        <title>${title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        ${styles}
      </head>
      <body>
        <div id="page-view">${content}</div>
      </body>
    </html>
  `;

  const relayInitData = relay.getStore().getSource().toJSON();

  return {
    documentHtml: dangerouslySkipEscape(result),

    pageContext: {
      translation,
      translationTime,
      relayInitData,
    },
  };
};
