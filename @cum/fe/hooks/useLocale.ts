import create, { createStore } from 'zustand';

interface LocaleState {
  locale: string | null;
  set: (locale: string) => void;
}

export const useLocale = create<LocaleState>((set) => ({
  locale: null,

  set: (locale) => set({ locale }),
}));
