import create, { createStore } from 'zustand';
import { PageContext } from 'core/types.js';

interface State {
  ctx: PageContext | null;

  setPage: (ctx: PageContext) => void;
}

export const pageStore = createStore<State>((set, get) => ({
  ctx: null,

  setPage: (ctx) => set({ ctx }),
}));

export const usePage = create(pageStore);
