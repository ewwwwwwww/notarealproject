import {
  Environment,
  GraphQLResponse,
  Network,
  RecordSource,
  RequestParameters,
  Store,
  Variables,
  Observable,
  ROOT_TYPE,
} from 'relay-runtime';
import { createClient, Client as WsClient } from 'graphql-ws';
import {
  MissingFieldHandler,
  RecordMap,
} from 'relay-runtime/lib/store/RelayStoreTypes.js';
import fetch from 'cross-fetch';
import { cacheUserId } from 'core/cacheUserId.js';

// TODO: remove this type
type MockFn = (
  params: RequestParameters,
  variables: Variables,
) => Promise<GraphQLResponse>;

async function fetchMock(params: RequestParameters, variables: Variables) {
  const mockFn = (await import('./useRelayMock.js').then(
    (m) => m.resolver,
  )) as MockFn;

  if (typeof window !== 'undefined') {
    console.log(await mockFn(params, variables));
  }

  return await mockFn(params, variables);
}

async function fetchServer(
  params: RequestParameters,
  variables: Variables,
  cookie?: string,
) {
  const headers = {
    'Content-Type': 'application/json',
  };

  if (cookie) {
    // @ts-ignore
    headers['Cookie'] = cookie;
  }

  const response = await fetch('https://cumfe.google/graphql', {
    credentials: 'include',
    method: 'POST',
    headers,
    body: JSON.stringify({
      query: params.text,
      variables,
    }),
  });

  return await response.json();
}

async function fetchClient(params: RequestParameters, variables: Variables) {
  console.log(
    `fetching query ${params.name} with ${JSON.stringify(variables)}`,
  );

  const response = await fetch('https://cumfe.google/graphql', {
    credentials: 'include',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: params.text,
      variables,
    }),
  });

  return await response.json();
}

let wsClient: WsClient;

function subscribe(operation: any, variables: Variables) {
  return Observable.create((sink) => {
    return wsClient.subscribe(
      {
        operationName: operation.name,
        query: operation.text,
        variables,
      },
      sink,
    );
  });
}

const missingFieldHandlers: MissingFieldHandler[] = [
  {
    handle(field, record, args) {
      // `me()` should always result in the record with the given `id`.
      // This code will cause relay to look for the record in the cache
      // when it hasn't been fetched via `me()` previously.
      if (
        record != null &&
        record.__typename === ROOT_TYPE &&
        (field.name === 'me' || field.name === 'instance')
      ) {
        return args.id;
      }
    },
    kind: 'linked',
  },
  {
    handle(field, record, args) {
      if (
        record != null &&
        record.__typename === ROOT_TYPE &&
        field.name === 'userById' &&
        args.hasOwnProperty('id')
      ) {
        console.log('looking up:', args.id);
        return args.id;
      }
    },
    kind: 'linked',
  },
  {
    handle(field, record, args) {
      if (
        record != null &&
        record.__typename === ROOT_TYPE &&
        field.name === 'userByName' &&
        args.hasOwnProperty('name') &&
        args.hasOwnProperty('domain')
      ) {
        const name = args.domain ? `${args.name}@${args.domain}` : args.name;
        const id = cacheUserId(name);

        console.log('checking:', name, id);

        if (id) return id;
      }
    },
    kind: 'linked',
  },
];

export const useRelayEnvironment = (
  initialData?: RecordMap,
  isServer = false,
  isMock = false,
  cookie?: string,
) => {
  let network;

  if (typeof window !== 'undefined') {
    wsClient = createClient({
      url: 'wss://cumfe.google/graphql',
    });
  }

  if (isMock) {
    network = Network.create(fetchMock);
  } else if (isServer) {
    network = Network.create((params, variables) =>
      fetchServer(params, variables, cookie),
    );
  } else {
    network = Network.create(fetchClient, subscribe as any);
  }

  const store = new Store(new RecordSource(initialData));

  return new Environment({
    configName: isServer ? 'server' : 'client',
    network,
    store,
    missingFieldHandlers,
  });
};
