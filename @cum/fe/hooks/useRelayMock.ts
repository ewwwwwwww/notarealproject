import { GraphQLHandler, embed } from 'graphql-mocks';
import { relayWrapper } from 'graphql-mocks/relay';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { buildSchema } from 'graphql';
import { RequestParameters, Variables } from 'relay-runtime';
import { falsoMiddleware } from '@graphql-mocks/falso';
import { hi, field, type, HIGHLIGHT_ALL } from 'graphql-mocks/highlight';

// TODO: fix import
// @ts-ignore
import sdlSchema from '../../db/data/schema.graphql?raw';
import {
  randBoolean,
  randEmail,
  randEmoji,
  randFullName,
  randNumber,
  randRecentDate,
  randSoonDate,
} from '@ngneat/falso';
import { randBody, randEmojiUrl, randField, randHash } from '@cum/fixtures';
const schemaString = buildSchema(sdlSchema);

// Make a GraphQL schema with no resolvers
const schema = makeExecutableSchema({ typeDefs: schemaString });

const resolverMap = {};

const hasName = (path: any, key: string, maxDepth = 10, currDepth = 0) => {
  if (currDepth > maxDepth) return false;
  if (path.key === key) return true;
  if (path.prev) return hasName(path.prev, key, maxDepth, currDepth + 1);
  return false;
};

const hasType = (path: any, key: string, maxDepth = 10, currDepth = 0) => {
  if (currDepth > maxDepth) return false;
  if (path.typename === key) return true;
  if (path.prev) return hasType(path.prev, key, maxDepth, currDepth + 1);
  return false;
};

type Resolver = (parent: string, args: any, ctx: any, info: any) => {};

const wildcardMiddlewares = (obj: Record<string, Resolver>) =>
  Object.entries(obj).map(([fieldName, resolver]) =>
    embed({
      replace: false,
      resolver,
      highlight: (h) => h.include(field([HIGHLIGHT_ALL, fieldName])),
    }),
  );

const wildcards = wildcardMiddlewares({
  nick: () => randFullName(),
  ctime: () => randRecentDate({ days: 2 }),
  mtime: () => randRecentDate({ days: 2 }),
  expires: () => randRecentDate({ days: 5 }),
  description: randBody,
  read: randBoolean,
  blurhash: () => randHash(36),
  totalCount: () => randNumber({ min: 0, max: 20000 }),
  notificationUnreadCount: () => randNumber({ min: 0, max: 1200 }),
  nameFull: randEmail,

  body: (parent: string, args: any, ctx: any, info: any) => {
    const { path } = info;

    if (hasType(path, 'Field')) {
      return randField();
    } else {
      return randBody();
    }
  },

  src: (parent: string, args: any, ctx: any, info: any) => {
    const { path } = info;
    const hash = randNumber({ min: 0, max: 10 });

    if (hasType(path, 'React') || hasType(path, 'ReactAggr')) {
      return randEmojiUrl();
    }

    if (hasType(path, 'Mention')) {
      return `https://api.lorem.space/image/face?w=30&h=30&hash=${hash}`;
    }

    if (hasType(path, 'Notification')) {
      return `https://api.lorem.space/image/face?w=30&h=30&hash=${hash}`;
    }

    if (hasType(path, 'Note')) {
      return `https://api.lorem.space/image/face?w=500&h=500&hash=${hash}`;
    }

    if (hasName(path, 'banner')) {
      return `https://api.lorem.space/image/face?w=300&h=800&hash=${hash}`;
    }

    if (hasName(path, 'followers') || hasName(path, 'following')) {
      return `https://api.lorem.space/image/face?w=100&h=100&hash=${hash}`;
    }

    if (hasType(path, 'User')) {
      return `https://api.lorem.space/image/face?w=300&h=300&hash=${hash}`;
    }

    return `https://api.lorem.space/image/face?w=200&h=200&hash=${hash}`;
  },
});

const falso = falsoMiddleware({
  fields: {
    React: {
      unicode: {
        falsoFn: 'randEmoji',
      },
    },

    ReactAggr: {
      unicode: {
        falsoFn: 'randEmoji',
      },
    },

    NotesConnection: {
      nodes: {
        listCount: { min: 100, max: 100 },
      },
    },

    MentionsConnection: {
      nodes: {
        listCount: { min: 0, max: 10 },
      },
    },

    NotificationsConnection: {
      nodes: {
        listCount: { min: 100, max: 200 },
      },
    },

    FieldsConnection: {
      nodes: {
        listCount: { min: 0, max: 5 },
      },
    },

    FollowsConnection: {
      nodes: {
        listCount: { min: 100, max: 200 },
      },
    },

    BlocksConnection: {
      nodes: {
        listCount: { min: 100, max: 200 },
      },
    },

    ReactsConnection: {
      nodes: {
        listCount: { min: 0, max: 4 },
      },
    },

    ReactAggrsConnection: {
      nodes: {
        listCount: { min: 0, max: 4 },
      },
    },

    Notification: {
      mention: { nullPercentage: 1.0 },
      follow: { nullPercentage: 0.0 },
      block: { nullPercentage: 1.0 },
      like: { nullPercentage: 1.0 },
      react: { nullPercentage: 1.0 },
      repeat: { nullPercentage: 1.0 },
    },
  },
});

const relayMiddleware = embed({
  wrappers: [
    relayWrapper({
      cursorForNode: (node: any) => node.id,
      force: false,
    }),
  ],
});

const handler = new GraphQLHandler({
  resolverMap,
  middlewares: [relayMiddleware, ...wildcards, falso],
  dependencies: {
    graphqlSchema: schemaString,
  },
});

export const resolver = (params: RequestParameters, variables: Variables) => {
  return handler.query(params.text ?? '{}', variables);
};
