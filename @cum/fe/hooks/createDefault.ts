// eslint-disable-next-line prefer-arrow-functions/prefer-arrow-functions
export function createDefaultPick<P, R extends Partial<P>, A>(
  fn: (props: P, ...args: A[]) => R,
) {
  return (props: P, ...args: A[]) => fn(props, ...args);
}

// eslint-disable-next-line prefer-arrow-functions/prefer-arrow-functions
export function createDefaultProps<P, R extends Partial<P>, A>(
  fn: (props: P, ...args: A[]) => Partial<P> & Required<R>,
) {
  return (props: P, ...args: A[]) => ({
    ...props,
    ...fn(props, ...args),
  });
}
