import { useMemo } from 'react';
import { OperationType, GraphQLSubscriptionConfig } from 'relay-runtime';
import { useSubscription } from 'react-relay';

export function useMemoSubscription<
  U extends OperationType,
  T = GraphQLSubscriptionConfig<U>,
>(
  props: Required<{
    subscription: GraphQLSubscriptionConfig<U>['subscription'];
    variables: GraphQLSubscriptionConfig<U>['variables'];
  }> &
    Partial<T>,
) {
  const config = useMemo(() => props, []);

  return useSubscription(config);
}
