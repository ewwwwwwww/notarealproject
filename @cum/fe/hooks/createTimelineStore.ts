import { VirtuosoHandle } from 'react-virtuoso';
import { createStore } from 'zustand';

export interface State<OrderBy, Condition> {
  id: string | null;
  virtuoso: VirtuosoHandle | null;
  isScrolling: boolean;
  orderBy: OrderBy;
  condition: Condition | {};

  reset: () => void;
  setId: (id: string | null) => void;
  setVirtuoso: (ref: VirtuosoHandle | null) => void;
  setScrolling: (isScrolling: boolean) => void;
  setCondition: <K extends keyof Condition>(
    key: K,
    value: Condition[K],
  ) => void;
  setOrderBy: (orderBy: OrderBy) => void;
  scrollTop: () => void;
}

const initialState = {
  id: null,
  virtuoso: null,
  isScrolling: false,
  condition: {},
};

export const createTimelineStore = <OrderBy, Condition>(orderBy: OrderBy) => {
  return createStore<State<OrderBy, Condition>>((set, get) => ({
    ...initialState,

    orderBy,

    reset: () => set({ ...initialState, orderBy }),

    setId: (id) => set({ ...get(), id }),

    setVirtuoso: (virtuoso) => set({ ...get(), virtuoso }),

    setScrolling: (isScrolling: boolean) => set({ ...get(), isScrolling }),

    setCondition: (key, value) =>
      set((state) => ({
        ...state,
        condition: { ...state.condition, [key]: value },
      })),

    setOrderBy: (key) => set({ ...get(), orderBy }),

    scrollTop: () => {
      const { virtuoso } = get();

      if (!virtuoso) return console.warn('no virtuoso scroll ref attached');

      virtuoso.scrollTo({ top: 0 });
    },
  }));
};
