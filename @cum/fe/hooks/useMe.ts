import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import create, { createStore } from 'zustand';
import {
  useMeFragment$data,
  useMeFragment$key,
} from './__generated__/useMeFragment.graphql.js';

const fragment = graphql`
  fragment useMeFragment on User {
    id
    userId
    domainId
    name
    nameFull
    nick
    dead
    body
    caption
    avatarId
    bannerId

    countFollowers
    countFollowing
    countBlockers
    countBlocking
    countNotifications
    countNotificationsUnread

    domain {
      id
      name
    }

    avatar {
      id
    }

    banner {
      id
    }

    manuallyApprovesFollowers
    ctime
  }
`;

interface MeFns {
  set: (data: useMeFragment$data) => void;
  clear: () => void;
}

export type Me = MeFns &
  (
    | {
        isLoggedIn: true;
        data: useMeFragment$data;
      }
    | {
        isLoggedIn: false;
        data: null;
      }
  );

export const store = createStore<Me>((set) => ({
  isLoggedIn: false,
  data: null,

  set: (data) =>
    set({
      isLoggedIn: true,
      data,
    }),

  clear: () =>
    set({
      isLoggedIn: false,
      data: null,
    }),
}));

export const { getState, setState, subscribe, destroy } = store;
export const useMe = create(store);

export const useMeFragment = (key: useMeFragment$key | null) => {
  const clear = useMe((state) => state.clear);
  const set = useMe((state) => state.set);

  if (!key) {
    return clear();
  }

  const data = useFragment(fragment, key);

  set(data);
};
