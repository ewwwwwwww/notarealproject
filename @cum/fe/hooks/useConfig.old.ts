import { syncedStore, getYjsValue } from '@syncedstore/core';
import { useSyncedStore } from '@syncedstore/react';
import { IndexeddbPersistence } from 'y-indexeddb';
// @ts-ignore
import { WebrtcProvider } from 'y-webrtc';
import { ColorScheme } from '@mantine/core';

export type NoteButtonType = 'floating' | 'compact' | 'normal';
export type ComposerType = 'floating' | 'expando' | 'popup';
export type Size = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

export type Locales = 'en' | 'psuedo';

// TODO: rename Non-plural
export type Colors =
  | 'dark'
  | 'gray'
  | 'red'
  | 'pink'
  | 'grape'
  | 'violet'
  | 'indigo'
  | 'blue'
  | 'cyan'
  | 'teal'
  | 'green'
  | 'lime'
  | 'yellow'
  | 'orange';

export type Config = {
  lang: Locales;
  avatarRadius: number;
  avatarSize: number;
  cardPadding: number;
  cardRadius: number;
  cardShadow: Size;
  composer: ComposerType;
  darkMode: boolean;
  emojiSize: number;
  noteButtonRadius: number;
  noteButtonType: NoteButtonType;
  notePadding: number;
  noteMargin: number;
  themeColor: Colors;
  themeScheme: ColorScheme;
};

export const configDefaults: Config = {
  lang: 'en', // TODO: set system locale?
  avatarRadius: 4,
  avatarSize: 50,
  cardPadding: 20,
  cardRadius: 8,
  cardShadow: 'sm',
  composer: 'floating',
  darkMode: false,
  emojiSize: 50,
  noteButtonRadius: 32,
  noteButtonType: 'floating',
  notePadding: 0,
  noteMargin: 0,
  themeColor: 'indigo',
  themeScheme: 'dark', // TODO: set system theme
};

export const localStore = syncedStore({
  config: {} as Config,
});

export const remoteStore = syncedStore({
  blocklist: {},
  whitelist: {},
  mutes: {},
});

export const globalStore = syncedStore({
  config: {},
  blocklist: {},
});

for (const key of Object.keys(localStore.config)) {
  // unset erroneous keys
  if (!(key in configDefaults)) {
    // @ts-ignore
    localStore.config[key] = undefined;
    // @ts-ignore
    delete localStore.config[key];
  }
}

if (typeof window !== 'undefined') {
  const docLocal = getYjsValue(localStore);
  const docRemote = getYjsValue(remoteStore);
  const docGlobal = getYjsValue(globalStore);

  new IndexeddbPersistence(
    'cumfe-store-local',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    docLocal as any,
  );

  new WebrtcProvider(
    'cumfe-store-local',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    docLocal as any,
    {
      signaling: [],
    },
  );

  new IndexeddbPersistence(
    'cumfe-store-remote',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    docRemote as any,
  );

  new IndexeddbPersistence(
    'cumfe-store-global',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    docGlobal as any,
  );
}

export const useConfigMut = () => useSyncedStore(localStore).config;

export const useConfig = (): Readonly<Config> => {
  return configDefaults;
  if (typeof window === 'undefined') {
    // TODO: return global defaults
  }

  // const res = {
  //   ...configDefaults,
  //   ...useSyncedStore(globalStore).config,
  //   ...useSyncedStore(localStore).config,
  // } as const;
  //
  // return res;
};

export const useBlocklist = () => {
  if (typeof window === 'undefined') {
    // TODO: return global defaults
    // TODO: return remote defaults
  }

  return {
    ...useSyncedStore(globalStore).blocklist,
    ...useSyncedStore(remoteStore).blocklist,
  };
};

export const useWhitelist = () => {
  if (typeof window === 'undefined') {
    // TODO: return remote defaults
  }

  return useSyncedStore(remoteStore).whitelist;
};

export const useMutes = () => {
  if (typeof window === 'undefined') {
    // TODO: return remote defaults
  }

  return useSyncedStore(remoteStore).mutes;
};
