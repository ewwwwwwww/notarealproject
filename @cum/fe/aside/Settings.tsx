import { FC, memo } from 'react';
import { t } from '@lingui/macro';
import { TableOfContents } from 'components/core/TableOfContents.jsx';
import { Box } from '@mantine/core';

export const Aside: FC = memo(() => {
  const rows = [
    {
      link: '#general',
      label: t`General`,
      order: 1,
    },
    {
      link: '#user',
      label: t`User Settings`,
      order: 1,
    },
    {
      link: '#avatar',
      label: t`Avatar`,
      order: 2,
    },
    {
      link: 'banner',
      label: t`Banner`,
      order: 2,
    },
    {
      link: '#profile',
      label: t`Profile`,
      order: 2,
    },
    {
      link: '#visibility',
      label: t`Visibility`,
      order: 2,
    },
    {
      link: '#appearance',
      label: t`Site Appearance`,
      order: 1,
    },
    {
      link: '#colors',
      label: t`Colors`,
      order: 2,
    },
    {
      link: '#sizing',
      label: t`Image Sizing`,
      order: 2,
    },
    {
      link: '#card',
      label: t`Card`,
      order: 2,
    },
  ];

  return (
    <Box sx={{ maxWidth: 300 }}>
      <TableOfContents links={rows} />
    </Box>
  );
});
