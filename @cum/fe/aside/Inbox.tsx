import { FC } from 'react';
import { PreloadedQuery, usePreloadedQuery } from 'react-relay';
import { graphql } from 'relay-runtime';
import { InboxTimeline } from 'components/inbox/InboxTimeline.jsx';
import type { InboxAsideQuery } from './__generated__/InboxAsideQuery.graphql.js';
import { Login } from 'components/landing/Login.jsx';

export { InboxBar as AsideBar } from 'components/inbox/InboxBar.jsx';

export const query = graphql`
  query InboxAsideQuery {
    me {
      ...InboxTimelineFragment
    }
  }
`;

interface Props {
  queryRef: PreloadedQuery<InboxAsideQuery, {}>;
}

export const Aside: FC<Props> = (props) => {
  const { queryRef } = props;
  const data = usePreloadedQuery(query, queryRef);

  if (!data.me) {
    return <Login />;
  }

  return <InboxTimeline data={data.me} />;
};
