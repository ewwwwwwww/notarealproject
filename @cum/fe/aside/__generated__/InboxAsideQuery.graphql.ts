/**
 * @generated SignedSource<<06f9fc3efb9ad46fb713c980e736bc8c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type InboxAsideQuery$variables = {};
export type InboxAsideQuery$data = {
  readonly me: {
    readonly " $fragmentSpreads": FragmentRefs<"InboxTimelineFragment">;
  } | null;
};
export type InboxAsideQuery = {
  response: InboxAsideQuery$data;
  variables: InboxAsideQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "condition",
    "value": {}
  },
  {
    "kind": "Literal",
    "name": "first",
    "value": 20
  },
  {
    "items": [
      {
        "kind": "Literal",
        "name": "orderBy.0",
        "value": "CTIME_DESC"
      }
    ],
    "kind": "ListValue",
    "name": "orderBy"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = [
  (v1/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "nick",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "nameFull",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "remote",
    "storageKey": null
  }
],
v3 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v2/*: any*/),
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v5 = [
  (v1/*: any*/),
  (v3/*: any*/),
  {
    "alias": null,
    "args": null,
    "concreteType": "Note",
    "kind": "LinkedField",
    "name": "note",
    "plural": false,
    "selections": [
      (v1/*: any*/),
      (v4/*: any*/)
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "InboxAsideQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "InboxTimelineFragment"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "InboxAsideQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": (v0/*: any*/),
            "concreteType": "NotificationsConnection",
            "kind": "LinkedField",
            "name": "notifications",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "NotificationsEdge",
                "kind": "LinkedField",
                "name": "edges",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "cursor",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Notification",
                    "kind": "LinkedField",
                    "name": "node",
                    "plural": false,
                    "selections": [
                      (v1/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "type",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "ctime",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Block",
                        "kind": "LinkedField",
                        "name": "block",
                        "plural": false,
                        "selections": [
                          (v1/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "User",
                            "kind": "LinkedField",
                            "name": "blocker",
                            "plural": false,
                            "selections": (v2/*: any*/),
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Follow",
                        "kind": "LinkedField",
                        "name": "follow",
                        "plural": false,
                        "selections": [
                          (v1/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "User",
                            "kind": "LinkedField",
                            "name": "follower",
                            "plural": false,
                            "selections": (v2/*: any*/),
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Like",
                        "kind": "LinkedField",
                        "name": "like",
                        "plural": false,
                        "selections": (v5/*: any*/),
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Mention",
                        "kind": "LinkedField",
                        "name": "mention",
                        "plural": false,
                        "selections": [
                          (v1/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "reply",
                            "storageKey": null
                          },
                          (v3/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Note",
                            "kind": "LinkedField",
                            "name": "note",
                            "plural": false,
                            "selections": [
                              (v1/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "scope",
                                "storageKey": null
                              },
                              (v4/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "React",
                        "kind": "LinkedField",
                        "name": "react",
                        "plural": false,
                        "selections": (v5/*: any*/),
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Repeat",
                        "kind": "LinkedField",
                        "name": "repeat",
                        "plural": false,
                        "selections": (v5/*: any*/),
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "__typename",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PageInfo",
                "kind": "LinkedField",
                "name": "pageInfo",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "endCursor",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hasNextPage",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "kind": "ClientExtension",
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "__id",
                    "storageKey": null
                  }
                ]
              }
            ],
            "storageKey": "notifications(condition:{},first:20,orderBy:[\"CTIME_DESC\"])"
          },
          {
            "alias": null,
            "args": (v0/*: any*/),
            "filters": [
              "condition",
              "orderBy"
            ],
            "handle": "connection",
            "key": "InboxTimelineFragment_notifications",
            "kind": "LinkedHandle",
            "name": "notifications"
          },
          (v1/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "b2a58ce9e975650bda570979a27a8702",
    "id": null,
    "metadata": {},
    "name": "InboxAsideQuery",
    "operationKind": "query",
    "text": "query InboxAsideQuery {\n  me {\n    ...InboxTimelineFragment\n    id\n  }\n}\n\nfragment EventBlockFragment on Block {\n  id\n  blocker {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventFollowFragment on Follow {\n  id\n  follower {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventLikeFragment on Like {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventMentionFragment on Mention {\n  id\n  reply\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReactFragment on React {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventRepeatFragment on Repeat {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment InboxCardFragment on Notification {\n  id\n  type\n  ctime\n  block {\n    id\n    ...EventBlockFragment\n  }\n  follow {\n    id\n    ...EventFollowFragment\n  }\n  like {\n    id\n    ...EventLikeFragment\n  }\n  mention {\n    id\n    ...EventMentionFragment\n  }\n  react {\n    id\n    ...EventReactFragment\n  }\n  repeat {\n    id\n    ...EventRepeatFragment\n  }\n}\n\nfragment InboxTimelineFragment on User {\n  notifications(first: 20, condition: {}, orderBy: [CTIME_DESC]) {\n    edges {\n      cursor\n      node {\n        id\n        ...InboxCardFragment\n        __typename\n      }\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n  id\n}\n\nfragment UserName_user on User {\n  id\n  nick\n  name\n  nameFull\n  remote\n}\n"
  }
};
})();

(node as any).hash = "7743c2b550eb541491f059be0c1645f9";

export default node;
