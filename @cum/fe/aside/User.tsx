import { FC } from 'react';
import { PreloadedQuery, usePreloadedQuery } from 'react-relay';
import { graphql } from 'relay-runtime';
import { UserCard } from 'components/user/UserCard.jsx';
import { UserMeta } from 'components/user/UserMeta.jsx';
import type { UserAsideQuery } from './__generated__/UserAsideQuery.graphql.js';
import { Box } from '@mantine/core';

export const query = graphql`
  query UserAsideQuery($name: String!, $domain: String) {
    user: userByName(name: $name, domain: $domain) {
      id
      ...UserCard_user
      ...UserMeta_user
    }
  }
`;

interface Props {
  queryRef: PreloadedQuery<UserAsideQuery, {}>;
}

export const Aside: FC<Props> = (props) => {
  const { queryRef } = props;
  const data = usePreloadedQuery(query, queryRef);

  if (!data.user) {
    return null;
  }

  return (
    <Box sx={{ maxWidth: 300 }}>
      <UserCard data={data.user} />
      <UserMeta data={data.user} />
    </Box>
  );
};
