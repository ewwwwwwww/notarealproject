declare type tribool = 'TRUE' | 'FALSE' | 'MAYBE';
declare type UUID = string;
declare type ID = string;
declare type Datetime = string;
