import { FC, ReactNode, Suspense } from 'react';
import { usePageContext } from 'hooks/usePageContext.jsx';
import { ErrorBoundary, type FallbackProps } from 'react-error-boundary';

// @ts-ignore
import * as generatedList from 'virtual:generated-list';

import { Loader, MantineProvider } from '@mantine/core';
import { useConfig } from 'hooks/useConfig.js';

const PageLoader = () => {
  const style: React.CSSProperties = {
    width: 100,
    height: 100,

    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,

    margin: 'auto',
  };

  return <Loader size="lg" variant="dots" style={style} />;
};

export const relayMock = true;
export const clientOnly = true;

// do not delete, needed for reseting layout
export const Layout: FC<{ children: ReactNode }> = (props) => {
  const { children } = props;

  return <div>{children}</div>;
};

export const Page: FC = (pageContext) => {
  const { routeParams } = usePageContext();
  const { story } = routeParams;
  const cfg = useConfig();

  if (!story) return null;
  if (!(story in generatedList.stories)) {
    throw new Error('story does not exist');
  }

  const Story = generatedList.stories[story].component;

  return (
    <MantineProvider
      theme={{ colorScheme: cfg.themeScheme, primaryColor: cfg.themeColor }}
      withGlobalStyles
      withNormalizeCSS
    >
      <Suspense fallback={<PageLoader />}>
        <Story />
      </Suspense>
    </MantineProvider>
  );
};
