import { FC, memo } from 'react';

import { BookShell } from 'components/BookShell.jsx';
import { usePageContext } from 'hooks/usePageContext.jsx';

export const relayMock = true;

export const Layout: FC = memo(() => {
  const { story } = usePageContext().routeParams;

  return <BookShell story={story} />;
});

export const Page: FC = () => <></>;
