import {
  ActionIcon,
  AppShell,
  Aside,
  Box,
  ColorSwatch,
  createStyles,
  Group,
  Header,
  Navbar,
  NavLink,
  ScrollArea,
  Select,
  Text,
  useMantineTheme,
} from '@mantine/core';
import { Colors, Locales, useConfig, useConfigMut } from 'hooks/useConfig.js';
import { set } from 'lodash-es';
import { Resizable, ResizeCallback } from 're-resizable';
import { FC, ReactNode, useState, memo } from 'react';

import { TbMoonStars as IconMoon } from 'react-icons/tb';
import { BiSun as IconSun } from 'react-icons/bi';
import { IoRefreshCircleOutline as IconRefresh } from 'react-icons/io5';
import { AiOutlineCheck as IconCheck } from 'react-icons/ai';
import { IoMdOpen as IconOpen } from 'react-icons/io';

// @ts-ignore
import * as storyGraph from 'virtual:generated-list';
import { syncedStore, getYjsValue } from '@syncedstore/core';
import { IndexeddbPersistence } from 'y-indexeddb';
import { useSyncedStore } from '@syncedstore/react';

// TODO: proper types
const reducer = (acc: any, [Provider, props]: [any, any]) => (
  <Provider {...props}>{acc}</Provider>
);

// TODO: proper types
const compose = (contexts: any[], children: ReactNode) =>
  contexts.reduceRight(reducer, children);

const clearAndUpper = (text: string) => text.replace(/-/, '').toUpperCase();

const toPascalCase = (text: string) =>
  text.replace(/(^\w|-\w)/g, clearAndUpper);

interface Props {
  story: string;
}

const useStyles = createStyles((theme) => ({
  resizable: {
    margin: '0 auto',
  },

  iframe: {
    width: '100%',
    height: '100%',
    border: 0,
  },

  iframeWrapper: {
    padding: 10,
    width: '100%',
    height: '100%',
    borderStyle: 'dotted',
    borderWidth: 4,
    borderColor:
      theme.colorScheme === 'dark'
        ? theme.colors.gray[9]
        : theme.colors.gray[2],
  },
}));

interface BookSettings {
  width: number;
  height: number;
}

const localStore = syncedStore({
  settings: {} as BookSettings,
});

const useSettings = () => {
  return {
    ...{
      width: 1600,
      height: 800,
    },
    ...useSyncedStore(localStore).settings,
  };
};

if (typeof window !== 'undefined') {
  const docLocal = getYjsValue(localStore);

  new IndexeddbPersistence(
    'cumfe-store-book',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    docLocal as any,
  );
}

export const BookShell: FC<Props> = memo((props) => {
  const { story } = props;
  const [ikey, setRefresh] = useState(0);
  const [size, setSize] = useState({ width: 1600, height: 800 });
  const { width, height } = useSettings();
  const settingsMut = useSyncedStore(localStore).settings;
  const { classes: cl } = useStyles();

  const refresh = () => setRefresh(ikey + 1);
  const resize: ResizeCallback = (_1, _2, _3, d) => {
    settingsMut.width = width + d.width;
    settingsMut.height = height + d.height;
  };

  return compose(
    [
      [
        AppShell,
        {
          navbar: <NavLeft story={story} />,
          header: <NavTop story={story} refresh={refresh} />,
          aside: <NavRight />,
        },
      ],
      [
        Resizable,
        {
          defaultSize: { width, height },
          onResizeStop: resize,
          bounds: 'parent',
          className: cl.resizable,
          style: {
            top: `calc(50% - ${Math.round(height / 2)}px)`,
          },
        },
      ],
      [Box, { className: cl.iframeWrapper }],
    ],
    story && <iframe key={ikey} src={`/book/${story}`} className={cl.iframe} />,
  );
});

type GroupedType = {
  [x: string]: RecursiveLinkProps | string;
};

const NavLeft: FC<{ story: string }> = memo((props) => {
  const { story } = props;
  const links = Object.keys(storyGraph.stories).map((k) => k.split('--'));
  const grouped: GroupedType = {};

  links.sort();

  for (const path of links) {
    set(grouped, path, path.join('--'));
  }

  // TODO: figure out open / close toggle
  // const [open, setOpen] = useState(
  //   Object.fromEntries(
  //     Object.keys(storyGraph.stories).map((k) => [k, story.startsWith(k)]),
  //   ),
  // );

  const children = Object.entries(grouped).map(([k, v]) => {
    return <RecursiveLink key={k} root={k} value={v} active={story} />;
  });

  return (
    <Navbar height="auto" p="xs" width={{ base: 250 }}>
      <Navbar.Section grow component={ScrollArea} mx="-xs" px="xs">
        <Box py="md">{children}</Box>
      </Navbar.Section>
      <Navbar.Section>
        <Text>Bottom</Text>
      </Navbar.Section>
    </Navbar>
  );
});

const NavTop: FC<{ story: string; refresh: () => void }> = memo((props) => {
  const { story, refresh } = props;

  return (
    <Header height={50}>
      <Group position="center" spacing="xl" p="xs">
        <ToggleSchema />
        <ToggleColor />
        <ActionIcon onClick={refresh}>
          <IconRefresh size={36} />
        </ActionIcon>
        <ToggleLang />
        <ActionIcon<'a'> component="a" href={'./book/' + story} target="_blank">
          <IconOpen size={36} />
        </ActionIcon>
      </Group>
    </Header>
  );
});

const ToggleSchema: FC = memo(() => {
  const cfg = useConfig();
  const cfgMut = useConfigMut();
  const isDark = cfg.themeScheme === 'dark';

  const toggle = () => {
    cfgMut.themeScheme = cfg.themeScheme === 'light' ? 'dark' : 'light';
  };

  return (
    <ActionIcon
      color={isDark ? 'yellow' : 'blue'}
      onClick={toggle}
      title="Toggle color scheme"
    >
      {isDark ? <IconSun size={36} /> : <IconMoon size={36} />}
    </ActionIcon>
  );
});

const ToggleColor: FC = memo(() => {
  const theme = useMantineTheme();
  const cfg = useConfig();
  const cfgMut = useConfigMut();

  const setColor = (color: Colors) => {
    cfgMut.themeColor = color;
  };

  const swatches = Object.keys(theme.colors).map((color) => (
    <ColorSwatch
      key={color}
      component="button"
      color={theme.colors[color][6]}
      onClick={() => setColor(color as Colors)}
    >
      {color === cfg.themeColor && <IconCheck width={10} />}
    </ColorSwatch>
  ));

  return (
    <Group position="center" spacing="xs">
      {swatches}
    </Group>
  );
});

const ToggleLang: FC = memo(() => {
  const cfg = useConfig();
  const cfgMut = useConfigMut();

  const setLang = (lang: Locales) => {
    cfgMut.lang = lang;
  };

  return (
    <Select
      placeholder="Language"
      data={[{ value: 'en' }, { value: 'psuedo' }]}
      size="xs"
      sx={{ width: 100 }}
    />
  );
});

const NavRight: FC = memo(() => {
  return (
    <Aside height="auto" p="xs" width={{ base: 250 }}>
      Hello World
    </Aside>
  );
});

type RecursiveLinkProps = {
  root: string;
  value: RecursiveLinkProps | string;
  active: string;
};

const RecursiveLink: FC<RecursiveLinkProps> = memo((props) => {
  const { root, value, active } = props;

  if (typeof value === 'string') {
    return (
      <NavLink<'a'>
        component="a"
        key={value}
        href={'?story=' + value}
        label={toPascalCase(root)}
        active={value === active}
      />
    );
  }

  const children = Object.entries(value).map(([k, v]) => (
    <RecursiveLink key={k} root={k} value={v} active={active} />
  ));

  return (
    <>
      <NavLink
        label={toPascalCase(root)}
        defaultOpened={
          false
          // (active?.length > 0 && active.startsWith(root)) || open[root]
        }
      >
        {children}
      </NavLink>
    </>
  );
});
