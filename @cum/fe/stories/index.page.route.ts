import { resolveRoute } from 'vite-plugin-ssr/routing';

// eslint-disable-next-line import/no-default-export
export default (pageContext: { urlPathname: string }) => {
  const { urlPathname, urlParsed } = pageContext;

  if (urlPathname === '/book' || urlPathname === '/book/') {
    return { routeParams: urlParsed.search };
  }

  return resolveRoute('/book/@file', pageContext.urlPathname);
};
