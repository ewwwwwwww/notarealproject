/**
 * @param {string} path
 * @return {string}
 */
const cleanupWindowsPath = (path) => path.replace(/\\/g, '/');

export default cleanupWindowsPath;
