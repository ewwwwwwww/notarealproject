import { FC, memo, useEffect, useMemo } from 'react';
import {
  Box,
  Checkbox,
  CloseButton,
  Group,
  Modal,
  ModalProps,
  Stack,
  Switch,
  Text,
  useMantineTheme,
} from '@mantine/core';
import { Dashboard } from '@uppy/react';
import Uppy, { UppyOptions } from '@uppy/core';
import Audio from '@uppy/audio';
import Compressor from '@uppy/compressor';
import GoldenRetriever from '@uppy/golden-retriever';
import ImageEditor from '@uppy/image-editor';
import ScreenCapture from '@uppy/screen-capture';
import Tus from '@uppy/tus';
import Url from '@uppy/url';
import Webcam from '@uppy/webcam';

import '@uppy/core/dist/style.css';
import '@uppy/dashboard/dist/style.css';

import '@uppy/audio/dist/style.css';
import '@uppy/image-editor/dist/style.css';
import '@uppy/screen-capture/dist/style.css';
import '@uppy/url/dist/style.css';
import '@uppy/webcam/dist/style.css';

interface Props extends ModalProps {
  single?: boolean;
  meta?: {
    action: 'BANNER' | 'AVATAR' | 'NOTE';
  };
  onComplete: (res: any) => void;
}

export const UploaderModal: FC<Props> = memo((props) => {
  const { single, meta, onComplete, ...other } = props;
  const theme = useMantineTheme();

  const uppy = useMemo(() => {
    const opts: UppyOptions = {
      meta,
      restrictions: { maxNumberOfFiles: single ? 1 : undefined },
    };

    return (
      new Uppy(opts)
        .use(Audio, { showAudioSourceDropdown: true })
        .use(Compressor)
        .use(GoldenRetriever)
        .use(ImageEditor, {
          actions: {
            revert: true,
            rotate: true,
            granularRotate: true,
            flip: true,
            zoomIn: true,
            zoomOut: true,
            cropSquare: true,
            cropWidescreen: true,
            cropWidescreenVertical: true,
          },
        })
        .use(ScreenCapture)
        .use(Tus, {
          endpoint: '/file-tmp',
          withCredentials: true,
        })
        // .use(Url)
        .use(Webcam, { showVideoSourceDropdown: true })
    );
  }, [meta]);

  uppy.on('complete', onComplete);

  useEffect(() => {
    return () => uppy.close({ reason: 'unmount' });
  }, [uppy]);

  return (
    <Modal
      size="auto"
      overlayColor={
        theme.colorScheme === 'dark'
          ? theme.colors.dark[9]
          : theme.colors.gray[2]
      }
      overlayOpacity={0.55}
      overlayBlur={3}
      centered
      padding={0}
      withCloseButton={false}
      {...other}
    >
      <Group position="apart" mb="xs" mt={5} mx="sm">
        <Switch.Group defaultChecked={true} mt={-15}>
          <Switch value="compress" label="compress images" />
          <Switch value="anonymize" label="anonymize filenames" />
          <Switch value="anonymize" label="sort by filenames" />
        </Switch.Group>
        <CloseButton mt={5} />
      </Group>
      <Dashboard
        uppy={uppy}
        plugins={['Audio', 'ImageEditor', 'ScreenCapture', 'Webcam']}
        theme={theme.colorScheme}
        note="Images and video only, 2–3 files, up to 1 MB"
        metaFields={[
          { id: 'name', name: 'Name', placeholder: 'file name' },
          { id: 'caption', name: 'Caption', placeholder: 'add description' },
        ]}
      />
    </Modal>
  );
});
