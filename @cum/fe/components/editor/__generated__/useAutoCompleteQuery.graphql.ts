/**
 * @generated SignedSource<<c79327a680b060cd65a8e91cf044b339>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type useAutoCompleteQuery$variables = {
  text: string;
};
export type useAutoCompleteQuery$data = {
  readonly users: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: ID;
        readonly value: string;
      };
    }>;
  } | null;
};
export type useAutoCompleteQuery = {
  response: useAutoCompleteQuery$data;
  variables: useAutoCompleteQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "text"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "autocomplete",
            "variableName": "text"
          }
        ],
        "kind": "ObjectValue",
        "name": "condition"
      },
      {
        "kind": "Literal",
        "name": "first",
        "value": 7
      }
    ],
    "concreteType": "UsersConnection",
    "kind": "LinkedField",
    "name": "users",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "UsersEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": "value",
                "args": null,
                "kind": "ScalarField",
                "name": "nameFull",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useAutoCompleteQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useAutoCompleteQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "0ef23afdf1417680bebee3567f49bf27",
    "id": null,
    "metadata": {},
    "name": "useAutoCompleteQuery",
    "operationKind": "query",
    "text": "query useAutoCompleteQuery(\n  $text: String!\n) {\n  users(condition: {autocomplete: $text}, first: 7) {\n    edges {\n      node {\n        id\n        value: nameFull\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "ab1a37d8ed09d258ee5821f2d9008b49";

export default node;
