/**
 * @generated SignedSource<<1a899ebe3202af6380d23b71221fe493>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type useAutoCompleteTagsQuery$variables = {
  text: string;
};
export type useAutoCompleteTagsQuery$data = {
  readonly users: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: ID;
        readonly value: string;
      };
    }>;
  } | null;
};
export type useAutoCompleteTagsQuery = {
  response: useAutoCompleteTagsQuery$data;
  variables: useAutoCompleteTagsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "text"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "autocomplete",
            "variableName": "text"
          }
        ],
        "kind": "ObjectValue",
        "name": "condition"
      },
      {
        "kind": "Literal",
        "name": "first",
        "value": 7
      }
    ],
    "concreteType": "UsersConnection",
    "kind": "LinkedField",
    "name": "users",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "UsersEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": "value",
                "args": null,
                "kind": "ScalarField",
                "name": "nameFull",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useAutoCompleteTagsQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useAutoCompleteTagsQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "5e73608fa5c7c75a288315da93eef98d",
    "id": null,
    "metadata": {},
    "name": "useAutoCompleteTagsQuery",
    "operationKind": "query",
    "text": "query useAutoCompleteTagsQuery(\n  $text: String!\n) {\n  users(condition: {autocomplete: $text}, first: 7) {\n    edges {\n      node {\n        id\n        value: nameFull\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "322b5934b4e3c174b18b60635994550b";

export default node;
