import {
  FC,
  memo,
  lazy,
  ReactNode,
  Suspense,
  useState,
  ReactComponentElement,
} from 'react';
import { Dialog } from '@mantine/core';
import { useEditor } from './useEditor.js';
import { useMe } from 'hooks/useMe.js';
import { useMediaQuery } from '@mantine/hooks';

interface Props {
  children: ReactNode;
}

export const EditorProvider: FC<Props> = (props) => {
  const { children } = props;
  const opened = useEditor((state) => state.opened);
  const isLoggedIn = useMe((state) => state.isLoggedIn);
  const close = useEditor((state) => state.close);
  const Editor = lazy(() => import('./Editor.jsx'));
  const fullscreen = useMediaQuery('(max-width: 800px)');

  return (
    <>
      {children}

      <Dialog
        opened={isLoggedIn && opened}
        onClose={close}
        size={600}
        withCloseButton
        position={
          fullscreen && opened
            ? { top: 0, bottom: 0, left: 0, right: 0 }
            : {
                right: 20,
                bottom: 20,
              }
        }
        sx={
          fullscreen && opened
            ? {
                height: '100%',
                width: '100%',
                borderRadius: 0,
              }
            : {}
        }
      >
        <Suspense>
          <Editor />
        </Suspense>
      </Dialog>
    </>
  );
};
