import create from 'zustand';

interface EditorState {
  opened: boolean;
  replyId?: string;
  replyUuid?: string;

  open: (replyId?: string, replyUuid?: string) => void;
  close: () => void;
}

export const useEditor = create<EditorState>()((set, get) => ({
  opened: false,

  open: (replyId?: string, replyUuid?: string) =>
    set({
      replyId,
      replyUuid,
      opened: true,
    }),

  close: () => set({ opened: false }),
}));
