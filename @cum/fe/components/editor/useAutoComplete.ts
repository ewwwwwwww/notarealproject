import mem from 'mem';
import { useRelayEnvironment } from 'react-relay';
import { fetchQuery, graphql } from 'relay-runtime';
import type { useAutoCompleteQuery } from './__generated__/useAutoCompleteQuery.graphql.js';
import type { useAutoCompleteTagsQuery } from './__generated__/useAutoCompleteTagsQuery.graphql.js';

const query = graphql`
  query useAutoCompleteQuery($text: String!) {
    users(condition: { autocomplete: $text }, first: 7) {
      edges {
        node {
          id
          value: nameFull
        }
      }
    }
  }
`;

export const useAutoCompleteUser = () => {
  const environment = useRelayEnvironment();

  const fn = mem(
    (text: string) => {
      if (text.length < 3) return [];

      let i = 0;

      return fetchQuery<useAutoCompleteQuery>(environment, query, { text })
        .toPromise()
        .then((res) => {
          if (!res || !res.users) return [];

          return res.users.edges.map((edge) => {
            return edge.node;
          });
        });
    },
    {
      maxAge: 60 * 1000,
    },
  );

  return fn;
};

const queryTags = graphql`
  query useAutoCompleteTagsQuery($text: String!) {
    users(condition: { autocomplete: $text }, first: 7) {
      edges {
        node {
          id
          value: nameFull
        }
      }
    }
  }
`;

export const useAutoCompleteTags = () => {
  const environment = useRelayEnvironment();

  const fn = mem(
    (text: string) => {
      if (text.length < 3) return [];

      let i = 0;

      return fetchQuery<useAutoCompleteTagsQuery>(environment, queryTags, {
        text,
      })
        .toPromise()
        .then((res) => {
          if (!res || !res.users) return [];

          return res.users.edges.map((edge) => {
            return edge.node;
          });
        });
    },
    {
      maxAge: 60 * 1000,
    },
  );

  return fn;
};
