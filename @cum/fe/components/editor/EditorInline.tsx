import { FC, memo, lazy, ReactNode, Suspense } from 'react';
import type { Props as EditorProps } from './Editor.jsx';

interface Props extends EditorProps {
  children: ReactNode | ReactNode[];
}

export const EditorInline: FC<Props> = (props) => {
  const { children, ...other } = props;

  const Editor = lazy(() => import('./Editor.jsx'));

  return <div>{children}</div>;
};
