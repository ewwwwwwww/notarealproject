import { FC, useState, Suspense, useMemo, useRef } from 'react';
import type { EditorMutation } from './__generated__/EditorMutation.graphql.js';
import { timelineStore } from 'components/timeline/useTimeline.js';
import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import {
  ActionIcon,
  Anchor,
  Button,
  Group,
  LoadingOverlay,
  Text,
  TextInput,
} from '@mantine/core';
import {
  RichTextEditor,
  Editor as QuillEditor,
  RichTextEditorProps,
} from '@mantine/rte';
import { getState } from 'hooks/useMe.js';
import { useEditor } from './useEditor.js';
import { useAutoCompleteUser, useAutoCompleteTags } from './useAutoComplete.js';
import { optimisticNote } from 'components/optimistic/note.js';
import { IconScope } from 'components/core/Scope.jsx';
import { UploaderModal } from './UploaderModal.jsx';

const mutation = graphql`
  mutation EditorMutation($note: NoteInput!, $connections: [ID!]!) {
    createNote(input: { note: $note }) {
      note @prependNode(connections: $connections, edgeTypeName: "NotesEdge") {
        id
        ...NoteCard_note
      }
    }
  }
`;

export interface Props {
  initialBody?: string;
}

const people = [
  { id: 1, value: 'Bill Horsefighter' },
  { id: 2, value: 'Amanda Hijacker' },
  { id: 3, value: 'Leo Summerhalter' },
  { id: 4, value: 'Jane Sinkspitter' },
];

const tags = [
  { id: 1, value: 'JavaScript' },
  { id: 2, value: 'TypeScript' },
  { id: 3, value: 'Ruby' },
  { id: 3, value: 'Python' },
];

const controls: RichTextEditorProps['controls'] = [
  ['bold', 'italic', 'underline', 'clean'],
  ['unorderedList', 'h1', 'h2', 'h3'],
  ['sup', 'sub'],
  ['alignLeft', 'alignCenter', 'alignRight'],
  ['codeBlock', 'blockquote'],
];

// const mentions = note.mentions.edges
//   .flatMap((edge) => edge.node?.user ?? [])
//   .concat(note.user ? [note.user] : [])
//   .map((user) => {
//     return `<a>@${user.nameFull}</a>`;
//   })
//   .join(' ');
//   import React from 'react'

export const Editor: FC<Props> = (props) => {
  const replyId = useEditor((state) => state.replyId);
  const replyUuid = useEditor((state) => state.replyUuid);
  const close = useEditor((state) => state.close);
  const autoCompleteUser = useAutoCompleteUser();
  const autoCompleteTags = useAutoCompleteTags();
  const [uploader, setUploader] = useState(false);
  const editorRef = useRef<QuillEditor>(null);
  const [commitMutation, isMutationInFlight] =
    useMutation<EditorMutation>(mutation);

  const [value, onChange] = useState('');

  // https://github.com/quill-mention/quill-mention/issues/229
  // https://github.com/quill-mention/quill-mention/issues/177
  const mentions = useMemo(
    () => ({
      allowedChars: /^[A-Za-z\sÅÄÖåäö@]*$/,
      isolateCharacter: true,
      mentionDenotationChars: ['@', '#'],

      source: async (searchTerm: any, renderList: any, mentionChar: any) => {
        const res =
          mentionChar === '@'
            ? autoCompleteUser(searchTerm)
            : autoCompleteTags(searchTerm);

        renderList(await res);
      },
    }),
    [],
  );

  const onSubmit = async () => {
    const { Uuid6 } = await import('id128');

    const me = getState();
    const { userId } = me.data ?? {};

    if (!userId) {
      // TODO: handle registration popup
      return console.log('not registered');
    }

    if (!value) {
      // TODO: handle empty body
      return console.log('warning empty body');
    }

    console.log(editorRef.current?.getEditor().getContents());

    const noteId = Uuid6.generate().toCanonical().toLowerCase();
    const body = value;
    const scope = 'PUBLIC';
    const connId = timelineStore.getState().id;

    commitMutation({
      variables: {
        note: {
          noteId,
          userId,
          body,
          scope,
          replyId: replyUuid,
        },

        connections: connId ? [connId] : [],
      },

      // TODO: figure out why virtuoso breaks optimistic updates
      optimisticResponse: {
        createNote: {
          note: optimisticNote(noteId, scope, body, replyUuid),
        },
      },
    });

    close();
  };

  return (
    <>
      <UploaderModal opened={uploader} onClose={() => setUploader(false)} />

      <Group>
        <ActionIcon variant="filled" color="indigo">
          <IconScope scope="PUBLIC" />
        </ActionIcon>
        <Text size="sm" weight={500}>
          This note will be visible to everyone. <Anchor>Read more.</Anchor>
        </Text>
      </Group>
      <TextInput placeholder="Subject (optional)" my="md" />
      <RichTextEditor
        id="rte"
        value={value}
        onChange={onChange}
        ref={editorRef}
        mentions={mentions}
        controls={controls}
      />
      <Button onClick={onSubmit} mt="md">
        Submit
      </Button>
      <Button onClick={() => setUploader(true)} mt="md">
        Images
      </Button>
    </>
  );
};

export default Editor;
