import { FC, ReactNode } from 'react';
import { Card } from '../core/Card.jsx';
import {
  Stack,
  Box,
  Text,
  Slider,
  Group,
  ColorSwatch,
  useMantineTheme,
  SimpleGrid,
  Divider,
} from '@mantine/core';
import { Label, Labeled } from './Label.jsx';

interface Props {}

export const SettingsCard: FC<Props> = (props) => {
  return (
    <>
      <Card>
        <Card.Section withBorder inheritPadding py="xs">
          <Label>Card</Label>
        </Card.Section>
        <Stack mt="md">
          <SimpleGrid cols={2}>
            <Labeled label="Border">
              <Slider />
            </Labeled>
            <Labeled label="Border Radius">
              <Slider />
            </Labeled>
            <Box>
              <Label>Padding</Label>
              <Slider />
            </Box>
            <Box>
              <Label>Margin</Label>
              <Slider />
            </Box>
            <Box>
              <Label>Shadow</Label>
              <Slider />
            </Box>
          </SimpleGrid>
          <Divider my="lg" mt="xl" />
          <Group grow>
            <Box>
              <Label>Button Radius</Label>
              <Slider />
            </Box>
            <Box>
              <Label>Button Radius</Label>
              <Slider />
            </Box>
          </Group>
        </Stack>
      </Card>
    </>
  );
};
