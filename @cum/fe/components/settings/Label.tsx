import { Box, Text } from '@mantine/core';
import { FC, memo, ReactNode } from 'react';

interface LabelProps {
  children: ReactNode;
}

export const Label: FC<LabelProps> = memo((props) => {
  return (
    <Text mb={2} size="sm">
      {props.children}
    </Text>
  );
});

interface LabeledProps {
  label: string;
  children: ReactNode;
}

export const Labeled: FC<LabeledProps> = memo((props) => {
  const { label, children } = props;

  return (
    <Box>
      <Label>{label}</Label>
      {children}
    </Box>
  );
});
