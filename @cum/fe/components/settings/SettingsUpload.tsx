import { FC, useState } from 'react';
import { Card } from '../core/Card.jsx';
import { Stack, CardSection, Button, Group, Box } from '@mantine/core';
import { UserAvatar } from 'components/user/UserAvatar.jsx';
import { Label, Labeled } from './Label.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { graphql } from 'relay-runtime';
import { SettingsUpload$key } from './__generated__/SettingsUpload.graphql.js';
import { UserBanner } from 'components/user/UserBanner.jsx';
import { UploaderModal } from 'components/editor/UploaderModal.jsx';

const fragment = graphql`
  fragment SettingsUpload on User {
    id
    ...UserAvatar_user
    ...UserBanner_user
  }
`;

interface Props {
  data: SettingsUpload$key;
}

export const SettingsUpload = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  const [avatar, setAvatar] = useState(false);
  const [banner, setBanner] = useState(false);

  const onComplete = (res: any) => {
    console.log(res);
    setAvatar(false);
    setBanner(false);
  };

  return (
    <>
      <UploaderModal
        opened={avatar}
        meta={{
          action: 'AVATAR',
        }}
        onComplete={onComplete}
        onClose={() => setAvatar(false)}
      />

      <UploaderModal
        opened={banner}
        meta={{
          action: 'BANNER',
        }}
        onComplete={onComplete}
        onClose={() => setBanner(false)}
      />

      <Card>
        <Card.Section withBorder inheritPadding py="xs">
          <Label>Avatar & Banner</Label>
        </Card.Section>
        <Stack mt="md">
          <Group>
            <UserAvatar data={data} />
            <UserAvatar data={data} size={70} />
            <UserAvatar data={data} size={100} />
            <Button mt="md" onClick={() => setAvatar(true)}>
              Upload
            </Button>
          </Group>
          <Box>
            <UserBanner data={data} />

            <Button mt="lg" onClick={() => setBanner(true)}>
              Upload
            </Button>
          </Box>
        </Stack>
      </Card>
    </>
  );
});
