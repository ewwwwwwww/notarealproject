import { FC, ReactNode } from 'react';
import { Card } from '../core/Card.jsx';
import {
  Stack,
  Box,
  Text,
  Slider,
  Group,
  ColorSwatch,
  useMantineTheme,
  SimpleGrid,
} from '@mantine/core';
import { UserAvatar } from 'components/user/UserAvatar.jsx';
import { Label } from './Label.jsx';

interface Props {}

export const SettingsTheme: FC<Props> = (props) => {
  const theme = useMantineTheme();

  const swatches = Object.keys(theme.colors).map((color) => (
    <ColorSwatch
      component="button"
      key={color}
      color={theme.colors[color][6]}
      sx={{ marginRight: 10 }}
    />
  ));

  return (
    <Card>
      <Card.Section withBorder inheritPadding py="xs">
        <Label>Theme</Label>
      </Card.Section>
      <Stack mt="md">
        <Box>
          <Label>Color</Label>
          {swatches}
        </Box>
      </Stack>
    </Card>
  );
};
