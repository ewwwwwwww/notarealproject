/**
 * @generated SignedSource<<03c40bed193de7b4e08cc92dcec066ea>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type SettingsUpload$data = {
  readonly id: ID;
  readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user" | "UserBanner_user">;
  readonly " $fragmentType": "SettingsUpload";
};
export type SettingsUpload$key = {
  readonly " $data"?: SettingsUpload$data;
  readonly " $fragmentSpreads": FragmentRefs<"SettingsUpload">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "SettingsUpload",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserBanner_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "74259037ae1f73f2432101ab2673a832";

export default node;
