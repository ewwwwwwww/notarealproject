import { FC, ReactNode } from 'react';
import { Card } from '../core/Card.jsx';
import { Stack, Box, Text, Slider, Group } from '@mantine/core';
import { UserAvatar } from 'components/user/UserAvatar.jsx';
import { Label } from './Label.jsx';

interface Props {}

export const SettingsSize: FC<Props> = (props) => {
  return (
    <>
      <Card>
        <Card.Section withBorder inheritPadding py="xs">
          <Label>Sizing</Label>
        </Card.Section>
        <Stack mt="md">
          <Group grow>
            <Group>
              <UserAvatar />
              <Box sx={{ flexGrow: 1 }}>
                <Label>Emoji Size</Label>
                <Slider />
              </Box>
            </Group>
            <Group>
              <UserAvatar />
              <Box sx={{ flexGrow: 1 }}>
                <Label>Avatar Size</Label>
                <Slider />
              </Box>
            </Group>
          </Group>
        </Stack>
      </Card>
    </>
  );
};
