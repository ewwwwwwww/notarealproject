import {
  FC,
  memo,
  lazy,
  ReactNode,
  Suspense,
  useState,
  forwardRef,
} from 'react';
import { Popover } from '@mantine/core';
import type { Props as PickerProps } from './EmojiPicker.jsx';
import { useAuth, useAuthPrompt } from 'components/landing/useAuth.js';
import { useMe } from 'hooks/useMe.js';

interface Props extends PickerProps {
  children: ReactNode | ReactNode[];
}

export const EmojiPopover: FC<Props> = memo((props) => {
  const { children, ...other } = props;
  const openPrompt = useAuthPrompt((state) => state.open);
  const isLoggedIn = useMe((state) => state.isLoggedIn);

  const Picker = lazy(() => import('./EmojiPicker.jsx'));

  return (
    <Popover withinPortal shadow="md" disabled={!isLoggedIn}>
      <Popover.Target>{children}</Popover.Target>
      <Popover.Dropdown px="xs" pb={0} pt={5}>
        <Suspense>
          <Picker {...other} />
        </Suspense>
      </Popover.Dropdown>
    </Popover>
  );
});
