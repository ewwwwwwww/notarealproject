import { FC, memo, useMemo, useRef, useState } from 'react';
import {
  Box,
  Tabs,
  Tooltip,
  Image,
  SimpleGrid,
  Text,
  UnstyledButton,
  UnstyledButtonProps,
  Checkbox,
  Group,
  TextInput,
  Space,
  Divider,
  Stack,
} from '@mantine/core';
import { Virtuoso, VirtuosoHandle } from 'react-virtuoso';
import { unicodeMapping } from 'core/unicodeMapping.js';
import { chunk } from 'lodash-es';
import { Index } from 'flexsearch';

import { HiOutlineEmojiHappy as IconPeople } from 'react-icons/hi';
import { FaDog as IconNature } from 'react-icons/fa';
import { TbApple as IconFoods } from 'react-icons/tb';
import { IoBasketballOutline as IconActivity } from 'react-icons/io5';
import { IoCarOutline as IconPlaces } from 'react-icons/io5';
import { MdLightbulbOutline as IconObjects } from 'react-icons/md';
import { MdOutlineEmojiSymbols as IconSymbols } from 'react-icons/md';
import { IoFlagOutline as IconFlags } from 'react-icons/io5';
import { GiAcidBlob as IconCustom } from 'react-icons/gi';
import { IoSearchOutline as IconSearch } from 'react-icons/io5';

// @ts-ignore
import EmojiMartData from '@emoji-mart/data';

const TabIcons = {
  people: IconPeople,
  nature: IconNature,
  foods: IconFoods,
  activity: IconActivity,
  places: IconPlaces,
  objects: IconObjects,
  symbols: IconSymbols,
  flags: IconFlags,
  custom: IconCustom,
};

function isTabIcon(name: string): name is keyof typeof TabIcons {
  if (!(name in TabIcons)) {
    console.warn('missing tab icon', name);
  }

  return true;
}

function isString(item: any): item is string {
  return typeof item === 'string';
}

interface EmojiData {
  id: string;
  name: string;
  native: string;
  keywords: string[];
  emoticons: string[];
  skins: {
    unified: string;
    native: string;
  }[];
  version: number;
}

interface EmojiCategory {
  id: string;
  emojis: string[];
}

interface EmojiRow {
  src: string;
  label: string;
  custom?: boolean;
}

type RowItem = string | EmojiRow[];

export interface Props {
  onEmojiSelect: (name: string, custom?: boolean) => any;
  extraOptions?: boolean;
  customEmojis?: EmojiRow[];
}

const CHUNK_SIZE = 7;
const EMOJI_SIZE = 42;
const VPADDING = 8;
const HPADDING = 8;

const groupNames: string[] = [];
const chunked: (string | EmojiRow[])[] = [];
const waypoints: [string, number][] = [];

const index = new Index({
  charset: 'latin',
  tokenize: 'full',
  resolution: 9,
  optimize: true,
});

let i = 0;

for (const idx in EmojiMartData.categories) {
  const cat: EmojiCategory = EmojiMartData.categories[idx];

  chunked.push(cat.id);
  groupNames.push(cat.id);
  waypoints.push([cat.id, chunked.length - 1]);

  i++;

  for (const chunks of chunk(cat.emojis, CHUNK_SIZE) as string[][]) {
    let r = 0;

    const row = chunks.map((key) => {
      const emoji: EmojiData = EmojiMartData.emojis[key];
      const { native } = emoji.skins[0];

      let src;

      if (native in unicodeMapping) {
        const { unified } = unicodeMapping[native];

        index.add(
          i * CHUNK_SIZE + r++,
          emoji.name +
            ' ' +
            emoji.id.replace('_', ' ') +
            emoji.keywords.join(' '),
        );

        src = `/twemoji/${unified}.svg`;
      } else {
        console.warn('missing emoji', native);

        src = '';
      }

      return { src, label: `:${key}:` };
    });

    chunked.push(row);

    i++;
  }
}

chunked.push('custom');
waypoints.push(['custom', chunked.length - 1]);
groupNames.push('custom');

const Header = memo(() => {
  return <Space m="md" />;
});

const ScrollSeekPlaceholder = memo((props) => {
  return (
    <Tooltip label="">
      <UnstyledButton>
        <Box sx={{ width: EMOJI_SIZE, height: EMOJI_SIZE }} />
      </UnstyledButton>
    </Tooltip>
  );
});

const Emoji: FC<EmojiRow & UnstyledButtonProps> = memo((props) => {
  const { src, label, ...other } = props;

  return (
    <Tooltip label={label}>
      <UnstyledButton {...other}>
        <Box sx={{ width: EMOJI_SIZE, height: EMOJI_SIZE }}>
          <Image src={src} imageProps={{ loading: 'lazy' }} />
        </Box>
      </UnstyledButton>
    </Tooltip>
  );
});

export const EmojiPicker: FC<Props> = (props) => {
  const { onEmojiSelect, extraOptions, customEmojis } = props;

  const [searchResult, setSearchResult] = useState<null | RowItem[]>(null);
  const [activeTab, setActiveTab] = useState(groupNames[0]);
  const virtuoso = useRef<VirtuosoHandle>(null);

  const clickTab = (value: string) => {
    setActiveTab(value);

    virtuoso?.current?.scrollToIndex({
      index: (waypoints.find((v) => v[0] === value) ?? ['', 0])[1],
      align: 'start',
      behavior: 'smooth',
    });
  };

  const tabs = groupNames.map((item, key) => {
    if (!isTabIcon(item)) return;

    const Icon = TabIcons[item];

    return (
      <Tabs.Tab key={key} value={item} onClick={() => clickTab(item)}>
        <Icon size={18} />
      </Tabs.Tab>
    );
  });

  const onSearch = async (value: string) => {
    if (!value && searchResult !== null) {
      return setSearchResult(null);
    }

    const result = await index
      .searchAsync(value, { suggest: true })
      .then((res) =>
        res.map((id) => {
          if (typeof id !== 'number') throw new Error('NaN');

          const row = Math.floor(id / CHUNK_SIZE);
          const col = Math.floor(id % CHUNK_SIZE);

          const item = chunked[row][col];

          if (isString(item)) throw new Error('whoops');

          return item;
        }),
      );

    setSearchResult(['search results', ...chunk(result, CHUNK_SIZE)]);
  };

  return (
    <Box sx={{ userSelect: 'none' }}>
      <Tabs value={activeTab} px={0}>
        <Tabs.List>{tabs}</Tabs.List>
      </Tabs>

      <Virtuoso
        ref={virtuoso}
        style={{ height: 300 }}
        data={searchResult ? searchResult : chunked}
        components={{ ScrollSeekPlaceholder }}
        overscan={200}
        itemContent={(_index: number, item: RowItem) => {
          if (Array.isArray(item)) {
            const children = item.map((props, idx) => {
              return (
                <Emoji
                  key={idx}
                  {...props}
                  // @ts-ignore
                  onClick={() => onEmojiSelect(props.label, props.custom)}
                />
              );
            });

            return (
              <SimpleGrid
                cols={CHUNK_SIZE}
                px={HPADDING}
                sx={{ height: EMOJI_SIZE + VPADDING }}
              >
                {children}
              </SimpleGrid>
            );
          } else {
            // Group Name
            return (
              <Box
                px={HPADDING}
                sx={{ width: '100%', height: EMOJI_SIZE + VPADDING }}
              >
                <Stack sx={{ height: '100%' }} justify="center">
                  <Divider my="xs" label={item} />
                </Stack>
              </Box>
            );
          }
        }}
        rangeChanged={(range) => {
          for (const [key, value] of waypoints) {
            if (range.startIndex <= value && range.endIndex >= value) {
              return setActiveTab(key);
            }
          }
        }}
      />
      <TextInput
        icon={<IconSearch size={16} />}
        p="xs"
        px={0}
        onChange={(event) => {
          onSearch(event.currentTarget.value);
        }}
      />
      {extraOptions && (
        <Group position="apart" px="xs">
          <Checkbox label="Keep open" />
          <Checkbox label="Add spaces" />
        </Group>
      )}
    </Box>
  );
};

export default EmojiPicker;
