// import type { Emoji_attachment$key } from './__generated__/Emoji_attachment.graphql.js';
//
// import { FC, memo } from 'react';
// import { graphql, useFragment } from 'react-relay';
// import {
//   createStyles,
//   Box,
//   BoxProps,
//   Text,
//   ActionIcon,
//   ActionIconProps,
//   Image,
// } from '@mantine/core';
// // import Image from "next/future/image";
//
// import { SuspenseWrapper } from '../core/SuspenseWrapper.jsx';
// import { createDefaultProps } from 'hooks/createDefault.js';
// import { useConfig } from 'hooks/useConfig.js';
// import { unicodeMapping } from 'core/unicodeMapping.js';
//
// const fragment = graphql`
//   fragment Emoji_attachment on Attachment {
//     id
//     remote
//     src
//   }
// `;
//
// interface Props extends ActionIconProps {
//   attachment?: Emoji_attachment$key | null;
//   unicode?: string | null;
//   size?: number;
// }
//
// const useStyles = createStyles((theme) => ({
//   box: {
//     display: 'flex !important',
//     padding: 10,
//   },
//
//   sticker: {
//     objectFit: 'contain',
//   },
// }));
//
// const useDefaultProps = createDefaultProps((props: Props) => {
//   const { emojiSize } = useConfig();
//
//   return {
//     size: props.size ?? emojiSize,
//   };
// });
//
// interface AttachmentProps {
//   attachment: Emoji_attachment$key;
// }
//
// const Attachment: FC<AttachmentProps> = (props) => {
//   const data = useFragment(fragment, props.attachment);
//   const { classes } = useStyles();
//
//   return (
//     <Image
//       className={classes.sticker}
//       src={data.src ?? ''}
//       alt="emoji"
//       imageProps={{ loading: 'lazy' }}
//     />
//   );
// };
//
// const Unicode: FC<{ unicode?: string | null }> = (props) => {
//   const { unicode } = props;
//
//   if (!unicode || !(unicode in unicodeMapping)) return null;
//
//   const { unified, shortcode } = unicodeMapping[unicode];
//
//   return (
//     <Image
//       src={`/twemoji/${unified}.svg`}
//       alt={shortcode}
//       imageProps={{
//         loading: 'lazy',
//       }}
//     />
//   );
// };
//
// // TODO: placeholder
// const Placeholder: FC = () => <div />;
//
// // TODO: shortcodes
// // TODO: local images
// const Loaded: FC<Props> = memo((props) => {
//   const { attachment, unicode, size, ...other } = useDefaultProps(props);
//   const { classes } = useStyles();
//
//   if (!unicode && !attachment) return null;
//
//   return (
//     <ActionIcon
//       className={classes.box}
//       sx={{ display: 'inline-block', width: size, height: size, padding: 5 }}
//       {...other}
//     >
//       {attachment ? (
//         <Attachment attachment={attachment} />
//       ) : (
//         <Unicode unicode={unicode} />
//       )}
//     </ActionIcon>
//   );
// });
//
// export const Emoji = SuspenseWrapper(Loaded);
//
