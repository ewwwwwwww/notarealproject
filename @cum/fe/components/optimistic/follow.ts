export const optimisticFollow = (
  remote: boolean,
  manuallyApprovesFollowers: boolean,
  followerRelayId: UUID,
  followerCount: number,
  followedRelayId: UUID,
  followedCount: number,
) => {
  if (remote || manuallyApprovesFollowers) {
    return {
      follower: {
        id: followerRelayId,
        countFollowing: followerCount,
      },
      followed: {
        id: followedRelayId,
        countFollowers: followedCount,
        isFollowedByYou: 'MAYBE',
      },
    };
  } else {
    return {
      follower: {
        id: followerRelayId,
        countFollowing: followerCount + 1,
      },
      followed: {
        id: followedRelayId,
        countFollowers: followedCount + 1,
        isFollowedByYou: 'TRUE',
      },
    };
  }
};

export const optimisticUnfollow = (
  followerRelayId: UUID,
  followerCount: number,
  followedRelayId: UUID,
  followedCount: number,
) => {
  return {
    deleteFollowByFollowerIdAndFollowedId: {
      follower: {
        id: followerRelayId,
        countFollowing: followerCount - 1,
      },

      followed: {
        id: followedRelayId,
        countFollowers: followedCount - 1,
        isFollowedByYou: 'false',
      },
    },
  };
};
