import { getState } from 'hooks/useMe.js';

export const optimisticNote = (
  noteId: string,
  scope: string,
  body: string,
  replyId?: string,
  threadId?: string,
) => {
  const me = getState();

  if (!me.isLoggedIn) {
    console.error('not logged in');
    return undefined;
  }

  const id = btoa(JSON.stringify(['notes', noteId]));

  return {
    id,
    noteId,
    body,
    ctime: new Date().toISOString().replace('Z', '000+00:00'),
    scope,
    domain: {
      id: me.data.domain?.id,
      host: me.data.domain?.name,
    },
    user: {
      id: me.data.id,
      name: me.data.name,
      nick: me.data.nick,
      nameFull: me.data.nameFull,
      remote: false,
      avatar: me.data.avatar
        ? {
            id: me.data.avatar.id,
            src: me.data.avatar.src,
            blake3: me.data.avatar.blake3,
            remote: false,
          }
        : null,
    },
    replyId: replyId ?? null,
    threadId: threadId ?? noteId,
    mentions: {
      edges: [],
      pageInfo: {
        endCursor: null,
        hasNextPage: false,
      },
    },
    reactAggrs: {
      edges: [],
    },
    countReplies: 0,
    countLikes: 0,
    countBookmarks: 0,
    countReacts: 0,
    countRepeats: 0,
    isLikedByYou: 0,
    isBookmarkedByYou: 0,
    isRepeatedByYou: 0,
  };
};
