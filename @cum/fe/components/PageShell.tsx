import '../styles/globals.css';

import { FC, ReactNode, useEffect, useState, memo } from 'react';
import { MantineProvider } from '@mantine/styles';
import {
  Environment,
  PreloadedQuery,
  RelayEnvironmentProvider,
} from 'react-relay';
import { EditorProvider } from './editor/EditorProvider.jsx';
import { AuthPromptProvider } from './landing/AuthPromptProvider.jsx';
import { ErrorBoundary } from 'react-error-boundary';
import { I18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';
import { useLocale } from 'hooks/useLocale.js';
import { Aside, Layout, Nav } from './layout/Layout.jsx';
import { FloatingActionButton } from './buttons/FloatingActionButton.jsx';
import { Notifications } from './inbox/Notifications.jsx';
import { NotificationsProvider } from '@mantine/notifications';

interface Props {
  relay: Environment;
  i18n: I18n;
  locale: string;
  children: ReactNode;
  aside: {
    Aside: any;
    AsideBar?: any;
    queryRef: PreloadedQuery<any, Record<string, unknown>>;
  } | null;
}

// TODO: proper types
const reducer = (acc: any, [Provider, props]: [any, any]) => (
  <Provider {...props}>{acc}</Provider>
);

// TODO: proper types
const compose = (contexts: any[], children: ReactNode) =>
  contexts.reduceRight(reducer, children);

export const PageShell: FC<Props> = memo((props) => {
  const { relay, i18n, locale, children, aside } = props;
  const activeLocale = useLocale((state) => state.locale);
  const setActiveLocale = useLocale((state) => state.set);

  // TODO: locale switching
  useEffect(() => {
    if (!activeLocale) return setActiveLocale(locale);

    // i18n.load(locale, translation);
    // i18n.activate(locale);

    console.log(`using locale ${activeLocale}`);
  }, [locale, activeLocale]);

  return compose(
    [
      [ErrorBoundary, { FallbackComponent: ErrorFallback }],
      [I18nProvider, { i18n }],
      [RelayEnvironmentProvider, { environment: relay }],
      [
        MantineProvider,
        {
          withGlobalStyles: true,
          withNormalizeCss: true,
          theme: { colorScheme: 'light', primaryColor: 'indigo' },
        },
      ],
      [NotificationsProvider, { position: 'bottom-left' }],
      [AuthPromptProvider, {}],
      [EditorProvider, {}],
    ],

    <>
      <Layout>
        <Nav span={2} />
        {children}
        {aside && (
          <Aside
            span={4}
            Component={aside.Aside}
            Header={aside.AsideBar}
            queryRef={aside.queryRef}
          />
        )}
      </Layout>
      <FloatingActionButton />
      <Notifications />
    </>,
  );
});

// TODO: move to page
const ErrorFallback: FC<any> = (props) => {
  const { error, resetErrorBoundary } = props;

  return (
    <div role="alert">
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
      <button onClick={resetErrorBoundary}>Try again</button>
    </div>
  );
};
