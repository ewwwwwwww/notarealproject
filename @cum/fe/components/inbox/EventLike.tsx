import type { EventLikeFragment$key } from './__generated__/EventLikeFragment.graphql.js';

import { Text } from '@mantine/core';
import { Fragment } from 'components/core/Fragment.jsx';
import { Html } from 'components/core/Html.jsx';
import { UserName } from 'components/user/UserName.jsx';
import { graphql } from 'relay-runtime';
import { InboxEvent } from './InboxEvent.jsx';
import { t } from '@lingui/macro';
import { BiHeart as IconLike } from 'react-icons/bi';

const fragment = graphql`
  fragment EventLikeFragment on Like {
    id
    user {
      id
      ...UserName_user
    }
    note {
      id
      body
    }
  }
`;

interface Props {
  data: EventLikeFragment$key;
  date: Date;
}

export const EventLike = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { user, note } = data;

  return (
    <InboxEvent
      id={note?.id}
      name={user && <UserName data={user} compact />}
      label={t`liked your post`}
      body={note?.body}
      bodyClamp={3}
      icon={<IconLike size={16} />}
      date={date}
    />
  );
});
