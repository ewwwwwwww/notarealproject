import { useMediaQuery } from '@mantine/hooks';
import { showNotification } from '@mantine/notifications';
import { useMe } from 'hooks/useMe.js';
import { FC } from 'react';
import { useNotiCreated } from './useNotiCreated.js';

interface Props {}

export const Notifications: FC<Props> = (props) => {
  const me = useMe((state) => state.data?.userId);
  const matches = useMediaQuery('(min-width: 900px)');

  if (!me) return null;

  useNotiCreated(me, '', (res) => {
    if (!matches) return;
    if (!res || !res.listen.relatedNode) return;

    console.log(res.listen.relatedNode);

    switch (res.listen.relatedNode.type) {
      case 'LIKE':
        return showNotification({
          title: res.listen.relatedNode.like?.user?.nameFull,
          message: `liked your post`,
        });
      case 'BLOCK':
        return showNotification({
          title: res.listen.relatedNode.block?.blocker?.nameFull,
          message: `blocked you`,
        });
      case 'FOLLOW':
        return showNotification({
          title: res.listen.relatedNode.follow?.follower?.nameFull,
          message: `followed you`,
        });
      case 'MENTION':
        return showNotification({
          title: res.listen.relatedNode.mention?.user?.nameFull,
          message: `mentioned you`,
        });
      case 'REPEAT':
        return showNotification({
          title: res.listen.relatedNode.repeat?.user?.nameFull,
          message: `repeated your post`,
        });
      case 'REACT':
        return showNotification({
          title: res.listen.relatedNode.react?.user?.nameFull,
          message: `reacted to your post`,
        });
      default:
        console.warn(
          'unimplemented notification type',
          res.listen.relatedNode.type,
        );
    }
  });

  return null;
};
