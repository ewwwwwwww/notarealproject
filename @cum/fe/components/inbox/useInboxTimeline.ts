import {
  NotificationsOrderBy,
  NotificationCondition,
} from './__generated__/InboxTimelinePaginationQuery.graphql.js';
import create from 'zustand';

import { createTimelineStore } from 'hooks/createTimelineStore.js';

export const inboxTimelineStore = createTimelineStore<
  NotificationsOrderBy,
  NotificationCondition
>('CTIME_DESC');

export const useInboxTimeline = create(inboxTimelineStore);
