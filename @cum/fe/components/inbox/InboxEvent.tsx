import { Group, ScrollArea, Stack, Text } from '@mantine/core';
import { Html } from 'components/core/Html.jsx';
import { ShortDate } from 'components/core/ShortDate.jsx';
import { NoteHoverWrapper } from 'components/note/NoteHover.jsx';
import { Children, FC, memo, ReactNode } from 'react';
import { IconType } from 'react-icons';
import { BiQuestionMark as IconQuestion } from 'react-icons/bi';

interface Props {
  id?: ID | null;
  name?: string | ReactNode;
  label?: string | ReactNode;
  icon?: ReactNode;
  body?: string | null;
  bodyClamp?: number;
  scrollable?: boolean;
  scrollerHeight?: number;
  date: Date;
}

const Scrollable: FC<{
  id?: ID | null;
  scrollable: boolean;
  height?: number;
  children: ReactNode | null;
}> = (props) => {
  const { id, scrollable, children, height } = props;
  const wrapped = id ? (
    <NoteHoverWrapper id={id}>{children}</NoteHoverWrapper>
  ) : (
    <>{children}</>
  );

  if (!scrollable) return wrapped;

  return (
    <ScrollArea style={{ height: height ?? 300 }} offsetScrollbars>
      {wrapped}
    </ScrollArea>
  );
};

export const InboxEvent: FC<Props> = memo((props) => {
  const {
    id,
    name,
    label,
    icon,
    body,
    bodyClamp,
    scrollable,
    scrollerHeight,
    date,
  } = props;

  const html = body ? (
    <Html
      content={body}
      size="sm"
      color={scrollable ? undefined : 'dimmed'}
      lineClamp={bodyClamp}
    />
  ) : null;

  return (
    <>
      <Group position="apart">
        <Text>
          {name && <>{name} </>}
          <Text component="span" size="sm">
            {label}
          </Text>
        </Text>

        <Stack>
          <Group position="apart" spacing="xs">
            <ShortDate date={date} color="dimmed" size="sm" />
            {icon ? icon : <IconQuestion size={16} />}
          </Group>
        </Stack>
      </Group>
      <Scrollable
        id={id}
        scrollable={scrollable ?? false}
        height={scrollerHeight}
      >
        {html}
      </Scrollable>
    </>
  );
});
