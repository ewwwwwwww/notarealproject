import type { EventBlockFragment$key } from './__generated__/EventBlockFragment.graphql.js';

import { Fragment } from 'components/core/Fragment.jsx';
import { UserName } from 'components/user/UserName.jsx';
import { graphql } from 'relay-runtime';
import { InboxEvent } from './InboxEvent.jsx';
import { t } from '@lingui/macro';
import { ImBlocked as IconBlock } from 'react-icons/im';

const fragment = graphql`
  fragment EventBlockFragment on Block {
    id
    blocker {
      id
      ...UserName_user
    }
  }
`;

interface Props {
  data: EventBlockFragment$key;
  date: Date;
}

export const EventBlock = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { blocker } = data;

  return (
    <InboxEvent
      name={blocker && <UserName data={blocker} compact />}
      label={t`blocked you`}
      icon={<IconBlock size={16} />}
      date={date}
    />
  );
});
