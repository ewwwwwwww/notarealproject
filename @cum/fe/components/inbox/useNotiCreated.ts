import { useMemoSubscription } from 'hooks/useMemoSubscription.js';
import { graphql } from 'relay-runtime';
import type {
  useNotiCreatedSubscription,
  useNotiCreatedSubscription$data,
} from './__generated__/useNotiCreatedSubscription.graphql.js';

// mother of god....
const subscription = graphql`
  subscription useNotiCreatedSubscription(
    $topic: String!
    $connections: [ID!]!
  ) {
    listen(topic: $topic) {
      relatedNode
        @prependNode(
          connections: $connections
          edgeTypeName: "NotificationsEdge"
        ) {
        ... on Notification {
          type
          ctime

          ...InboxCardFragment

          mention {
            note {
              body
            }
            user {
              nameFull
              ...UserName_user
            }
          }
          block {
            blocker {
              nameFull
              ...UserName_user
            }
          }
          like {
            user {
              nameFull
              ...UserName_user
            }
            note {
              body
            }
          }
          react {
            note {
              body
            }
            user {
              nameFull
              ...UserName_user
            }
          }
          repeat {
            note {
              body
            }
            user {
              nameFull
              ...UserName_user
            }
          }
          follow {
            follower {
              nameFull
              ...UserName_user
            }
          }
        }
      }
    }
  }
`;

export const useNotiCreated = (
  userId: UUID,
  connectionID?: string,
  onNext?: (res?: useNotiCreatedSubscription$data | null) => void,
) => {
  const topic = `noti:created:${userId}`;

  return useMemoSubscription<useNotiCreatedSubscription>({
    subscription,
    variables: {
      topic,
      connections: connectionID ? [connectionID] : [],
    },

    onNext,
  });
};
