import type { InboxCardFragment$key } from './__generated__/InboxCardFragment.graphql.js';

import { Card } from 'components/core/Card.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { graphql } from 'relay-runtime';
import { EventLike } from './EventLike.jsx';
import { EventMention } from './EventMention.jsx';
import { EventFollow } from './EventFollow.jsx';
import { EventBlock } from './EventBlock.jsx';
import { EventReact } from './EventReact.jsx';
import { EventRepeat } from './EventRepeat.jsx';
import { Box } from '@mantine/core';

const fragment = graphql`
  fragment InboxCardFragment on Notification {
    id
    type
    ctime

    block {
      id
      ...EventBlockFragment
    }

    follow {
      id
      ...EventFollowFragment
    }

    like {
      id
      ...EventLikeFragment
    }

    mention {
      id
      ...EventMentionFragment
    }

    react {
      id
      ...EventReactFragment
    }

    repeat {
      id
      ...EventRepeatFragment
    }
  }
`;

interface Props {
  data: InboxCardFragment$key;
}

export const InboxCard = Fragment<Props>(fragment, (props) => {
  const { data } = props;

  let item;

  const date = new Date(data.ctime);

  switch (data.type) {
    case 'BLOCK':
      item = <EventBlock data={data.block!} date={date} />;
      break;
    case 'FOLLOW':
      item = <EventFollow data={data.follow!} date={date} />;
      break;
    case 'MENTION':
      item = <EventMention data={data.mention!} date={date} />;
      break;
    case 'LIKE':
      item = <EventLike data={data.like!} date={date} />;
      break;
    case 'REACT':
      item = <EventReact data={data.react!} date={date} />;
      break;
    case 'REPEAT':
      item = <EventRepeat data={data.repeat!} date={date} />;
      break;
    default:
      item = <div>unimplemented</div>;
  }

  return <Box>{item}</Box>;
});
