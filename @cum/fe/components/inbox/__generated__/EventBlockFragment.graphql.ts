/**
 * @generated SignedSource<<fb3430eb0fe16723cd805045731798d4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventBlockFragment$data = {
  readonly blocker: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
  } | null;
  readonly id: ID;
  readonly " $fragmentType": "EventBlockFragment";
};
export type EventBlockFragment$key = {
  readonly " $data"?: EventBlockFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventBlockFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventBlockFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "blocker",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Block",
  "abstractKey": null
};
})();

(node as any).hash = "35ee17da8d5e168856393c0550f4d57f";

export default node;
