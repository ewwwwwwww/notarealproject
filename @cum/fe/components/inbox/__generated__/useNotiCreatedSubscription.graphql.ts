/**
 * @generated SignedSource<<a1bde9d3c98ecd3535dc5b0e4732cb67>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NotificationType = "BLOCK" | "FOLLOW" | "LIKE" | "MENTION" | "REACT" | "REPEAT" | "%future added value";
export type useNotiCreatedSubscription$variables = {
  connections: ReadonlyArray<ID>;
  topic: string;
};
export type useNotiCreatedSubscription$data = {
  readonly listen: {
    readonly relatedNode: {
      readonly block?: {
        readonly blocker: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly ctime?: Datetime;
      readonly follow?: {
        readonly follower: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly like?: {
        readonly note: {
          readonly body: string | null;
        } | null;
        readonly user: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly mention?: {
        readonly note: {
          readonly body: string | null;
        } | null;
        readonly user: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly react?: {
        readonly note: {
          readonly body: string | null;
        } | null;
        readonly user: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly repeat?: {
        readonly note: {
          readonly body: string | null;
        } | null;
        readonly user: {
          readonly nameFull: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
        } | null;
      } | null;
      readonly type?: NotificationType;
      readonly " $fragmentSpreads": FragmentRefs<"InboxCardFragment">;
    } | null;
  };
};
export type useNotiCreatedSubscription = {
  response: useNotiCreatedSubscription$data;
  variables: useNotiCreatedSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "topic"
},
v2 = [
  {
    "kind": "Variable",
    "name": "topic",
    "variableName": "topic"
  }
],
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "ctime",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "concreteType": "Note",
  "kind": "LinkedField",
  "name": "note",
  "plural": false,
  "selections": [
    (v5/*: any*/)
  ],
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v8 = [
  (v7/*: any*/),
  {
    "args": null,
    "kind": "FragmentSpread",
    "name": "UserName_user"
  }
],
v9 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v8/*: any*/),
  "storageKey": null
},
v10 = [
  (v6/*: any*/),
  (v9/*: any*/)
],
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v12 = [
  (v11/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "nick",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  },
  (v7/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "remote",
    "storageKey": null
  }
],
v13 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v12/*: any*/),
  "storageKey": null
},
v14 = [
  (v11/*: any*/),
  (v13/*: any*/),
  {
    "alias": null,
    "args": null,
    "concreteType": "Note",
    "kind": "LinkedField",
    "name": "note",
    "plural": false,
    "selections": [
      (v11/*: any*/),
      (v5/*: any*/)
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "useNotiCreatedSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "kind": "InlineFragment",
                "selections": [
                  (v3/*: any*/),
                  (v4/*: any*/),
                  {
                    "args": null,
                    "kind": "FragmentSpread",
                    "name": "InboxCardFragment"
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "mention",
                    "plural": false,
                    "selections": (v10/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Block",
                    "kind": "LinkedField",
                    "name": "block",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "blocker",
                        "plural": false,
                        "selections": (v8/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Like",
                    "kind": "LinkedField",
                    "name": "like",
                    "plural": false,
                    "selections": [
                      (v9/*: any*/),
                      (v6/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "React",
                    "kind": "LinkedField",
                    "name": "react",
                    "plural": false,
                    "selections": (v10/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Repeat",
                    "kind": "LinkedField",
                    "name": "repeat",
                    "plural": false,
                    "selections": (v10/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Follow",
                    "kind": "LinkedField",
                    "name": "follow",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "follower",
                        "plural": false,
                        "selections": (v8/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "type": "Notification",
                "abstractKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "useNotiCreatedSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "__typename",
                "storageKey": null
              },
              (v11/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": [
                  (v3/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Block",
                    "kind": "LinkedField",
                    "name": "block",
                    "plural": false,
                    "selections": [
                      (v11/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "blocker",
                        "plural": false,
                        "selections": (v12/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Follow",
                    "kind": "LinkedField",
                    "name": "follow",
                    "plural": false,
                    "selections": [
                      (v11/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "follower",
                        "plural": false,
                        "selections": (v12/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Like",
                    "kind": "LinkedField",
                    "name": "like",
                    "plural": false,
                    "selections": (v14/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "mention",
                    "plural": false,
                    "selections": [
                      (v11/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "reply",
                        "storageKey": null
                      },
                      (v13/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Note",
                        "kind": "LinkedField",
                        "name": "note",
                        "plural": false,
                        "selections": [
                          (v11/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scope",
                            "storageKey": null
                          },
                          (v5/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "React",
                    "kind": "LinkedField",
                    "name": "react",
                    "plural": false,
                    "selections": (v14/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Repeat",
                    "kind": "LinkedField",
                    "name": "repeat",
                    "plural": false,
                    "selections": (v14/*: any*/),
                    "storageKey": null
                  }
                ],
                "type": "Notification",
                "abstractKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "relatedNode",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "NotificationsEdge"
              }
            ]
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "7aa48fa4bb0bca6a2b77a3df26d41fe3",
    "id": null,
    "metadata": {},
    "name": "useNotiCreatedSubscription",
    "operationKind": "subscription",
    "text": "subscription useNotiCreatedSubscription(\n  $topic: String!\n) {\n  listen(topic: $topic) {\n    relatedNode {\n      __typename\n      ... on Notification {\n        type\n        ctime\n        ...InboxCardFragment\n        mention {\n          note {\n            body\n            id\n          }\n          user {\n            nameFull\n            ...UserName_user\n            id\n          }\n          id\n        }\n        block {\n          blocker {\n            nameFull\n            ...UserName_user\n            id\n          }\n          id\n        }\n        like {\n          user {\n            nameFull\n            ...UserName_user\n            id\n          }\n          note {\n            body\n            id\n          }\n          id\n        }\n        react {\n          note {\n            body\n            id\n          }\n          user {\n            nameFull\n            ...UserName_user\n            id\n          }\n          id\n        }\n        repeat {\n          note {\n            body\n            id\n          }\n          user {\n            nameFull\n            ...UserName_user\n            id\n          }\n          id\n        }\n        follow {\n          follower {\n            nameFull\n            ...UserName_user\n            id\n          }\n          id\n        }\n      }\n      id\n    }\n  }\n}\n\nfragment EventBlockFragment on Block {\n  id\n  blocker {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventFollowFragment on Follow {\n  id\n  follower {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventLikeFragment on Like {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventMentionFragment on Mention {\n  id\n  reply\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReactFragment on React {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventRepeatFragment on Repeat {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment InboxCardFragment on Notification {\n  id\n  type\n  ctime\n  block {\n    id\n    ...EventBlockFragment\n  }\n  follow {\n    id\n    ...EventFollowFragment\n  }\n  like {\n    id\n    ...EventLikeFragment\n  }\n  mention {\n    id\n    ...EventMentionFragment\n  }\n  react {\n    id\n    ...EventReactFragment\n  }\n  repeat {\n    id\n    ...EventRepeatFragment\n  }\n}\n\nfragment UserName_user on User {\n  id\n  nick\n  name\n  nameFull\n  remote\n}\n"
  }
};
})();

(node as any).hash = "16f245afac8a95b4bf38ab30a53c2b49";

export default node;
