/**
 * @generated SignedSource<<ca12e69eeb0d88d436a20ea91ddc4ab5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type NotificationType = "BLOCK" | "FOLLOW" | "LIKE" | "MENTION" | "REACT" | "REPEAT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type InboxCardFragment$data = {
  readonly block: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventBlockFragment">;
  } | null;
  readonly ctime: Datetime;
  readonly follow: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventFollowFragment">;
  } | null;
  readonly id: ID;
  readonly like: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventLikeFragment">;
  } | null;
  readonly mention: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventMentionFragment">;
  } | null;
  readonly react: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventReactFragment">;
  } | null;
  readonly repeat: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"EventRepeatFragment">;
  } | null;
  readonly type: NotificationType;
  readonly " $fragmentType": "InboxCardFragment";
};
export type InboxCardFragment$key = {
  readonly " $data"?: InboxCardFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"InboxCardFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "InboxCardFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ctime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Block",
      "kind": "LinkedField",
      "name": "block",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventBlockFragment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Follow",
      "kind": "LinkedField",
      "name": "follow",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventFollowFragment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Like",
      "kind": "LinkedField",
      "name": "like",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventLikeFragment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Mention",
      "kind": "LinkedField",
      "name": "mention",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventMentionFragment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "React",
      "kind": "LinkedField",
      "name": "react",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventReactFragment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Repeat",
      "kind": "LinkedField",
      "name": "repeat",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventRepeatFragment"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Notification",
  "abstractKey": null
};
})();

(node as any).hash = "655d896f2d03ce2a540060f9b94d9768";

export default node;
