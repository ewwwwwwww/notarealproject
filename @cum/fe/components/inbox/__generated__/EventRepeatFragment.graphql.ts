/**
 * @generated SignedSource<<655b90b3571e3f09e3ecab4f0a4ad37a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventRepeatFragment$data = {
  readonly id: ID;
  readonly note: {
    readonly body: string | null;
    readonly id: ID;
  } | null;
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
  } | null;
  readonly " $fragmentType": "EventRepeatFragment";
};
export type EventRepeatFragment$key = {
  readonly " $data"?: EventRepeatFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventRepeatFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventRepeatFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Note",
      "kind": "LinkedField",
      "name": "note",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "body",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Repeat",
  "abstractKey": null
};
})();

(node as any).hash = "1dca24ad50d037e25f8b73466e804080";

export default node;
