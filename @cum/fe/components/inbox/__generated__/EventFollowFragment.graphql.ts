/**
 * @generated SignedSource<<f39e8afbd4f18417ae7f34ed3f9fece6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventFollowFragment$data = {
  readonly follower: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
  } | null;
  readonly id: ID;
  readonly " $fragmentType": "EventFollowFragment";
};
export type EventFollowFragment$key = {
  readonly " $data"?: EventFollowFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventFollowFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventFollowFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "follower",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Follow",
  "abstractKey": null
};
})();

(node as any).hash = "ed37d5adb07c4d0bb359fe960e7029bb";

export default node;
