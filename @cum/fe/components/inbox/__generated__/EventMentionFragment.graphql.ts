/**
 * @generated SignedSource<<884d99b61cd909f45a906d06524e8640>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type Scope = "DIRECT" | "LOCAL" | "PRIVATE" | "PUBLIC" | "UNLISTED" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type EventMentionFragment$data = {
  readonly id: ID;
  readonly note: {
    readonly body: string | null;
    readonly id: ID;
    readonly scope: Scope;
  } | null;
  readonly reply: boolean;
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
  } | null;
  readonly " $fragmentType": "EventMentionFragment";
};
export type EventMentionFragment$key = {
  readonly " $data"?: EventMentionFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventMentionFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventMentionFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "reply",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Note",
      "kind": "LinkedField",
      "name": "note",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "scope",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "body",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Mention",
  "abstractKey": null
};
})();

(node as any).hash = "03920c5439dcd811957947436917f8f4";

export default node;
