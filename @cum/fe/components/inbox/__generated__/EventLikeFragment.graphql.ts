/**
 * @generated SignedSource<<deba93521d91203a1cee9ae0180cc848>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventLikeFragment$data = {
  readonly id: ID;
  readonly note: {
    readonly body: string | null;
    readonly id: ID;
  } | null;
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
  } | null;
  readonly " $fragmentType": "EventLikeFragment";
};
export type EventLikeFragment$key = {
  readonly " $data"?: EventLikeFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventLikeFragment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventLikeFragment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Note",
      "kind": "LinkedField",
      "name": "note",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "body",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Like",
  "abstractKey": null
};
})();

(node as any).hash = "ead2cb08beed4bc8f56424980299acd4";

export default node;
