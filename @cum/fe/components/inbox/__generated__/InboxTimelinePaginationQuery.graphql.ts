/**
 * @generated SignedSource<<124e06235e8800adb3105d3a3d41d95d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NotificationsOrderBy = "BLOCK_ID_ASC" | "BLOCK_ID_DESC" | "CTIME_ASC" | "CTIME_DESC" | "FOLLOW_ID_ASC" | "FOLLOW_ID_DESC" | "LIKE_ID_ASC" | "LIKE_ID_DESC" | "MENTION_ID_ASC" | "MENTION_ID_DESC" | "NATURAL" | "NOTIFICATION_ID_ASC" | "NOTIFICATION_ID_DESC" | "PRIMARY_KEY_ASC" | "PRIMARY_KEY_DESC" | "REACT_ID_ASC" | "REACT_ID_DESC" | "READ_ASC" | "READ_DESC" | "REPEAT_ID_ASC" | "REPEAT_ID_DESC" | "USER_ID_ASC" | "USER_ID_DESC" | "%future added value";
export type NotificationCondition = {
  blockId?: UUID | null;
  ctime?: Datetime | null;
  followId?: UUID | null;
  likeId?: UUID | null;
  mentionId?: UUID | null;
  notificationId?: UUID | null;
  reactId?: UUID | null;
  read?: boolean | null;
  repeatId?: UUID | null;
  userId?: UUID | null;
};
export type InboxTimelinePaginationQuery$variables = {
  condition?: NotificationCondition | null;
  count?: number | null;
  cursor?: any | null;
  id: ID;
  orderBy?: NotificationsOrderBy | null;
};
export type InboxTimelinePaginationQuery$data = {
  readonly node: {
    readonly " $fragmentSpreads": FragmentRefs<"InboxTimelineFragment">;
  } | null;
};
export type InboxTimelinePaginationQuery = {
  response: InboxTimelinePaginationQuery$data;
  variables: InboxTimelinePaginationQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": {},
  "kind": "LocalArgument",
  "name": "condition"
},
v1 = {
  "defaultValue": 20,
  "kind": "LocalArgument",
  "name": "count"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "cursor"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v4 = {
  "defaultValue": "CTIME_DESC",
  "kind": "LocalArgument",
  "name": "orderBy"
},
v5 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v6 = {
  "kind": "Variable",
  "name": "condition",
  "variableName": "condition"
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v9 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "cursor"
  },
  (v6/*: any*/),
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "count"
  },
  {
    "items": [
      {
        "kind": "Variable",
        "name": "orderBy.0",
        "variableName": "orderBy"
      }
    ],
    "kind": "ListValue",
    "name": "orderBy"
  }
],
v10 = [
  (v8/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "nick",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "nameFull",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "remote",
    "storageKey": null
  }
],
v11 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v10/*: any*/),
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v13 = [
  (v8/*: any*/),
  (v11/*: any*/),
  {
    "alias": null,
    "args": null,
    "concreteType": "Note",
    "kind": "LinkedField",
    "name": "note",
    "plural": false,
    "selections": [
      (v8/*: any*/),
      (v12/*: any*/)
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "InboxTimelinePaginationQuery",
    "selections": [
      {
        "alias": null,
        "args": (v5/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "args": [
              (v6/*: any*/),
              {
                "kind": "Variable",
                "name": "count",
                "variableName": "count"
              },
              {
                "kind": "Variable",
                "name": "cursor",
                "variableName": "cursor"
              },
              {
                "kind": "Variable",
                "name": "orderBy",
                "variableName": "orderBy"
              }
            ],
            "kind": "FragmentSpread",
            "name": "InboxTimelineFragment"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v4/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Operation",
    "name": "InboxTimelinePaginationQuery",
    "selections": [
      {
        "alias": null,
        "args": (v5/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v7/*: any*/),
          (v8/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": (v9/*: any*/),
                "concreteType": "NotificationsConnection",
                "kind": "LinkedField",
                "name": "notifications",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "NotificationsEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "cursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Notification",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          (v8/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "type",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "ctime",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Block",
                            "kind": "LinkedField",
                            "name": "block",
                            "plural": false,
                            "selections": [
                              (v8/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "User",
                                "kind": "LinkedField",
                                "name": "blocker",
                                "plural": false,
                                "selections": (v10/*: any*/),
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Follow",
                            "kind": "LinkedField",
                            "name": "follow",
                            "plural": false,
                            "selections": [
                              (v8/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "User",
                                "kind": "LinkedField",
                                "name": "follower",
                                "plural": false,
                                "selections": (v10/*: any*/),
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Like",
                            "kind": "LinkedField",
                            "name": "like",
                            "plural": false,
                            "selections": (v13/*: any*/),
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Mention",
                            "kind": "LinkedField",
                            "name": "mention",
                            "plural": false,
                            "selections": [
                              (v8/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "reply",
                                "storageKey": null
                              },
                              (v11/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "Note",
                                "kind": "LinkedField",
                                "name": "note",
                                "plural": false,
                                "selections": [
                                  (v8/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "scope",
                                    "storageKey": null
                                  },
                                  (v12/*: any*/)
                                ],
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "React",
                            "kind": "LinkedField",
                            "name": "react",
                            "plural": false,
                            "selections": (v13/*: any*/),
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Repeat",
                            "kind": "LinkedField",
                            "name": "repeat",
                            "plural": false,
                            "selections": (v13/*: any*/),
                            "storageKey": null
                          },
                          (v7/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PageInfo",
                    "kind": "LinkedField",
                    "name": "pageInfo",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "endCursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hasNextPage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "kind": "ClientExtension",
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "__id",
                        "storageKey": null
                      }
                    ]
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v9/*: any*/),
                "filters": [
                  "condition",
                  "orderBy"
                ],
                "handle": "connection",
                "key": "InboxTimelineFragment_notifications",
                "kind": "LinkedHandle",
                "name": "notifications"
              }
            ],
            "type": "User",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e6582434443d90689adb4a5cc74efa94",
    "id": null,
    "metadata": {},
    "name": "InboxTimelinePaginationQuery",
    "operationKind": "query",
    "text": "query InboxTimelinePaginationQuery(\n  $condition: NotificationCondition = {}\n  $count: Int = 20\n  $cursor: Cursor\n  $orderBy: NotificationsOrderBy = CTIME_DESC\n  $id: ID!\n) {\n  node(id: $id) {\n    __typename\n    ...InboxTimelineFragment_3JHJXe\n    id\n  }\n}\n\nfragment EventBlockFragment on Block {\n  id\n  blocker {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventFollowFragment on Follow {\n  id\n  follower {\n    id\n    ...UserName_user\n  }\n}\n\nfragment EventLikeFragment on Like {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventMentionFragment on Mention {\n  id\n  reply\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReactFragment on React {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment EventRepeatFragment on Repeat {\n  id\n  user {\n    id\n    ...UserName_user\n  }\n  note {\n    id\n    body\n  }\n}\n\nfragment InboxCardFragment on Notification {\n  id\n  type\n  ctime\n  block {\n    id\n    ...EventBlockFragment\n  }\n  follow {\n    id\n    ...EventFollowFragment\n  }\n  like {\n    id\n    ...EventLikeFragment\n  }\n  mention {\n    id\n    ...EventMentionFragment\n  }\n  react {\n    id\n    ...EventReactFragment\n  }\n  repeat {\n    id\n    ...EventRepeatFragment\n  }\n}\n\nfragment InboxTimelineFragment_3JHJXe on User {\n  notifications(after: $cursor, first: $count, condition: $condition, orderBy: [$orderBy]) {\n    edges {\n      cursor\n      node {\n        id\n        ...InboxCardFragment\n        __typename\n      }\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n  id\n}\n\nfragment UserName_user on User {\n  id\n  nick\n  name\n  nameFull\n  remote\n}\n"
  }
};
})();

(node as any).hash = "23948fbb7e463045762a5e3089af17b9";

export default node;
