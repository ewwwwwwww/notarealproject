import { Fragment } from 'components/core/Fragment.jsx';
import { UserName } from 'components/user/UserName.jsx';
import { graphql } from 'relay-runtime';
import { InboxEvent } from './InboxEvent.jsx';
import { t } from '@lingui/macro';
import { EventMentionFragment$key } from './__generated__/EventMentionFragment.graphql.js';
import { IconScope } from 'components/core/Scope.jsx';

const fragment = graphql`
  fragment EventMentionFragment on Mention {
    id
    reply
    user {
      id
      ...UserName_user
    }
    note {
      id
      scope
      body
    }
  }
`;

interface Props {
  data: EventMentionFragment$key;
  date: Date;
}

export const EventMention = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { user, note } = data;

  const icon = <IconScope scope={note?.scope ?? 'PUBLIC'} />;

  return (
    <InboxEvent
      id={note?.id}
      name={user && <UserName data={user} compact />}
      label={data.reply ? t`replied to you` : t`mentioned you`}
      body={note?.body}
      date={date}
      icon={icon}
      scrollable
    />
  );
});
