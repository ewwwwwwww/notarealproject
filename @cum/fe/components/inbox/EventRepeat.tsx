import { Fragment } from 'components/core/Fragment.jsx';
import { UserName } from 'components/user/UserName.jsx';
import { graphql } from 'relay-runtime';
import { InboxEvent } from './InboxEvent.jsx';
import { t } from '@lingui/macro';
import { FaRetweet as IconRepeat } from 'react-icons/fa';
import { EventRepeatFragment$key } from './__generated__/EventRepeatFragment.graphql.js';

const fragment = graphql`
  fragment EventRepeatFragment on Repeat {
    id
    user {
      id
      ...UserName_user
    }
    note {
      id
      body
    }
  }
`;

interface Props {
  data: EventRepeatFragment$key;
  date: Date;
}

export const EventRepeat = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { user, note } = data;

  return (
    <InboxEvent
      id={note?.id}
      name={user && <UserName data={user} compact />}
      label={t`repeated your post`}
      body={note?.body}
      bodyClamp={3}
      icon={<IconRepeat size={16} />}
      date={date}
    />
  );
});
