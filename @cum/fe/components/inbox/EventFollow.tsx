import type { EventFollowFragment$key } from './__generated__/EventFollowFragment.graphql.js';
import { Fragment } from 'components/core/Fragment.jsx';
import { UserName } from 'components/user/UserName.jsx';
import { graphql } from 'relay-runtime';
import { InboxEvent } from './InboxEvent.jsx';
import { t } from '@lingui/macro';
import { RiUserFollowLine as IconFollow } from 'react-icons/ri';

const fragment = graphql`
  fragment EventFollowFragment on Follow {
    id
    follower {
      id
      ...UserName_user
    }
  }
`;

interface Props {
  data: EventFollowFragment$key;
  date: Date;
}

export const EventFollow = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { follower } = data;

  return (
    <InboxEvent
      name={follower && <UserName data={follower} compact />}
      label={t`followed you`}
      icon={<IconFollow size={16} />}
      date={date}
    />
  );
});
