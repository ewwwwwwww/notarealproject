import { FC, memo, ReactNode, useRef } from 'react';
import { Paper } from '@mantine/core';
import { Card } from 'components/core/Card.jsx';
import { useScrollStyles } from 'components/timeline/useScrollStyles.js';
import { usePaginationFragment } from 'react-relay';
import { Virtuoso } from 'react-virtuoso';
import { graphql } from 'relay-runtime';
import { InboxCard } from './InboxCard.jsx';
import { useInboxTimeline } from './useInboxTimeline.js';
import { InboxTimelineFragment$key } from './__generated__/InboxTimelineFragment.graphql.js';

const fragment = graphql`
  fragment InboxTimelineFragment on User
  @argumentDefinitions(
    cursor: { type: "Cursor" }
    count: { type: "Int", defaultValue: 20 }
    condition: { type: "NotificationCondition", defaultValue: {} }
    orderBy: { type: "NotificationsOrderBy", defaultValue: CTIME_DESC }
  )
  @refetchable(queryName: "InboxTimelinePaginationQuery") {
    notifications(
      after: $cursor
      first: $count
      condition: $condition
      orderBy: [$orderBy]
    ) @connection(key: "InboxTimelineFragment_notifications") {
      __id
      edges {
        cursor
        node {
          id
          ...InboxCardFragment
        }
      }
    }
  }
`;

interface Props {
  data: InboxTimelineFragment$key;
  children?: ReactNode | ReactNode[];
}

const Header: FC = memo(() => {
  return <Card> </Card>;
});

export const InboxTimeline: FC<Props> = (props) => {
  const { children } = props;
  const { data, loadNext } = usePaginationFragment(fragment, props.data);
  const { classes } = useScrollStyles(false);

  const setVirtuoso = useInboxTimeline((state) => state.setVirtuoso);
  const setScrolling = useInboxTimeline((state) => state.setScrolling);
  const setId = useInboxTimeline((state) => state.setId);

  const connectionID = data?.notifications?.__id ?? '';
  const virtuoso = useRef(null);

  if (!data?.notifications) return null;

  setId(connectionID);
  setVirtuoso(virtuoso.current);

  const edges = data.notifications.edges;

  return (
    <Paper sx={{ height: '100%' }}>
      <Virtuoso
        ref={virtuoso}
        className={classes.scroller}
        data={edges}
        components={{ Header }}
        endReached={() => {
          loadNext(20);
        }}
        isScrolling={setScrolling}
        initialItemCount={edges.length > 20 ? 21 : edges.length}
        itemContent={(idx, edge) => {
          return <InboxCard key={idx} data={edge.node} />;
        }}
      />
    </Paper>
  );
};
