import { Fragment } from 'components/core/Fragment.jsx';
import { graphql } from 'relay-runtime';
import { t } from '@lingui/macro';
import { HiOutlineEmojiHappy as IconReact } from 'react-icons/hi';
import { EventReactFragment$key } from './__generated__/EventReactFragment.graphql.js';
import { InboxEvent } from './InboxEvent.jsx';
import { UserName } from 'components/user/UserName.jsx';

const fragment = graphql`
  fragment EventReactFragment on React {
    id
    user {
      id
      ...UserName_user
    }
    note {
      id
      body
    }
  }
`;

interface Props {
  data: EventReactFragment$key;
  date: Date;
}

export const EventReact = Fragment<Props>(fragment, (props) => {
  const { data, date } = props;
  const { user, note } = data;

  return (
    <InboxEvent
      id={note?.id}
      name={user && <UserName data={user} compact />}
      label={t`repeated your post`}
      body={note?.body}
      bodyClamp={3}
      icon={<IconReact size={16} />}
      date={date}
    />
  );
});
