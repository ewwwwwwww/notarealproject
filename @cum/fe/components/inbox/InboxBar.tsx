import { FC, memo } from 'react';
import { FilterBar } from 'components/layout/FilterBar.jsx';
import { FilterData } from 'components/layout/FilterHover.jsx';
import { useInboxTimeline } from './useInboxTimeline.js';

interface Props {}

export const InboxBar: FC<Props> = memo((props) => {
  const scrollTop = useInboxTimeline((state) => state.scrollTop);
  const data: FilterData[] = [];

  return (
    <FilterBar
      data={data}
      label="Inbox"
      searchable
      unread={25}
      unreadLabel="Unread"
      onScrollTop={scrollTop}
    />
  );
});
