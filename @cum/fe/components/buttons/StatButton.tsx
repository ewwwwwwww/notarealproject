import { FC, memo, ReactNode } from 'react';
import { Anchor, MantineColor, Text } from '@mantine/core';
import { ExpandoButton, Props as ExpandoProps } from './ExpandoButton.jsx';
import { useLingui } from '@lingui/react';
import { ShortDate } from 'components/core/ShortDate.jsx';

interface Props extends ExpandoProps {
  count?: number;
  date?: Date;
  href?: string;
  unit?: string | ReactNode;
  label: string | ReactNode;
  color?: MantineColor;
  subTextColor?: MantineColor;
  transparent?: boolean;
}

export const StatButton: FC<Props> = memo((props) => {
  const { href, date, count, unit, label, color, subTextColor, ...others } =
    props;
  const { i18n } = useLingui();
  const n = (v: number) => i18n.number(v, { notation: 'compact' });

  // prettier-ignore
  const content =
    date !== undefined  ? <ShortDate date={date} /> :
    count !== undefined ? n(count)
                        : '';

  const children = (
    <ExpandoButton {...others}>
      <Text align="center" color={color} weight={600}>
        {content}
        <Text component="span" weight={100} size="sm">
          {' '}
          {unit}
        </Text>
      </Text>
      <Text align="center" color={subTextColor ?? 'dimmed'} size="sm">
        {label}
      </Text>
    </ExpandoButton>
  );

  return href ? <Anchor href={href}></Anchor> : children;
});
