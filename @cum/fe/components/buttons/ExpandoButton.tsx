import {
  createStyles,
  UnstyledButton,
  UnstyledButtonProps,
} from '@mantine/core';
import { FC, memo } from 'react';

export interface Props extends UnstyledButtonProps {}

const useStyles = createStyles((theme) => {
  const hover = theme.fn.variant({
    color: theme.primaryColor,
    variant: 'light',
  });

  return {
    button: {
      height: '100%',
      width: '100%',
      borderRadius: 5,
      padding: 10,
      transition: 'box-shadow 150ms ease, transform 100ms ease',
      backgroundColor:
        theme.colorScheme === 'dark'
          ? 'rgba(0, 0, 0, 0.05)'
          : 'rgba(0, 0, 0, 0.01)',

      '&:hover': {
        boxShadow: `${theme.shadows.md} !important`,
        transform: 'scale(1.05)',
        backgroundColor:
          theme.colorScheme === 'dark'
            ? theme.colors.dark[7]
            : hover.background,
      },
    },
  };
});

export const ExpandoButton: FC<Props> = memo((props) => {
  const { children, className, ...other } = props;
  const { classes, cx } = useStyles();

  return (
    <UnstyledButton className={cx(classes.button, className)} {...other}>
      {children}
    </UnstyledButton>
  );
});
