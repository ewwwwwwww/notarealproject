import { Badge, createStyles, NavLink, Tooltip } from '@mantine/core';
import { FC, memo } from 'react';
import { IconType } from 'react-icons';

interface Props {
  href?: string;
  label: string;
  count?: number;
  Icon: IconType;
}

const useStyles = createStyles((theme) => ({
  link: {
    color:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[0]
        : theme.colors.gray[7],
  },
}));

export const NavButton: FC<Props> = memo((props) => {
  const { label, href, count, Icon } = props;
  const { classes } = useStyles();

  const rightSection = count ? (
    <Badge size="md" variant="filled">
      {count}
    </Badge>
  ) : null;

  return (
    <Tooltip label={label}>
      <NavLink<'a'>
        component="a"
        href={href}
        className={classes.link}
        icon={<Icon size={18} />}
        active={false}
        // rightSection={rightSection}
        label={label}
        styles={(theme) => ({
          root: {
            [`@media (max-width: 1400px)`]: {
              paddingLeft: 10,
              paddingRight: 0,
            },
          },

          label: {
            [`@media (max-width: 1400px)`]: {
              display: 'none',
            },
          },
        })}
      />
    </Tooltip>
  );
});
