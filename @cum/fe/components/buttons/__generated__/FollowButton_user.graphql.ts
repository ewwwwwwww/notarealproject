/**
 * @generated SignedSource<<b1ae08b68d876b0bd929329dc49455e4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type Tribool = "FALSE" | "MAYBE" | "TRUE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type FollowButton_user$data = {
  readonly countFollowers: number;
  readonly id: ID;
  readonly isBlockedByYou: boolean | null;
  readonly isBlockingYou: boolean | null;
  readonly isFollowedByYou: Tribool | null;
  readonly manuallyApprovesFollowers: boolean;
  readonly remote: boolean;
  readonly userId: UUID;
  readonly " $fragmentType": "FollowButton_user";
};
export type FollowButton_user$key = {
  readonly " $data"?: FollowButton_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"FollowButton_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "FollowButton_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "userId",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countFollowers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "remote",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "manuallyApprovesFollowers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isFollowedByYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockingYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockedByYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "2ad1445305ae740effeed0459e44a55f";

export default node;
