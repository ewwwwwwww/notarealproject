/**
 * @generated SignedSource<<61aa926654bacf769f328c00f3c2ebbf>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type Tribool = "FALSE" | "MAYBE" | "TRUE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type UserFollowButton_user$data = {
  readonly countFollowers: number;
  readonly id: ID;
  readonly isBlockedByYou: boolean | null;
  readonly isBlockingYou: boolean | null;
  readonly isFollowedByYou: Tribool | null;
  readonly manuallyApprovesFollowers: boolean;
  readonly remote: boolean;
  readonly userId: UUID;
  readonly " $fragmentType": "UserFollowButton_user";
};
export type UserFollowButton_user$key = {
  readonly " $data"?: UserFollowButton_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserFollowButton_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserFollowButton_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "userId",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countFollowers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "remote",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "manuallyApprovesFollowers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isFollowedByYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockingYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockedByYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "7804c7a699c47384fc12c2d305fd073a";

export default node;
