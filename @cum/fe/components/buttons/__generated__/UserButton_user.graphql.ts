/**
 * @generated SignedSource<<4eb64651e3565ef29f1ca0b95f9d357f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserButton_user$data = {
  readonly id: ID;
  readonly " $fragmentType": "UserButton_user";
};
export type UserButton_user$key = {
  readonly " $data"?: UserButton_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserButton_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserButton_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "f429b216e694ac542957eb49bbd913e4";

export default node;
