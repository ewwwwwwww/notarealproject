/**
 * @generated SignedSource<<8e507a9d67028ea196759dadd1a258fb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type Tribool = "FALSE" | "MAYBE" | "TRUE" | "%future added value";
export type useFollowMutation$variables = {
  followedId: UUID;
  followerId: UUID;
};
export type useFollowMutation$data = {
  readonly createFollow: {
    readonly followed: {
      readonly countFollowers: number;
      readonly id: ID;
      readonly isFollowedByYou: Tribool | null;
    } | null;
    readonly follower: {
      readonly countFollowing: number;
      readonly id: ID;
    } | null;
  } | null;
};
export type useFollowMutation = {
  response: useFollowMutation$data;
  variables: useFollowMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "followedId"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "followerId"
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "followedId",
                "variableName": "followedId"
              },
              {
                "kind": "Variable",
                "name": "followerId",
                "variableName": "followerId"
              }
            ],
            "kind": "ObjectValue",
            "name": "follow"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "CreateFollowPayload",
    "kind": "LinkedField",
    "name": "createFollow",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "follower",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowing",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "followed",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowers",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "useFollowMutation",
    "selections": (v3/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "useFollowMutation",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "fe5b81ac80d51fce073a85ae4f0481a6",
    "id": null,
    "metadata": {},
    "name": "useFollowMutation",
    "operationKind": "mutation",
    "text": "mutation useFollowMutation(\n  $followerId: UUID!\n  $followedId: UUID!\n) {\n  createFollow(input: {follow: {followerId: $followerId, followedId: $followedId}}) {\n    follower {\n      id\n      countFollowing\n    }\n    followed {\n      id\n      countFollowers\n      isFollowedByYou\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "309a322e5fb9206f8867907c89e8ee9d";

export default node;
