import {
  optimisticFollow,
  optimisticUnfollow,
} from 'components/optimistic/follow.js';
import { isUuid } from 'core/is.js';
import { getState, useMe } from 'hooks/useMe.js';
import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { useFollowDeleteMutation } from './__generated__/useFollowDeleteMutation.graphql.js';
import type { useFollowMutation } from './__generated__/useFollowMutation.graphql.js';
import type { FollowButton_user$data } from './__generated__/FollowButton_user.graphql.js';

const mutation = graphql`
  mutation useFollowMutation($followerId: UUID!, $followedId: UUID!) {
    createFollow(
      input: { follow: { followerId: $followerId, followedId: $followedId } }
    ) {
      follower {
        id
        countFollowing
      }

      followed {
        id
        countFollowers
        isFollowedByYou
      }
    }
  }
`;

const mutationDelete = graphql`
  mutation useFollowDeleteMutation($followerId: UUID!, $followedId: UUID!) {
    deleteFollowByFollowerIdAndFollowedId(
      input: { followerId: $followerId, followedId: $followedId }
    ) {
      follower {
        id
        countFollowing
      }

      followed {
        id
        countFollowers
        isFollowedByYou
      }
    }
  }
`;

type ReturnHook = [(state?: boolean) => void, boolean];

export const useFollow = (followed: FollowButton_user$data): ReturnHook => {
  const [commitMutation, isFollowInFlight] =
    useMutation<useFollowMutation>(mutation);
  const [commitMutationDelete, isDeleteInFlight] =
    useMutation<useFollowDeleteMutation>(mutationDelete);

  const {
    id: followedRelayId,
    userId: followedId,
    countFollowers: followedCount,
    remote,
    manuallyApprovesFollowers,
  } = followed;

  const fn = (state = true) => {
    const me = getState();

    if (!me.isLoggedIn) {
      return console.error('follow mutation missing logged in user');
    }

    const {
      id: followerRelayId,
      userId: followerId,
      countFollowing: followerCount,
    } = me.data;

    if (!isUuid(followerId) || !isUuid(followedId)) {
      return console.error('invalid uuids', followerId, followedId);
    }

    const variables = {
      followerId,
      followedId,
    };

    if (!state) {
      return commitMutationDelete({
        variables,
        optimisticResponse: optimisticUnfollow(
          followerRelayId,
          followerCount,
          followedRelayId,
          followedCount,
        ),
      });
    }

    commitMutation({
      variables,
      optimisticResponse: {
        createFollow: optimisticFollow(
          remote,
          manuallyApprovesFollowers,
          followerRelayId,
          followerCount,
          followedRelayId,
          followedCount,
        ),
      },
    });
  };

  return [fn, isFollowInFlight || isDeleteInFlight];
};
