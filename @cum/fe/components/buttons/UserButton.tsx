import { Fragment } from 'components/core/Fragment.jsx';
import { FC, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { UserButton_user$key } from './__generated__/UserButton_user.graphql.js';

const fragment = graphql`
  fragment UserButton_user on User {
    id
  }
`;

interface Props {
  data: UserButton_user$key;
}

export const UserButton = Fragment<Props>(fragment, (props) => {
  return <div />;
});
