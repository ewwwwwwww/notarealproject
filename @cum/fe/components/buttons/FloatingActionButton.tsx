import { FC, memo } from 'react';
import { ActionIcon, Affix, createStyles, MediaQuery } from '@mantine/core';
import { IoPulseOutline as IconFloatingActionButton } from 'react-icons/io5';
import { useMe } from 'hooks/useMe.js';

const useStyles = createStyles((theme) => ({
  button: {
    // backgroundColor:
    //   theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.white,
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors[theme.primaryColor][5]
        : theme.colors[theme.primaryColor][5],
    boxShadow:
      'rgb(0 0 0 / 20%) 0px 3px 5px -1px, rgb(0 0 0 / 14%) 0px 6px 10px 0px, rgb(0 0 0 / 12%) 0px 1px 18px 0px',
  },
}));

export const FloatingActionButton: FC = memo((props) => {
  const unread = useMe((state) => state.data?.countNotificationsUnread);
  const { classes } = useStyles();

  return (
    <MediaQuery largerThan="lg" styles={{ display: 'none' }}>
      <Affix position={{ right: 32, bottom: 32 }}>
        <ActionIcon
          size={56}
          variant="filled"
          radius={56}
          className={classes.button}
        >
          <IconFloatingActionButton size={24} />
        </ActionIcon>
      </Affix>
    </MediaQuery>
  );
});
