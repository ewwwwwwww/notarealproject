import { Button, ButtonProps, Loader } from '@mantine/core';
import { FC, memo, ReactNode } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { FollowButton_user$key } from './__generated__/FollowButton_user.graphql.js';

import {
  AiOutlineUserAdd as IconFollow,
  AiOutlineUserDelete as IconUnfollow,
  AiOutlineUserSwitch as IconMaybe,
} from 'react-icons/ai';
import { useMe } from 'hooks/useMe.js';
import { useFollow } from './useFollow.js';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment FollowButton_user on User {
    id
    userId
    countFollowers
    remote
    manuallyApprovesFollowers
    isFollowedByYou
    isBlockingYou
    isBlockedByYou
  }
`;

interface Props extends ButtonProps {
  data: FollowButton_user$key;
}

export const FollowButton = Fragment<Props>(fragment, (props) => {
  const { data, ...other } = props;
  const me = useMe((state) => state.data?.userId);
  const [follow, inFlight] = useFollow(data);
  const isYou = data.userId === me;

  let icon: ReactNode;
  let text: string;

  const state = data.isFollowedByYou;

  switch (state) {
    case 'TRUE':
      icon = <IconUnfollow size={20} />;
      text = 'Unfollow';
      break;
    case 'MAYBE':
      icon = <Loader color="white" variant="dots" size="sm" />;
      text = 'Pending';
      break;
    default:
      icon = <IconFollow size={20} />;
      text = 'Follow';
  }

  return (
    <Button
      disabled={
        (data.remote && data.isBlockingYou === true) || isYou || inFlight
      }
      rightIcon={!isYou && icon}
      onClick={() => {
        if (!me) return;

        if (state === 'TRUE') follow(false);
        else if (state === 'MAYBE') follow(false);
        else follow(true);
      }}
      fullWidth
      {...other}
    >
      {isYou ? "it's you!" : text}
    </Button>
  );
});
