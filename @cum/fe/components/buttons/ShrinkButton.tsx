import { FC } from 'react';
import { Button, ButtonProps, MantineSize } from '@mantine/core';
import { isNumber } from 'core/is.js';

interface Props extends ButtonProps {
  smallerThan?: MantineSize | number;
}

export const ShrinkButton: FC<Props> = (props) => {
  const { children, smallerThan, ...other } = props;

  const bp = smallerThan ?? 'xs';

  return (
    <Button
      {...other}
      styles={(theme) => ({
        root: {
          [`@media (max-width: ${
            isNumber(bp) ? bp : theme.breakpoints[bp]
          }px)`]: {
            paddingLeft: 10,
            paddingRight: 0,
          },
        },

        label: {
          [`@media (max-width: ${
            isNumber(bp) ? bp : theme.breakpoints[bp]
          }px)`]: {
            display: 'none',
          },
        },
      })}
    >
      {children}
    </Button>
  );
};
