import { FC, ReactNode } from 'react';
import { Text, Modal } from '@mantine/core';
import { useAuthPrompt } from './useAuth.js';

interface Props {
  children: ReactNode;
}

export const AuthPromptProvider: FC<Props> = (props) => {
  const { children } = props;
  const isOpen = useAuthPrompt((state) => state.opened);
  const close = useAuthPrompt((state) => state.close);

  return (
    <>
      {children}
      <Modal opened={isOpen} onClose={close}>
        <Text>You need to be logged in to do this</Text>
      </Modal>
    </>
  );
};
