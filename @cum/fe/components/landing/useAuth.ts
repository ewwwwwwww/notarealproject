import { useMe } from 'hooks/useMe.js';
import create from 'zustand';

interface AuthPromptState {
  opened: boolean;

  open: () => void;
  close: () => void;
}

export const useAuthPrompt = create<AuthPromptState>()((set) => ({
  opened: false,

  open: () => set({ opened: true }),
  close: () => set({ opened: false }),
}));

export const useAuth = () => {
  const isLoggedIn = useMe((state) => state.isLoggedIn);
  const open = useAuthPrompt((state) => state.open);

  return (fn: (...props: any) => any) =>
    (...props: any) => {
      if (!isLoggedIn) open();

      return fn(...props);
    };
};
