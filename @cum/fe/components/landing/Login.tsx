import type {
  LoginMutation,
  LoginMutation$data,
} from './__generated__/LoginMutation.graphql.js';

import { FC, memo, useState } from 'react';
import {
  Alert,
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Text,
  Group,
  Button,
  Box,
  BoxProps,
} from '@mantine/core';
import { Card } from '../core/Card.jsx';
import { Trans, t } from '@lingui/macro';
import { MdLockOutline as IconLock } from 'react-icons/md';
import { HiAtSymbol as IconAt } from 'react-icons/hi';
import { BsPersonFill as IconName } from 'react-icons/bs';
import { graphql } from 'relay-runtime';
import { useMutation } from 'react-relay';
import { useForm } from '@mantine/form';

const mutation = graphql`
  mutation LoginMutation($name: String!, $pass: String!) {
    userLogin(input: { name: $name, pass: $pass }) {
      clientMutationId
      jwtToken
    }
  }
`;

export const Form: FC = () => {
  const [invalid, setInvalid] = useState(false);
  const [commitMutation, isMutationInFlight] =
    useMutation<LoginMutation>(mutation);

  const form = useForm({
    initialValues: {
      name: '',
      pass: '',
    },
  });

  const onCompleted = (data: LoginMutation$data) => {
    if (data?.userLogin?.jwtToken) {
      window.location.reload();
    } else {
      setInvalid(true);
    }
  };

  const onSubmit = (values: any) => {
    commitMutation({ variables: values, onCompleted });
  };

  return (
    <Card p="xl">
      {invalid && (
        <Alert color="red" variant="filled" mb="lg">
          Login credentials incorrect
        </Alert>
      )}

      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          label={t`Name`}
          placeholder={t`name`}
          icon={<IconName size={18} />}
          variant="filled"
          required
          autoComplete="username"
          {...form.getInputProps('name')}
        />
        <PasswordInput
          label={t`Password`}
          placeholder={t`your password`}
          icon={<IconLock size={18} />}
          required
          variant="filled"
          mt="md"
          autoComplete="current-password"
          {...form.getInputProps('pass')}
        />
        <Group position="apart" mt="md">
          <Checkbox label={t`Remember me`} />
          <Anchor<'a'> href="#" size="sm" onClick={(e) => e.preventDefault()}>
            <Trans>Forgot password?</Trans>
          </Anchor>
        </Group>
        <Button mt="xl" type="submit" fullWidth disabled={isMutationInFlight}>
          Sign In
        </Button>
      </form>
    </Card>
  );
};

interface Props extends BoxProps {}

export const Login: FC<Props> = memo((props) => (
  <Box {...props} sx={{ maxWidth: 400 }}>
    <Form />
    <Text color="dimmed" size="sm" align="center" mt="lg">
      <Trans>Do not have an account yet?</Trans>{' '}
      <Anchor<'a'> href="#" size="sm" onClick={(e) => e.preventDefault()}>
        <Trans>Create account</Trans>
      </Anchor>
    </Text>
  </Box>
));
