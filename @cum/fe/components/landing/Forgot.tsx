import type { FC } from 'react';
import {
  createStyles,
  Paper,
  Title,
  Text,
  TextInput,
  Button,
  Container,
  Group,
  Anchor,
  Center,
  Box,
} from '@mantine/core';

import { Trans } from '@lingui/macro';
import { HiArrowLeft as ArrowLeft } from 'react-icons/hi';

const useHeaderStyles = createStyles((theme) => ({
  title: {
    fontSize: 26,
    fontWeight: 900,
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
  },
}));

export const Header: FC = () => {
  const { classes } = useHeaderStyles();

  return (
    <>
      <Title className={classes.title} align="center">
        <Trans>Forgot your password?</Trans>
      </Title>
      <Text color="dimmed" size="sm" align="center">
        <Trans>Enter your email to get a reset link</Trans>
      </Text>
    </>
  );
};

const useFormStyles = createStyles((theme) => ({
  controls: {
    [theme.fn.smallerThan('xs')]: {
      flexDirection: 'column-reverse',
    },
  },

  control: {
    [theme.fn.smallerThan('xs')]: {
      width: '100%',
      textAlign: 'center',
    },
  },
}));

export const Form: FC = () => {
  const { classes } = useFormStyles();

  return (
    <Paper withBorder shadow="md" p={30} radius="md" mt="xl">
      <TextInput
        label={<Trans>Your email</Trans>}
        placeholder="me@mantine.dev"
        required
      />
      <Group position="apart" mt="lg" className={classes.controls}>
        <Anchor color="dimmed" size="sm" className={classes.control}>
          <Center inline>
            <ArrowLeft size={12} />
            <Box ml={5}>
              <Trans>Back to login page</Trans>
            </Box>
          </Center>
        </Anchor>
        <Button className={classes.control}>
          <Trans>Reset password</Trans>
        </Button>
      </Group>
    </Paper>
  );
};

export const Forgot: FC = () => (
  <Container size={460} my={30}>
    <Header />
    <Form />
  </Container>
);
