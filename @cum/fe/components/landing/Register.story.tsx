import { FC } from 'react';
import { Register as Component } from './Register.jsx';

export const Register: FC = () => <Component />;

export default {
  title: 'Landing',
};
