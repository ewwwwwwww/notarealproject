import type { FC } from 'react';
import { useState } from 'react';

import {
  Card,
  Stepper,
  Group,
  TextInput,
  PasswordInput,
  Text,
  Container,
  Button,
} from '@mantine/core';

import { Trans, t } from '@lingui/macro';
import {
  MdAccountCircle as IconAccount,
  MdLockOutline as IconLock,
} from 'react-icons/md';
import { BiCheckShield as IconTos } from 'react-icons/bi';
import { IoNewspaperOutline as IconCaptcha } from 'react-icons/io5';
import { HiAtSymbol as IconAt } from 'react-icons/hi';
import { BsPersonFill as IconName } from 'react-icons/bs';
import { useForm } from '@mantine/form';
import { graphql } from 'relay-runtime';

// const mutation = graphql`
//
// `

export const Form: FC = () => {
  const [active, setActive] = useState(0);
  const form = useForm({});

  const onSubmit = () => {};

  const nextStep = () =>
    setActive((current) => (current < 3 ? current + 1 : current));
  const prevStep = () =>
    setActive((current) => (current > 0 ? current - 1 : current));

  return (
    <form onSubmit={form.onSubmit(onSubmit)}>
      <Stepper active={active} onStepClick={setActive} breakpoint="sm">
        <Stepper.Step icon={<IconAccount size={18} />}>
          <TextInput
            label={t`Name`}
            placeholder={t`name`}
            icon={<IconName size={18} />}
            required
          />
          <TextInput
            label={t`Email`}
            placeholder={t`jane@example.com`}
            mt="md"
            icon={<IconAt size={18} />}
            required
          />
          <PasswordInput
            label={t`Password`}
            placeholder={t`your password`}
            icon={<IconLock size={18} />}
            mt="md"
            required
          />
          <PasswordInput
            label={t`Confirm password`}
            placeholder={t`your password`}
            mt="md"
            icon={<IconLock size={18} />}
            required
          />
        </Stepper.Step>
        <Stepper.Step icon={<IconCaptcha size={18} />}>
          <Text>Rules!</Text>
        </Stepper.Step>
        <Stepper.Step icon={<IconTos size={18} />}>
          <TextInput label={t`Captcha`} placeholder={t`Enter captcha code`} />
        </Stepper.Step>
        <Stepper.Completed>
          <Text>Done!</Text>
        </Stepper.Completed>
      </Stepper>

      <Group position="center" mt="xl">
        <Button variant="default" onClick={prevStep}>
          <Trans>Back</Trans>
        </Button>
        <Button onClick={nextStep}>
          <Trans>Next step</Trans>
        </Button>
      </Group>
    </form>
  );
};

export const Register: FC = () => (
  <Container size={520} my={40}>
    <Card withBorder shadow="md" p={30} radius="sm">
      <Text align="center" size="lg" weight={500} mb="md">
        <Trans>Register Account</Trans>
      </Text>
      <Form />
    </Card>
  </Container>
);
