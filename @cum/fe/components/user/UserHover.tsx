import { FC, memo, ReactNode, Suspense } from 'react';
import { useLazyLoadQuery } from 'react-relay';
import { graphql } from 'relay-runtime';
import {
  Anchor,
  Avatar,
  Card,
  createStyles,
  Group,
  HoverCard,
  Image,
  Space,
  Stack,
  Text,
} from '@mantine/core';
import type { UserHoverQuery } from './__generated__/UserHoverQuery.graphql.js';
import { UserCardStats } from './UserCardStats.jsx';
import { UserAvatar } from './UserAvatar.jsx';
import { UserName } from './UserName.jsx';
import { FollowButton } from '../buttons/FollowButton.jsx';

const query = graphql`
  query UserHoverQuery($id: ID!) {
    user: userById(id: $id) {
      id

      remote
      nameFull
      name

      ...UserAvatar_user
      ...UserName_user
      ...UserCardStats_user
      ...FollowButton_user
    }
  }
`;

interface Props {
  id: string;
}

const useStyles = createStyles((theme) => ({
  banner: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
  },

  avatar: {
    border: `3px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white
    }`,
  },
}));

const image = 'https://picsum.photos/200/200';

const UserHover: FC<Props> = memo((props) => {
  const { id } = props;
  const { user: data } = useLazyLoadQuery<UserHoverQuery>(query, { id });
  const { classes } = useStyles();

  if (!data) return null;

  const href = `/user/${data.remote ? data.nameFull : data.name}`;

  return (
    <>
      <Card.Section mx={-17} className={classes.banner}>
        <Image src={image} height={140} />
      </Card.Section>
      <UserAvatar
        data={data}
        size={80}
        radius={80}
        mx="auto"
        mt={-40}
        mb="xs"
        className={classes.avatar}
      />
      <UserName data={data} />
      <Space my="xs" />
      <UserCardStats data={data} />
      <FollowButton data={data} mt="lg" />
    </>
  );
});

interface WrapperProps {
  id: string;
  children: ReactNode;
}

export const UserHoverWrapper: FC<WrapperProps> = memo((props) => {
  const { children, id } = props;

  return (
    <HoverCard
      width={300}
      shadow="md"
      position="left-start"
      openDelay={300}
      withinPortal
    >
      <HoverCard.Target>{children}</HoverCard.Target>
      <HoverCard.Dropdown pt={0}>
        <Suspense>
          <UserHover id={id} />
        </Suspense>
      </HoverCard.Dropdown>
    </HoverCard>
  );
});
