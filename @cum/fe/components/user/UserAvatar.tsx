import { Anchor, Avatar, AvatarProps } from '@mantine/core';
import { FC, forwardRef, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { UserAvatar_user$key } from './__generated__/UserAvatar_user.graphql.js';
import { UserHoverWrapper } from './UserHover.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { UserLink } from './UserLink.jsx';
import { getSrcPath } from 'core/paths.js';

const fragment = graphql`
  fragment UserAvatar_user on User {
    id
    ...UserLink
    avatar {
      id
      src
      file {
        id
        blake3
      }
    }
  }
`;

interface Props extends AvatarProps {
  data: UserAvatar_user$key;
  hover?: boolean;
}

export const UserAvatar = Fragment<Props>(
  fragment,
  (props, ref) => {
    const { data, hover, ...others } = props;

    const src = data.avatar?.file?.blake3
      ? getSrcPath(data.avatar?.file?.blake3)
      : data.avatar?.src;
    // const src = 'https://api.lorem.space/image/face?w=300&h=300';

    const avatar = (
      <UserLink data={data}>
        <Avatar src={src} imageProps={{ loading: 'lazy' }} {...others} />
      </UserLink>
    );

    return hover ? (
      <UserHoverWrapper id={data.id}>{avatar}</UserHoverWrapper>
    ) : (
      avatar
    );
  },
  {
    fallback: <Avatar />,
    forwardRef: true,
  },
);
