import type { UserCardStats_user$key } from './__generated__/UserCardStats_user.graphql.js';

import { FC, memo, ReactNode, Suspense } from 'react';
import { graphql } from 'relay-runtime';
import { useFragment } from 'react-relay';
import {
  Text,
  Group,
  Stack,
  MantineColor,
  createStyles,
  Anchor,
} from '@mantine/core';
import { StatButton } from 'components/buttons/StatButton.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserCardStats_user on User {
    countNotes
    countFollowers
    countFollowing
  }
`;

interface Props {
  data: UserCardStats_user$key;
  color?: MantineColor;
  subTextColor?: MantineColor;
  blur?: boolean;
}

const useStyles = createStyles((theme, blur?: boolean) => ({
  root: {},
  button: blur
    ? {
        backdropFilter: 'blur(30px)',
        backgroundColor: 'rgba(0, 0, 0, 0.20)',
        padding: 10,
        borderRadius: 5,
      }
    : {},
}));

export const UserCardStats = Fragment<Props>(fragment, (props) => {
  const { data, color, subTextColor, blur } = props;
  const { classes } = useStyles(blur ?? false);

  return (
    <Group position="center" className={classes.root}>
      <Stack spacing={0}>
        <StatButton
          href=""
          count={data.countFollowers}
          label="Followers"
          color={color}
          subTextColor={subTextColor}
          className={classes.button}
        />
      </Stack>
      <Stack spacing={0}>
        <StatButton
          href=""
          count={data.countNotes}
          label="Notes"
          color={color}
          subTextColor={subTextColor}
          className={classes.button}
        />
      </Stack>
      <Stack spacing={0}>
        <StatButton
          href=""
          count={data.countFollowing}
          label="Following"
          color={color}
          subTextColor={subTextColor}
          className={classes.button}
        />
      </Stack>
    </Group>
  );
});
