import { graphql } from 'relay-runtime';
import { getUserPath } from 'core/paths.js';
import { Anchor, AnchorProps } from '@mantine/core';
import { Fragment } from 'components/core/Fragment.jsx';
import { ReactNode } from 'react';
import { UserLink$key } from './__generated__/UserLink.graphql.js';

const fragment = graphql`
  fragment UserLink on User {
    id
    remote
    name
    nameFull
  }
`;

interface Props extends AnchorProps {
  data: UserLink$key;
  children: ReactNode | ReactNode[];
}

export const UserLink = Fragment<Props>(fragment, (props) => {
  const { data, children, ...other } = props;

  const href = getUserPath(data);

  return (
    <Anchor href={href} {...other}>
      {children}
    </Anchor>
  );
});
