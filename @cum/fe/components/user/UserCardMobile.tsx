import { createStyles, Group, MediaQuery, Overlay, Stack } from '@mantine/core';
import { FC, memo, useEffect, useState } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import { UserAvatar } from './UserAvatar.js';
import { UserCardStats } from './UserCardStats.js';
import { FollowButton } from '../buttons/FollowButton.jsx';
import type { UserCardMobile_user$key } from './__generated__/UserCardMobile_user.graphql.js';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserCardMobile_user on User {
    id
    ...UserAvatar_user
    ...UserCardStats_user
    ...FollowButton_user
  }
`;

interface Props {
  data: UserCardMobile_user$key;
}

const useStyles = createStyles((theme) => ({
  root: {
    position: 'absolute',
    top: 75,
    width: '100%',
    height: 130,
    paddingLeft: 100,
    paddingRight: 100,

    [`@media (min-width: ${theme.breakpoints.md}px)`]: {
      display: 'none',
    },

    [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
      padding: '0 !important',
    },
  },

  actions: {
    height: '100%',
    color: 'white',

    [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
      margin: '0 auto',
    },
  },

  avatar: {
    borderRadius: 10,
    boxShadow:
      'rgb(0 0 0 / 20%) 0px 3px 5px -1px, rgb(0 0 0 / 14%) 0px 6px 10px 0px, rgb(0 0 0 / 12%) 0px 1px 18px 0px',

    border: `2px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white
    }`,

    [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
      borderLeft: 0,
    },
  },
}));

export const UserCardMobile = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  const { classes } = useStyles();

  return (
    <Group position="apart" spacing={0} className={classes.root}>
      <MediaQuery
        smallerThan="sm"
        styles={{
          borderTopLeftRadius: 0,
          borderBottomLeftRadius: 0,
        }}
      >
        <UserAvatar
          data={data}
          radius={0}
          size={130}
          className={classes.avatar}
        />
      </MediaQuery>
      <Stack spacing="xs" justify="center" className={classes.actions}>
        <UserCardStats data={data} color="white" subTextColor="white" blur />
        <FollowButton data={data} />
      </Stack>
    </Group>
  );
});
