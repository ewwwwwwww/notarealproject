import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserProfile as Comp } from './UserProfile.jsx';
import type { UserProfileMockQuery } from './__generated__/UserProfileMockQuery.graphql.js';

const query = graphql`
  query UserProfileMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserProfile_user
    }
  }
`;

export const UserProfile: FC = () => {
  const data = useLazyLoadQuery<UserProfileMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'User',
};
