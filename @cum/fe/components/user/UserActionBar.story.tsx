import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserActionBar as Comp } from './UserActionBar.jsx';
import type { UserActionBarMockQuery } from './__generated__/UserActionBarMockQuery.graphql.js';

const query = graphql`
  query UserActionBarMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserActionBar_user
    }
  }
`;

export const UserActionBar: FC = () => {
  const data = useLazyLoadQuery<UserActionBarMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'User',
};
