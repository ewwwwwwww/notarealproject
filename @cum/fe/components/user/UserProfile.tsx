import type { UserProfile_user$key } from './__generated__/UserProfile_user.graphql.js';

import { FC, memo } from 'react';
import { Trans } from '@lingui/macro';
import { graphql, useFragment } from 'react-relay';

import { Html } from '../core/Html.jsx';
import { Card } from '../core/Card.jsx';

import {
  createStyles,
  Text,
  SimpleGrid,
  Divider,
  Badge,
  Spoiler,
  Box,
  Image,
  MediaQuery,
  Center,
} from '@mantine/core';

import { UserActionBar } from './UserActionBar.jsx';
import { UserCardMobile } from './UserCardMobile.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { UserBanner } from './UserBanner.jsx';

const fragment = graphql`
  fragment UserProfile_user on User {
    caption
    body
    fields {
      nodes {
        body
        label
      }
    }
    ...UserCardMobile_user
    ...UserActionBar_user
    ...UserBanner_user
  }
`;

interface Props {
  data: UserProfile_user$key;
}

// TODO: fix these
const tempProps = {
  permission: 'admin',
};

const useStyles = createStyles((theme) => ({
  permission: {
    position: 'absolute',
    bottom: 5,
    left: 5,
    opacity: 0.8,
  },

  caption: {
    position: 'absolute',
    bottom: 5,
    right: 5,
    opacity: 0.8,
  },
}));

export const UserProfile = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  const { permission } = tempProps;
  const { classes, cx } = useStyles();

  const fieldItems = (data.fields.nodes ?? []).map(({ label, body }, idx) => (
    <div key={idx}>
      <Text color="dimmed" size="xs">
        {label}
      </Text>
      <Html content={body} />
    </div>
  ));

  return (
    <MediaQuery
      smallerThan="md"
      styles={{
        boxShadow: 'none',
      }}
    >
      <Card pt={0}>
        <UserBanner data={data}>
          <UserCardMobile data={data} />
          {permission && (
            <Badge
              variant="filled"
              size="md"
              radius="sm"
              color="dark"
              className={classes.permission}
            >
              {permission}
            </Badge>
          )}

          {data.caption && (
            <Badge
              variant="filled"
              size="md"
              radius="sm"
              color="dark"
              className={classes.caption}
            >
              {data.caption}
            </Badge>
          )}
        </UserBanner>

        <UserActionBar data={data} />

        {data.body && data.body.length > 0 && (
          <>
            <Divider my="lg" variant="dotted" />
            <Text mt="lg" color="dimmed" size="xs">
              <Trans>Description</Trans>
            </Text>
            <Text>
              <Spoiler maxHeight={1000} showLabel="Show more" hideLabel="Hide">
                <Html content={data.body} />
              </Spoiler>
            </Text>
          </>
        )}

        {fieldItems.length > 0 && (
          <>
            <Divider my="lg" variant="dotted" />
            <SimpleGrid cols={2}>{fieldItems}</SimpleGrid>
          </>
        )}
      </Card>
    </MediaQuery>
  );
});
