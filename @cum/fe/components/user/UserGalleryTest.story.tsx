import { FC } from 'react';
import { UserGalleryTest as Comp } from './UserGalleryTest.jsx';

export const UserGalleryTest: FC = () => <Comp />;

export default {
  title: 'User',
};
