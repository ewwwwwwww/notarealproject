import {
  createStyles,
  Stack,
  SimpleGrid,
  Text,
  Box,
  Overlay,
  Image,
} from '@mantine/core';
import { Trans } from '@lingui/macro';
import { Card } from '../core/Card.js';
import { memo } from 'react';

export interface Props {}

const props: { href: string }[] = [
  { href: 'https://picsum.photos/200/300/?1' },
  { href: 'https://picsum.photos/200/300/?2' },
  { href: 'https://picsum.photos/200/300/?3' },
  { href: 'https://picsum.photos/200/300/?4' },
  { href: 'https://picsum.photos/200/300/?5' },
  { href: 'https://picsum.photos/200/300/?6' },
  { href: 'https://picsum.photos/200/300/?7' },
  { href: 'https://picsum.photos/200/300/?8' },
];

const useStyles = createStyles((theme) => ({
  img: {
    objectFit: 'cover',
    width: '100%',
    height: 250,
  },
}));

const UserGalleryTest = memo(() => {
  const { classes: cl } = useStyles();
  const center = {
    zIndex: 10,
    margin: 'auto',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  };

  const images = props.map((image, idx) => {
    if (idx === props.length - 1) {
      // TODO: fix centering
      return (
        <Box key={idx} sx={{ position: 'relative' }}>
          <Text sx={{ ...center, position: 'absolute' }}>View More...</Text>
          <Overlay opacity={0.7} blur={1} zIndex={2} />
          <Image
            src={image.href}
            width={200}
            height={200}
            alt=""
            className={cl.img}
            imageProps={{ loading: 'lazy' }}
          />
        </Box>
      );
    } else {
      return (
        <Image
          key={idx}
          src={image.href}
          width={200}
          height={200}
          alt=""
          className={cl.img}
        />
      );
    }
  });

  return (
    <Card>
      <Card.Section>
        <SimpleGrid cols={4} spacing={2}>
          {images}
        </SimpleGrid>
      </Card.Section>
    </Card>
  );
});
