import { Anchor, Text } from '@mantine/core';
import { Fragment } from 'components/core/Fragment.jsx';
import { cacheUserId } from 'core/cacheUserId.js';
import { FC, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { UserName_user$key } from './__generated__/UserName_user.graphql.js';

const fragment = graphql`
  fragment UserName_user on User {
    id
    nick
    name
    nameFull
    remote
  }
`;

interface Props {
  data: UserName_user$key;
  className?: string;
  classNameFull?: string;
  unstyled?: boolean;
  compact?: boolean;
}

export const UserName = Fragment<Props>(fragment, (props) => {
  const { data, className, classNameFull, unstyled, compact } = props;

  cacheUserId(data.remote ? data.nameFull : data.name, data.id);

  if (compact) {
    return (
      <>
        <Text component="span" className={className}>
          {data.nick ?? data.name}
        </Text>
      </>
    );
  } else if (unstyled) {
    return (
      <>
        <Text className={className}>{data.nick ?? data.name}</Text>
        <Text color="dimmed" className={classNameFull}>
          {data.nameFull}
        </Text>
      </>
    );
  } else {
    return (
      <>
        <Text
          align="center"
          size="lg"
          weight={500}
          mt="sm"
          className={className}
        >
          {data.nick ?? data.name}
        </Text>
        <Text align="center" size="sm" color="dimmed" className={classNameFull}>
          {data.nameFull}
        </Text>
      </>
    );
  }
});
