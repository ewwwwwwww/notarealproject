import type { UserCard_user$key } from './__generated__/UserCard_user.graphql.js';
import { graphql } from 'relay-runtime';
import { Card } from '../core/Card.jsx';
import { FollowButton } from '../buttons/FollowButton.jsx';
import { UserCardStats } from './UserCardStats.jsx';
import { UserAvatar } from './UserAvatar.jsx';
import { Divider, Space, Text } from '@mantine/core';
import { UserName } from './UserName.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserCard_user on User {
    id

    ...UserAvatar_user
    ...UserName_user
    ...UserCardStats_user
    ...FollowButton_user
  }
`;

interface Props {
  data: UserCard_user$key;
}

export const UserCard = Fragment<Props>(fragment, (props) => {
  const { data } = props;

  return (
    <Card>
      <Card.Section>
        <UserAvatar
          data={data}
          sx={{ width: '100%', height: 300, borderRadius: 0 }}
        />
      </Card.Section>
      <UserName data={data} />
      <Space my="xs" />
      <UserCardStats data={data} />
      <Divider mb="lg" mt="sm" variant="dotted" />
      <FollowButton data={data} />
    </Card>
  );
});
