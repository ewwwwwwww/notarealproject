import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserAvatar as Comp } from './UserAvatar.jsx';
import type { UserAvatarMockQuery } from './__generated__/UserAvatarMockQuery.graphql.js';

const query = graphql`
  query UserAvatarMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserAvatar_user
    }
  }
`;

export const UserAvatar: FC = () => {
  const data = useLazyLoadQuery<UserAvatarMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'User',
};
