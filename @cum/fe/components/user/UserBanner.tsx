import { createStyles } from '@mantine/styles';
import { Card } from 'components/core/Card.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { getSrcPath } from 'core/paths.js';
import { graphql } from 'relay-runtime';

const fragment = graphql`
  fragment UserBanner_user on User {
    id
    banner {
      id
      src
      file {
        id
        blake3
      }
    }
  }
`;

const useStyles = createStyles((theme, { banner }: { banner: string }) => ({
  banner: {
    position: 'relative',
    backgroundImage: `url("${banner}")`,
    backgroundSize: 'cover',
    height: 300,
  },
}));

// TODO: fix these
const tempProps = {
  permission: 'admin',
};

export const UserBanner = Fragment<any>(fragment, (props) => {
  const { data, children } = props;
  const src = data.banner?.file?.blake3
    ? getSrcPath(data.banner?.file?.blake3)
    : data.banner?.src;
  // const src = 'https://picsum.photos/800/300?random=1';

  const { classes } = useStyles({ banner: src });

  return <Card.Section className={classes.banner}>{children}</Card.Section>;
});
