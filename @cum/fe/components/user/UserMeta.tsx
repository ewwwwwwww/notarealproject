import type { UserMeta_user$key } from './__generated__/UserMeta_user.graphql.js';

import { FC, memo } from 'react';
import { Grid } from '@mantine/core';
import { Card } from 'components/core/Card.jsx';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import { ShortDate } from 'components/core/ShortDate.jsx';
import { StatButton } from 'components/buttons/StatButton.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserMeta_user on User {
    id
    ctime
    countBlockers
    countBlocking
    countNotes
  }
`;

interface Props {
  data: UserMeta_user$key;
}

export const UserMeta = Fragment<Props>(fragment, (props) => {
  const { data } = props;

  return (
    <Card>
      <Grid>
        <Grid.Col span={6}>
          <StatButton count={data.countNotes} label={'notes per day'} />
        </Grid.Col>
        <Grid.Col span={6}>
          <StatButton date={new Date(data.ctime)} label={'date registered'} />
        </Grid.Col>
        <Grid.Col span={6}>
          <StatButton count={data.countBlocking} label={'blocks'} />
        </Grid.Col>
        <Grid.Col span={6}>
          <StatButton count={data.countBlockers} label={'blockers'} />
        </Grid.Col>
      </Grid>
    </Card>
  );
});
