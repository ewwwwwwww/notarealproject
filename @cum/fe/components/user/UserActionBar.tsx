import type { UserActionBar_user$key } from './__generated__/UserActionBar_user.graphql.js';

import { FC, forwardRef, memo } from 'react';
import { Trans, t } from '@lingui/macro';
import { graphql, useFragment } from 'react-relay';

import { Button, Group, ActionIcon, Tooltip } from '@mantine/core';

import { ImBlocked as IconBlock } from 'react-icons/im';
import { BiVolumeMute as IconMute, BiBell as IconBell } from 'react-icons/bi';
import { FaPencilAlt as IconMention } from 'react-icons/fa';
import { HiOutlineDotsVertical as IconSettings } from 'react-icons/hi';
import { ShrinkButton } from 'components/buttons/ShrinkButton.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserActionBar_user on User {
    id
    isBlockedByYou
  }
`;

interface Props {
  data: UserActionBar_user$key;
}

const Subscribe = forwardRef<HTMLDivElement>((props, ref) => (
  <div ref={ref} {...props}>
    <IconBell size={24} />
  </div>
));

const Menu = forwardRef<HTMLDivElement>((props, ref) => (
  <div ref={ref} {...props}>
    <IconSettings size={24} />
  </div>
));

export const UserActionBar = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  const bp = 450;

  return (
    <Group mt="md">
      <ActionIcon variant="light" size={36}>
        <Tooltip label={t`Subscribe`} position="right" transition="slide-right">
          <Subscribe />
        </Tooltip>
      </ActionIcon>
      <Group position="center" style={{ flexGrow: 1 }}>
        <ShrinkButton
          smallerThan={bp}
          variant="light"
          leftIcon={<IconBlock />}
          color="pink"
        >
          {data.isBlockedByYou === true ? (
            <Trans>Unblock</Trans>
          ) : (
            <Trans>Block</Trans>
          )}
        </ShrinkButton>
        <ShrinkButton
          smallerThan={bp}
          variant="light"
          leftIcon={<IconMute />}
          color="violet"
        >
          <Trans>Mute</Trans>
        </ShrinkButton>
        <ShrinkButton
          smallerThan={bp}
          variant="light"
          leftIcon={<IconMention />}
          color="blue"
        >
          <Trans>Message</Trans>
        </ShrinkButton>
      </Group>
      <ActionIcon variant="light" size={36}>
        <Tooltip label={t`Settings`} position="left" transition="slide-left">
          <Menu />
        </Tooltip>
      </ActionIcon>
    </Group>
  );
});
