import type { UserGallery_user$key } from './__generated__/UserGallery_user.graphql.js';

import { FC } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment UserGallery_user on User {
    id
    notes(first: 8) {
      nodes {
        id
        noteId

        noteAttachments(first: 1) {
          nodes {
            id
            attachmentId
          }
        }
      }
    }
  }
`;

interface Props {
  data: UserGallery_user$key;
}

export const UserGallery = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  return <div />;
});
