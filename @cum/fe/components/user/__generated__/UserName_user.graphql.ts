/**
 * @generated SignedSource<<80e779cb7f16f4840bfb5ebbf5c7ed4e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserName_user$data = {
  readonly id: ID;
  readonly name: string;
  readonly nameFull: string;
  readonly nick: string | null;
  readonly remote: boolean;
  readonly " $fragmentType": "UserName_user";
};
export type UserName_user$key = {
  readonly " $data"?: UserName_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserName_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserName_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nick",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nameFull",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "remote",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "4b55f59d815aef37810ca098d27247ea";

export default node;
