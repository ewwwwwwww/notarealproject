/**
 * @generated SignedSource<<e489cd5c599a8dbea561c749147e9935>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserAvatar_user$data = {
  readonly avatar: {
    readonly file: {
      readonly blake3: string;
      readonly id: ID;
    } | null;
    readonly id: ID;
    readonly src: string | null;
  } | null;
  readonly id: ID;
  readonly " $fragmentSpreads": FragmentRefs<"UserLink">;
  readonly " $fragmentType": "UserAvatar_user";
};
export type UserAvatar_user$key = {
  readonly " $data"?: UserAvatar_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserAvatar_user",
  "selections": [
    (v0/*: any*/),
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserLink"
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "UserAttachment",
      "kind": "LinkedField",
      "name": "avatar",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "src",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "File",
          "kind": "LinkedField",
          "name": "file",
          "plural": false,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "blake3",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "717cc6bda8f3c61bd14a2de1c1437bd5";

export default node;
