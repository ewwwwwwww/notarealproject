/**
 * @generated SignedSource<<c8e4b23b40cd2d552a640cc25dd2a541>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserBanner_user$data = {
  readonly banner: {
    readonly file: {
      readonly blake3: string;
      readonly id: ID;
    } | null;
    readonly id: ID;
    readonly src: string | null;
  } | null;
  readonly id: ID;
  readonly " $fragmentType": "UserBanner_user";
};
export type UserBanner_user$key = {
  readonly " $data"?: UserBanner_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserBanner_user">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserBanner_user",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "UserAttachment",
      "kind": "LinkedField",
      "name": "banner",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "src",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "File",
          "kind": "LinkedField",
          "name": "file",
          "plural": false,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "blake3",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "dc8ea4c673904d7d0997b723192c8fa5";

export default node;
