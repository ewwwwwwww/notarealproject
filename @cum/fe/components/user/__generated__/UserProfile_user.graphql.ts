/**
 * @generated SignedSource<<bd8e2ead4362f9154fc91763f1e78e55>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserProfile_user$data = {
  readonly body: string | null;
  readonly caption: string | null;
  readonly fields: {
    readonly nodes: ReadonlyArray<{
      readonly body: string;
      readonly label: string;
    }>;
  };
  readonly " $fragmentSpreads": FragmentRefs<"UserActionBar_user" | "UserBanner_user" | "UserCardMobile_user">;
  readonly " $fragmentType": "UserProfile_user";
};
export type UserProfile_user$key = {
  readonly " $data"?: UserProfile_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserProfile_user">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserProfile_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "caption",
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "FieldsConnection",
      "kind": "LinkedField",
      "name": "fields",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Field",
          "kind": "LinkedField",
          "name": "nodes",
          "plural": true,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "label",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserCardMobile_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserActionBar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserBanner_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "f44e9282dd1ef1b69ba36d358629b0fd";

export default node;
