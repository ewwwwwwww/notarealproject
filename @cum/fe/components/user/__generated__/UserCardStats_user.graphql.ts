/**
 * @generated SignedSource<<399845094df2a22f3a784633486b7c76>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCardStats_user$data = {
  readonly countFollowers: number;
  readonly countFollowing: number;
  readonly countNotes: number;
  readonly " $fragmentType": "UserCardStats_user";
};
export type UserCardStats_user$key = {
  readonly " $data"?: UserCardStats_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserCardStats_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserCardStats_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countNotes",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countFollowers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countFollowing",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "ca5aec81ae559aee3984b4ea9495ea3f";

export default node;
