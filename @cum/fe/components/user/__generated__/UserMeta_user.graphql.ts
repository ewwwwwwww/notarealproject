/**
 * @generated SignedSource<<7069809f325fa5af6eac31fddeee1262>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserMeta_user$data = {
  readonly countBlockers: number;
  readonly countBlocking: number;
  readonly countNotes: number;
  readonly ctime: Datetime;
  readonly id: ID;
  readonly " $fragmentType": "UserMeta_user";
};
export type UserMeta_user$key = {
  readonly " $data"?: UserMeta_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserMeta_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserMeta_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ctime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countBlockers",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countBlocking",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countNotes",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "9abb667a49a1c91fd0149f8b830d6e36";

export default node;
