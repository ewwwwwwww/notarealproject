/**
 * @generated SignedSource<<9dc6d682c253dbdb87abd0eae727ddb8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserLink$data = {
  readonly id: ID;
  readonly name: string;
  readonly nameFull: string;
  readonly remote: boolean;
  readonly " $fragmentType": "UserLink";
};
export type UserLink$key = {
  readonly " $data"?: UserLink$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserLink">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserLink",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "remote",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nameFull",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "06cffb02787ddb091b1597b7ba03aa67";

export default node;
