/**
 * @generated SignedSource<<7193ca8ebe10ae55876a302ef10a4a7f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserProfileMockQuery$variables = {
  userId: UUID;
};
export type UserProfileMockQuery$data = {
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserProfile_user">;
  } | null;
};
export type UserProfileMockQuery = {
  response: UserProfileMockQuery$data;
  variables: UserProfileMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v4 = [
  (v2/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "src",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "concreteType": "File",
    "kind": "LinkedField",
    "name": "file",
    "plural": false,
    "selections": [
      (v2/*: any*/),
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "blake3",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UserProfileMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserProfile_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UserProfileMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "caption",
            "storageKey": null
          },
          (v3/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "FieldsConnection",
            "kind": "LinkedField",
            "name": "fields",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Field",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "label",
                    "storageKey": null
                  },
                  (v2/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "remote",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "nameFull",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserAttachment",
            "kind": "LinkedField",
            "name": "avatar",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countNotes",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowers",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowing",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "userId",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "manuallyApprovesFollowers",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserAttachment",
            "kind": "LinkedField",
            "name": "banner",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "c008f06748026029d33d216d13f7aedb",
    "id": null,
    "metadata": {},
    "name": "UserProfileMockQuery",
    "operationKind": "query",
    "text": "query UserProfileMockQuery(\n  $userId: UUID!\n) {\n  user(userId: $userId) {\n    id\n    ...UserProfile_user\n  }\n}\n\nfragment FollowButton_user on User {\n  id\n  userId\n  countFollowers\n  remote\n  manuallyApprovesFollowers\n  isFollowedByYou\n  isBlockingYou\n  isBlockedByYou\n}\n\nfragment UserActionBar_user on User {\n  id\n  isBlockedByYou\n}\n\nfragment UserAvatar_user on User {\n  id\n  ...UserLink\n  avatar {\n    id\n    src\n    file {\n      id\n      blake3\n    }\n  }\n}\n\nfragment UserBanner_user on User {\n  id\n  banner {\n    id\n    src\n    file {\n      id\n      blake3\n    }\n  }\n}\n\nfragment UserCardMobile_user on User {\n  id\n  ...UserAvatar_user\n  ...UserCardStats_user\n  ...FollowButton_user\n}\n\nfragment UserCardStats_user on User {\n  countNotes\n  countFollowers\n  countFollowing\n}\n\nfragment UserLink on User {\n  id\n  remote\n  name\n  nameFull\n}\n\nfragment UserProfile_user on User {\n  caption\n  body\n  fields {\n    nodes {\n      body\n      label\n      id\n    }\n  }\n  ...UserCardMobile_user\n  ...UserActionBar_user\n  ...UserBanner_user\n}\n"
  }
};
})();

(node as any).hash = "89257f638e08220f583fafdf7561547b";

export default node;
