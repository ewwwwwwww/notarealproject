/**
 * @generated SignedSource<<bbd6c91c34cb52d319183c1284f81afc>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCard_user$data = {
  readonly id: ID;
  readonly " $fragmentSpreads": FragmentRefs<"FollowButton_user" | "UserAvatar_user" | "UserCardStats_user" | "UserName_user">;
  readonly " $fragmentType": "UserCard_user";
};
export type UserCard_user$key = {
  readonly " $data"?: UserCard_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserCard_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserCard_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserName_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserCardStats_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "FollowButton_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "9246aed26b2add32db2dbc8579ad0dd2";

export default node;
