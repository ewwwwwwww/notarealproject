/**
 * @generated SignedSource<<b0cdbde151efc1bf385a22f1179fe6c3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserActionBarMockQuery$variables = {
  userId: UUID;
};
export type UserActionBarMockQuery$data = {
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserActionBar_user">;
  } | null;
};
export type UserActionBarMockQuery = {
  response: UserActionBarMockQuery$data;
  variables: UserActionBarMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UserActionBarMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserActionBar_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UserActionBarMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "590490e7e64d43247589a6f51ba00bbd",
    "id": null,
    "metadata": {},
    "name": "UserActionBarMockQuery",
    "operationKind": "query",
    "text": "query UserActionBarMockQuery(\n  $userId: UUID!\n) {\n  user(userId: $userId) {\n    id\n    ...UserActionBar_user\n  }\n}\n\nfragment UserActionBar_user on User {\n  id\n  isBlockedByYou\n}\n"
  }
};
})();

(node as any).hash = "77855b22c56fbe0a43dba6cfb36db4ec";

export default node;
