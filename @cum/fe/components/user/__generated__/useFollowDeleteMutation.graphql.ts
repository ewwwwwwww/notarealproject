/**
 * @generated SignedSource<<e3cf3cd487af1d61c382b32e9838b465>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type Tribool = "FALSE" | "MAYBE" | "TRUE" | "%future added value";
export type useFollowDeleteMutation$variables = {
  followedId: UUID;
  followerId: UUID;
};
export type useFollowDeleteMutation$data = {
  readonly deleteFollowByFollowerIdAndFollowedId: {
    readonly followed: {
      readonly countFollowers: number;
      readonly id: ID;
      readonly isFollowedByYou: Tribool | null;
    } | null;
    readonly follower: {
      readonly countFollowing: number;
      readonly id: ID;
    } | null;
  } | null;
};
export type useFollowDeleteMutation = {
  response: useFollowDeleteMutation$data;
  variables: useFollowDeleteMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "followedId"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "followerId"
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "followedId",
            "variableName": "followedId"
          },
          {
            "kind": "Variable",
            "name": "followerId",
            "variableName": "followerId"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "DeleteFollowPayload",
    "kind": "LinkedField",
    "name": "deleteFollowByFollowerIdAndFollowedId",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "follower",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowing",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "followed",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countFollowers",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "useFollowDeleteMutation",
    "selections": (v3/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "useFollowDeleteMutation",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "02e3bb6994ada8fd24c97d8f608318f7",
    "id": null,
    "metadata": {},
    "name": "useFollowDeleteMutation",
    "operationKind": "mutation",
    "text": "mutation useFollowDeleteMutation(\n  $followerId: UUID!\n  $followedId: UUID!\n) {\n  deleteFollowByFollowerIdAndFollowedId(input: {followerId: $followerId, followedId: $followedId}) {\n    follower {\n      id\n      countFollowing\n    }\n    followed {\n      id\n      countFollowers\n      isFollowedByYou\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "11b4e77c04ce256b9d8dc612d170615b";

export default node;
