/**
 * @generated SignedSource<<f0a68eaaa08ce87e88401c783de4e371>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserGallery_user$data = {
  readonly id: ID;
  readonly notes: {
    readonly nodes: ReadonlyArray<{
      readonly id: ID;
      readonly noteAttachments: {
        readonly nodes: ReadonlyArray<{
          readonly attachmentId: UUID;
          readonly id: ID;
        }>;
      };
      readonly noteId: UUID;
    }>;
  };
  readonly " $fragmentType": "UserGallery_user";
};
export type UserGallery_user$key = {
  readonly " $data"?: UserGallery_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserGallery_user">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserGallery_user",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "first",
          "value": 8
        }
      ],
      "concreteType": "NotesConnection",
      "kind": "LinkedField",
      "name": "notes",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Note",
          "kind": "LinkedField",
          "name": "nodes",
          "plural": true,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "noteId",
              "storageKey": null
            },
            {
              "alias": null,
              "args": [
                {
                  "kind": "Literal",
                  "name": "first",
                  "value": 1
                }
              ],
              "concreteType": "NoteAttachmentsConnection",
              "kind": "LinkedField",
              "name": "noteAttachments",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "NoteAttachment",
                  "kind": "LinkedField",
                  "name": "nodes",
                  "plural": true,
                  "selections": [
                    (v0/*: any*/),
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "attachmentId",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                }
              ],
              "storageKey": "noteAttachments(first:1)"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "notes(first:8)"
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "653763bb0bb9a19ed06168689e4ea88b";

export default node;
