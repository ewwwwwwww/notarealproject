/**
 * @generated SignedSource<<0c948edcf86cdf305b8c8ebbc7e32e9d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserActionBar_user$data = {
  readonly id: ID;
  readonly isBlockedByYou: boolean | null;
  readonly " $fragmentType": "UserActionBar_user";
};
export type UserActionBar_user$key = {
  readonly " $data"?: UserActionBar_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserActionBar_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserActionBar_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockedByYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "3070db57f0ab521c184317c459a593db";

export default node;
