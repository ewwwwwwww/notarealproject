/**
 * @generated SignedSource<<66085e300f5f8ac10c828f13842ae231>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCardMobile_user$data = {
  readonly id: ID;
  readonly " $fragmentSpreads": FragmentRefs<"FollowButton_user" | "UserAvatar_user" | "UserCardStats_user">;
  readonly " $fragmentType": "UserCardMobile_user";
};
export type UserCardMobile_user$key = {
  readonly " $data"?: UserCardMobile_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserCardMobile_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserCardMobile_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserCardStats_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "FollowButton_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "5973656e510c1a75f07920f1185a9441";

export default node;
