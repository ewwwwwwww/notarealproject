import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserCard as Comp } from './UserCard.js';
import type { UserCardMockQuery } from './__generated__/UserCardMockQuery.graphql.js';

const query = graphql`
  query UserCardMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserCard_user
    }
  }
`;

export const Loaded: FC = () => {
  const data = useLazyLoadQuery<UserCardMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

// export const Placeholder: FC = () => <Comp user={null} />;

export default {
  title: 'User / Card',
};
