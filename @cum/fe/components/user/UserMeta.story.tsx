import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserMeta as Comp } from './UserMeta.jsx';
import type { UserMetaMockQuery } from './__generated__/UserMetaMockQuery.graphql.js';

const query = graphql`
  query UserMetaMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserMeta_user
    }
  }
`;

export const UserMeta: FC = () => {
  const data = useLazyLoadQuery<UserMetaMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'User',
};
