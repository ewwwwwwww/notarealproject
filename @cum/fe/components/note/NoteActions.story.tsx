import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { NoteActions as Comp } from './NoteActions.jsx';
import type { NoteActionsMockQuery } from './__generated__/NoteActionsMockQuery.graphql.js';

const query = graphql`
  query NoteActionsMockQuery($noteId: UUID!) {
    note(noteId: $noteId) {
      id
      ...NoteActions_note
    }
  }
`;

export const NoteActions: FC = () => {
  const data = useLazyLoadQuery<NoteActionsMockQuery>(query, {
    noteId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.note) return <div>oops</div>;

  return <Comp note={data.note} />;
};

export default {
  title: 'Note',
};
