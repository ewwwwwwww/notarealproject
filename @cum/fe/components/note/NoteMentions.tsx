import { FC, Suspense } from 'react';
import { Badge } from '@mantine/core';
import { UserAvatar } from 'components/user/UserAvatar.jsx';
import { usePaginationFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { NoteMentions_note$key } from './__generated__/NoteMentions_note.graphql.js';

const fragment = graphql`
  fragment NoteMentions_note on Note
  @refetchable(queryName: "NoteMentionsQuery")
  @argumentDefinitions(
    mentionCount: { type: "Int", defaultValue: 10 }
    mentionCursor: { type: "Cursor" }
  ) {
    replyId
    mentions(first: $mentionCount, after: $mentionCursor)
      @connection(key: "NoteMentions_user_mentions") {
      edges {
        node {
          id
          user {
            id
            name
            ...UserAvatar_user
          }
        }
      }
    }
  }
`;

interface Props {
  note: NoteMentions_note$key;
}

export const NoteMentions: FC<Props> = (props) => {
  const { data, hasNext, loadNext } = usePaginationFragment(
    fragment,
    props.note,
  );

  const mentions = data.mentions.edges.map((edge) => {
    if (!edge.node.user) return null;

    const avatar = <UserAvatar data={edge.node.user} />;

    return <Badge leftSection={avatar}>{edge.node.user?.name}</Badge>;
  });

  return <div>{mentions}</div>;
};

// export const NoteMentions: FC<Props> = memo((props) => {
//   const data = useFragment(fragment, props.note);
//
//   if (!data?.mentions?.totalCount || !data?.mentions?.nodes?.length) {
//     return null;
//   }
//
//   const mentions = data.mentions.nodes.map(({ user }, idx) => {
//     if (!user) return;
//
//     // FIXME: button cannot appear as descendant of button
//     const avatar = <UserAvatar size={24} mr={5} user={user} />;
//
//     return (
//       <Badge
//         key={idx}
//         sx={{ paddingLeft: 0 }}
//         size="md"
//         radius="sm"
//         variant="light"
//         leftSection={avatar}
//       >
//         {user.name}
//       </Badge>
//     );
//   });
//
//   return (
//     <Spoiler
//       maxHeight={92}
//       showLabel={<IconMore size={12} />}
//       hideLabel={<IconLess size={12} />}
//     >
//       <Group spacing="xs">
//         <ActionIcon variant="subtle" size="sm">
//           <IconMention size={18} />
//         </ActionIcon>
//         {mentions}
//       </Group>
//     </Spoiler>
//   );
// })
