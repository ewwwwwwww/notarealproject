import type { NoteActions_note$key } from './__generated__/NoteActions_note.graphql.js';

import { FC, forwardRef, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import { useNoteBookmark } from './useNoteBookmark.jsx';
import { useNoteLike } from './useNoteLike.jsx';
import { useNoteRepeat } from './useNoteRepeat.jsx';
import { useNoteReact } from './useNoteReact.jsx';
import { useEditor } from '../editor/useEditor.js';
import { EmojiPopover } from '../emoji/EmojiPopover.jsx';
import { NoteActionMenu } from './NoteActionMenu.jsx';

import { emojis } from '@cum/fixtures/emojis';
import { useAuth } from 'components/landing/useAuth.js';
import {
  ActionIcon,
  ActionIconProps,
  Box,
  createStyles,
  Group,
  GroupProps,
  MediaQuery,
} from '@mantine/core';
import * as Icon from './NoteActionIcon.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment NoteActions_note on Note {
    id
    noteId
    countReplies
    countLikes
    countBookmarks
    countReacts
    countRepeats
    isLikedByYou
    isBookmarkedByYou
    isRepeatedByYou
  }
`;

interface ActionProps extends ActionIconProps {
  onClick?: () => any;
  count?: number;
  active?: boolean | null;
}

// TODO: active colors
const useButtonStyles = createStyles(
  (theme, { active, color }: { active: boolean; color?: string | null }) => {
    const { colorScheme: cs } = theme;
    const actualColor = color
      ? theme.colors[color]
      : theme.colors[theme.primaryColor];

    return {
      button: {
        backgroundColor:
          cs === 'dark'
            ? theme.colors.dark[7]
            : active
            ? actualColor[6]
            : theme.white,
        borderColor:
          cs === 'dark'
            ? theme.colors.dark[4]
            : active
            ? actualColor[6]
            : theme.colors.gray[4],
        color:
          cs === 'dark'
            ? theme.colors.dark[1]
            : // : active ? theme.white : colors.dark[5],
            active
            ? theme.white
            : actualColor[5],

        '&:hover': {
          // color: cs === 'dark' ? theme.colors.dark[1] : theme.white,
          borderColor: actualColor[6],
        },
      },
    };
  },
);

const Action: FC<ActionProps> = memo(
  forwardRef((props, ref: any) => {
    const { children, count, onClick, active, color, className, ...other } =
      props;
    const auth = useAuth();
    const { classes, cx } = useButtonStyles({ active: active ?? false, color });

    const handleClick = onClick
      ? auth((event: MouseEvent) => {
          event.stopPropagation();
          onClick();
        })
      : auth(() => {});

    return (
      <MediaQuery
        smallerThan="sm"
        styles={{
          borderRadius: 4,
        }}
      >
        <ActionIcon
          ref={ref}
          {...other}
          onClick={handleClick}
          className={cx(classes.button, className)}
          radius="xl"
          size="lg"
        >
          {children}
        </ActionIcon>
      </MediaQuery>
    );
  }),
);

interface Props extends GroupProps {
  data: NoteActions_note$key;
}

const useStyles = createStyles((theme) => ({
  grid: {
    paddingLeft: '5%',
    paddingRight: '5%',
    transform: 'translateY(100%)',
    marginTop: -34,
    zIndex: 10,
  },

  inner: {
    gap: '8%',
  },
}));

export const NoteActions = Fragment<Props>(fragment, (props) => {
  const { data, ...other } = props;
  const { classes } = useStyles();
  const [setLike, likeInFlight] = useNoteLike(data);
  const [setBookmark, bookmarkInFlight] = useNoteBookmark(data);
  const [setRepeat, repeatInFlight] = useNoteRepeat(data);
  const [createReact, reactInFlight] = useNoteReact(data);
  const editor = useEditor((state) => state.open);

  const customEmojis = emojis.map((label) => {
    return { src: '/emoji/' + label, label };
  });

  const {
    countReplies,
    countLikes,
    countRepeats,
    countReacts,
    countBookmarks,
    isLikedByYou,
    isRepeatedByYou,
    isBookmarkedByYou,
  } = data;

  const onEmojiSelect = (data: string, custom?: boolean) => {
    if (custom) {
      // createReact(undefined, data);
    } else {
      // createReact(data);
    }
  };

  return (
    <MediaQuery
      largerThan="sm"
      styles={{
        paddingLeft: '5%',
        paddingRight: '5%',
        transform: 'translateY(100%)',
        marginTop: -24,
        zIndex: 10,
      }}
    >
      <Group {...other}>
        <Action
          onClick={() => editor(data.id, data.noteId)}
          count={countReplies}
        >
          <Icon.Reply active={false} />
        </Action>
        <Group
          position="center"
          className={classes.inner}
          // @ts-ignore
          sx={{ flexGrow: '1 !important' }}
        >
          <Action
            onClick={() => setRepeat(!isRepeatedByYou)}
            count={countRepeats}
            disabled={repeatInFlight}
            active={isRepeatedByYou}
            color="blue"
          >
            <Icon.Repeat active={isRepeatedByYou} />
          </Action>
          <Action
            onClick={() => setLike(!isLikedByYou)}
            count={countLikes}
            disabled={likeInFlight}
            active={isLikedByYou}
            color="red"
          >
            <Icon.Like active={isLikedByYou} />
          </Action>
          <EmojiPopover
            customEmojis={customEmojis}
            onEmojiSelect={onEmojiSelect}
          >
            <Action count={countReacts} disabled={reactInFlight} color="violet">
              <Icon.React />
            </Action>
          </EmojiPopover>
          <MediaQuery smallerThan={400} styles={{ display: 'none' }}>
            <Action
              onClick={() => setBookmark(!isBookmarkedByYou)}
              count={countBookmarks}
              disabled={bookmarkInFlight}
              active={isBookmarkedByYou}
              color="teal"
            >
              <Icon.Bookmark active={isBookmarkedByYou} />
            </Action>
          </MediaQuery>
        </Group>
        <NoteActionMenu>
          <Action>
            <Icon.More />
          </Action>
        </NoteActionMenu>
      </Group>
    </MediaQuery>
  );
});
