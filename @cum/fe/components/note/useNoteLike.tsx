import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { NoteActions_note$data } from './__generated__/NoteActions_note.graphql.js';
import type { useNoteLikeMutation } from './__generated__/useNoteLikeMutation.graphql.js';

const mutation = graphql`
  mutation useNoteLikeMutation($noteId: UUID!, $state: Boolean!) {
    noteSetLike(input: { noteId: $noteId, state: $state }) {
      note {
        id
        countLikes
        isLikedByYou
      }
    }
  }
`;

type ReturnFn = (state: boolean) => void;
type ReturnHook = [ReturnFn, boolean];

export const useNoteLike = (data: NoteActions_note$data): ReturnHook => {
  const [commitMutation, isMutationInFlight] =
    useMutation<useNoteLikeMutation>(mutation);

  const fn: ReturnFn = (state) => {
    const { id, noteId, countLikes } = data;
    const newCount = state ? countLikes + 1 : Math.max(countLikes - 1, 0);

    commitMutation({
      variables: { noteId, state },
      optimisticResponse: {
        noteSetLike: {
          note: {
            id,
            countLikes: newCount,
            isLikedByYou: state,
          },
        },
      },
    });
  };

  return [fn, isMutationInFlight];
};
