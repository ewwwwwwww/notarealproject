import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { NoteActions_note$data } from './__generated__/NoteActions_note.graphql.js';
import type { useNoteBookmarkMutation } from './__generated__/useNoteBookmarkMutation.graphql.js';

const mutation = graphql`
  mutation useNoteBookmarkMutation($noteId: UUID!, $state: Boolean!) {
    noteSetBookmark(input: { noteId: $noteId, state: $state }) {
      note {
        id
        countBookmarks
        isBookmarkedByYou
      }
    }
  }
`;

type ReturnFn = (state: boolean) => void;
type ReturnHook = [ReturnFn, boolean];

export const useNoteBookmark = (data: NoteActions_note$data): ReturnHook => {
  const [commitMutation, isMutationInFlight] =
    useMutation<useNoteBookmarkMutation>(mutation);

  const fn: ReturnFn = (state) => {
    const { id, noteId, countBookmarks } = data;
    const newCount = state
      ? countBookmarks + 1
      : Math.max(countBookmarks - 1, 0);

    commitMutation({
      variables: { noteId, state },
      optimisticResponse: {
        noteSetBookmark: {
          note: {
            id,
            countBookmarks: newCount,
            isBookmarkedByYou: state,
          },
        },
      },
    });
  };

  return [fn, isMutationInFlight];
};
