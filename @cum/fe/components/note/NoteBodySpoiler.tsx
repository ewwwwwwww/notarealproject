import { FC, memo } from 'react';
import { Html } from 'components/core/Html.jsx';
import { Spoiler } from '@mantine/core';
import { t } from '@lingui/macro';

interface Props {
  content: string | null;
}

export const NoteBody: FC<Props> = memo((props) => {
  const { content } = props;

  if (!content) return null;

  return (
    <Spoiler
      maxHeight={350}
      showLabel={t`Show More`}
      hideLabel={t`Show Less`}
      sx={{
        cursor: 'default',
      }}
      onClick={(event) => {
        event.stopPropagation();
        event.preventDefault();
      }}
    >
      <Html content={content} />
    </Spoiler>
  );
});
