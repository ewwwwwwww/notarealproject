import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { NoteReacts as Comp } from './NoteReacts.jsx';
import type { NoteReactsMockQuery } from './__generated__/NoteReactsMockQuery.graphql.js';

const query = graphql`
  query NoteReactsMockQuery($noteId: UUID!) {
    note(noteId: $noteId) {
      id
      ...NoteReacts_note
    }
  }
`;

export const NoteReact: FC = () => {
  const data = useLazyLoadQuery<NoteReactsMockQuery>(query, {
    noteId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.note) return <div>oops</div>;

  return <Comp note={data.note} />;
};

export default {
  title: 'Note',
};
