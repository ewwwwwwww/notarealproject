import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { NoteActions_note$data } from './__generated__/NoteActions_note.graphql.js';
import type { useNoteRepeatMutation } from './__generated__/useNoteRepeatMutation.graphql.js';

const mutation = graphql`
  mutation useNoteRepeatMutation($noteId: UUID!, $state: Boolean!) {
    noteSetRepeat(input: { noteId: $noteId, state: $state }) {
      note {
        id
        countRepeats
        isRepeatedByYou
      }
    }
  }
`;

type ReturnFn = (state: boolean) => void;
type ReturnHook = [ReturnFn, boolean];

export const useNoteRepeat = (data: NoteActions_note$data): ReturnHook => {
  const [commitMutation, isMutationInFlight] =
    useMutation<useNoteRepeatMutation>(mutation);

  const fn: ReturnFn = (state) => {
    const { id, noteId, countRepeats } = data;
    const newCount = state ? countRepeats + 1 : Math.max(countRepeats - 1, 0);

    commitMutation({
      variables: { noteId, state },
      optimisticResponse: {
        noteSetRepeat: {
          note: {
            id,
            countRepeats: newCount,
            isRepeatedByYou: state,
          },
        },
      },
    });
  };

  return [fn, isMutationInFlight];
};
