import { FC, memo, ReactNode } from 'react';
import { Menu, Button, Text } from '@mantine/core';

interface Props {
  children: ReactNode | ReactNode[];
}

export const NoteActionMenu: FC<Props> = memo((props) => {
  const { children } = props;

  return (
    <Menu>
      <Menu.Target>{children}</Menu.Target>
      <Menu.Dropdown>
        <Menu.Label>Application</Menu.Label>
      </Menu.Dropdown>
    </Menu>
  );
});
