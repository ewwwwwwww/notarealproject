import { HoverCard } from '@mantine/core';
import { FC, ReactNode } from 'react';
import { useLazyLoadQuery } from 'react-relay';
import { graphql } from 'relay-runtime';
import { NoteBody } from './NoteBodyScroller.jsx';
import { NoteHeader } from './NoteHeader.jsx';
import { NoteMentions } from './NoteMentions.jsx';
import { NoteReacts } from './NoteReacts.jsx';
import { NoteHoverQuery } from './__generated__/NoteHoverQuery.graphql.js';

const query = graphql`
  query NoteHoverQuery($id: ID!) {
    note: noteById(id: $id) {
      id
      body
      ...NoteHeader_note
      ...NoteMentions_note
      ...NoteReacts_note
      ...NoteActions_note
    }
  }
`;

interface NoteHoverProps {
  id: ID;
}

const NoteHover: FC<NoteHoverProps> = (props) => {
  const data = useLazyLoadQuery<NoteHoverQuery>(query, props);

  if (!data?.note) return null;

  const { note } = data;

  return (
    <>
      <NoteHeader data={note} />
      <NoteMentions note={note} />
      <NoteBody content={note.body} />
      <NoteReacts data={note} />
    </>
  );
};

interface WrapperProps {
  id: ID;
  children: ReactNode;
}

export const NoteHoverWrapper: FC<WrapperProps> = (props) => {
  const { id, children } = props;

  return (
    <HoverCard
      width={600}
      shadow="md"
      position="left-start"
      openDelay={300}
      withinPortal
    >
      <HoverCard.Target>{children}</HoverCard.Target>
      <HoverCard.Dropdown>
        <NoteHover id={id} />
      </HoverCard.Dropdown>
    </HoverCard>
  );
};
