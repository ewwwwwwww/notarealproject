import { FC, memo } from 'react';
import { Html } from 'components/core/Html.jsx';
import { t } from '@lingui/macro';
import { ScrollArea } from '@mantine/core';

interface Props {
  content: string | null;
}

export const NoteBody: FC<Props> = memo((props) => {
  const { content } = props;

  if (!content) return null;

  return (
    <ScrollArea.Autosize type="always" maxHeight={300} offsetScrollbars>
      <Html content={content} />
    </ScrollArea.Autosize>
  );
});
