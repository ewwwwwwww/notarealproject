import type { NoteReacts_note$key } from './__generated__/NoteReacts_note.graphql.js';

import { graphql } from 'react-relay';
import { createStyles, Box, BoxProps, Group, ThemeIcon } from '@mantine/core';

import { Emoji } from '../emoji/Emoji.jsx';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment NoteReacts_note on Note {
    reactAggrs {
      edges {
        node {
          count
        }
      }
    }
  }
`;

interface Props {
  data: NoteReacts_note$key;
}

const useStyles = createStyles((theme) => ({
  button: {
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[4]
        : theme.colors.gray[1],
  },
}));

export const NoteReacts = Fragment<Props>(fragment, (props) => {
  const { data } = props;
  const { classes: cl } = useStyles();

  if (!data.reactAggrs.edges.length) {
    return null;
  }

  const children = data.reactAggrs.edges.map((edge, idx) => {
    return null;
    // return (
    //   <Emoji
    //     key={idx}
    //     attachment={edge.node.attachment}
    //     unicode={edge.node.unicode}
    //     variant="light"
    //     className={cl.button}
    //   />
    // );
  });

  return (
    <Group mt="lg" spacing="sm">
      {children}
    </Group>
  );
});
