import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { NoteMentions as Comp } from './NoteMentions.jsx';
import type { NoteMentionsMockQuery } from './__generated__/NoteMentionsMockQuery.graphql.js';

const query = graphql`
  query NoteMentionsMockQuery($noteId: UUID!) {
    note(noteId: $noteId) {
      id
      ...NoteMentions_note
    }
  }
`;

export const NoteMentions: FC = () => {
  const data = useLazyLoadQuery<NoteMentionsMockQuery>(query, {
    noteId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.note) return <div>oops</div>;

  return <Comp note={data.note} />;
};

export default {
  title: 'Note',
};
