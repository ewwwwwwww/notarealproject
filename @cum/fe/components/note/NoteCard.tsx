import type { NoteCard_note$key } from './__generated__/NoteCard_note.graphql.js';
import React, { useMemo } from 'react';
import { Card } from 'components/core/Card.jsx';
import { graphql } from 'relay-runtime';
import { NoteActions } from './NoteActions.jsx';
import { NoteHeader } from './NoteHeader.jsx';
import { NoteBody } from './NoteBodySpoiler.jsx';
import { NoteMentions } from './NoteMentions.jsx';
import { NoteReacts } from './NoteReacts.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { navigate } from 'vite-plugin-ssr/client/router';
import shortUUID from 'short-uuid';

const translator = shortUUID();

interface Props {}

const fragment = graphql`
  fragment NoteCard_note on Note {
    id
    body
    threadId
    noteId
    ...NoteHeader_note
    ...NoteMentions_note
    ...NoteReacts_note
    ...NoteActions_note
  }
`;

interface Props {
  data: NoteCard_note$key;
}

export const NoteCard = Fragment<Props>(fragment, (props) => {
  const { data } = props;

  const onClick = useMemo(() => {
    if (!data.threadId) return () => {};

    const threadId: UUID = translator.fromUUID(data.threadId);

    // @ts-ignore
    return () => navigate(`/thread/${threadId}`);
  }, [data.threadId]);

  return (
    <Card sx={{ cursor: 'pointer', overflow: 'visible' }} onClick={onClick}>
      <NoteHeader data={data} />
      <NoteMentions note={data} />
      <NoteBody content={data.body} />
      <NoteReacts data={data} />
      <NoteActions data={data} />
    </Card>
  );
});
