/**
 * @generated SignedSource<<66e2dc4862f32fc75be23c75a2326f8b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteReactsMockQuery$variables = {
  noteId: UUID;
};
export type NoteReactsMockQuery$data = {
  readonly note: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"NoteReacts_note">;
  } | null;
};
export type NoteReactsMockQuery = {
  response: NoteReactsMockQuery$data;
  variables: NoteReactsMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "noteId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "noteId",
    "variableName": "noteId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NoteReactsMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteReacts_note"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NoteReactsMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "ReactAggrsConnection",
            "kind": "LinkedField",
            "name": "reactAggrs",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "ReactAggrsEdge",
                "kind": "LinkedField",
                "name": "edges",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactAggr",
                    "kind": "LinkedField",
                    "name": "node",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "count",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "21745f23fd00479ab63be10a89044cd7",
    "id": null,
    "metadata": {},
    "name": "NoteReactsMockQuery",
    "operationKind": "query",
    "text": "query NoteReactsMockQuery(\n  $noteId: UUID!\n) {\n  note(noteId: $noteId) {\n    id\n    ...NoteReacts_note\n  }\n}\n\nfragment NoteReacts_note on Note {\n  reactAggrs {\n    edges {\n      node {\n        count\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "331020ac492c34cf58e009f6bc530caa";

export default node;
