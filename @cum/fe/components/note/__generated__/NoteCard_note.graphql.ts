/**
 * @generated SignedSource<<bbb5e5be7cc57b05726c8f99e7b84660>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteCard_note$data = {
  readonly body: string | null;
  readonly id: ID;
  readonly noteId: UUID;
  readonly threadId: UUID | null;
  readonly " $fragmentSpreads": FragmentRefs<"NoteActions_note" | "NoteHeader_note" | "NoteMentions_note" | "NoteReacts_note">;
  readonly " $fragmentType": "NoteCard_note";
};
export type NoteCard_note$key = {
  readonly " $data"?: NoteCard_note$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteCard_note">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NoteCard_note",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "body",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "threadId",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "noteId",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NoteHeader_note"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NoteMentions_note"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NoteReacts_note"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NoteActions_note"
    }
  ],
  "type": "Note",
  "abstractKey": null
};

(node as any).hash = "86121fb6cdfa71c4260291783e28dd5c";

export default node;
