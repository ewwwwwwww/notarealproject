/**
 * @generated SignedSource<<48e44739f3524ac23aa65a1429b68ff5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteMentions_note$data = {
  readonly id: ID;
  readonly mentions: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: ID;
        readonly user: {
          readonly id: ID;
          readonly name: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user">;
        } | null;
      };
    }>;
  };
  readonly replyId: UUID | null;
  readonly " $fragmentType": "NoteMentions_note";
};
export type NoteMentions_note$key = {
  readonly " $data"?: NoteMentions_note$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteMentions_note">;
};

import NoteMentionsQuery_graphql from './NoteMentionsQuery.graphql';

const node: ReaderFragment = (function(){
var v0 = [
  "mentions"
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [
    {
      "defaultValue": 10,
      "kind": "LocalArgument",
      "name": "mentionCount"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "mentionCursor"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "mentionCount",
        "cursor": "mentionCursor",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "mentionCount",
          "cursor": "mentionCursor"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [
        "node"
      ],
      "operation": NoteMentionsQuery_graphql,
      "identifierField": "id"
    }
  },
  "name": "NoteMentions_note",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "replyId",
      "storageKey": null
    },
    {
      "alias": "mentions",
      "args": null,
      "concreteType": "MentionsConnection",
      "kind": "LinkedField",
      "name": "__NoteMentions_user_mentions_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "MentionsEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Mention",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "User",
                  "kind": "LinkedField",
                  "name": "user",
                  "plural": false,
                  "selections": [
                    (v1/*: any*/),
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "name",
                      "storageKey": null
                    },
                    {
                      "args": null,
                      "kind": "FragmentSpread",
                      "name": "UserAvatar_user"
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    (v1/*: any*/)
  ],
  "type": "Note",
  "abstractKey": null
};
})();

(node as any).hash = "9c0f2d0d1b7e6c10d9e07622ba04d36b";

export default node;
