/**
 * @generated SignedSource<<7787f6d0f915714f269aede3e6a0d04c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type useNoteLikeMutation$variables = {
  noteId: UUID;
  state: boolean;
};
export type useNoteLikeMutation$data = {
  readonly noteSetLike: {
    readonly note: {
      readonly countLikes: number;
      readonly id: ID;
      readonly isLikedByYou: boolean | null;
    } | null;
  } | null;
};
export type useNoteLikeMutation = {
  response: useNoteLikeMutation$data;
  variables: useNoteLikeMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "noteId"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "state"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "noteId",
            "variableName": "noteId"
          },
          {
            "kind": "Variable",
            "name": "state",
            "variableName": "state"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "NoteSetLikePayload",
    "kind": "LinkedField",
    "name": "noteSetLike",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countLikes",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isLikedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteLikeMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useNoteLikeMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "f2b5ee5909c48b00766e48f86fa033b6",
    "id": null,
    "metadata": {},
    "name": "useNoteLikeMutation",
    "operationKind": "mutation",
    "text": "mutation useNoteLikeMutation(\n  $noteId: UUID!\n  $state: Boolean!\n) {\n  noteSetLike(input: {noteId: $noteId, state: $state}) {\n    note {\n      id\n      countLikes\n      isLikedByYou\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "a6f746b2809b7b0617faf56c4149b283";

export default node;
