/**
 * @generated SignedSource<<3240c2d81c8406d35095ebc26d7c4765>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type useNoteBookmarkMutation$variables = {
  noteId: UUID;
  state: boolean;
};
export type useNoteBookmarkMutation$data = {
  readonly noteSetBookmark: {
    readonly note: {
      readonly countBookmarks: number;
      readonly id: ID;
      readonly isBookmarkedByYou: boolean | null;
    } | null;
  } | null;
};
export type useNoteBookmarkMutation = {
  response: useNoteBookmarkMutation$data;
  variables: useNoteBookmarkMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "noteId"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "state"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "noteId",
            "variableName": "noteId"
          },
          {
            "kind": "Variable",
            "name": "state",
            "variableName": "state"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "NoteSetBookmarkPayload",
    "kind": "LinkedField",
    "name": "noteSetBookmark",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countBookmarks",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBookmarkedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteBookmarkMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useNoteBookmarkMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "be4c7f498ffc68e8a79e1f39b7fa05da",
    "id": null,
    "metadata": {},
    "name": "useNoteBookmarkMutation",
    "operationKind": "mutation",
    "text": "mutation useNoteBookmarkMutation(\n  $noteId: UUID!\n  $state: Boolean!\n) {\n  noteSetBookmark(input: {noteId: $noteId, state: $state}) {\n    note {\n      id\n      countBookmarks\n      isBookmarkedByYou\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "11314b19380c9776c87af215557222d7";

export default node;
