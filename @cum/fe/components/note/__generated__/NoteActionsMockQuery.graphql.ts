/**
 * @generated SignedSource<<6e8017ed3592f7c4f5bed19e0867721f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteActionsMockQuery$variables = {
  noteId: UUID;
};
export type NoteActionsMockQuery$data = {
  readonly note: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"NoteActions_note">;
  } | null;
};
export type NoteActionsMockQuery = {
  response: NoteActionsMockQuery$data;
  variables: NoteActionsMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "noteId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "noteId",
    "variableName": "noteId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NoteActionsMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteActions_note"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NoteActionsMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "noteId",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countReplies",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countLikes",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countBookmarks",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countReacts",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countRepeats",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isLikedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBookmarkedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isRepeatedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "f57fb87cc55520429ba31dd8ec9e19bc",
    "id": null,
    "metadata": {},
    "name": "NoteActionsMockQuery",
    "operationKind": "query",
    "text": "query NoteActionsMockQuery(\n  $noteId: UUID!\n) {\n  note(noteId: $noteId) {\n    id\n    ...NoteActions_note\n  }\n}\n\nfragment NoteActions_note on Note {\n  id\n  noteId\n  countReplies\n  countLikes\n  countBookmarks\n  countReacts\n  countRepeats\n  isLikedByYou\n  isBookmarkedByYou\n  isRepeatedByYou\n}\n"
  }
};
})();

(node as any).hash = "bb0dc35c77e023ba026a0f0814beb52d";

export default node;
