/**
 * @generated SignedSource<<2a028301f34fa1d5bd223d2d725c7f41>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type Scope = "DIRECT" | "LOCAL" | "PRIVATE" | "PUBLIC" | "UNLISTED" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type NoteHeader_note$data = {
  readonly ctime: Datetime;
  readonly domain: {
    readonly " $fragmentSpreads": FragmentRefs<"NoteFavicon_domain">;
  } | null;
  readonly scope: Scope;
  readonly user: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user" | "UserName_user">;
  } | null;
  readonly " $fragmentType": "NoteHeader_note";
};
export type NoteHeader_note$key = {
  readonly " $data"?: NoteHeader_note$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteHeader_note">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NoteHeader_note",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ctime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Domain",
      "kind": "LinkedField",
      "name": "domain",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "NoteFavicon_domain"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserAvatar_user"
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "UserName_user"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Note",
  "abstractKey": null
};

(node as any).hash = "d0cc5a48e78ac6005dd4523819f6b429";

export default node;
