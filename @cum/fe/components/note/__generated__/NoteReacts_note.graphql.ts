/**
 * @generated SignedSource<<78b284cc6acc6f151fc96637b41d844a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteReacts_note$data = {
  readonly reactAggrs: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly count: number;
      };
    }>;
  };
  readonly " $fragmentType": "NoteReacts_note";
};
export type NoteReacts_note$key = {
  readonly " $data"?: NoteReacts_note$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteReacts_note">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NoteReacts_note",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ReactAggrsConnection",
      "kind": "LinkedField",
      "name": "reactAggrs",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ReactAggrsEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "ReactAggr",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "count",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Note",
  "abstractKey": null
};

(node as any).hash = "1882eb629c9c01f5fac1094360e864a6";

export default node;
