/**
 * @generated SignedSource<<7bd3ff2ecf7e5f544d64ce7516967e41>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ReactInput = {
  ctime?: Datetime | null;
  fileId?: string | null;
  mtime?: Datetime | null;
  noteId: UUID;
  reactId?: UUID | null;
  src?: string | null;
  userId: UUID;
};
export type useNoteReactMutation$variables = {
  react: ReactInput;
};
export type useNoteReactMutation$data = {
  readonly createReact: {
    readonly note: {
      readonly id: ID;
    } | null;
  } | null;
};
export type useNoteReactMutation = {
  response: useNoteReactMutation$data;
  variables: useNoteReactMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "react"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "react",
            "variableName": "react"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "CreateReactPayload",
    "kind": "LinkedField",
    "name": "createReact",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteReactMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useNoteReactMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "dc0680159b3719daa6429923749aa467",
    "id": null,
    "metadata": {},
    "name": "useNoteReactMutation",
    "operationKind": "mutation",
    "text": "mutation useNoteReactMutation(\n  $react: ReactInput!\n) {\n  createReact(input: {react: $react}) {\n    note {\n      id\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "4da08772c6d9ed8373f396cbd1af6a24";

export default node;
