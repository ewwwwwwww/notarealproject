/**
 * @generated SignedSource<<81cc34aa95e94c7823232db9ce81eb91>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteActions_note$data = {
  readonly countBookmarks: number;
  readonly countLikes: number;
  readonly countReacts: number;
  readonly countRepeats: number;
  readonly countReplies: number;
  readonly id: ID;
  readonly isBookmarkedByYou: boolean | null;
  readonly isLikedByYou: boolean | null;
  readonly isRepeatedByYou: boolean | null;
  readonly noteId: UUID;
  readonly " $fragmentType": "NoteActions_note";
};
export type NoteActions_note$key = {
  readonly " $data"?: NoteActions_note$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteActions_note">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NoteActions_note",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "noteId",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countReplies",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countLikes",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countBookmarks",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countReacts",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countRepeats",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isLikedByYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBookmarkedByYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isRepeatedByYou",
      "storageKey": null
    }
  ],
  "type": "Note",
  "abstractKey": null
};

(node as any).hash = "b390ba6a625c1e60661da06371126229";

export default node;
