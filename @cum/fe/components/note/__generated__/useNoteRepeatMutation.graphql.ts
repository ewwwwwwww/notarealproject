/**
 * @generated SignedSource<<4daab646ca2bb2e61e3018fab841aa5a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type useNoteRepeatMutation$variables = {
  noteId: UUID;
  state: boolean;
};
export type useNoteRepeatMutation$data = {
  readonly noteSetRepeat: {
    readonly note: {
      readonly countRepeats: number;
      readonly id: ID;
      readonly isRepeatedByYou: boolean | null;
    } | null;
  } | null;
};
export type useNoteRepeatMutation = {
  response: useNoteRepeatMutation$data;
  variables: useNoteRepeatMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "noteId"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "state"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "kind": "Variable",
            "name": "noteId",
            "variableName": "noteId"
          },
          {
            "kind": "Variable",
            "name": "state",
            "variableName": "state"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "NoteSetRepeatPayload",
    "kind": "LinkedField",
    "name": "noteSetRepeat",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "note",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countRepeats",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isRepeatedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteRepeatMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "useNoteRepeatMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "d0c8b358b6248d4025a89993e8cc9558",
    "id": null,
    "metadata": {},
    "name": "useNoteRepeatMutation",
    "operationKind": "mutation",
    "text": "mutation useNoteRepeatMutation(\n  $noteId: UUID!\n  $state: Boolean!\n) {\n  noteSetRepeat(input: {noteId: $noteId, state: $state}) {\n    note {\n      id\n      countRepeats\n      isRepeatedByYou\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "91953ca7c5739e782b89c96fcce69033";

export default node;
