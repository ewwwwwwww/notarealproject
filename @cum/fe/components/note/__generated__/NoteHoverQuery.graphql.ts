/**
 * @generated SignedSource<<c4c4dec907839c80591d48ffe16eba3c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteHoverQuery$variables = {
  id: ID;
};
export type NoteHoverQuery$data = {
  readonly note: {
    readonly body: string | null;
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"NoteActions_note" | "NoteHeader_note" | "NoteMentions_note" | "NoteReacts_note">;
  } | null;
};
export type NoteHoverQuery = {
  response: NoteHoverQuery$data;
  variables: NoteHoverQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "concreteType": "UserAttachment",
  "kind": "LinkedField",
  "name": "avatar",
  "plural": false,
  "selections": [
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "src",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "File",
      "kind": "LinkedField",
      "name": "file",
      "plural": false,
      "selections": [
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "blake3",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "storageKey": null
},
v8 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 10
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NoteHoverQuery",
    "selections": [
      {
        "alias": "note",
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "noteById",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          (v3/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteHeader_note"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteMentions_note"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteReacts_note"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "NoteActions_note"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NoteHoverQuery",
    "selections": [
      {
        "alias": "note",
        "args": (v1/*: any*/),
        "concreteType": "Note",
        "kind": "LinkedField",
        "name": "noteById",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          (v3/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "ctime",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "scope",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Domain",
            "kind": "LinkedField",
            "name": "domain",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "host",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v6/*: any*/),
              (v7/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "nick",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "replyId",
            "storageKey": null
          },
          {
            "alias": null,
            "args": (v8/*: any*/),
            "concreteType": "MentionsConnection",
            "kind": "LinkedField",
            "name": "mentions",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "MentionsEdge",
                "kind": "LinkedField",
                "name": "edges",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "node",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "user",
                        "plural": false,
                        "selections": [
                          (v2/*: any*/),
                          (v5/*: any*/),
                          (v4/*: any*/),
                          (v6/*: any*/),
                          (v7/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "__typename",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "cursor",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PageInfo",
                "kind": "LinkedField",
                "name": "pageInfo",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "endCursor",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hasNextPage",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "mentions(first:10)"
          },
          {
            "alias": null,
            "args": (v8/*: any*/),
            "filters": null,
            "handle": "connection",
            "key": "NoteMentions_user_mentions",
            "kind": "LinkedHandle",
            "name": "mentions"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "ReactAggrsConnection",
            "kind": "LinkedField",
            "name": "reactAggrs",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "ReactAggrsEdge",
                "kind": "LinkedField",
                "name": "edges",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactAggr",
                    "kind": "LinkedField",
                    "name": "node",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "count",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "noteId",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countReplies",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countLikes",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countBookmarks",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countReacts",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countRepeats",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isLikedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBookmarkedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isRepeatedByYou",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "1528d292625d46e57c228cf0e713512e",
    "id": null,
    "metadata": {},
    "name": "NoteHoverQuery",
    "operationKind": "query",
    "text": "query NoteHoverQuery(\n  $id: ID!\n) {\n  note: noteById(id: $id) {\n    id\n    body\n    ...NoteHeader_note\n    ...NoteMentions_note\n    ...NoteReacts_note\n    ...NoteActions_note\n  }\n}\n\nfragment NoteActions_note on Note {\n  id\n  noteId\n  countReplies\n  countLikes\n  countBookmarks\n  countReacts\n  countRepeats\n  isLikedByYou\n  isBookmarkedByYou\n  isRepeatedByYou\n}\n\nfragment NoteFavicon_domain on Domain {\n  id\n  host\n}\n\nfragment NoteHeader_note on Note {\n  ctime\n  scope\n  domain {\n    ...NoteFavicon_domain\n    id\n  }\n  user {\n    id\n    ...UserAvatar_user\n    ...UserName_user\n  }\n}\n\nfragment NoteMentions_note on Note {\n  replyId\n  mentions(first: 10) {\n    edges {\n      node {\n        id\n        user {\n          id\n          name\n          ...UserAvatar_user\n        }\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n  id\n}\n\nfragment NoteReacts_note on Note {\n  reactAggrs {\n    edges {\n      node {\n        count\n      }\n    }\n  }\n}\n\nfragment UserAvatar_user on User {\n  id\n  ...UserLink\n  avatar {\n    id\n    src\n    file {\n      id\n      blake3\n    }\n  }\n}\n\nfragment UserLink on User {\n  id\n  remote\n  name\n  nameFull\n}\n\nfragment UserName_user on User {\n  id\n  nick\n  name\n  nameFull\n  remote\n}\n"
  }
};
})();

(node as any).hash = "b1f908ab8fdd364d55f2f9347226d437";

export default node;
