/**
 * @generated SignedSource<<6422653f298f7cc5ac2e516d2e85051d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NoteFavicon_domain$data = {
  readonly host: string | null;
  readonly id: ID;
  readonly " $fragmentType": "NoteFavicon_domain";
};
export type NoteFavicon_domain$key = {
  readonly " $data"?: NoteFavicon_domain$data;
  readonly " $fragmentSpreads": FragmentRefs<"NoteFavicon_domain">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NoteFavicon_domain",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "host",
      "storageKey": null
    }
  ],
  "type": "Domain",
  "abstractKey": null
};

(node as any).hash = "61e93ab34a4c2c37006fb9c496b675c2";

export default node;
