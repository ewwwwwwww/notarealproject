import { useMutation } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { NoteActions_note$data } from './__generated__/NoteActions_note.graphql.js';
import type { useNoteReactMutation } from './__generated__/useNoteReactMutation.graphql.js';

const mutation = graphql`
  mutation useNoteReactMutation($react: ReactInput!) {
    createReact(input: { react: $react }) {
      note {
        id
      }
    }
  }
`;

type ReturnFn = (unicode: string, attachmentId: string) => void;
type ReturnHook = [ReturnFn, boolean];

export const useNoteReact = (data: NoteActions_note$data): ReturnHook => {
  const [commitMutation, isMutationInFlight] =
    useMutation<useNoteReactMutation>(mutation);

  const fn: ReturnFn = (unicode, attachmentId) => {
    const { id, noteId } = data;

    if (unicode && attachmentId) {
      throw new Error('cannot have both unicode + custom emoji');
    }

    commitMutation({
      variables: {
        react: {
          userId: '',
          noteId,
          unicode,
          attachmentId,
        },
      },
    });
  };

  return [fn, isMutationInFlight];
};
