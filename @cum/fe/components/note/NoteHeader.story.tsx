import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { NoteHeader as Comp } from './NoteHeader.jsx';
import type { NoteHeaderMockQuery } from './__generated__/NoteHeaderMockQuery.graphql.js';

const query = graphql`
  query NoteHeaderMockQuery($noteId: UUID!) {
    note(noteId: $noteId) {
      id
      ...NoteHeader_note
    }
  }
`;

export const NoteHeader: FC = () => {
  const data = useLazyLoadQuery<NoteHeaderMockQuery>(query, {
    noteId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.note) return <div>oops</div>;

  return <Comp note={data.note} />;
};

export default {
  title: 'Note',
};
