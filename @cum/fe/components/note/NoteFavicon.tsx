import { FC, memo } from 'react';
import { Box, Text, Tooltip } from '@mantine/core';
import { IoLogoTwitter as IconFavicon } from 'react-icons/io';
import { graphql } from 'relay-runtime';
import { useFragment } from 'react-relay';
import type { NoteFavicon_domain$key } from './__generated__/NoteFavicon_domain.graphql.js';
import { Fragment } from 'components/core/Fragment.jsx';

const fragment = graphql`
  fragment NoteFavicon_domain on Domain {
    id
    host
  }
`;

interface Props {
  data: NoteFavicon_domain$key;
}

export const NoteFavicon: FC<Props> = Fragment(fragment, (props) => {
  const { data } = props;

  return (
    <Box>
      <Tooltip label={data.host} position="left">
        <Text color="dimmed">
          <IconFavicon size={18} />
        </Text>
      </Tooltip>
    </Box>
  );
});
