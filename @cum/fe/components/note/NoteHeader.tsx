import { FC, ReactNode, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import { Box, createStyles, Group, Text } from '@mantine/core';
import type { NoteHeader_note$key } from './__generated__/NoteHeader_note.graphql.js';
import { UserAvatar } from 'components/user/UserAvatar.jsx';
import { IconScope } from 'components/core/Scope.jsx';
import { ShortDate } from 'components/core/ShortDate.jsx';
import { NoteFavicon } from './NoteFavicon.jsx';
import { Fragment } from 'components/core/Fragment.jsx';
import { UserName } from 'components/user/UserName.jsx';

const fragment = graphql`
  fragment NoteHeader_note on Note {
    ctime
    scope

    domain {
      ...NoteFavicon_domain
    }

    user {
      id
      ...UserAvatar_user
      ...UserName_user
    }
  }
`;

const useStyles = createStyles((theme) => ({
  avatar: {
    float: 'left',
  },

  nick: {
    flexBasis: '100%',
  },

  nameFull: {
    flexGrow: 1,
  },

  middle: {
    flexGrow: 1,
  },

  corner: {
    position: 'absolute',
    right: 16,
    marginTop: -1,
  },
}));

interface Props {
  data: NoteHeader_note$key;
}

export const NoteHeader: FC<Props> = Fragment(fragment, (props) => {
  const { data } = props;
  const { classes } = useStyles();

  const date = new Date(data.ctime);

  const { user } = data;

  if (!user) return null;

  return (
    <Box mb="xs">
      <UserAvatar
        data={user}
        className={classes.avatar}
        size={56}
        mr="lg"
        hover
      />
      <Group className={classes.corner}>
        {data.domain && <NoteFavicon data={data.domain} />}
        <Text color="dimmed">
          <IconScope scope={data.scope} />
        </Text>
      </Group>
      <Group spacing={4}>
        <UserName
          data={user}
          className={classes.nick}
          classNameFull={classes.nameFull}
          unstyled
        />
        <ShortDate date={date} color="dimmed" />
      </Group>
    </Box>
  );
});
