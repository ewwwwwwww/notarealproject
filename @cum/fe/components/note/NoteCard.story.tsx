import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { NoteCard as Comp } from './NoteCard.jsx';
import type { NoteCardMockQuery } from './__generated__/NoteCardMockQuery.graphql.js';

const query = graphql`
  query NoteCardMockQuery($noteId: UUID!) {
    note(noteId: $noteId) {
      id
      ...NoteCard_note
    }
  }
`;

export const NoteCard: FC = () => {
  const data = useLazyLoadQuery<NoteCardMockQuery>(query, {
    noteId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.note) return <div>oops</div>;

  return <Comp note={data.note} />;
};

export default {
  title: 'Note',
};
