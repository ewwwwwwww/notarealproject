import { FC, memo } from 'react';
import { IconType } from 'react-icons';

const ICON_SIZE = 16;

// TODO: vivusjs animation on press
interface ActionProps extends Partial<IconType> {
  active?: boolean | null;
}

type ActionIcon = FC<ActionProps>;

import { HiReply } from 'react-icons/hi';

export const Reply: ActionIcon = memo(({ active, ...other }) => {
  return <HiReply size={ICON_SIZE} {...other} />;
});

import { FaRetweet } from 'react-icons/fa';

export const Repeat: ActionIcon = memo(({ active, ...other }) => {
  return <FaRetweet size={ICON_SIZE} {...other} />;
});

import { FaHeart, FaRegHeart } from 'react-icons/fa';

export const Like: ActionIcon = memo(({ active, ...other }) => {
  return active ? (
    <FaHeart size={ICON_SIZE} {...other} />
  ) : (
    <FaRegHeart size={ICON_SIZE} {...other} />
  );
});

import { FaBookmark, FaRegBookmark } from 'react-icons/fa';

export const Bookmark: ActionIcon = memo(({ active, ...other }) => {
  return active ? (
    <FaBookmark size={ICON_SIZE} {...other} />
  ) : (
    <FaRegBookmark size={ICON_SIZE} {...other} />
  );
});

import { FiSmile } from 'react-icons/fi';

export const React: ActionIcon = memo(({ active, ...other }) => {
  return <FiSmile size={ICON_SIZE} {...other} />;
});

import { FiMoreHorizontal } from 'react-icons/fi';

export const More: ActionIcon = memo(({ active, ...other }) => {
  return <FiMoreHorizontal size={ICON_SIZE} {...other} />;
});
