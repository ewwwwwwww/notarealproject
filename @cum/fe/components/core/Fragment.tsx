import type { KeyType, KeyTypeData } from 'react-relay/relay-hooks/helpers.js';

import { FC, Suspense, memo, forwardRef, ReactNode } from 'react';
import { Loader } from '@mantine/core';
import { GraphQLTaggedNode } from 'relay-runtime';
import { useFragment } from 'react-relay';

export const Fragment = <P extends { data: KeyType<unknown> }>(
  fragment: GraphQLTaggedNode,
  Component: FC<{ data: KeyTypeData<P['data']> } & Omit<P, 'data'>>,
  options?: {
    fallback?: ReactNode;
    memo?: boolean;
    suspend?: boolean;
    forwardRef?: boolean;
  },
) => {
  const fallback: ReactNode = options?.fallback ?? <Loader />;
  const suspend = options?.suspend ?? true;

  const fn = (
    props: Omit<P, 'data'> & { data?: P['data'] | null },
    ref?: any,
  ) => {
    if (!props.data) return <>{fallback}</>;

    const data = useFragment(fragment, props.data);

    if (!data) return <>{fallback}</>;

    const children = <Component {...props} data={data} />;

    return suspend ? (
      <Suspense fallback={fallback}>{children}</Suspense>
    ) : (
      children
    );
  };

  const fn2 = !options || options.memo ? memo(fn) : fn;
  const fn3 = options && options.forwardRef ? forwardRef(fn) : fn;

  return fn3;
};
