import { Text, TextProps } from '@mantine/core';
import { FC, memo, useEffect, useMemo, useRef } from 'react';
import TimeAgo from 'react-time-ago';
import create, { createStore } from 'zustand';

interface State {
  offset: number;
  setOffset: (now: string | null) => void;
}

export const shortDateStore = createStore<State>((set) => ({
  offset: 0,

  setOffset: (now) => {
    const date = now ? new Date(now).getTime() : Date.now();
    const offset = Date.now() - date;

    set({ offset });
  },
}));

export const useShortDate = create(shortDateStore);

interface Props extends TextProps {
  date: Date;
  long?: boolean;
}

// TODO: replace mantine popover labels
// FIXME: time offsets lags out
export const ShortDate: FC<Props> = memo((props) => {
  const { date, long, ...other } = props;
  const offsetRef = useRef(shortDateStore.getState().offset);

  useEffect(
    () => useShortDate.subscribe((state) => (offsetRef.current = state.offset)),
    [],
  );

  return (
    <Text {...other}>
      <TimeAgo date={date} timeStyle={long ? undefined : 'twitter'} />
    </Text>
  );
});
