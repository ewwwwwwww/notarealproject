import { FC, useCallback, useEffect, useMemo, useState } from 'react';
import { createStyles, Box, Text, Group } from '@mantine/core';
import { AiOutlineUnorderedList as IconList } from 'react-icons/ai';
import { useHash } from '@mantine/hooks';

const LINK_HEIGHT = 38;
const INDICATOR_SIZE = 10;
const INDICATOR_OFFSET = (LINK_HEIGHT - INDICATOR_SIZE) / 2;

const useStyles = createStyles((theme) => ({
  link: {
    ...theme.fn.focusStyles(),
    display: 'block',
    textDecoration: 'none',
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
    lineHeight: `${LINK_HEIGHT}px`,
    fontSize: theme.fontSizes.sm,
    height: LINK_HEIGHT,
    borderTopRightRadius: theme.radius.sm,
    borderBottomRightRadius: theme.radius.sm,
    borderLeft: `2px solid ${
      theme.colorScheme === 'dark' ? theme.colors.dark[4] : theme.colors.gray[2]
    }`,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    },
  },

  linkActive: {
    fontWeight: 500,
    color:
      theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 3 : 7],
  },

  links: {
    position: 'relative',
  },

  indicator: {
    transition: 'transform 150ms ease',
    border: `2px solid ${
      theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 3 : 7]
    }`,
    backgroundColor:
      theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    height: INDICATOR_SIZE,
    width: INDICATOR_SIZE,
    borderRadius: INDICATOR_SIZE,
    position: 'absolute',
    left: -INDICATOR_SIZE / 2 + 1,
  },
}));

interface Props {
  links: { label: string; link: string; order: number }[];
}

export const TableOfContents: FC<Props> = (props) => {
  const { links } = props;
  const { classes, cx } = useStyles();
  const [hash] = useHash();

  const active = useMemo(() => {
    const item = links.findIndex((x) => x.link === hash);

    return item >= 0 ? item : 0;
  }, [hash]);

  const items = links.map((item, index) => (
    <Box<'a'>
      component="a"
      href={item.link}
      key={item.label}
      className={cx(classes.link, { [classes.linkActive]: active === index })}
      sx={(theme) => ({ paddingLeft: item.order * theme.spacing.lg })}
    >
      {item.label}
    </Box>
  ));

  return (
    <Box mt="xl" p="lg">
      <div className={classes.links}>
        <div
          className={classes.indicator}
          style={{
            transform: `translateY(${
              active * LINK_HEIGHT + INDICATOR_OFFSET
            }px)`,
          }}
        />
        {items}
      </div>
    </Box>
  );
};
