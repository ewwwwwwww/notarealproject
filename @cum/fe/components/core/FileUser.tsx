import { Image } from '@mantine/core';
import { graphql } from 'relay-runtime';
import { File } from './File.jsx';
import { Fragment } from './Fragment.jsx';
import { FileUser$key } from './__generated__/FileUser.graphql.js';

const fragment = graphql`
  fragment FileUser on UserAttachment {
    id
  }
`;

interface Props {
  data: FileUser$key;
}

export const FileUser = Fragment<Props>(fragment, (props) => {
  const { data, ...other } = props;

  return <File src={''} />;
});
