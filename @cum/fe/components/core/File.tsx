import { Image, ImageProps } from '@mantine/core';
import { FC, memo } from 'react';

interface Props extends ImageProps {
  src: string | null;
}

export const File: FC<Props> = memo((props) => {
  const { src, ...other } = props;

  return <Image imageProps={{ loading: 'lazy' }} {...other} />;
});
