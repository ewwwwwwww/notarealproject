import { FC, memo, ReactNode } from 'react';
import { ScrollArea, ScrollAreaProps } from '@mantine/core';

type Props = ScrollAreaProps;

export const Scroll: FC<Props> = memo(({ children, sx, ...props }) => (
  <ScrollArea sx={{ ...sx, height: '100%' }} type="never" {...props}>
    {children}
  </ScrollArea>
));
