import { Image } from '@mantine/core';
import { graphql } from 'relay-runtime';
import { File } from './File.jsx';
import { Fragment } from './Fragment.jsx';
import { FileNote$key } from './__generated__/FileNote.graphql.js';

const fragment = graphql`
  fragment FileNote on UserAttachment {
    id
  }
`;

interface Props {
  data: FileNote$key;
}

export const FileNote = Fragment<Props>(fragment, (props) => {
  const { data, ...other } = props;

  return <File src={''} />;
});
