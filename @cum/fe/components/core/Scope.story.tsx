import { IconScope } from './Scope.jsx';
import { Card, Group, Text } from '@mantine/core';

export const Scopes = () => (
  <Card sx={{ maxWidth: 300 }}>
    <Group grow>
      <Text>Public</Text>
      <IconScope scope="PUBLIC" />
      <IconScope outline scope="PUBLIC" />
    </Group>
    <Group grow>
      <Text>Private</Text>
      <IconScope scope="PRIVATE" />
      <IconScope outline scope="PRIVATE" />
    </Group>
    <Group grow>
      <Text>Local</Text>
      <IconScope scope="LOCAL" />
      <IconScope outline scope="LOCAL" />
    </Group>
    <Group grow>
      <Text>Unlisted</Text>
      <IconScope scope="UNLISTED" />
      <IconScope outline scope="UNLISTED" />
    </Group>
    <Group grow>
      <Text>Direct</Text>
      <IconScope scope="DIRECT" />
      <IconScope outline scope="DIRECT" />
    </Group>
  </Card>
);

export default {
  title: 'Core',
};
