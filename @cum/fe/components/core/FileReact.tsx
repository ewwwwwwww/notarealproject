import { Image } from '@mantine/core';
import { graphql } from 'relay-runtime';
import { File } from './File.jsx';
import { Fragment } from './Fragment.jsx';
import { FileReact$key } from './__generated__/FileReact.graphql.js';

const fragment = graphql`
  fragment FileReact on UserAttachment {
    id
  }
`;

interface Props {
  data: FileReact$key;
}

export const FileReact = Fragment<Props>(fragment, (props) => {
  const { data, ...other } = props;

  return <File src={''} />;
});
