/**
 * @generated SignedSource<<c93eef96543d11892bdc3b1a66606cf6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type FileReact$data = {
  readonly id: ID;
  readonly " $fragmentType": "FileReact";
};
export type FileReact$key = {
  readonly " $data"?: FileReact$data;
  readonly " $fragmentSpreads": FragmentRefs<"FileReact">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "FileReact",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "UserAttachment",
  "abstractKey": null
};

(node as any).hash = "693c28d628dd07784c2e8b95314506ba";

export default node;
