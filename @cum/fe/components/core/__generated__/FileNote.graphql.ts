/**
 * @generated SignedSource<<d9c6c7e0e2abfc2bed66c9166fbe69cc>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type FileNote$data = {
  readonly id: ID;
  readonly " $fragmentType": "FileNote";
};
export type FileNote$key = {
  readonly " $data"?: FileNote$data;
  readonly " $fragmentSpreads": FragmentRefs<"FileNote">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "FileNote",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "UserAttachment",
  "abstractKey": null
};

(node as any).hash = "23db8dba1dd47b6b9a3261d2a4bb0794";

export default node;
