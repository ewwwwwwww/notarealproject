/**
 * @generated SignedSource<<1dac2ada700c3fcc21a47b930ab9d0bf>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type FileUser$data = {
  readonly id: ID;
  readonly " $fragmentType": "FileUser";
};
export type FileUser$key = {
  readonly " $data"?: FileUser$data;
  readonly " $fragmentSpreads": FragmentRefs<"FileUser">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "FileUser",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "UserAttachment",
  "abstractKey": null
};

(node as any).hash = "4c6d2168ba1413ec68538bd56b5a4038";

export default node;
