import { FC, forwardRef, memo } from 'react';
import DOMPurify from 'isomorphic-dompurify';
import { createStyles, Text, TextProps } from '@mantine/core';

interface Props extends TextProps {
  content: string | null;
}

const sanitize = (dirty: string) => {
  return DOMPurify.sanitize(dirty, {
    ALLOWED_TAGS: ['b', 'i', 'br', 'p', 'tr', 'td'],
  });
};

const useStyles = createStyles((theme) => ({
  html: {
    '& p:first-of-type': {
      paddingTop: 0,
      marginTop: 0,
    },

    '& p:last-child': {
      paddingBottom: 0,
      marginBottom: 0,
    },
  },
}));

// FIXME: vite >3.1 breaks dom purify
// github.com/sveltejs/kit/issues/7026
// github.com/vitejs/vite/issues/10255
export const Html = memo(
  forwardRef<HTMLDivElement, Props>((props, ref) => {
    if (!props.content) return null;

    const { content, className, ...other } = props;
    const { classes, cx } = useStyles();

    const __html = sanitize(content);

    return (
      <Text
        ref={ref}
        dangerouslySetInnerHTML={{ __html }}
        className={cx(classes.html, className)}
        {...other}
      />
    );
  }),
);
