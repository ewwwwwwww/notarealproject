import { FC, ReactNode, memo } from 'react';
import {
  Card as MantineCard,
  CardProps,
  createStyles,
  MediaQuery,
} from '@mantine/core';
import { motion } from 'framer-motion';
import { usePage } from 'hooks/usePage.js';

export interface Props extends CardProps {
  children: ReactNode | ReactNode[];
  onClick?: () => void;
  compact?: boolean;
}

const defaultCfg = {
  cardPadding: 16,
  cardRadius: 8,
  cardShadow: 'sm',
  cardBorder: true,
};

const compactStyle = {
  margin: 0,
  borderRadius: 0,
  borderLeft: 0,
  borderRight: 0,
  borderTop: 0,
  boxShadow: 'none',
  width: '100%',
};

const useStyles = createStyles((theme) => ({
  card: {
    [`@media (max-width: ${theme.breakpoints.sm}px)`]: compactStyle,
  },

  compact: compactStyle,
}));

const Root: FC<Props> = memo((props) => {
  const { children, compact, className, ...other } = props;
  const { cardShadow, cardRadius, cardPadding, cardBorder } = defaultCfg;
  const { classes, cx } = useStyles();

  return (
    <MantineCard
      shadow={cardShadow}
      radius={cardRadius}
      p={cardPadding}
      withBorder={cardBorder}
      mt={30}
      className={cx(classes.card, compact && classes.compact, className)}
      {...other}
    >
      {children}
    </MantineCard>
  );
});

export const Card = Object.assign(Root, {
  Section: MantineCard.Section,
});
