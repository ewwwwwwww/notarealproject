import { FC, memo } from 'react';

import {
  FaGlobe as IconOutlinePublic,
  FaGlobe as IconPublic,
  FaRegEnvelope as IconOutlinePrivate,
  FaEnvelope as IconPrivate,
} from 'react-icons/fa';

import {
  BsPeople as IconOutlineLocal,
  BsPeopleFill as IconLocal,
  BsQuestion as IconQuestion,
} from 'react-icons/bs';

import {
  HiOutlineLockOpen as IconOutlineUnlisted,
  HiLockOpen as IconUnlisted,
  HiOutlineLockClosed as IconOutlineDirect,
  HiLockClosed as IconDirect,
} from 'react-icons/hi';

export type ScopeType = 'PUBLIC' | 'PRIVATE' | 'LOCAL' | 'UNLISTED' | 'DIRECT';

interface Props {
  outline?: boolean;
  size?: number;
  scope: ScopeType | '%future added value';
}

export const IconScope: FC<Props> = memo((props) => {
  const { outline, scope, size } = props;
  const n = size ? size : 18;

  let Icon;

  switch (scope) {
    case 'PUBLIC':
      Icon = outline ? IconOutlinePublic : IconPublic;
      break;
    case 'PRIVATE':
      Icon = outline ? IconOutlinePrivate : IconPrivate;
      break;
    case 'LOCAL':
      Icon = outline ? IconOutlineLocal : IconLocal;
      break;
    case 'UNLISTED':
      Icon = outline ? IconOutlineUnlisted : IconUnlisted;
      break;
    case 'DIRECT':
      Icon = outline ? IconOutlineDirect : IconDirect;
      break;

    default:
      Icon = IconQuestion;
  }

  return <Icon size={n} />;
});
