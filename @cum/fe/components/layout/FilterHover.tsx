import { FC, memo } from 'react';

export interface FilterData {
  type: string;
  label: string;
  value: string;
  onChange: (value: string) => void;
  presets?: string[];
}

interface Props {
  data: FilterData[];
}

export const FilterHover: FC<Props> = memo((props) => {
  return <div />;
});
