import { FC, memo, ReactNode } from 'react';
import {
  Container,
  Grid,
  Stack,
  Box,
  createStyles,
  MediaQuery,
} from '@mantine/core';

const HEADER_SIZE = 56;

interface SectionProps {
  className?: string;
  header?: ReactNode;
  children?: ReactNode | ReactNode[];
  span?: number | 'auto' | 'content';
  small?: boolean;
  fullWidth?: boolean;
}

const useStyles = createStyles((theme) => ({
  headerFix: {
    height: HEADER_SIZE,
    position: 'absolute',
    width: '100%',
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[9]
        : theme.colors[theme.primaryColor][4],
  },

  header: {
    height: HEADER_SIZE,
    flexShrink: 0,
    color: 'white',
    zIndex: 1,
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[9]
        : theme.colors[theme.primaryColor][4],
  },
}));

export const Section: FC<SectionProps> = memo((props) => {
  const { children, header, span, small, className } = props;
  const { classes } = useStyles();

  const maxWidth = small ? 300 : undefined;

  return (
    <MediaQuery
      smallerThan="sm"
      styles={{
        padding: 0,
      }}
    >
      <Grid.Col span={span} py={0} className={className}>
        <Stack sx={{ height: '100%', maxWidth }} spacing={0}>
          <MediaQuery
            smallerThan="sm"
            styles={{
              paddingLeft: 24,
              paddingRight: 16,
            }}
          >
            <Stack justify="space-around" className={classes.header}>
              {header}
            </Stack>
          </MediaQuery>
          {children}
        </Stack>
      </Grid.Col>
    </MediaQuery>
  );
});

export const Aside: FC<SectionProps> = memo((props) => {
  return (
    <MediaQuery
      smallerThan="md"
      styles={{
        display: 'none',
      }}
    >
      <Section {...props} />
    </MediaQuery>
  );
});

export const Nav: FC<SectionProps> = memo((props) => {
  return (
    <MediaQuery
      smallerThan="lg"
      styles={{
        display: 'none',
      }}
    >
      <Section {...props} />
    </MediaQuery>
  );
});

interface Props {
  children?: ReactNode | ReactNode[];
}

const HeaderBgFix: FC = memo(() => {
  const { classes } = useStyles();

  return <Box className={classes.headerFix} />;
});

export const Layout: FC<Props> = memo((props) => {
  const { children } = props;

  return (
    <>
      <HeaderBgFix />
      <MediaQuery smallerThan="sm" styles={{ padding: 0 }}>
        <Container size={1500} sx={{ height: '100%' }}>
          <Grid p={0} m={0} sx={{ height: '100%' }}>
            {children}
          </Grid>
        </Container>
      </MediaQuery>
    </>
  );
});
