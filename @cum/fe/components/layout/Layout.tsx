import {
  Box,
  ColProps,
  Container,
  Grid,
  MediaQuery,
  Stack,
} from '@mantine/core';
import { createStyles } from '@mantine/styles';
import { FC, memo, ReactNode } from 'react';
import { PreloadedQuery } from 'react-relay';
import { Sidebar } from './Sidebar.jsx';

const HEADER_SIZE = 56;

const useSectionStyles = createStyles((theme) => ({
  col: {
    [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
      padding: 0,
    },
  },

  stack: {
    height: '100%',
  },

  header: {
    height: HEADER_SIZE,
    flexShrink: 0,
    zIndex: 1,

    [`@media (max-width: ${theme.breakpoints.sm}px)`]: {
      padding: 8,
    },
  },
}));

interface SectionProps extends ColProps {
  Header?: FC;
}

export const Section: FC<SectionProps> = memo((props) => {
  const { className, span, Header, children } = props;
  const { classes, cx } = useSectionStyles();

  return (
    <Grid.Col
      py={0}
      span={span ?? 'auto'}
      className={cx(classes.col, className)}
    >
      <Stack spacing={0} className={classes.stack}>
        <Box className={classes.header}>{Header && <Header />}</Box>
        {children}
      </Stack>
    </Grid.Col>
  );
});

export const Nav: FC<SectionProps> = memo((props) => {
  console.log('rerender nav');

  return (
    <MediaQuery
      smallerThan="lg"
      styles={{
        display: 'none',
      }}
    >
      <Section {...props}>
        <Sidebar />
      </Section>
    </MediaQuery>
  );
});

export const Aside: FC<
  SectionProps & {
    Component: any;
    queryRef: PreloadedQuery<any, Record<string, unknown>>;
  }
> = memo((props) => {
  const { Component, Header, queryRef } = props;

  return (
    <MediaQuery
      smallerThan="lg"
      styles={{
        display: 'none',
      }}
    >
      <Section {...props}>
        <Component queryRef={queryRef} />
      </Section>
    </MediaQuery>
  );
});

const useStyles = createStyles((theme) => ({
  headerFix: {
    position: 'absolute',
    height: HEADER_SIZE,
    width: '100%',
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[9]
        : theme.colors[theme.primaryColor][4],
  },

  header: {
    height: HEADER_SIZE,
    flexShrink: 0,
    color: 'white',
    zIndex: 1,
    backgroundColor:
      theme.colorScheme === 'dark'
        ? theme.colors.dark[9]
        : theme.colors[theme.primaryColor][4],
  },

  container: {
    height: '100%',
    padding: 0,
  },

  grid: {
    height: '100%',
  },
}));

interface Props {
  children: ReactNode;
}

export const Layout: FC<Props> = memo((props) => {
  const { children } = props;
  const { classes } = useStyles();

  return (
    <>
      <Box className={classes.headerFix} />
      <Container size={1500} className={classes.container}>
        <Grid p={0} m={0} className={classes.grid}>
          {children}
        </Grid>
      </Container>
    </>
  );
});
