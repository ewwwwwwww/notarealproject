import { Logo } from './Logo.jsx';

import { graphql, useLazyLoadQuery } from 'react-relay';
import type { LogoMockQuery } from './__generated__/LogoMockQuery.graphql.js';
import { FC } from 'react';

const query = graphql`
  query LogoMockQuery {
    instance {
      ...Logo_instance
    }
  }
`;

export const Loaded: FC = () => {
  const data = useLazyLoadQuery<LogoMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.instance) return <div>oops</div>;

  return <Logo instance={data.instance} />;
};

// export const Placehodler: FC = () => <Logo />;

export default {
  title: 'Layout / Logo',
};
