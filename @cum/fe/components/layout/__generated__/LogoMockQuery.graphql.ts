/**
 * @generated SignedSource<<091ce73ff41645433c801fef56e8564d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type LogoMockQuery$variables = {};
export type LogoMockQuery$data = {
  readonly instance: {
    readonly " $fragmentSpreads": FragmentRefs<"Logo_instance">;
  } | null;
};
export type LogoMockQuery = {
  response: LogoMockQuery$data;
  variables: LogoMockQuery$variables;
};

const node: ConcreteRequest = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "LogoMockQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Domain",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Logo_instance"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "LogoMockQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Domain",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "2602c13d7fd72671494411e0b665950e",
    "id": null,
    "metadata": {},
    "name": "LogoMockQuery",
    "operationKind": "query",
    "text": "query LogoMockQuery {\n  instance {\n    ...Logo_instance\n    id\n  }\n}\n\nfragment Logo_instance on Domain {\n  id\n  name\n}\n"
  }
};

(node as any).hash = "bdef025aaf5b7f310f904567ba7cf82c";

export default node;
