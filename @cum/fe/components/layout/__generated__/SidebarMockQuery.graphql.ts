/**
 * @generated SignedSource<<5ed76ffd702a1f08271bb3cdd100562c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type SidebarMockQuery$variables = {};
export type SidebarMockQuery$data = {
  readonly me: {
    readonly id: ID;
    readonly " $fragmentSpreads": FragmentRefs<"Sidebar_user">;
  } | null;
};
export type SidebarMockQuery = {
  response: SidebarMockQuery$data;
  variables: SidebarMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "SidebarMockQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Sidebar_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "SidebarMockQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "countNotificationsUnread",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "be13821f5be3abec9d1f6ffc9ead3718",
    "id": null,
    "metadata": {},
    "name": "SidebarMockQuery",
    "operationKind": "query",
    "text": "query SidebarMockQuery {\n  me {\n    id\n    ...Sidebar_user\n  }\n}\n\nfragment Sidebar_user on User {\n  id\n  countNotificationsUnread\n  ...UserButton_user\n}\n\nfragment UserButton_user on User {\n  id\n}\n"
  }
};
})();

(node as any).hash = "cb7b12c2a7a93a95423ea10962095b38";

export default node;
