/**
 * @generated SignedSource<<c736f2670cb2d4ab91be63ccbcf2f2c2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type Sidebar_user$data = {
  readonly countNotificationsUnread: number;
  readonly id: ID;
  readonly " $fragmentSpreads": FragmentRefs<"UserButton_user">;
  readonly " $fragmentType": "Sidebar_user";
};
export type Sidebar_user$key = {
  readonly " $data"?: Sidebar_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"Sidebar_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "Sidebar_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "countNotificationsUnread",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserButton_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "d70c440c5dae32e79dd7a3465313b2d5";

export default node;
