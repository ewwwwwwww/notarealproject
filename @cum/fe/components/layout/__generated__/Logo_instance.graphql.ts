/**
 * @generated SignedSource<<84a95a418c6e241a880d1ebcdbb782ad>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type Logo_instance$data = {
  readonly id: ID;
  readonly name: string | null;
  readonly " $fragmentType": "Logo_instance";
};
export type Logo_instance$key = {
  readonly " $data"?: Logo_instance$data;
  readonly " $fragmentSpreads": FragmentRefs<"Logo_instance">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "Logo_instance",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Domain",
  "abstractKey": null
};

(node as any).hash = "0804b8645a01b924891d820dec742341";

export default node;
