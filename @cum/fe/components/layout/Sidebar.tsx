import type { Sidebar_user$key } from './__generated__/Sidebar_user.graphql.js';

import { FC, memo } from 'react';
import { graphql } from 'relay-runtime';
import { useFragment } from 'react-relay';
import { useEditor } from 'components/editor/useEditor.js';
import { Card } from 'components/core/Card.jsx';
import { Button } from '@mantine/core';
import { NavButton } from '../buttons/NavButton.jsx';
import { t, Trans } from '@lingui/macro';

// TODO: check if react-icons has hi2
import { BiHome as IconHome } from 'react-icons/bi';
import {
  IoMdPeople as IconLocal,
  IoMdInformationCircleOutline as IconInfo,
} from 'react-icons/io';
import {
  HiGlobe as IconGlobe,
  HiOutlineBookmark as IconBookmark,
} from 'react-icons/hi';
import { HiOutlineTrendingUp as IconTrending } from 'react-icons/hi';
import { RiEdit2Fill as IconPen, RiMoreLine as IconMore } from 'react-icons/ri';
// import { HiGlobeAmericas as IconGlobe } from "react-icons/hi2";
import { HiOutlineInbox as IconInbox } from 'react-icons/hi';
import { VscSettings as IconSettings } from 'react-icons/vsc';
import { useMe } from 'hooks/useMe.js';

const fragment = graphql`
  fragment Sidebar_user on User {
    id
    countNotificationsUnread
    ...UserButton_user
  }
`;

export const Sidebar: FC = memo(() => {
  const unread = useMe((state) => state.data?.countNotificationsUnread);
  const open = useEditor((state) => state.open);

  return (
    <Card>
      <Card.Section></Card.Section>

      <Card.Section py="xs">
        <NavButton href="/" label={t`Home`} Icon={IconHome} />
        <NavButton label={t`Instance`} Icon={IconLocal} />
        <NavButton label={t`Fediverse`} Icon={IconGlobe} />
        <NavButton label={t`Trending`} Icon={IconTrending} />
        <NavButton label={t`Inbox`} Icon={IconInbox} count={unread} />
        <NavButton label={t`Bookmarks`} Icon={IconBookmark} />
        <NavButton label={t`About`} Icon={IconInfo} />
        <NavButton href="/settings" label={t`Settings`} Icon={IconSettings} />
        <NavButton label={t`More`} Icon={IconMore} />
      </Card.Section>

      <Button
        radius="lg"
        rightIcon={<IconPen size={20} />}
        onClick={() => open()}
        fullWidth
      >
        <Trans>Compose</Trans>
      </Button>
    </Card>
  );
});
