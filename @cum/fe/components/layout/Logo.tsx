import { Anchor } from '@mantine/core';
import { createStyles } from '@mantine/styles';
import { FC, memo } from 'react';
import { useFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { Logo_instance$key } from './__generated__/Logo_instance.graphql.js';

const fragment = graphql`
  fragment Logo_instance on Domain {
    id
    name
  }
`;

interface Props {
  instance: Logo_instance$key | null;
}

const useStyles = createStyles((theme) => ({
  logo: {
    color: 'white',
    flexGrow: 1,
    letterSpacing: 3,
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    // width: 'calc(60%)',
    fontFamily: 'Futura',
  },
}));

export const Logo: FC<Props> = memo((props) => {
  const data = useFragment(fragment, props.instance);
  const { classes } = useStyles();

  return (
    <div>
      <Anchor href="/" className={classes.logo}>
        {data?.name ?? 'SITE LOGO'}
      </Anchor>
    </div>
  );
});
