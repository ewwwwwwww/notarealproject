import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { Sidebar } from './Sidebar.jsx';
import type { SidebarMockQuery } from './__generated__/SidebarMockQuery.graphql.js';

export const query = graphql`
  query SidebarMockQuery {
    me {
      id
      ...Sidebar_user
    }
  }
`;

export const Loaded: FC = () => {
  const data = useLazyLoadQuery<SidebarMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.me) return <div>oops</div>;

  return <Sidebar user={data.me} />;
};

export default {
  title: 'Layout / Sidebar',
};
