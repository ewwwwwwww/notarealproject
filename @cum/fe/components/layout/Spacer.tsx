import { FC } from 'react';
import { Space } from '@mantine/core';

export const Spacer: FC = () => <Space h="lg" />;
