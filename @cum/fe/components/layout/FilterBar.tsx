import {
  ActionIcon,
  Box,
  Button,
  createStyles,
  Group,
  HoverCard,
  MediaQuery,
  Text,
  Tooltip,
} from '@mantine/core';
import { FC, memo, useState } from 'react';
import { FilterHover, FilterData } from './FilterHover.jsx';

import {
  FiChevronDown as IconChevronDown,
  FiChevronUp as IconChevronUp,
} from 'react-icons/fi';
import { HiCheck as IconClear } from 'react-icons/hi';
import { IoMdSearch as IconSearch } from 'react-icons/io';
import { IoClose as IconClose } from 'react-icons/io5';

interface Props {
  data?: FilterData[];
  label?: string;
  searchable?: boolean;
  searchLabel?: string;
  unread?: number;
  unreadLabel?: string;
  onReadAll?: () => void;
  onScrollTop?: () => void;
  onSearchOpen?: () => void;
  onSearchClose?: () => void;
  onSearchChange?: (value: string) => void;
}

const useStyles = createStyles((theme) => ({
  button: {
    backgroundColor:
      theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.white,
    color:
      theme.colorScheme === 'dark'
        ? theme.colors[theme.primaryColor][2]
        : theme.colors[theme.primaryColor][7],
  },
}));

export const FilterBar: FC<Props> = memo((props) => {
  const {
    data,
    label,
    searchable,
    searchLabel,
    unread,
    unreadLabel,
    onReadAll,
    onScrollTop,
    onSearchOpen,
    onSearchClose,
    onSearchChange,
  } = props;

  const { classes } = useStyles();

  return (
    <Group
      spacing="xs"
      sx={{ flexWrap: 'nowrap', color: 'white', height: '100%' }}
    >
      {data && (
        <HoverCard>
          <HoverCard.Target>
            <Button
              variant="light"
              size="xs"
              rightIcon={<IconChevronDown size={16} />}
              className={classes.button}
            >
              Filters
            </Button>
          </HoverCard.Target>
          <HoverCard.Dropdown>
            <FilterHover data={data} />
          </HoverCard.Dropdown>
        </HoverCard>
      )}

      <Box sx={{ flexGrow: '1 !important', overflow: 'hidden' }}>
        <Text
          component="div"
          sx={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
          }}
        >
          {searchLabel ?? label ?? ''}
        </Text>
      </Box>

      {unread && (
        <Tooltip label={`Mark items as read`} openDelay={300}>
          <Button
            variant="light"
            size="xs"
            rightIcon={<IconClear size={16} />}
            onClick={onReadAll}
            className={classes.button}
          >
            {unread} {unreadLabel}
          </Button>
        </Tooltip>
      )}

      {searchable && !searchLabel && (
        <Tooltip label={`Search content`} openDelay={300}>
          <ActionIcon
            variant="light"
            size={30}
            onClick={searchLabel ? onSearchClose : onSearchOpen}
            className={classes.button}
          >
            {searchLabel ? <IconClose size={16} /> : <IconSearch size={16} />}
          </ActionIcon>
        </Tooltip>
      )}

      {onScrollTop && (
        <Tooltip label={`Scroll to top`} openDelay={300}>
          <ActionIcon
            variant="light"
            size={30}
            onClick={onScrollTop}
            className={classes.button}
          >
            <IconChevronUp size={16} />
          </ActionIcon>
        </Tooltip>
      )}
    </Group>
  );
});
