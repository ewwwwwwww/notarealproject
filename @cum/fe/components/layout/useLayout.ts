import create, { createStore } from 'zustand';

export type AsideType = null | 'INBOX' | 'USER';

export function isAsideType(x: string | null): x is AsideType {
  return x === null || x === 'INBOX' || x === 'USER';
}

interface State {
  aside: null | 'INBOX' | 'USER';

  setAside: (aside: AsideType) => void;
}

export const layoutStore = createStore<State>((set, get) => ({
  aside: null,

  setAside: (aside) => {
    // if (!isAsideType(aside)) {
    //   throw new Error(`aside type \`${aside}\` is invalid, check aside export`);
    // }

    set({ ...get(), aside });
  },
}));

export const useLayout = create(layoutStore);
