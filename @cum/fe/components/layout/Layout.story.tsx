import { FC } from 'react';
import { Layout as Comp } from './Layout.jsx';

export const Layout: FC = () => {
  return <Comp>Test</Comp>;
};

export default {
  title: 'Layout',
};
