import { useMemoSubscription } from 'hooks/useMemoSubscription.js';
import { graphql } from 'relay-runtime';
import type { useNoteUpdatedBodySubscription } from './__generated__/useNoteUpdatedBodySubscription.graphql.js';

const subscription = graphql`
  subscription useNoteUpdatedBodySubscription {
    listen(topic: "note:updated:body") {
      relatedNode {
        ... on Note {
          id
          title
          body
        }
      }
    }
  }
`;

export const useNoteUpdatedBody = () => {
  return useMemoSubscription<useNoteUpdatedBodySubscription>({
    subscription,
    variables: {},
  });
};
