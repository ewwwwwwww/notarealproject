import { FC, ReactNode, useRef, Suspense, memo, useEffect } from 'react';
import { usePaginationFragment } from 'react-relay';
import { graphql } from 'relay-runtime';
import type { TimelineFragment$key } from './__generated__/TimelineFragment.graphql.js';
import { useTimeline } from 'components/timeline/useTimeline.js';
import { NoteCard } from 'components/note/NoteCard.jsx';
import { useNoteCreated } from './useNoteCreated.js';
import { useNoteUpdatedBody } from './useNoteUpdatedBody.js';
import { useNoteDeleted } from './useNoteDeleted.js';
import { Virtuoso } from 'react-virtuoso';
import { useScrollStyles } from './useScrollStyles.js';
import { motion } from 'framer-motion';

export { TimelineBar } from './TimelineBar.jsx';

const fragment = graphql`
  fragment TimelineFragment on Query
  @argumentDefinitions(
    cursor: { type: "Cursor" }
    count: { type: "Int", defaultValue: 10 }
    condition: { type: "NoteCondition", defaultValue: {} }
    orderBy: { type: "NotesOrderBy", defaultValue: CTIME_DESC }
  )
  @refetchable(queryName: "TimelinePaginationQuery") {
    notes(
      after: $cursor
      first: $count
      condition: $condition
      orderBy: [$orderBy]
    ) @connection(key: "TimelineFragment_notes") {
      __id
      edges {
        cursor
        node {
          id
          ...NoteCard_note
        }
      }
    }
  }
`;

interface Props {
  data: TimelineFragment$key;
  children?: ReactNode | ReactNode[];
}

export const Timeline: FC<Props> = (props) => {
  const { children } = props;
  const { data, loadNext } = usePaginationFragment(fragment, props.data);
  const { classes } = useScrollStyles(false);

  const setVirtuoso = useTimeline((state) => state.setVirtuoso);
  const setScrolling = useTimeline((state) => state.setScrolling);
  const setId = useTimeline((state) => state.setId);

  const connectionID = data.notes?.__id ?? '';
  const virtuoso = useRef(null);

  setId(connectionID);
  setVirtuoso(virtuoso.current);

  if (!data.notes) return null;

  useNoteCreated(connectionID);
  useNoteUpdatedBody();
  useNoteDeleted();

  const edges = data.notes.edges;
  const Header = children ? memo(() => <>{children}</>) : undefined;

  return (
    <Virtuoso
      ref={virtuoso}
      className={classes.scroller}
      data={edges}
      components={{ Header }}
      endReached={() => {
        loadNext(10);
      }}
      isScrolling={setScrolling}
      initialItemCount={edges.length > 10 ? 11 : edges.length}
      itemContent={(idx, edge) => {
        return <NoteCard key={idx} data={edge.node} />;
      }}
    />
  );
};
