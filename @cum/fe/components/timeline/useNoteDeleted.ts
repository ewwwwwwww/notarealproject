import { useMemoSubscription } from 'hooks/useMemoSubscription.js';
import { graphql } from 'relay-runtime';
import type { useNoteDeletedSubscription } from './__generated__/useNoteDeletedSubscription.graphql.js';

const subscription = graphql`
  subscription useNoteDeletedSubscription {
    listen(topic: "note:deleted") {
      relatedNode {
        ... on Note {
          id @deleteRecord
        }
      }
    }
  }
`;

export const useNoteDeleted = () => {
  return useMemoSubscription<useNoteDeletedSubscription>({
    subscription,
    variables: {},

    onNext(next) {
      console.log('deleted', next);
    },
  });
};
