/**
 * @generated SignedSource<<0d4a2b04f75e1a24bac5370a5c12617c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
export type useNoteUpdatedBodySubscription$variables = {};
export type useNoteUpdatedBodySubscription$data = {
  readonly listen: {
    readonly relatedNode: {
      readonly body?: string | null;
      readonly id?: ID;
      readonly title?: string | null;
    } | null;
  };
};
export type useNoteUpdatedBodySubscription = {
  response: useNoteUpdatedBodySubscription$data;
  variables: useNoteUpdatedBodySubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "topic",
    "value": "note:updated:body"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "title",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteUpdatedBodySubscription",
    "selections": [
      {
        "alias": null,
        "args": (v0/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "kind": "InlineFragment",
                "selections": [
                  (v1/*: any*/),
                  (v2/*: any*/),
                  (v3/*: any*/)
                ],
                "type": "Note",
                "abstractKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": "listen(topic:\"note:updated:body\")"
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "useNoteUpdatedBodySubscription",
    "selections": [
      {
        "alias": null,
        "args": (v0/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "__typename",
                "storageKey": null
              },
              (v1/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": [
                  (v2/*: any*/),
                  (v3/*: any*/)
                ],
                "type": "Note",
                "abstractKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": "listen(topic:\"note:updated:body\")"
      }
    ]
  },
  "params": {
    "cacheID": "cd811f5c8311d7b642bdc3e0833d8f36",
    "id": null,
    "metadata": {},
    "name": "useNoteUpdatedBodySubscription",
    "operationKind": "subscription",
    "text": "subscription useNoteUpdatedBodySubscription {\n  listen(topic: \"note:updated:body\") {\n    relatedNode {\n      __typename\n      ... on Note {\n        id\n        title\n        body\n      }\n      id\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "815a9d5e21ebced6d1dc30c3c45312b5";

export default node;
