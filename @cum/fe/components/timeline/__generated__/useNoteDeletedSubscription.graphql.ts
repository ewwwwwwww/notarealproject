/**
 * @generated SignedSource<<9146a5a74d400dc57f0d82fbbbb9887d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
export type useNoteDeletedSubscription$variables = {};
export type useNoteDeletedSubscription$data = {
  readonly listen: {
    readonly relatedNode: {
      readonly id?: ID;
    } | null;
  };
};
export type useNoteDeletedSubscription = {
  response: useNoteDeletedSubscription$data;
  variables: useNoteDeletedSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "Literal",
    "name": "topic",
    "value": "note:deleted"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "useNoteDeletedSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v0/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "kind": "InlineFragment",
                "selections": [
                  (v1/*: any*/)
                ],
                "type": "Note",
                "abstractKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": "listen(topic:\"note:deleted\")"
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "useNoteDeletedSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v0/*: any*/),
        "concreteType": "ListenPayload",
        "kind": "LinkedField",
        "name": "listen",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "relatedNode",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "__typename",
                "storageKey": null
              },
              (v1/*: any*/)
            ],
            "storageKey": null
          }
        ],
        "storageKey": "listen(topic:\"note:deleted\")"
      }
    ]
  },
  "params": {
    "cacheID": "afc8714a7225c782ee3b4f0cf6b4fb58",
    "id": null,
    "metadata": {},
    "name": "useNoteDeletedSubscription",
    "operationKind": "subscription",
    "text": "subscription useNoteDeletedSubscription {\n  listen(topic: \"note:deleted\") {\n    relatedNode {\n      __typename\n      ... on Note {\n        id\n      }\n      id\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "67f9e96b7717b7ff7bce3593655c95e4";

export default node;
