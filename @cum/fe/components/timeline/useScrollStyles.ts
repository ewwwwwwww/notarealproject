import { createStyles } from '@mantine/core';

// TODO: add styled scrollbar support and setting
export const useScrollStyles = createStyles((theme, visible: boolean) => ({
  scroller: {
    height: '100%',

    // firefox
    scrollbarWidth: visible ? 'thin' : 'none',
    scrollbarColor: '#8f54a0 #ffffff',

    /* Chrome, Edge, and Safari */
    '&::-webkit-scrollbar': {
      width: visible ? 12 : 0,
    },

    '&::-webkit-scrollbar-track': {
      background: '#ffffff',
    },

    '&::-webkit-scrollbar-thumb': {
      backgroundColor: theme.colors[theme.primaryColor][2],
      borderRadius: 10,
      border: '3px solid #ffffff',
    },
  },
}));
