import { FC, memo } from 'react';
import { FilterBar } from 'components/layout/FilterBar.jsx';
import { FilterData } from 'components/layout/FilterHover.jsx';
import { useTimeline } from 'components/timeline/useTimeline.js';

interface Props {}

export const TimelineBar: FC<Props> = memo((props) => {
  const scrollTop = useTimeline((state) => state.scrollTop);

  const data: FilterData[] = [];

  return (
    <FilterBar
      data={data}
      label="Timeline"
      searchable
      onScrollTop={scrollTop}
    />
  );
});
