import {
  NoteCondition,
  NotesOrderBy,
} from './__generated__/TimelinePaginationQuery.graphql.js';
import create from 'zustand';

import { createTimelineStore } from 'hooks/createTimelineStore.js';

export const timelineStore = createTimelineStore<NotesOrderBy, NoteCondition>(
  'CTIME_DESC',
);

export const useTimeline = create(timelineStore);
