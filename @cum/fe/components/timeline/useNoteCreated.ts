import { useMemoSubscription } from 'hooks/useMemoSubscription.js';
import { graphql } from 'relay-runtime';
import type { useNoteCreatedSubscription } from './__generated__/useNoteCreatedSubscription.graphql.js';

const subscription = graphql`
  subscription useNoteCreatedSubscription($connections: [ID!]!) {
    listen(topic: "note:created") {
      relatedNode
        @prependNode(connections: $connections, edgeTypeName: "NotesEdge") {
        ... on Note {
          id
          ...NoteCard_note
        }
      }
    }
  }
`;

export const useNoteCreated = (connectionID: string) => {
  return useMemoSubscription<useNoteCreatedSubscription>({
    subscription,
    variables: {
      connections: [connectionID],
    },
  });
};
