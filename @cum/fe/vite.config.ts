import react from '@vitejs/plugin-react';
import ssr from 'vite-plugin-ssr/plugin';
import { renderPage } from 'vite-plugin-ssr';
import relay from 'vite-plugin-relay';
import { defineConfig } from 'vite';
import { config } from '@cum/config';
import cookie from 'cookie';

// @ts-ignore
import { resolve } from 'node:path';

// @ts-ignore
import ladle from './stories/ladle/vite-plugin.js';

const headers = {
  name: 'header-plugin',
  configureServer(server: any) {
    return () => {
      server.middlewares.use(async (req: any, res: any, next: any) => {
        if (res.headersSent) return next();
        if (!(req.originalUrl || req.url)) return next();

        const initialPageContext = {
          urlOriginal: req.originalUrl || req.url,
          userAgent: req.headers['user-agent'],
          cookiesInit: req.headers['cookie'],
        };

        const pageContext = await renderPage(initialPageContext);

        if (!pageContext.httpResponse) return next();

        res.setHeader('Content-Type', pageContext.httpResponse.contentType);
        res.writeHead(pageContext.httpResponse.statusCode);

        pageContext.httpResponse.pipe(res);
      });
    };
  },
};

// eslint-disable-next-line import/no-default-export
export default defineConfig(({ mode }) => ({
  server: {
    port: config.frontend.port,
    proxy: {
      '/file': {
        target: 'http://localhost:5010',
        changeOrigin: true,
        secure: false,
      },
      '/graphql': {
        target: 'http://localhost:5010',
        changeOrigin: true,
        secure: false,
      },
    },
  },

  esbuild: {
    drop: mode === 'production' ? ['console', 'debugger'] : [],
  },

  resolve: {
    alias: {
      // @ts-ignore
      hooks: resolve(__dirname, 'hooks'),
      // @ts-ignore
      core: resolve(__dirname, 'core'),
      // @ts-ignore
      components: resolve(__dirname, 'components'),
      // @ts-ignore
      locales: resolve(__dirname, 'locales'),
      // @ts-ignore
      aside: resolve(__dirname, 'aside'),
    },
  },

  plugins: [
    relay,

    // @ts-ignore
    react({
      babel: {
        plugins: ['macros'],
      },
    }),

    headers,

    // @ts-ignore
    ssr({
      prerender: false,
    }),

    ladle(
      {
        stories: 'components/**/*.story.{js,jsx,ts,tsx}',
        addons: {
          source: { enabled: true },
        },
      },
      '',
      '',
    ),

    // viteStaticCopy({
    //   targets: [{ src: '../fixtures/emoji', dest: 'assets' }],
    // }),
  ],

  optimizeDeps: {
    include: ['cross-fetch'],
  },

  // We manually add a list of dependencies to be pre-bundled, in order to avoid a page reload at dev start which breaks vite-plugin-ssr's CI
  // optimizeDeps: {
  //   disabled: false,
  //   include: [
  //     'cross-fetch',
  //     'react-relay',
  //     'react-dom/client',
  //     'react/jsx-runtime',
  //     '@mantine/core',
  //     'react-icons',
  //     // 'jsdom',
  //   ],
  // },

  assetsInclude: ['**/*.graphql'],
}));
