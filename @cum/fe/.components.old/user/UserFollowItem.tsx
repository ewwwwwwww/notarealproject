import type { UserFollowItem_user$key } from './__generated__/UserFollowItem_user.graphql.js';

import { FC, memo } from 'react';
import { graphql, useFragment } from 'react-relay';
import { Trans } from '@lingui/macro';

import {
  UnstyledButton,
  Grid,
  Button,
  Group,
  Box,
  Text,
  Stack,
  createStyles,
} from '@mantine/core';

import { SuspenseWrapper } from '../core/SuspenseWrapper.jsx';
import { Card } from '../core/Card.jsx';
import { UserAvatar } from './UserAvatar.jsx';
import { UserFollowButton } from './UserFollowButton.jsx';
import { ShortDate } from '../core/ShortDate.jsx';

const fragment = graphql`
  fragment UserFollowItem_user on User {
    nick
    nameFull
    ...UserAvatar_user
    ...UserFollowButton_user
  }
`;

const useStyles = createStyles((theme) => ({
  user: {
    display: 'block',
    width: '100%',
    padding: theme.spacing.md,
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[8]
          : theme.colors.gray[0],
    },
  },

  nameFull: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: 'calc(100%)',
  },
}));

interface Props {
  user: UserFollowItem_user$key;
  date: Date;
}

// TODO: placeholder
// FIXME: timeago
const Placeholder: FC = () => <div />;

const Loaded: FC<Props> = memo((props) => {
  const data = useFragment(fragment, props.user);
  const { classes: cl } = useStyles();

  return (
    <Card mb="lg" p={0}>
      <Grid gutter="sm" align="center">
        <Grid.Col span={2} sx={{ maxWidth: 100 }}>
          <UserAvatar user={data} size={100} radius={0} />
        </Grid.Col>
        <Grid.Col span={7} pl={20}>
          <Stack spacing={0}>
            <Text size="md" weight={500}>
              {data.nick}
            </Text>
            <Text color="dimmed" size="md" className={cl.nameFull}>
              {data.nameFull}
            </Text>
            <Text color="dimmed">
              following since <ShortDate date={props.date} long />
            </Text>
          </Stack>
        </Grid.Col>
        <Grid.Col span={3} pl={10}>
          <Stack>
            <UserFollowButton user={data} variant="light" />
          </Stack>
        </Grid.Col>
      </Grid>
    </Card>
  );
});

export const UserFollowItem = SuspenseWrapper(Loaded);
