/**
 * @generated SignedSource<<e1057df31fa7766efc7765023666eb9f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserStatMore_user$data = {
  readonly blockers: {
    readonly totalCount: number;
  };
  readonly blocking: {
    readonly totalCount: number;
  };
  readonly ctime: any;
  readonly notes: {
    readonly totalCount: number;
  };
  readonly " $fragmentType": "UserStatMore_user";
};
export type UserStatMore_user$key = {
  readonly " $data"?: UserStatMore_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserStatMore_user">;
};

const node: ReaderFragment = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "totalCount",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserStatMore_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ctime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "orderBy",
          "value": "CTIME_DESC"
        }
      ],
      "concreteType": "NotesConnection",
      "kind": "LinkedField",
      "name": "notes",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": "notes(orderBy:\"CTIME_DESC\")"
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "BlocksConnection",
      "kind": "LinkedField",
      "name": "blocking",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "BlocksConnection",
      "kind": "LinkedField",
      "name": "blockers",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "a88966413d85662a813fc35f8574a118";

export default node;
