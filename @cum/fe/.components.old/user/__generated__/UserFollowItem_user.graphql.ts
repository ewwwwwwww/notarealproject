/**
 * @generated SignedSource<<21760f9b4ea05a955088267950b99dc1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserFollowItem_user$data = {
  readonly nameFull: string;
  readonly nick: string | null;
  readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user" | "UserFollowButton_user">;
  readonly " $fragmentType": "UserFollowItem_user";
};
export type UserFollowItem_user$key = {
  readonly " $data"?: UserFollowItem_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserFollowItem_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserFollowItem_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nick",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nameFull",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserFollowButton_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "d4c058e90da4b3efbccf3394e7883509";

export default node;
