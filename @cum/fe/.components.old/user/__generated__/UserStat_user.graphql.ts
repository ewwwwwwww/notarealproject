/**
 * @generated SignedSource<<6731a69371a940100f167bc9adc6ab74>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserStat_user$data = {
  readonly followers: {
    readonly totalCount: number;
  };
  readonly following: {
    readonly totalCount: number;
  };
  readonly notes: {
    readonly totalCount: number;
  };
  readonly " $fragmentType": "UserStat_user";
};
export type UserStat_user$key = {
  readonly " $data"?: UserStat_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserStat_user">;
};

const node: ReaderFragment = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "totalCount",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserStat_user",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "orderBy",
          "value": "CTIME_DESC"
        }
      ],
      "concreteType": "NotesConnection",
      "kind": "LinkedField",
      "name": "notes",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": "notes(orderBy:\"CTIME_DESC\")"
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "FollowsConnection",
      "kind": "LinkedField",
      "name": "followers",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "FollowsConnection",
      "kind": "LinkedField",
      "name": "following",
      "plural": false,
      "selections": (v0/*: any*/),
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "2692a79d42a7166f01d9429d044eff58";

export default node;
