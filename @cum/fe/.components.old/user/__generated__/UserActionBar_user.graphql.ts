/**
 * @generated SignedSource<<113fd797b33dcccb30c5e7eab0f824a6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserActionBar_user$data = {
  readonly isBlockedByYou: boolean | null;
  readonly " $fragmentType": "UserActionBar_user";
};
export type UserActionBar_user$key = {
  readonly " $data"?: UserActionBar_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserActionBar_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserActionBar_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockedByYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "f91ccf6fefa3576bcbe2f6d3f069c1c8";

export default node;
