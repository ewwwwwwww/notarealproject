/**
 * @generated SignedSource<<b0601c59f8a7c2cf9f6f6054a1b53d76>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCard_user$data = {
  readonly name: string;
  readonly nameFull: string;
  readonly nick: string | null;
  readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user" | "UserCard_followsYou" | "UserFollowButton_user" | "UserStat_user">;
  readonly " $fragmentType": "UserCard_user";
};
export type UserCard_user$key = {
  readonly " $data"?: UserCard_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserCard_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserCard_user",
  "selections": [
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserStat_user"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserCard_followsYou"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserFollowButton_user"
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nick",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nameFull",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "0aab66ab120defcc99067133abaf3020";

export default node;
