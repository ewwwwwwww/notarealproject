/**
 * @generated SignedSource<<f8759d7c267e5ed9a165f503deac4ac1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserStatMoreMockQuery$variables = {
  userId: any;
};
export type UserStatMoreMockQuery$data = {
  readonly user: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"UserStatMore_user">;
  } | null;
};
export type UserStatMoreMockQuery = {
  response: UserStatMoreMockQuery$data;
  variables: UserStatMoreMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "totalCount",
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UserStatMoreMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserStatMore_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UserStatMoreMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "ctime",
            "storageKey": null
          },
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "orderBy",
                "value": "CTIME_DESC"
              }
            ],
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": (v3/*: any*/),
            "storageKey": "notes(orderBy:\"CTIME_DESC\")"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blocking",
            "plural": false,
            "selections": (v3/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blockers",
            "plural": false,
            "selections": (v3/*: any*/),
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "4fd5efde44a1fc8232bd5da35e9786a7",
    "id": null,
    "metadata": {},
    "name": "UserStatMoreMockQuery",
    "operationKind": "query",
    "text": "query UserStatMoreMockQuery(\n  $userId: UUID!\n) {\n  user(userId: $userId) {\n    id\n    ...UserStatMore_user\n  }\n}\n\nfragment UserStatMore_user on User {\n  ctime\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  blocking {\n    totalCount\n  }\n  blockers {\n    totalCount\n  }\n}\n"
  }
};
})();

(node as any).hash = "a8b50fddca2a808c47cd1b3cf6d7165b";

export default node;
