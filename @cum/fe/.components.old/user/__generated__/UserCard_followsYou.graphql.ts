/**
 * @generated SignedSource<<ebd5ed8b8c8d9cfc65f888c0b137d259>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCard_followsYou$data = {
  readonly isBlockingYou: boolean | null;
  readonly isFollowingYou: boolean | null;
  readonly " $fragmentType": "UserCard_followsYou";
};
export type UserCard_followsYou$key = {
  readonly " $data"?: UserCard_followsYou$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserCard_followsYou">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserCard_followsYou",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isFollowingYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isBlockingYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "f3b16a23e463db14a8587c9813e3f6a6";

export default node;
