/**
 * @generated SignedSource<<6e52067c60ad2e8ba1106465996f9c20>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserCardMockQuery$variables = {
  userId: any;
};
export type UserCardMockQuery$data = {
  readonly user: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"UserCard_user">;
  } | null;
};
export type UserCardMockQuery = {
  response: UserCardMockQuery$data;
  variables: UserCardMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v4 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "totalCount",
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UserCardMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserCard_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UserCardMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "nameFull",
            "storageKey": null
          },
          (v3/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Attachment",
            "kind": "LinkedField",
            "name": "avatar",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "blurhash",
                "storageKey": null
              },
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "src",
                "storageKey": null
              },
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "orderBy",
                "value": "CTIME_DESC"
              }
            ],
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": "notes(orderBy:\"CTIME_DESC\")"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "followers",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "following",
            "plural": false,
            "selections": (v4/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "nick",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "875bb9f587a29b6ac32a9e8d61d7fb87",
    "id": null,
    "metadata": {},
    "name": "UserCardMockQuery",
    "operationKind": "query",
    "text": "query UserCardMockQuery(\n  $userId: UUID!\n) {\n  user(userId: $userId) {\n    id\n    ...UserCard_user\n  }\n}\n\nfragment UserAvatar_user on User {\n  id\n  name\n  nameFull\n  remote\n  avatar {\n    blurhash\n    remote\n    src\n    id\n  }\n}\n\nfragment UserCard_followsYou on User {\n  isFollowingYou\n  isBlockingYou\n}\n\nfragment UserCard_user on User {\n  ...UserAvatar_user\n  ...UserStat_user\n  ...UserCard_followsYou\n  ...UserFollowButton_user\n  name\n  nick\n  nameFull\n}\n\nfragment UserFollowButton_user on User {\n  isFollowingYou\n  isFollowedByYou\n}\n\nfragment UserStat_user on User {\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  followers {\n    totalCount\n  }\n  following {\n    totalCount\n  }\n}\n"
  }
};
})();

(node as any).hash = "38962e473bef0094710317c69608bb19";

export default node;
