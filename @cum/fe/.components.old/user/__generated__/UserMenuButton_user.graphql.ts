/**
 * @generated SignedSource<<3280db8a411a567404dd33876e26dec0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserMenuButton_user$data = {
  readonly nameFull: string;
  readonly nick: string | null;
  readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user">;
  readonly " $fragmentType": "UserMenuButton_user";
};
export type UserMenuButton_user$key = {
  readonly " $data"?: UserMenuButton_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserMenuButton_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserMenuButton_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nick",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "nameFull",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "UserAvatar_user"
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "a63e189120fcf840204f51ffbd0f73a8";

export default node;
