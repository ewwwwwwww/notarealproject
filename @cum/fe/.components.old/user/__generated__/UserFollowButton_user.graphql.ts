/**
 * @generated SignedSource<<26fbc4e06700722c76d1d17b0bb67a16>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type UserFollowButton_user$data = {
  readonly isFollowedByYou: boolean | null;
  readonly isFollowingYou: boolean | null;
  readonly " $fragmentType": "UserFollowButton_user";
};
export type UserFollowButton_user$key = {
  readonly " $data"?: UserFollowButton_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"UserFollowButton_user">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "UserFollowButton_user",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isFollowingYou",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isFollowedByYou",
      "storageKey": null
    }
  ],
  "type": "User",
  "abstractKey": null
};

(node as any).hash = "4502c219e318bb87d8571d1e0495e5c2";

export default node;
