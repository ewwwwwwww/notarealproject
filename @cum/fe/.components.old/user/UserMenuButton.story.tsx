import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { UserMenuButton as Comp } from './UserMenuButton.jsx';
import type { UserMenuButtonMockQuery } from './__generated__/UserMenuButtonMockQuery.graphql.js';

const query = graphql`
  query UserMenuButtonMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...UserMenuButton_user
    }
  }
`;

export const UserMenuButton: FC = () => {
  const data = useLazyLoadQuery<UserMenuButtonMockQuery>(query, {
    userId: '766aca3c-3427-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'User',
};
