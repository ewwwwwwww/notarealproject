import type { UserMenuButton_user$key } from './__generated__/UserMenuButton_user.graphql.js';

import { FC, memo, ReactNode } from 'react';
import { graphql, useFragment } from 'react-relay';

import {
  UnstyledButton,
  Grid,
  Group,
  Box,
  Text,
  Stack,
  createStyles,
} from '@mantine/core';

import { FiChevronRight as Icon } from 'react-icons/fi';
import { UserAvatar } from './UserAvatar.jsx';

const fragment = graphql`
  fragment UserMenuButton_user on User {
    nick
    nameFull
    ...UserAvatar_user
  }
`;

const useStyles = createStyles((theme) => ({
  user: {
    display: 'block',
    width: '100%',
    padding: theme.spacing.md,
    color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

    '&:hover': {
      backgroundColor:
        theme.colorScheme === 'dark'
          ? theme.colors.dark[8]
          : theme.colors.gray[0],
    },
  },

  nameFull: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: 'calc(100%)',
  },
}));

interface Props {
  user: UserMenuButton_user$key;
}

export const UserMenuButton: FC<Props> = memo((props) => {
  const data = useFragment(fragment, props.user);
  const { classes: cl } = useStyles();

  return (
    <UnstyledButton className={cl.user}>
      <Grid gutter="sm" align="center">
        <Grid.Col span={2}>
          <UserAvatar user={data} size={38} />
        </Grid.Col>
        <Grid.Col span={9} pl={10}>
          <Stack spacing={0}>
            <Text size="sm" weight={500}>
              {data.nick}
            </Text>
            <Text color="dimmed" size="xs" className={cl.nameFull}>
              {data.nameFull}
            </Text>
          </Stack>
        </Grid.Col>
        <Grid.Col span={1}>
          <Icon size={14} />
        </Grid.Col>
      </Grid>
    </UnstyledButton>
  );
});
