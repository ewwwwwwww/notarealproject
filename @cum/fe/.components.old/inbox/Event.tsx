import { FC, ReactNode, memo } from 'react';
import { SuspenseWrapper } from '../core/SuspenseWrapper.jsx';
import { Box, Text, Group, Stack } from '@mantine/core';
import { IconType } from 'react-icons';
import { IconScope, ScopeType } from '../core/Scope.jsx';
import { ShortDate } from '../core/ShortDate.jsx';

interface Props {
  title: string | ReactNode;
  scope?: ScopeType;
  Icon?: IconType;
  date: Date;
  children?: ReactNode | ReactNode[];
}

// TODO: placeholder
const Placeholder: FC = () => <div></div>;

// FIXME: fix time delta
// https://gitlab.com/catamphetamine/react-time-ago/-/issues/4

// FIXME: locales
const Loaded: FC<Props> = memo((props) => {
  const { title, scope, date, Icon, children } = props;
  const sx = children ? {} : { paddingTop: 3, marginBottom: -3 };

  return (
    <Stack spacing={0}>
      <Group sx={sx} spacing="xs">
        <Text sx={{ flexGrow: '1 !important' }}>{title}</Text>
        <Text color="dimmed">
          <ShortDate date={date} />
        </Text>
        <Box sx={{ marginBottom: -6 }}>
          {scope && <IconScope size={18} scope={scope} outline />}
          {Icon && <Icon size={18} />}
        </Box>
      </Group>
      {children}
    </Stack>
  );
});

export const Event = SuspenseWrapper(Loaded);
