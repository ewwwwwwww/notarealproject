import type { InboxCard_user$key } from './__generated__/InboxCard_user.graphql.js';
import type { InboxCard_notification$key } from './__generated__/InboxCard_notification.graphql.js';

import { FC, memo } from 'react';
import { graphql, useFragment } from 'react-relay';
import { Timeline, ScrollArea } from '@mantine/core';
import { Spacer } from '../layout/Spacer.jsx';
import { Scroll } from '../layout/Scroller.jsx';
import { Card } from '../core/Card.jsx';
import { SuspenseWrapper } from '../core/SuspenseWrapper.jsx';
import { UserAvatar } from '../user/UserAvatar.jsx';

import { EventFollow } from './EventFollow.jsx';
import { EventMention } from './EventMention.jsx';
import { EventLike } from './EventLike.jsx';
import { EventRepeat } from './EventRepeat.jsx';
import { EventReact } from './EventReact.jsx';
import { EventBlock } from './EventBlock.jsx';

const fragment = graphql`
  fragment InboxCard_user on User {
    notifications(first: 100, orderBy: CTIME_DESC) {
      nodes {
        id
        read

        user {
          id
          name
          ...UserAvatar_user
        }

        ...InboxCard_notification
      }
    }
  }
`;

const fragmentNoti = graphql`
  fragment InboxCard_notification on Notification {
    ctime
    type

    follow {
      id
      ...EventFollow_follow
    }

    mention {
      id
      ...EventMention_mention
    }

    like {
      id
      ...EventLike_like
    }

    repeat {
      ...EventRepeat_repeat
    }

    react {
      ...EventReact_react
    }

    block {
      ...EventBlock_block
    }
  }
`;
interface NotiProps {
  noti: InboxCard_notification$key;
}

const Notification: FC<NotiProps> = memo((props) => {
  const data = useFragment(fragmentNoti, props.noti);
  const date = new Date(data.ctime);
  const type = data.type;

  let res = null;

  if (type === 'MENTION' && data.mention) {
    res = <EventMention date={date} mention={data.mention} />;
  } else if (type === 'FOLLOW' && data.follow) {
    res = <EventFollow date={date} follow={data.follow} />;
  } else if (type === 'LIKE' && data.like) {
    res = <EventLike date={date} like={data.like} />;
  } else if (type === 'REPEAT' && data.repeat) {
    res = <EventRepeat date={date} repeat={data.repeat} />;
  } else if (type === 'REACT' && data.react) {
    res = <EventReact date={date} react={data.react} />;
  } else if (type === 'BLOCK' && data.block) {
    res = <EventBlock date={date} block={data.block} />;
  }

  return res;
});

interface Props {
  user?: InboxCard_user$key | null;
}

// TODO: placeholder
const Placeholder: FC = () => <div></div>;

const Loaded: FC<Props> = memo((props) => {
  if (!props.user) return <Placeholder />;

  const data = useFragment(fragment, props.user);

  const items = data.notifications.nodes.map((item, idx) => {
    const bullet = item.user ? (
      <UserAvatar size={26} user={item.user} radius="xl" />
    ) : null;

    return (
      <Timeline.Item key={idx} bullet={bullet}>
        <Notification noti={item} />
      </Timeline.Item>
    );
  });

  return (
    <>
      <Spacer />
      <Card p="xs" pt="sm" pb="md" sx={{ overflow: 'visible' }}>
        <Timeline lineWidth={2} bulletSize={30} active={3}>
          {items}
        </Timeline>
      </Card>
    </>
  );
});

export const InboxCard = SuspenseWrapper(Loaded);
