/**
 * @generated SignedSource<<3cdfbc0ae9c7ec0c130834af305e54a3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventRepeat_repeat$data = {
  readonly user: {
    readonly id: string;
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventRepeat_repeat";
};
export type EventRepeat_repeat$key = {
  readonly " $data"?: EventRepeat_repeat$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventRepeat_repeat">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventRepeat_repeat",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Repeat",
  "abstractKey": null
};

(node as any).hash = "4f4a0d7897353eafa49c0a3fef79bc37";

export default node;
