/**
 * @generated SignedSource<<e068c78199e92877f961cbcc39da6cec>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type Scope = "DIRECT" | "LOCAL" | "PRIVATE" | "PUBLIC" | "UNLISTED" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type EventMention_mention$data = {
  readonly note: {
    readonly body: string | null;
    readonly id: string;
    readonly scope: Scope;
  } | null;
  readonly reply: boolean;
  readonly user: {
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventMention_mention";
};
export type EventMention_mention$key = {
  readonly " $data"?: EventMention_mention$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventMention_mention">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventMention_mention",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "reply",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Note",
      "kind": "LinkedField",
      "name": "note",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "scope",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "body",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Mention",
  "abstractKey": null
};

(node as any).hash = "91b79ccd13ca807d8a3f02189e42c3c9";

export default node;
