/**
 * @generated SignedSource<<74ac5eca450b8b228ca1eaf125ab56dd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventFollow_follow$data = {
  readonly follower: {
    readonly id: string;
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventFollow_follow";
};
export type EventFollow_follow$key = {
  readonly " $data"?: EventFollow_follow$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventFollow_follow">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventFollow_follow",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "follower",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Follow",
  "abstractKey": null
};

(node as any).hash = "8866fc261dcb2f36e9b556f1af4d8196";

export default node;
