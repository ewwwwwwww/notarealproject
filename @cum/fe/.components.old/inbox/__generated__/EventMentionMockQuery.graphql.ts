/**
 * @generated SignedSource<<489b27894d6681b84f626057c82f2a9d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventMentionMockQuery$variables = {
  mentionId: any;
};
export type EventMentionMockQuery$data = {
  readonly mention: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventMention_mention">;
  } | null;
};
export type EventMentionMockQuery = {
  response: EventMentionMockQuery$data;
  variables: EventMentionMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "mentionId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "mentionId",
    "variableName": "mentionId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventMentionMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Mention",
        "kind": "LinkedField",
        "name": "mention",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventMention_mention"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventMentionMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Mention",
        "kind": "LinkedField",
        "name": "mention",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "reply",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Note",
            "kind": "LinkedField",
            "name": "note",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "body",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "0e99a1715293c49b998ad5880a384ad2",
    "id": null,
    "metadata": {},
    "name": "EventMentionMockQuery",
    "operationKind": "query",
    "text": "query EventMentionMockQuery(\n  $mentionId: UUID!\n) {\n  mention(mentionId: $mentionId) {\n    id\n    ...EventMention_mention\n  }\n}\n\nfragment EventMention_mention on Mention {\n  reply\n  user {\n    name\n    id\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n"
  }
};
})();

(node as any).hash = "90236e6b6f3cd985d3a0f68a42e77863";

export default node;
