/**
 * @generated SignedSource<<5da05c79b3617f0f0da7fb9ec8da9ef4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventBlock_block$data = {
  readonly blocker: {
    readonly id: string;
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventBlock_block";
};
export type EventBlock_block$key = {
  readonly " $data"?: EventBlock_block$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventBlock_block">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventBlock_block",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "blocker",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Block",
  "abstractKey": null
};

(node as any).hash = "a7376be9e3a08397f410aa82d5898fec";

export default node;
