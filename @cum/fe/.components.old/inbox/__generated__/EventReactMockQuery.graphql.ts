/**
 * @generated SignedSource<<ab451debf1a5ca4ae1628bfa6fa49cfe>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventReactMockQuery$variables = {
  reactId: any;
};
export type EventReactMockQuery$data = {
  readonly react: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventReact_react">;
  } | null;
};
export type EventReactMockQuery = {
  response: EventReactMockQuery$data;
  variables: EventReactMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "reactId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "reactId",
    "variableName": "reactId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventReactMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "React",
        "kind": "LinkedField",
        "name": "react",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventReact_react"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventReactMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "React",
        "kind": "LinkedField",
        "name": "react",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "unicode",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Attachment",
            "kind": "LinkedField",
            "name": "attachment",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "remote",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "src",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "4084c63b5afa6c58463acae40d9a9ad1",
    "id": null,
    "metadata": {},
    "name": "EventReactMockQuery",
    "operationKind": "query",
    "text": "query EventReactMockQuery(\n  $reactId: UUID!\n) {\n  react(reactId: $reactId) {\n    id\n    ...EventReact_react\n  }\n}\n\nfragment Emoji_attachment on Attachment {\n  id\n  remote\n  src\n}\n\nfragment EventReact_react on React {\n  unicode\n  attachment {\n    ...Emoji_attachment\n    id\n  }\n  user {\n    id\n    name\n  }\n}\n"
  }
};
})();

(node as any).hash = "27eb3b8d3acc3beb0b943f1575e004b8";

export default node;
