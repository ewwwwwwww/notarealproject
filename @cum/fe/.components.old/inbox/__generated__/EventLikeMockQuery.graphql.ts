/**
 * @generated SignedSource<<edb815dc141a53b44064de19b2695789>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventLikeMockQuery$variables = {
  likeId: any;
};
export type EventLikeMockQuery$data = {
  readonly like: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventLike_like">;
  } | null;
};
export type EventLikeMockQuery = {
  response: EventLikeMockQuery$data;
  variables: EventLikeMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "likeId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "likeId",
    "variableName": "likeId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventLikeMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Like",
        "kind": "LinkedField",
        "name": "like",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventLike_like"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventLikeMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Like",
        "kind": "LinkedField",
        "name": "like",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "7139241849227c409475ccb19f565da2",
    "id": null,
    "metadata": {},
    "name": "EventLikeMockQuery",
    "operationKind": "query",
    "text": "query EventLikeMockQuery(\n  $likeId: UUID!\n) {\n  like(likeId: $likeId) {\n    id\n    ...EventLike_like\n  }\n}\n\nfragment EventLike_like on Like {\n  user {\n    id\n    name\n  }\n}\n"
  }
};
})();

(node as any).hash = "a80671b3282fe8527ec31e33b448a036";

export default node;
