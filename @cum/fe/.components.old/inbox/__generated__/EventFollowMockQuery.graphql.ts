/**
 * @generated SignedSource<<86aaab467d46e7feb710e6adc0286866>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventFollowMockQuery$variables = {
  followId: any;
};
export type EventFollowMockQuery$data = {
  readonly follow: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventFollow_follow">;
  } | null;
};
export type EventFollowMockQuery = {
  response: EventFollowMockQuery$data;
  variables: EventFollowMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "followId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "followId",
    "variableName": "followId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventFollowMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Follow",
        "kind": "LinkedField",
        "name": "follow",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventFollow_follow"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventFollowMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Follow",
        "kind": "LinkedField",
        "name": "follow",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "follower",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "6be88cea2e855d21a3fac9259af03698",
    "id": null,
    "metadata": {},
    "name": "EventFollowMockQuery",
    "operationKind": "query",
    "text": "query EventFollowMockQuery(\n  $followId: UUID!\n) {\n  follow(followId: $followId) {\n    id\n    ...EventFollow_follow\n  }\n}\n\nfragment EventFollow_follow on Follow {\n  follower {\n    id\n    name\n  }\n}\n"
  }
};
})();

(node as any).hash = "e1660d1e50a98d6caa85c809c76d9850";

export default node;
