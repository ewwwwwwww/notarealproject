/**
 * @generated SignedSource<<3d82ccb29939e9d8129d6fd6d9e7f9f9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventReact_react$data = {
  readonly attachment: {
    readonly " $fragmentSpreads": FragmentRefs<"Emoji_attachment">;
  } | null;
  readonly unicode: string | null;
  readonly user: {
    readonly id: string;
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventReact_react";
};
export type EventReact_react$key = {
  readonly " $data"?: EventReact_react$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventReact_react">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventReact_react",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "unicode",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Attachment",
      "kind": "LinkedField",
      "name": "attachment",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "Emoji_attachment"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "React",
  "abstractKey": null
};

(node as any).hash = "e7cf5bf49f6c7f705c9307244aba47f4";

export default node;
