/**
 * @generated SignedSource<<c5b31d12132537f46f0be7a49a01b47d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type NotificationType = "BLOCK" | "FOLLOW" | "LIKE" | "MENTION" | "REACT" | "REPEAT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type InboxCard_notification$data = {
  readonly block: {
    readonly " $fragmentSpreads": FragmentRefs<"EventBlock_block">;
  } | null;
  readonly ctime: any;
  readonly follow: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventFollow_follow">;
  } | null;
  readonly like: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventLike_like">;
  } | null;
  readonly mention: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventMention_mention">;
  } | null;
  readonly react: {
    readonly " $fragmentSpreads": FragmentRefs<"EventReact_react">;
  } | null;
  readonly repeat: {
    readonly " $fragmentSpreads": FragmentRefs<"EventRepeat_repeat">;
  } | null;
  readonly type: NotificationType;
  readonly " $fragmentType": "InboxCard_notification";
};
export type InboxCard_notification$key = {
  readonly " $data"?: InboxCard_notification$data;
  readonly " $fragmentSpreads": FragmentRefs<"InboxCard_notification">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "InboxCard_notification",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "ctime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Follow",
      "kind": "LinkedField",
      "name": "follow",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventFollow_follow"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Mention",
      "kind": "LinkedField",
      "name": "mention",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventMention_mention"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Like",
      "kind": "LinkedField",
      "name": "like",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventLike_like"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Repeat",
      "kind": "LinkedField",
      "name": "repeat",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventRepeat_repeat"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "React",
      "kind": "LinkedField",
      "name": "react",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventReact_react"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Block",
      "kind": "LinkedField",
      "name": "block",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EventBlock_block"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Notification",
  "abstractKey": null
};
})();

(node as any).hash = "0258ce1f7fd664238978ac5a50693410";

export default node;
