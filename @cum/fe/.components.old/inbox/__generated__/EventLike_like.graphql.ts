/**
 * @generated SignedSource<<a8329d415e473614e79a4e61c6880783>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventLike_like$data = {
  readonly user: {
    readonly id: string;
    readonly name: string;
  } | null;
  readonly " $fragmentType": "EventLike_like";
};
export type EventLike_like$key = {
  readonly " $data"?: EventLike_like$data;
  readonly " $fragmentSpreads": FragmentRefs<"EventLike_like">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EventLike_like",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Like",
  "abstractKey": null
};

(node as any).hash = "fff18e55a14c15be7ee79e37541771c5";

export default node;
