/**
 * @generated SignedSource<<02464f0ec87f019a5c0c91aae11437c0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventRepeatMockQuery$variables = {
  repeatId: any;
};
export type EventRepeatMockQuery$data = {
  readonly repeat: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventRepeat_repeat">;
  } | null;
};
export type EventRepeatMockQuery = {
  response: EventRepeatMockQuery$data;
  variables: EventRepeatMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "repeatId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "repeatId",
    "variableName": "repeatId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventRepeatMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Repeat",
        "kind": "LinkedField",
        "name": "repeat",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventRepeat_repeat"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventRepeatMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Repeat",
        "kind": "LinkedField",
        "name": "repeat",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "user",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "beafc11847d1be23cd2a7d9c4ecb2f7e",
    "id": null,
    "metadata": {},
    "name": "EventRepeatMockQuery",
    "operationKind": "query",
    "text": "query EventRepeatMockQuery(\n  $repeatId: UUID!\n) {\n  repeat(repeatId: $repeatId) {\n    id\n    ...EventRepeat_repeat\n  }\n}\n\nfragment EventRepeat_repeat on Repeat {\n  user {\n    id\n    name\n  }\n}\n"
  }
};
})();

(node as any).hash = "ea75fcb4cc4fa5b72ef82774810fa842";

export default node;
