/**
 * @generated SignedSource<<da313754476a4c4abe1438d04c9cf55e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EventBlockMockQuery$variables = {
  blockId: any;
};
export type EventBlockMockQuery$data = {
  readonly block: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"EventBlock_block">;
  } | null;
};
export type EventBlockMockQuery = {
  response: EventBlockMockQuery$data;
  variables: EventBlockMockQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "blockId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "blockId",
    "variableName": "blockId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EventBlockMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Block",
        "kind": "LinkedField",
        "name": "block",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "EventBlock_block"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EventBlockMockQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Block",
        "kind": "LinkedField",
        "name": "block",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "User",
            "kind": "LinkedField",
            "name": "blocker",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "6e595c6f886b808f99d81ea30308ae5e",
    "id": null,
    "metadata": {},
    "name": "EventBlockMockQuery",
    "operationKind": "query",
    "text": "query EventBlockMockQuery(\n  $blockId: UUID!\n) {\n  block(blockId: $blockId) {\n    id\n    ...EventBlock_block\n  }\n}\n\nfragment EventBlock_block on Block {\n  blocker {\n    id\n    name\n  }\n}\n"
  }
};
})();

(node as any).hash = "fdb2f490d2e426ef5cd81172e651d639";

export default node;
