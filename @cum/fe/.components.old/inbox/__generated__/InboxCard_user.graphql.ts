/**
 * @generated SignedSource<<6124e5209dadd9029fa8d47783b2fd70>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type InboxCard_user$data = {
  readonly notifications: {
    readonly nodes: ReadonlyArray<{
      readonly id: string;
      readonly read: boolean;
      readonly user: {
        readonly id: string;
        readonly name: string;
        readonly " $fragmentSpreads": FragmentRefs<"UserAvatar_user">;
      } | null;
      readonly " $fragmentSpreads": FragmentRefs<"InboxCard_notification">;
    }>;
  };
  readonly " $fragmentType": "InboxCard_user";
};
export type InboxCard_user$key = {
  readonly " $data"?: InboxCard_user$data;
  readonly " $fragmentSpreads": FragmentRefs<"InboxCard_user">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "InboxCard_user",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "first",
          "value": 100
        },
        {
          "kind": "Literal",
          "name": "orderBy",
          "value": "CTIME_DESC"
        }
      ],
      "concreteType": "NotificationsConnection",
      "kind": "LinkedField",
      "name": "notifications",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Notification",
          "kind": "LinkedField",
          "name": "nodes",
          "plural": true,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "read",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "User",
              "kind": "LinkedField",
              "name": "user",
              "plural": false,
              "selections": [
                (v0/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "name",
                  "storageKey": null
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "UserAvatar_user"
                }
              ],
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "InboxCard_notification"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "notifications(first:100,orderBy:\"CTIME_DESC\")"
    }
  ],
  "type": "User",
  "abstractKey": null
};
})();

(node as any).hash = "f33c9d8ddb6beb75b5141ed9142e7f08";

export default node;
