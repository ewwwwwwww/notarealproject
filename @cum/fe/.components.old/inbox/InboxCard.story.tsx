import { FC } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { InboxCard as Comp } from './InboxCard.js';
import type { InboxCardMockQuery } from './__generated__/InboxCardMockQuery.graphql.js';

const query = graphql`
  query InboxCardMockQuery($userId: UUID!) {
    user(userId: $userId) {
      id
      ...InboxCard_user
    }
  }
`;

export const InboxCard: FC = () => {
  const data = useLazyLoadQuery<InboxCardMockQuery>(query, {
    userId: '58146070-356c-11ed-a261-0242ac120002',
  });

  if (!data?.user) return <div>oops</div>;

  return <Comp user={data.user} />;
};

export default {
  title: 'Inbox',
};
