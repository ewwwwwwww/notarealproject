import type {
  ProfileQuery,
  ProfileQuery$data,
} from './__generated__/ProfileQuery.graphql.js';

import { FC } from 'react';
import { createStyles } from '@mantine/core';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { Virtuoso } from 'react-virtuoso';

import { useScrollStyles } from 'hooks/useScrollStyle.js';
import { Spacer } from '../layout/Spacer.jsx';
import { Scroll } from '../layout/Scroller.jsx';
import { Sidebar } from '../layout/Sidebar.jsx';
import { NoteCard } from '../note/NoteCard.jsx';
import { Header as H } from '../layout/Header.jsx';
import { Body as B } from '../layout/Body.jsx';
import { HeaderLogo } from '../layout/HeaderLogo.jsx';
import { HeaderBarTimeline } from '../layout/HeaderBarTimeline.jsx';

import { UserProfile } from '../user/UserProfile.jsx';
import { UserCard } from '../user/UserCard.jsx';
import { UserMediaThumb } from '../user/UserMediaThumb.jsx';
import { UserStatMore } from '../user/UserStatMore.jsx';

export const query = graphql`
  query ProfileQuery($userId: UUID!) {
    instance {
      ...HeaderLogo_instance
    }

    user(userId: $userId) {
      id
      ...UserCard_user
      ...UserProfile_user
      ...UserStatMore_user

      notes(orderBy: CTIME_DESC) {
        nodes {
          id
          ...NoteCard_note
        }
      }
    }

    me {
      id
      ...Sidebar_user
      ...InboxCard_user
    }
  }
`;

interface Props {
  userId: string;
}

const ProfileData: React.ComponentType<{ context?: ProfileQuery$data }> = ({
  context,
}) => {
  if (!context?.user) return null;

  return (
    <>
      <Spacer />
      <UserProfile user={context.user} />
      <Spacer />
      <UserMediaThumb />
      <Spacer />
    </>
  );
};

export const Profile: FC<Props> = (props) => {
  const data = useLazyLoadQuery<ProfileQuery>(query, { userId: props.userId });
  const { classes: cl } = useScrollStyles(false);

  if (!data?.user) return null;

  const loadMore = () => {};

  return (
    <>
      <H height={56}>
        <H.Left>
          <HeaderLogo instance={data.instance} />
        </H.Left>
        <H.Middle span={6}>
          <HeaderBarTimeline />
        </H.Middle>
        <H.Right span={3}> </H.Right>
      </H>
      <B headerHeight={56}>
        <B.Left>
          <Spacer />
          <Sidebar user={data.me} />
        </B.Left>
        <B.Middle span={6}>
          <Virtuoso
            className={cl.scroller}
            endReached={loadMore}
            context={data}
            components={{ Header: ProfileData }}
            data={data.user.notes.nodes}
            overscan={1000}
            itemContent={(idx, note) => <NoteCard key={idx} note={note} />}
          />
        </B.Middle>
        <B.Right span={3}>
          <Scroll sx={{ maxWidth: 300 }}>
            <Spacer />
            <UserCard user={data.user} />
            <Spacer />
            <UserStatMore user={data.user} />
          </Scroll>
        </B.Right>
      </B>
    </>
  );
};
