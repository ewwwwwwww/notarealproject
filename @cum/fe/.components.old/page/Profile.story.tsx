import { FC } from 'react';
import { Profile as Comp } from './Profile.jsx';

export const Profile: FC = () => (
  <Comp userId="766aca3c-3427-11ed-a261-0242ac120002" />
);

export default {
  title: 'Page',
};
