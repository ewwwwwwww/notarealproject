import type { TimelineQuery } from './__generated__/TimelineQuery.graphql.js';

import { FC, forwardRef } from 'react';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { Virtuoso } from 'react-virtuoso';
import { Body as B } from '../layout/Body.jsx';
import { Header as H } from '../layout/Header.jsx';
import { HeaderLogo } from '../layout/HeaderLogo.jsx';
import { useScrollStyles } from 'hooks/useScrollStyle.js';
import { HeaderBarTimeline } from '../layout/HeaderBarTimeline.jsx';
import { HeaderBarInbox } from '../layout/HeaderBarInbox.jsx';
import { Spacer } from '../layout/Spacer.jsx';
import { Sidebar } from '../layout/Sidebar.jsx';
import { NoteCard } from '../note/NoteCard.jsx';
import { InboxCard } from '../inbox/InboxCard.jsx';
import { Scroll } from '../layout/Scroller.jsx';

export const query = graphql`
  query TimelineQuery {
    instance {
      ...HeaderLogo_instance
    }

    notes {
      nodes {
        id
        ...NoteCard_note
      }
    }

    me {
      id
      ...Sidebar_user
      ...InboxCard_user
    }
  }
`;

interface Props {}

export const Timeline: FC<Props> = () => {
  const data = useLazyLoadQuery<TimelineQuery>(query, {});
  const { classes: cl } = useScrollStyles(false);

  const loadMore = () => {
    console.log('load more');
  };

  return (
    <>
      <H height={56}>
        <H.Left>
          <HeaderLogo instance={data.instance} />
        </H.Left>
        <H.Middle>
          <HeaderBarTimeline />
        </H.Middle>
        <H.Right>
          <HeaderBarInbox />
        </H.Right>
      </H>
      <B headerHeight={56}>
        <B.Left>
          <Spacer />
          <Sidebar user={data.me} />
        </B.Left>
        <B.Middle>
          <Virtuoso
            className={cl.scroller}
            endReached={loadMore}
            data={data.notes?.nodes}
            itemContent={(_, note) => <NoteCard key={note.id} note={note} />}
            components={{ Header: Spacer }}
          />
        </B.Middle>
        <B.Right>
          <InboxCard user={data.me} />
        </B.Right>
      </B>
    </>
  );
};
