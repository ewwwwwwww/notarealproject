import type {
  FollowsQuery,
  FollowsQuery$data,
} from './__generated__/FollowsQuery.graphql.js';

import { FC, memo } from 'react';
import { createStyles } from '@mantine/core';
import { graphql, useLazyLoadQuery } from 'react-relay';
import { Virtuoso } from 'react-virtuoso';

import { useScrollStyles } from 'hooks/useScrollStyle.js';
import { Spacer } from '../layout/Spacer.jsx';
import { Scroll } from '../layout/Scroller.jsx';
import { Sidebar } from '../layout/Sidebar.jsx';
import { UserCard } from '../user/UserCard.jsx';
import { UserStatMore } from '../user/UserStatMore.jsx';
import { UserFollowItem } from '../user/UserFollowItem.jsx';

import { Header as H } from '../layout/Header.jsx';
import { Body as B } from '../layout/Body.jsx';
import { HeaderLogo } from '../layout/HeaderLogo.jsx';
import { HeaderBarTimeline } from '../layout/HeaderBarTimeline.jsx';

export const query = graphql`
  query FollowsQuery($userId: UUID!) {
    instance {
      ...HeaderLogo_instance
    }

    user(userId: $userId) {
      id
      ...UserCard_user
      ...UserStatMore_user

      followers {
        totalCount
        nodes {
          id
          ctime
          follower {
            id
            ...UserFollowItem_user
          }
        }
      }

      following {
        totalCount
        nodes {
          id
          ctime
          followed {
            id
            ...UserFollowItem_user
          }
        }
      }
    }

    me {
      id
      ...Sidebar_user
      ...InboxCard_user
    }
  }
`;

interface Props {
  userId: string;
}

export const Follows: FC<Props> = memo((props) => {
  const data = useLazyLoadQuery<FollowsQuery>(query, { userId: props.userId });
  const { classes: cl } = useScrollStyles(false);

  if (!data?.user) return null;

  const loadMore = () => {};

  return (
    <>
      <H height={56}>
        <H.Left>
          <HeaderLogo instance={data.instance} />
        </H.Left>
        <H.Middle span={6}>
          <HeaderBarTimeline />
        </H.Middle>
        <H.Right span={3}> </H.Right>
      </H>
      <B headerHeight={56}>
        <B.Left>
          <Spacer />
          <Sidebar user={data.me} />
        </B.Left>
        <B.Middle span={6}>
          <Virtuoso
            className={cl.scroller}
            endReached={loadMore}
            context={data}
            data={data.user.followers.nodes}
            overscan={1000}
            components={{ Header: Spacer }}
            itemContent={(idx, follow) => {
              const date = new Date(follow.ctime);

              return follow.follower ? (
                <UserFollowItem key={idx} user={follow.follower} date={date} />
              ) : null;
            }}
          />
        </B.Middle>
        <B.Right span={3}>
          <Scroll sx={{ maxWidth: 300 }}>
            <Spacer />
            <UserCard user={data.user} />
            <Spacer />
            <UserStatMore user={data.user} />
          </Scroll>
        </B.Right>
      </B>
    </>
  );
});
