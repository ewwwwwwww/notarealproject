/**
 * @generated SignedSource<<c7bd413ad0b7e42aab32adce63942159>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type TimelineQuery$variables = {};
export type TimelineQuery$data = {
  readonly instance: {
    readonly " $fragmentSpreads": FragmentRefs<"HeaderLogo_instance">;
  } | null;
  readonly me: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"InboxCard_user" | "Sidebar_user">;
  } | null;
  readonly notes: {
    readonly nodes: ReadonlyArray<{
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"NoteCard_note">;
    }>;
  } | null;
};
export type TimelineQuery = {
  response: TimelineQuery$data;
  variables: TimelineQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "totalCount",
  "storageKey": null
},
v4 = [
  (v3/*: any*/)
],
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "ctime",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nick",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "src",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "concreteType": "Attachment",
  "kind": "LinkedField",
  "name": "avatar",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "blurhash",
      "storageKey": null
    },
    (v8/*: any*/),
    (v9/*: any*/),
    (v0/*: any*/)
  ],
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "unicode",
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "concreteType": "Attachment",
  "kind": "LinkedField",
  "name": "attachment",
  "plural": false,
  "selections": [
    (v0/*: any*/),
    (v8/*: any*/),
    (v9/*: any*/)
  ],
  "storageKey": null
},
v13 = [
  (v0/*: any*/),
  (v1/*: any*/)
],
v14 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v13/*: any*/),
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "TimelineQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "HeaderLogo_instance"
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "NotesConnection",
        "kind": "LinkedField",
        "name": "notes",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Note",
            "kind": "LinkedField",
            "name": "nodes",
            "plural": true,
            "selections": [
              (v0/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "NoteCard_note"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Sidebar_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "InboxCard_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "TimelineQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          (v1/*: any*/)
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "NotesConnection",
        "kind": "LinkedField",
        "name": "notes",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Note",
            "kind": "LinkedField",
            "name": "nodes",
            "plural": true,
            "selections": [
              (v0/*: any*/),
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "isLikedByYou",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "isBookmarkedByYou",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "isRepeatedByYou",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "LikesConnection",
                "kind": "LinkedField",
                "name": "likes",
                "plural": false,
                "selections": (v4/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ReactsConnection",
                "kind": "LinkedField",
                "name": "reacts",
                "plural": false,
                "selections": (v4/*: any*/),
                "storageKey": null
              },
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "User",
                "kind": "LinkedField",
                "name": "user",
                "plural": false,
                "selections": [
                  (v6/*: any*/),
                  (v1/*: any*/),
                  (v7/*: any*/),
                  (v0/*: any*/),
                  (v8/*: any*/),
                  (v10/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": [
                  {
                    "kind": "Literal",
                    "name": "first",
                    "value": 20
                  }
                ],
                "concreteType": "MentionsConnection",
                "kind": "LinkedField",
                "name": "mentions",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "nodes",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "user",
                        "plural": false,
                        "selections": [
                          (v1/*: any*/),
                          (v0/*: any*/),
                          (v7/*: any*/),
                          (v8/*: any*/),
                          (v10/*: any*/)
                        ],
                        "storageKey": null
                      },
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": "mentions(first:20)"
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ReactAggrsConnection",
                "kind": "LinkedField",
                "name": "reactAggrs",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactAggr",
                    "kind": "LinkedField",
                    "name": "nodes",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "count",
                        "storageKey": null
                      },
                      (v11/*: any*/),
                      (v12/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "notificationUnreadCount",
            "storageKey": null
          },
          (v6/*: any*/),
          (v7/*: any*/),
          (v1/*: any*/),
          (v8/*: any*/),
          (v10/*: any*/),
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "first",
                "value": 100
              },
              {
                "kind": "Literal",
                "name": "orderBy",
                "value": "CTIME_DESC"
              }
            ],
            "concreteType": "NotificationsConnection",
            "kind": "LinkedField",
            "name": "notifications",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Notification",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v0/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "read",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "user",
                    "plural": false,
                    "selections": [
                      (v0/*: any*/),
                      (v1/*: any*/),
                      (v7/*: any*/),
                      (v8/*: any*/),
                      (v10/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v5/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "type",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Follow",
                    "kind": "LinkedField",
                    "name": "follow",
                    "plural": false,
                    "selections": [
                      (v0/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "follower",
                        "plural": false,
                        "selections": (v13/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "mention",
                    "plural": false,
                    "selections": [
                      (v0/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "reply",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "user",
                        "plural": false,
                        "selections": [
                          (v1/*: any*/),
                          (v0/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Note",
                        "kind": "LinkedField",
                        "name": "note",
                        "plural": false,
                        "selections": [
                          (v0/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scope",
                            "storageKey": null
                          },
                          (v2/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Like",
                    "kind": "LinkedField",
                    "name": "like",
                    "plural": false,
                    "selections": [
                      (v0/*: any*/),
                      (v14/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Repeat",
                    "kind": "LinkedField",
                    "name": "repeat",
                    "plural": false,
                    "selections": [
                      (v14/*: any*/),
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "React",
                    "kind": "LinkedField",
                    "name": "react",
                    "plural": false,
                    "selections": [
                      (v11/*: any*/),
                      (v12/*: any*/),
                      (v14/*: any*/),
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Block",
                    "kind": "LinkedField",
                    "name": "block",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "blocker",
                        "plural": false,
                        "selections": (v13/*: any*/),
                        "storageKey": null
                      },
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notifications(first:100,orderBy:\"CTIME_DESC\")"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "d3df15940a24c53c22ef8e6ebd3199cb",
    "id": null,
    "metadata": {},
    "name": "TimelineQuery",
    "operationKind": "query",
    "text": "query TimelineQuery {\n  instance {\n    ...HeaderLogo_instance\n  }\n  notes {\n    nodes {\n      id\n      ...NoteCard_note\n    }\n  }\n  me {\n    id\n    ...Sidebar_user\n    ...InboxCard_user\n  }\n}\n\nfragment Emoji_attachment on Attachment {\n  id\n  remote\n  src\n}\n\nfragment EventBlock_block on Block {\n  blocker {\n    id\n    name\n  }\n}\n\nfragment EventFollow_follow on Follow {\n  follower {\n    id\n    name\n  }\n}\n\nfragment EventLike_like on Like {\n  user {\n    id\n    name\n  }\n}\n\nfragment EventMention_mention on Mention {\n  reply\n  user {\n    name\n    id\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReact_react on React {\n  unicode\n  attachment {\n    ...Emoji_attachment\n    id\n  }\n  user {\n    id\n    name\n  }\n}\n\nfragment EventRepeat_repeat on Repeat {\n  user {\n    id\n    name\n  }\n}\n\nfragment HeaderLogo_instance on Instance {\n  name\n}\n\nfragment InboxCard_notification on Notification {\n  ctime\n  type\n  follow {\n    id\n    ...EventFollow_follow\n  }\n  mention {\n    id\n    ...EventMention_mention\n  }\n  like {\n    id\n    ...EventLike_like\n  }\n  repeat {\n    ...EventRepeat_repeat\n    id\n  }\n  react {\n    ...EventReact_react\n    id\n  }\n  block {\n    ...EventBlock_block\n    id\n  }\n}\n\nfragment InboxCard_user on User {\n  notifications(first: 100, orderBy: CTIME_DESC) {\n    nodes {\n      id\n      read\n      user {\n        id\n        name\n        ...UserAvatar_user\n      }\n      ...InboxCard_notification\n    }\n  }\n}\n\nfragment NoteActionBar_note on Note {\n  isLikedByYou\n  isBookmarkedByYou\n  isRepeatedByYou\n  likes {\n    totalCount\n  }\n  reacts {\n    totalCount\n  }\n}\n\nfragment NoteCard_note on Note {\n  body\n  ...NoteActionBar_note\n  ...NoteHeader_note\n  ...NoteReacts_note\n}\n\nfragment NoteHeader_note on Note {\n  ctime\n  user {\n    nick\n    name\n    nameFull\n    ...UserAvatar_user\n    id\n  }\n  ...NoteMentions_note\n}\n\nfragment NoteMentions_note on Note {\n  mentions(first: 20) {\n    totalCount\n    nodes {\n      user {\n        name\n        ...UserAvatar_user\n        id\n      }\n      id\n    }\n  }\n}\n\nfragment NoteReacts_note on Note {\n  reactAggrs {\n    nodes {\n      count\n      unicode\n      attachment {\n        ...Emoji_attachment\n        id\n      }\n    }\n  }\n}\n\nfragment Sidebar_user on User {\n  notificationUnreadCount\n  ...UserMenuButton_user\n}\n\nfragment UserAvatar_user on User {\n  id\n  name\n  nameFull\n  remote\n  avatar {\n    blurhash\n    remote\n    src\n    id\n  }\n}\n\nfragment UserMenuButton_user on User {\n  nick\n  nameFull\n  ...UserAvatar_user\n}\n"
  }
};
})();

(node as any).hash = "dae8763cce7674444252b4cd88c653a5";

export default node;
