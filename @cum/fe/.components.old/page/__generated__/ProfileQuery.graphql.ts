/**
 * @generated SignedSource<<2382ef18f1479009d6dcffa4e261f4ce>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProfileQuery$variables = {
  userId: any;
};
export type ProfileQuery$data = {
  readonly instance: {
    readonly " $fragmentSpreads": FragmentRefs<"HeaderLogo_instance">;
  } | null;
  readonly me: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"InboxCard_user" | "Sidebar_user">;
  } | null;
  readonly user: {
    readonly id: string;
    readonly notes: {
      readonly nodes: ReadonlyArray<{
        readonly id: string;
        readonly " $fragmentSpreads": FragmentRefs<"NoteCard_note">;
      }>;
    };
    readonly " $fragmentSpreads": FragmentRefs<"UserCard_user" | "UserProfile_user" | "UserStatMore_user">;
  } | null;
};
export type ProfileQuery = {
  response: ProfileQuery$data;
  variables: ProfileQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "kind": "Literal",
  "name": "orderBy",
  "value": "CTIME_DESC"
},
v4 = [
  (v3/*: any*/)
],
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "src",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "concreteType": "Attachment",
  "kind": "LinkedField",
  "name": "avatar",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "blurhash",
      "storageKey": null
    },
    (v7/*: any*/),
    (v8/*: any*/),
    (v2/*: any*/)
  ],
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "totalCount",
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "body",
  "storageKey": null
},
v12 = [
  (v10/*: any*/)
],
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "ctime",
  "storageKey": null
},
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nick",
  "storageKey": null
},
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "unicode",
  "storageKey": null
},
v16 = {
  "alias": null,
  "args": null,
  "concreteType": "Attachment",
  "kind": "LinkedField",
  "name": "attachment",
  "plural": false,
  "selections": [
    (v2/*: any*/),
    (v7/*: any*/),
    (v8/*: any*/)
  ],
  "storageKey": null
},
v17 = [
  (v2/*: any*/),
  (v5/*: any*/)
],
v18 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v17/*: any*/),
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ProfileQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "HeaderLogo_instance"
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserCard_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserProfile_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserStatMore_user"
          },
          {
            "alias": null,
            "args": (v4/*: any*/),
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Note",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  {
                    "args": null,
                    "kind": "FragmentSpread",
                    "name": "NoteCard_note"
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notes(orderBy:\"CTIME_DESC\")"
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Sidebar_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "InboxCard_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ProfileQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          (v5/*: any*/)
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          (v5/*: any*/),
          (v6/*: any*/),
          (v7/*: any*/),
          (v9/*: any*/),
          {
            "alias": null,
            "args": (v4/*: any*/),
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": [
              (v10/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Note",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  (v11/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isLikedByYou",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isBookmarkedByYou",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "isRepeatedByYou",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "LikesConnection",
                    "kind": "LinkedField",
                    "name": "likes",
                    "plural": false,
                    "selections": (v12/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactsConnection",
                    "kind": "LinkedField",
                    "name": "reacts",
                    "plural": false,
                    "selections": (v12/*: any*/),
                    "storageKey": null
                  },
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "user",
                    "plural": false,
                    "selections": [
                      (v14/*: any*/),
                      (v5/*: any*/),
                      (v6/*: any*/),
                      (v2/*: any*/),
                      (v7/*: any*/),
                      (v9/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": [
                      {
                        "kind": "Literal",
                        "name": "first",
                        "value": 20
                      }
                    ],
                    "concreteType": "MentionsConnection",
                    "kind": "LinkedField",
                    "name": "mentions",
                    "plural": false,
                    "selections": [
                      (v10/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Mention",
                        "kind": "LinkedField",
                        "name": "nodes",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "User",
                            "kind": "LinkedField",
                            "name": "user",
                            "plural": false,
                            "selections": [
                              (v5/*: any*/),
                              (v2/*: any*/),
                              (v6/*: any*/),
                              (v7/*: any*/),
                              (v9/*: any*/)
                            ],
                            "storageKey": null
                          },
                          (v2/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": "mentions(first:20)"
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ReactAggrsConnection",
                    "kind": "LinkedField",
                    "name": "reactAggrs",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ReactAggr",
                        "kind": "LinkedField",
                        "name": "nodes",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "count",
                            "storageKey": null
                          },
                          (v15/*: any*/),
                          (v16/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notes(orderBy:\"CTIME_DESC\")"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "followers",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "following",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockingYou",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isFollowedByYou",
            "storageKey": null
          },
          (v14/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "caption",
            "storageKey": null
          },
          (v11/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "FieldsConnection",
            "kind": "LinkedField",
            "name": "fields",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Field",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v11/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "label",
                    "storageKey": null
                  },
                  (v2/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockedByYou",
            "storageKey": null
          },
          (v13/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blocking",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blockers",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "notificationUnreadCount",
            "storageKey": null
          },
          (v14/*: any*/),
          (v6/*: any*/),
          (v5/*: any*/),
          (v7/*: any*/),
          (v9/*: any*/),
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "first",
                "value": 100
              },
              (v3/*: any*/)
            ],
            "concreteType": "NotificationsConnection",
            "kind": "LinkedField",
            "name": "notifications",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Notification",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "read",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "user",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      (v5/*: any*/),
                      (v6/*: any*/),
                      (v7/*: any*/),
                      (v9/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "type",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Follow",
                    "kind": "LinkedField",
                    "name": "follow",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "follower",
                        "plural": false,
                        "selections": (v17/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "mention",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "reply",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "user",
                        "plural": false,
                        "selections": [
                          (v5/*: any*/),
                          (v2/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Note",
                        "kind": "LinkedField",
                        "name": "note",
                        "plural": false,
                        "selections": [
                          (v2/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scope",
                            "storageKey": null
                          },
                          (v11/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Like",
                    "kind": "LinkedField",
                    "name": "like",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      (v18/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Repeat",
                    "kind": "LinkedField",
                    "name": "repeat",
                    "plural": false,
                    "selections": [
                      (v18/*: any*/),
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "React",
                    "kind": "LinkedField",
                    "name": "react",
                    "plural": false,
                    "selections": [
                      (v15/*: any*/),
                      (v16/*: any*/),
                      (v18/*: any*/),
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Block",
                    "kind": "LinkedField",
                    "name": "block",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "blocker",
                        "plural": false,
                        "selections": (v17/*: any*/),
                        "storageKey": null
                      },
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notifications(first:100,orderBy:\"CTIME_DESC\")"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e1f46c007c074cd622c27f2097c62789",
    "id": null,
    "metadata": {},
    "name": "ProfileQuery",
    "operationKind": "query",
    "text": "query ProfileQuery(\n  $userId: UUID!\n) {\n  instance {\n    ...HeaderLogo_instance\n  }\n  user(userId: $userId) {\n    id\n    ...UserCard_user\n    ...UserProfile_user\n    ...UserStatMore_user\n    notes(orderBy: CTIME_DESC) {\n      nodes {\n        id\n        ...NoteCard_note\n      }\n    }\n  }\n  me {\n    id\n    ...Sidebar_user\n    ...InboxCard_user\n  }\n}\n\nfragment Emoji_attachment on Attachment {\n  id\n  remote\n  src\n}\n\nfragment EventBlock_block on Block {\n  blocker {\n    id\n    name\n  }\n}\n\nfragment EventFollow_follow on Follow {\n  follower {\n    id\n    name\n  }\n}\n\nfragment EventLike_like on Like {\n  user {\n    id\n    name\n  }\n}\n\nfragment EventMention_mention on Mention {\n  reply\n  user {\n    name\n    id\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReact_react on React {\n  unicode\n  attachment {\n    ...Emoji_attachment\n    id\n  }\n  user {\n    id\n    name\n  }\n}\n\nfragment EventRepeat_repeat on Repeat {\n  user {\n    id\n    name\n  }\n}\n\nfragment HeaderLogo_instance on Instance {\n  name\n}\n\nfragment InboxCard_notification on Notification {\n  ctime\n  type\n  follow {\n    id\n    ...EventFollow_follow\n  }\n  mention {\n    id\n    ...EventMention_mention\n  }\n  like {\n    id\n    ...EventLike_like\n  }\n  repeat {\n    ...EventRepeat_repeat\n    id\n  }\n  react {\n    ...EventReact_react\n    id\n  }\n  block {\n    ...EventBlock_block\n    id\n  }\n}\n\nfragment InboxCard_user on User {\n  notifications(first: 100, orderBy: CTIME_DESC) {\n    nodes {\n      id\n      read\n      user {\n        id\n        name\n        ...UserAvatar_user\n      }\n      ...InboxCard_notification\n    }\n  }\n}\n\nfragment NoteActionBar_note on Note {\n  isLikedByYou\n  isBookmarkedByYou\n  isRepeatedByYou\n  likes {\n    totalCount\n  }\n  reacts {\n    totalCount\n  }\n}\n\nfragment NoteCard_note on Note {\n  body\n  ...NoteActionBar_note\n  ...NoteHeader_note\n  ...NoteReacts_note\n}\n\nfragment NoteHeader_note on Note {\n  ctime\n  user {\n    nick\n    name\n    nameFull\n    ...UserAvatar_user\n    id\n  }\n  ...NoteMentions_note\n}\n\nfragment NoteMentions_note on Note {\n  mentions(first: 20) {\n    totalCount\n    nodes {\n      user {\n        name\n        ...UserAvatar_user\n        id\n      }\n      id\n    }\n  }\n}\n\nfragment NoteReacts_note on Note {\n  reactAggrs {\n    nodes {\n      count\n      unicode\n      attachment {\n        ...Emoji_attachment\n        id\n      }\n    }\n  }\n}\n\nfragment Sidebar_user on User {\n  notificationUnreadCount\n  ...UserMenuButton_user\n}\n\nfragment UserActionBar_user on User {\n  isBlockedByYou\n}\n\nfragment UserAvatar_user on User {\n  id\n  name\n  nameFull\n  remote\n  avatar {\n    blurhash\n    remote\n    src\n    id\n  }\n}\n\nfragment UserCard_followsYou on User {\n  isFollowingYou\n  isBlockingYou\n}\n\nfragment UserCard_user on User {\n  ...UserAvatar_user\n  ...UserStat_user\n  ...UserCard_followsYou\n  ...UserFollowButton_user\n  name\n  nick\n  nameFull\n}\n\nfragment UserFollowButton_user on User {\n  isFollowingYou\n  isFollowedByYou\n}\n\nfragment UserMenuButton_user on User {\n  nick\n  nameFull\n  ...UserAvatar_user\n}\n\nfragment UserProfile_user on User {\n  caption\n  body\n  fields {\n    nodes {\n      body\n      label\n      id\n    }\n  }\n  ...UserActionBar_user\n}\n\nfragment UserStatMore_user on User {\n  ctime\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  blocking {\n    totalCount\n  }\n  blockers {\n    totalCount\n  }\n}\n\nfragment UserStat_user on User {\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  followers {\n    totalCount\n  }\n  following {\n    totalCount\n  }\n}\n"
  }
};
})();

(node as any).hash = "56510d1dcac9ed609324941008d25023";

export default node;
