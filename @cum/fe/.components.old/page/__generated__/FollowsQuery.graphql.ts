/**
 * @generated SignedSource<<dc1b033a4056178d75442c3f6c12ddf5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type FollowsQuery$variables = {
  userId: any;
};
export type FollowsQuery$data = {
  readonly instance: {
    readonly " $fragmentSpreads": FragmentRefs<"HeaderLogo_instance">;
  } | null;
  readonly me: {
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"InboxCard_user" | "Sidebar_user">;
  } | null;
  readonly user: {
    readonly followers: {
      readonly nodes: ReadonlyArray<{
        readonly ctime: any;
        readonly follower: {
          readonly id: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserFollowItem_user">;
        } | null;
        readonly id: string;
      }>;
      readonly totalCount: number;
    };
    readonly following: {
      readonly nodes: ReadonlyArray<{
        readonly ctime: any;
        readonly followed: {
          readonly id: string;
          readonly " $fragmentSpreads": FragmentRefs<"UserFollowItem_user">;
        } | null;
        readonly id: string;
      }>;
      readonly totalCount: number;
    };
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"UserCard_user" | "UserStatMore_user">;
  } | null;
};
export type FollowsQuery = {
  response: FollowsQuery$data;
  variables: FollowsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "userId"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "userId",
    "variableName": "userId"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "totalCount",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "ctime",
  "storageKey": null
},
v5 = [
  (v2/*: any*/),
  {
    "args": null,
    "kind": "FragmentSpread",
    "name": "UserFollowItem_user"
  }
],
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nameFull",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "remote",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "src",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "concreteType": "Attachment",
  "kind": "LinkedField",
  "name": "avatar",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "blurhash",
      "storageKey": null
    },
    (v8/*: any*/),
    (v9/*: any*/),
    (v2/*: any*/)
  ],
  "storageKey": null
},
v11 = {
  "kind": "Literal",
  "name": "orderBy",
  "value": "CTIME_DESC"
},
v12 = [
  (v3/*: any*/)
],
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nick",
  "storageKey": null
},
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "isFollowingYou",
  "storageKey": null
},
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "isFollowedByYou",
  "storageKey": null
},
v16 = [
  (v2/*: any*/),
  (v13/*: any*/),
  (v7/*: any*/),
  (v6/*: any*/),
  (v8/*: any*/),
  (v10/*: any*/),
  (v14/*: any*/),
  (v15/*: any*/)
],
v17 = [
  (v2/*: any*/),
  (v6/*: any*/)
],
v18 = {
  "alias": null,
  "args": null,
  "concreteType": "User",
  "kind": "LinkedField",
  "name": "user",
  "plural": false,
  "selections": (v17/*: any*/),
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "FollowsQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "HeaderLogo_instance"
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserCard_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "UserStatMore_user"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "followers",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Follow",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "follower",
                    "plural": false,
                    "selections": (v5/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "following",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Follow",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "followed",
                    "plural": false,
                    "selections": (v5/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "Sidebar_user"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "InboxCard_user"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "FollowsQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Instance",
        "kind": "LinkedField",
        "name": "instance",
        "plural": false,
        "selections": [
          (v6/*: any*/)
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "user",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          (v6/*: any*/),
          (v7/*: any*/),
          (v8/*: any*/),
          (v10/*: any*/),
          {
            "alias": null,
            "args": [
              (v11/*: any*/)
            ],
            "concreteType": "NotesConnection",
            "kind": "LinkedField",
            "name": "notes",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": "notes(orderBy:\"CTIME_DESC\")"
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "followers",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Follow",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "follower",
                    "plural": false,
                    "selections": (v16/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "FollowsConnection",
            "kind": "LinkedField",
            "name": "following",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Follow",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "followed",
                    "plural": false,
                    "selections": (v16/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v14/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "isBlockingYou",
            "storageKey": null
          },
          (v15/*: any*/),
          (v13/*: any*/),
          (v4/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blocking",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "BlocksConnection",
            "kind": "LinkedField",
            "name": "blockers",
            "plural": false,
            "selections": (v12/*: any*/),
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "User",
        "kind": "LinkedField",
        "name": "me",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "notificationUnreadCount",
            "storageKey": null
          },
          (v13/*: any*/),
          (v7/*: any*/),
          (v6/*: any*/),
          (v8/*: any*/),
          (v10/*: any*/),
          {
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "first",
                "value": 100
              },
              (v11/*: any*/)
            ],
            "concreteType": "NotificationsConnection",
            "kind": "LinkedField",
            "name": "notifications",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Notification",
                "kind": "LinkedField",
                "name": "nodes",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "read",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "User",
                    "kind": "LinkedField",
                    "name": "user",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      (v6/*: any*/),
                      (v7/*: any*/),
                      (v8/*: any*/),
                      (v10/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "type",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Follow",
                    "kind": "LinkedField",
                    "name": "follow",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "follower",
                        "plural": false,
                        "selections": (v17/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Mention",
                    "kind": "LinkedField",
                    "name": "mention",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "reply",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "user",
                        "plural": false,
                        "selections": [
                          (v6/*: any*/),
                          (v2/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Note",
                        "kind": "LinkedField",
                        "name": "note",
                        "plural": false,
                        "selections": [
                          (v2/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scope",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "body",
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Like",
                    "kind": "LinkedField",
                    "name": "like",
                    "plural": false,
                    "selections": [
                      (v2/*: any*/),
                      (v18/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Repeat",
                    "kind": "LinkedField",
                    "name": "repeat",
                    "plural": false,
                    "selections": [
                      (v18/*: any*/),
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "React",
                    "kind": "LinkedField",
                    "name": "react",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "unicode",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Attachment",
                        "kind": "LinkedField",
                        "name": "attachment",
                        "plural": false,
                        "selections": [
                          (v2/*: any*/),
                          (v8/*: any*/),
                          (v9/*: any*/)
                        ],
                        "storageKey": null
                      },
                      (v18/*: any*/),
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Block",
                    "kind": "LinkedField",
                    "name": "block",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "User",
                        "kind": "LinkedField",
                        "name": "blocker",
                        "plural": false,
                        "selections": (v17/*: any*/),
                        "storageKey": null
                      },
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": "notifications(first:100,orderBy:\"CTIME_DESC\")"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "fb67d2e3cf142199e521601302fd407e",
    "id": null,
    "metadata": {},
    "name": "FollowsQuery",
    "operationKind": "query",
    "text": "query FollowsQuery(\n  $userId: UUID!\n) {\n  instance {\n    ...HeaderLogo_instance\n  }\n  user(userId: $userId) {\n    id\n    ...UserCard_user\n    ...UserStatMore_user\n    followers {\n      totalCount\n      nodes {\n        id\n        ctime\n        follower {\n          id\n          ...UserFollowItem_user\n        }\n      }\n    }\n    following {\n      totalCount\n      nodes {\n        id\n        ctime\n        followed {\n          id\n          ...UserFollowItem_user\n        }\n      }\n    }\n  }\n  me {\n    id\n    ...Sidebar_user\n    ...InboxCard_user\n  }\n}\n\nfragment Emoji_attachment on Attachment {\n  id\n  remote\n  src\n}\n\nfragment EventBlock_block on Block {\n  blocker {\n    id\n    name\n  }\n}\n\nfragment EventFollow_follow on Follow {\n  follower {\n    id\n    name\n  }\n}\n\nfragment EventLike_like on Like {\n  user {\n    id\n    name\n  }\n}\n\nfragment EventMention_mention on Mention {\n  reply\n  user {\n    name\n    id\n  }\n  note {\n    id\n    scope\n    body\n  }\n}\n\nfragment EventReact_react on React {\n  unicode\n  attachment {\n    ...Emoji_attachment\n    id\n  }\n  user {\n    id\n    name\n  }\n}\n\nfragment EventRepeat_repeat on Repeat {\n  user {\n    id\n    name\n  }\n}\n\nfragment HeaderLogo_instance on Instance {\n  name\n}\n\nfragment InboxCard_notification on Notification {\n  ctime\n  type\n  follow {\n    id\n    ...EventFollow_follow\n  }\n  mention {\n    id\n    ...EventMention_mention\n  }\n  like {\n    id\n    ...EventLike_like\n  }\n  repeat {\n    ...EventRepeat_repeat\n    id\n  }\n  react {\n    ...EventReact_react\n    id\n  }\n  block {\n    ...EventBlock_block\n    id\n  }\n}\n\nfragment InboxCard_user on User {\n  notifications(first: 100, orderBy: CTIME_DESC) {\n    nodes {\n      id\n      read\n      user {\n        id\n        name\n        ...UserAvatar_user\n      }\n      ...InboxCard_notification\n    }\n  }\n}\n\nfragment Sidebar_user on User {\n  notificationUnreadCount\n  ...UserMenuButton_user\n}\n\nfragment UserAvatar_user on User {\n  id\n  name\n  nameFull\n  remote\n  avatar {\n    blurhash\n    remote\n    src\n    id\n  }\n}\n\nfragment UserCard_followsYou on User {\n  isFollowingYou\n  isBlockingYou\n}\n\nfragment UserCard_user on User {\n  ...UserAvatar_user\n  ...UserStat_user\n  ...UserCard_followsYou\n  ...UserFollowButton_user\n  name\n  nick\n  nameFull\n}\n\nfragment UserFollowButton_user on User {\n  isFollowingYou\n  isFollowedByYou\n}\n\nfragment UserFollowItem_user on User {\n  nick\n  nameFull\n  ...UserAvatar_user\n  ...UserFollowButton_user\n}\n\nfragment UserMenuButton_user on User {\n  nick\n  nameFull\n  ...UserAvatar_user\n}\n\nfragment UserStatMore_user on User {\n  ctime\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  blocking {\n    totalCount\n  }\n  blockers {\n    totalCount\n  }\n}\n\nfragment UserStat_user on User {\n  notes(orderBy: CTIME_DESC) {\n    totalCount\n  }\n  followers {\n    totalCount\n  }\n  following {\n    totalCount\n  }\n}\n"
  }
};
})();

(node as any).hash = "4ba2fcad371fb081c518fc208252141a";

export default node;
