import { FC } from 'react';
import { Follows as Comp } from './Follows.jsx';

export const Follows: FC = () => (
  <Comp userId="766aca3c-3427-11ed-a261-0242ac120002" />
);

export default {
  title: 'Page',
};
