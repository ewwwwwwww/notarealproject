import {
  randBoolean,
  randFirstName,
  randUserName,
  randQuote,
  rand,
  randNumber,
  randEmoji,
  randAlphaNumeric,
} from '@ngneat/falso';

export const randName = () => {
  return randBoolean() ? randFirstName() : randUserName();
};

export const randImg = () => {
  return 'https://picsum.photos/300/300';
};

export const randScope = () => {
  return rand([
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'private',
    'local',
    'unlisted',
    'direct',
  ]);
};

export const randBlake3 = () => {
  return randAlphaNumeric({ length: 36 }).join('');
};

export const randBody = () => {
  const n = randNumber({ min: 0, max: 20 });

  return Array(n)
    .fill(0)
    .map((v) => randQuote())
    .map((v) => (randBoolean() ? v + ' ' + randEmoji() : v))
    .map((v) => (randBoolean() ? v + '<br />' : v))
    .join('<br />');
};
