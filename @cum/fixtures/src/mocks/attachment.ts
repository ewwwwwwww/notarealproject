import {
  randBoolean,
  randUuid,
  randRecentDate,
  randAlphaNumeric,
  rand,
  randNumber,
} from '@ngneat/falso';

import { randName, randBody, randImg, randBlake3 } from './common.js';

export const randAttachment = {
  attachment_id: randUuid,
  remote: randBoolean,
  src: randImg,
  blake3: randBlake3,
  type: () => rand(['image', 'video', 'animation', 'other']),
  width: () => randNumber({ min: 50, max: 3000 }),
  height: () => randNumber({ min: 50, max: 3000 }),
  blurhash: () => randAlphaNumeric({ length: 16 }),
  ctime: () => randRecentDate({ days: 2 }),
  mtime: () => randRecentDate({ days: 2 }),
};
