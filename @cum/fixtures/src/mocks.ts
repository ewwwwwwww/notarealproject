import { emojis } from './emojis.js';

import {
  rand,
  randAlphaNumeric,
  randCatchPhrase,
  randPhrase,
  randBoolean,
  randNumber,
  randQuote,
  randEmoji,
  randFirstName,
  randUserName,
  randUuid,
  randUrl,
  randFullName,
  randJobTitle,
  randRecentDate,
  randWord,
} from '@ngneat/falso';

type Scope = 'public' | 'private' | 'local' | 'unlisted' | 'direct';

export const randActivityUrl = () => {
  return `https://${randUrl}/activities/${randUuid()}`;
};

export const randObjectUrl = () => {
  return `https://${randUrl}/objects/${randUuid()}`;
};

export const randActorUrl = () => {
  return `https://${randUrl}/users/${randName()}`;
};

export const weightedRandom = (min: number, max: number) =>
  Math.round(max / (Math.random() * max + min));

export const randName = () =>
  randBoolean() ? randFirstName() : randUserName();

export const randField = () =>
  randBoolean() ? randCatchPhrase() : randPhrase();
export const randBody = () => {
  const n = weightedRandom(0, 10);

  return Array(n)
    .fill(0)
    .map(() => randQuote())
    .map((v) => (randBoolean() ? v + ' ' + randEmoji() : v))
    .map((v) => (randBoolean() ? v + '<br />' : v))
    .join('<br />');
};

export const randHash = (length: number) =>
  randAlphaNumeric({ length }).join('');
export const randBlake3 = () => randAlphaNumeric({ length: 36 }).join('');

export const randEmojiUrl = () => {
  const n = randNumber({ min: 0, max: emojis.length - 1 });

  return `/emoji/${emojis[n]}`;
};

export const randImg = () => 'https://picsum.photos/300/300';

export const randScope = (): Scope =>
  rand([
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'public',
    'private',
    'local',
    'unlisted',
    'direct',
  ]);

export const randUser = {
  user_id: randUuid,
  domain_id: randUuid,
  name: randName,
  nick: randFullName,
  dead: randBoolean,
  remote: randBoolean,
  body: randBody,
  caption: randJobTitle,
  avatar_id: randUuid,
  banner_id: randUuid,
  ctime: () => randRecentDate({ days: 2 }),
  mtime: () => randRecentDate({ days: 2 }),
};

const randTags = () => {
  const n = randNumber({ min: 0, max: 20 });

  return Array(n)
    .fill(0)
    .map((v) => randWord());
};

export const randNote = {
  note_id: randUuid,
  user_id: randUuid,
  announce: randBoolean,
  remote: randBoolean,
  scope: randScope,
  has_attachment: randBoolean,
  reply_id: randUuid,
  body: randBody,
  tags: randTags,
  ctime: () => randRecentDate({ days: 2 }),
  mtime: () => randRecentDate({ days: 2 }),
};

export const randAttachment = {
  attachment_id: randUuid,
  remote: randBoolean,
  src: randImg,
  blake3: randBlake3,
  type: () => rand(['image', 'video', 'animation', 'other']),
  width: () => randNumber({ min: 50, max: 3000 }),
  height: () => randNumber({ min: 50, max: 3000 }),
  blurhash: () => randAlphaNumeric({ length: 16 }),
  ctime: () => randRecentDate({ days: 2 }),
  mtime: () => randRecentDate({ days: 2 }),
};
