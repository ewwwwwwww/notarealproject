import cliProgress from 'cli-progress';

export const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

export const createBar = () => {
  return new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
};

export const barred = async (
  n: number,
  fn: (n: number) => Promise<any>,
  sleepMs = 10,
) => {
  const bar = createBar();

  bar.start(n, 0);

  for (let i = 0; i < n + 1; i++) {
    await sleep(sleepMs);
    await fn(i);
    bar.update(i);
  }

  bar.stop();
};
