import { barred } from './util.js';
import { pgPool } from './pool.js';

import {
  randDomainName,
  randJSON,
  randUrl,
  randQuote,
  rand,
  randEmoji,
  randProductCategory,
} from '@ngneat/falso';

import {
  emojis,
  randBlake3,
  randBody,
  randScope,
  randImg,
  randUser,
  randNote,
} from '@cum/fixtures';

import { truncate, dropJobs } from '@cum/db/query/db';

import {
  insertDomain,
  insertActivity,
  insertNote,
  insertUser,
  insertAttachment,
  addAttachment,
  addNoteReact,
  addNoteReactEmoji,
  addLike,
  addBookmark,
  addFollow,
  addBlock,
  addRepeat,
  addField,
} from '@cum/db/query/mock';
import { config } from '@cum/config';

const catched = async (n: number, fn: () => Promise<any>) => {
  return barred(n, async () => {
    try {
      await fn();
    } catch (err) {
      if (
        // @ts-ignore
        err.routine !== '_bt_check_unique' &&
        // @ts-ignore
        err.constraint !== 'prevent_block_self' &&
        // @ts-ignore
        err.constraint !== 'prevent_follow_self'
      ) {
        console.log(err);
      }
    }
  });
};

export const mock = async () => {
  const pool = await pgPool();
  const client = await pool.connect();

  // await catched(1, async () => {
  //   await insertDomain.run({ name: config.core.domain }, client);
  // });
  //
  // console.log('\n adding domains..');
  // await catched(0, async () => {
  //   await insertDomain.run({ name: randDomainName() }, client);
  // });
  //
  // console.log('\n adding users..');
  // await catched(10, async () => {
  //   await insertUser.run(
  //     {
  //       name: randUser.name().toLowerCase(),
  //       nick: randUser.nick(),
  //       body: randUser.body(),
  //       caption: randUser.caption(),
  //     },
  //     client,
  //   );
  //
  //   await dropJobs.run(undefined, client);
  // });
  //
  // console.log('\n adding user fields..');
  // await catched(10, async () => {
  //   const res = await addField.run(
  //     {
  //       label: randProductCategory(),
  //       body: randQuote(),
  //     },
  //     client,
  //   );
  // });
  //
  // console.log('\n adding attachments..');
  // await catched(500, async () => {
  //   await insertAttachment.run(
  //     {
  //       // src: randImg(),
  //       blake3: randBlake3(),
  //       remote: true,
  //       // emoji: false,
  //     },
  //     client,
  //   );
  // });
  //
  // console.log('\n adding notes..');
  // await catched(1000, async () => {
  //   const res = await insertActivity.run(
  //     {
  //       uri: randUrl(),
  //       body: '{}',
  //       blake3: randBlake3(),
  //     },
  //     client,
  //   );
  //
  //   await insertNote.run(
  //     {
  //       activityId: res[0].activityId,
  //       scope: randNote.scope(),
  //       body: randNote.body(),
  //     },
  //     client,
  //   );
  // });
  //
  // console.log('\n adding note attachments..');
  // await catched(1000, async () => {
  //   await addAttachment.run(undefined, client);
  // });
  //
  // const emojiIds: string[] = [];
  //
  // console.log('\n adding emoji attachments..');
  // await catched(Object.entries(emojis).length, async () => {
  //   const emoji = rand(emojis);
  //
  //   const res = await insertAttachment.run(
  //     {
  //       // src: `./public/emoji/${emoji}`,
  //       blake3: randBlake3(),
  //       remote: false,
  //       // emoji: true,
  //     },
  //     client,
  //   );
  //
  //   // if (res[0].attachmentId) {
  //   //   emojiIds.push(res[0].attachmentId);
  //   // }
  // });
  //
  // console.log('\n reacting to posts with custom emoji..');
  // await catched(1000, async () => {
  //   const attachmentId = rand(emojiIds);
  //
  //   await addNoteReact.run(
  //     {
  //       attachmentId,
  //     },
  //     client,
  //   );
  // });
  //
  // console.log('\n reacting to posts with unicode emoji..');
  // await catched(1000, async () => {
  //   await addNoteReactEmoji.run(
  //     {
  //       emoji: randEmoji(),
  //     },
  //     client,
  //   );
  // });
  //
  // console.log('\n liking posts..');
  // await catched(10000, async () => {
  //   await addLike.run(undefined, client);
  // });
  //
  // console.log('\n bookmarking posts..');
  // await catched(1000, async () => {
  //   await addBookmark.run(undefined, client);
  // });
  //
  // console.log('\n adding follows..');
  // await catched(1000, async () => {
  //   await addFollow.run(undefined, client);
  // });
  //
  // console.log('\n adding blocks..');
  // await catched(100, async () => {
  //   await addBlock.run(undefined, client);
  // });
  //
  // console.log('\n repeating posts..');
  // await catched(1000, async () => {
  //   await addRepeat.run(undefined, client);
  // });
  //
  // console.log('\n dropping jobs..');
  // await dropJobs.run(undefined, client);
  //
  // console.log('\n DONE! 😊');

  client.release();
};
