import { Command } from 'commander';
import { mock } from './mock.js';
import { setup } from './setup.js';
import { clear } from './clear.js';

const program = new Command();

// TODO: naming
program
  .name('cum-cmder')
  .description('CLI to some JavaScript string utilities')
  .version('0.8.0');

program
  .command('setup')
  .description('setup your cumbe server')
  .option('-y, --yes', 'automatically say yes to all')
  .action(async (args) => await setup(args ?? {}));

program
  .command('mock')
  .description('install development mock data')
  .action(async () => await mock());

program
  .command('clear')
  .description('wipe database data')
  .action(async () => await clear());

program.parse();
