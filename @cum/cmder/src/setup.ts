import pg from 'pg';
import prompts from 'prompts';
import type { Config } from '@cum/config';
import { promises as fs } from 'node:fs';
import { stringify } from '@iarna/toml';
import { sleep } from './util.js';
import { resolve } from 'path';

const { Pool } = pg;

interface Options {
  yes?: boolean | null;
}

export const setup = async (options: Options) => {
  try {
    await import('@cum/config');

    console.log(options);

    if (!options.yes) {
      await clear(options);
    }
  } catch (err) {
    console.log(err);
    await fresh(options);
  } finally {
    await finish(options);
  }
};

const promptOptions = {
  onCancel() {
    process.exit(1);
  },
};

const clear = async (options: Options) => {
  const reuse = await prompts([
    {
      type: 'confirm',
      name: 'confirm',
      message: 'You already have a config, reuse it?',
      initial: true,
    },
  ]);

  if (reuse.confirm) return;

  const remove = await prompts([
    {
      type: 'confirm',
      name: 'confirm',
      message: 'Is it okay to overwrite it?',
      initial: true,
    },
  ]);

  if (!remove.confirm) {
    console.log('exiting...');
    process.exit(1);
  }

  return fresh(options);
};

const fresh = async (options: Options) => {
  const { nanoid } = await import('nanoid');

  const res = await prompts(
    [
      {
        type: 'text',
        name: 'core.domain',
        message: 'What is your instance domain name?',
        initial: 'example.com',
      },
      {
        type: 'text',
        name: 'database.superuser.user',
        message: 'What is the superuser?',
        initial: 'postgres',
      },
      {
        type: 'text',
        name: 'database.superuser.pass',
        message: 'What is the password?',
        initial: 'postgres',
      },
      {
        type: 'text',
        name: 'database.superuser.name',
        message: 'What is the superuser db name?',
        initial: 'postgres',
      },
      {
        type: 'text',
        name: 'database.core.host',
        message: 'What is the hostname?',
        initial: 'localhost',
      },
      {
        type: 'number',
        name: 'database.core.port',
        message: 'What is the port?',
        initial: 5432,
      },
      {
        type: 'confirm',
        name: 'database.core.ssl',
        message: 'is the database using ssl?',
        initial: false,
      },
    ],
    promptOptions,
  );

  const config: Config = {
    core: {
      domain: res['core.domain'],
      ssl: false,
    },

    backend: {
      port: 5010,
    },

    frontend: {
      port: 5011,
    },

    proxy: {
      port: 5012,
    },

    admin: {
      port: 5013,
    },

    worker: {
      port: 5014,
    },

    database: {
      host: res['database.core.host'],
      port: res['database.core.port'],
      user: 'cumbe',
      pass: nanoid(64),
      name: 'cumbe',
      ssl: res['database.core.ssl'],

      auth: {
        user: 'cumbe_auth',
        pass: nanoid(64),
      },

      visitor: {
        user: 'cumbe_visitor',
      },

      superuser: {
        name: res['database.superuser.name'],
        user: res['database.superuser.user'],
        pass: res['database.superuser.pass'],
      },
    },

    credentials: {
      jwt: nanoid(64),
    },
  };

  const path = resolve('./config.toml');
  const text = stringify(config)
    .replaceAll(/^  /gm, '') // TODO: find better toml stringifier lol
    .replaceAll(/ = 5_/gm, ' = 5');

  console.log(`\nwriting file to ${path}`);

  await fs.writeFile('./config.toml', text, 'utf8');
  await sleep(1000);
};

const finish = async (options: Options) => {
  const { config: c, isProd } = await import('@cum/config');

  if (!c.database.superuser) {
    throw new Error('cannot setup without superuser');
  }

  const pool = new Pool({
    database: c.database.superuser.name,
    user: c.database.superuser.user,
    password: c.database.superuser.pass,
    host: c.database.host,
    port: c.database.port,
    ssl: c.database.ssl,

    max: 20, // set pool max size to 20
    idleTimeoutMillis: 1000, // close idle clients after 1 second
    connectionTimeoutMillis: 1000, // return an error after 1 second if connection could not be established
    maxUses: 7500, // close (and replace) a connection after it has been used 7500 times (see below for discussion)
  });

  const client = await pool.connect();

  try {
    console.log('\ndropping databases...');
    await client.query(`drop database if exists ${c.database.name};`);
    await client.query(`drop database if exists ${c.database.name}_shadow;`);

    console.log('dropping roles...');
    await client.query(`drop role if exists ${c.database.user};`);
    await client.query(`drop role if exists ${c.database.auth.user};`);
    await client.query(`drop role if exists ${c.database.visitor.user};`);

    console.log('creating databases...');
    await client.query(`create database ${c.database.name};`);

    console.log('creating roles...');
    if (!isProd()) {
      await client.query(`create database ${c.database.name}_shadow;`);
      await client.query(
        `create role ${c.database.user} with login password '${c.database.pass}' superuser;`,
      );
    } else {
      await client.query(
        `create role ${c.database.user} with login password '${c.database.pass}';`,
      );
    }

    await client.query(
      `create role ${c.database.auth.user} with login password '${c.database.auth.pass}' noinherit;`,
    );
    await client.query(`create role ${c.database.visitor.user};`);
    await client.query(
      `grant ${c.database.visitor.user} to ${c.database.auth.user};`,
    );

    console.log('granting privileges...');
    await client.query(
      `grant all privileges on database ${c.database.name} to ${c.database.user}`,
    );

    if (!isProd()) {
      await client.query(
        `grant all privileges on database ${c.database.name}_shadow to ${c.database.user}`,
      );
    }
  } finally {
    client.release();
  }
};
