import { barred } from './util.js';
import { dropJobs, truncate, insertDomain } from '@cum/db/query/db';
import { config } from '@cum/config';

export const clear = async () => {
  const { pgPool } = await import('./pool.js');

  const pool = await pgPool();
  const client = await pool.connect();

  await dropJobs.run(undefined, client);
  await truncate.run(undefined, client);
  await insertDomain.run({ name: config.core.domain }, client);

  client.release();
};
