import { Task } from 'graphile-worker';
import { Guard } from '../util/guard.js';
import { Type } from '@sinclair/typebox';
import { userPubKeyUpdate, userPrivKeyUpdate } from '@cum/db/query/user';
import { promisify } from 'node:util';
import crypto from 'node:crypto';

const generateKeyPair = promisify(crypto.generateKeyPair);

const genKeys = () => {
  return generateKeyPair('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem',
    },

    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem',
    },
  });
};

const guard = Guard(
  Type.Object({
    userId: Type.String(),
  }),
);

const task: Task = async (payload, helpers) => {
  if (!guard(payload)) {
    return helpers.logger.error('invalid payload for genUserKeys');
  }

  const { userId } = payload;
  const { publicKey, privateKey } = await genKeys();

  helpers.logger.info('generated crypto keys', { userId });

  await helpers.withPgClient((client) => {
    return Promise.all([
      userPubKeyUpdate.run({ id: userId, pubkey: publicKey }, client),
      userPrivKeyUpdate.run({ id: userId, privkey: privateKey }, client),
    ]);
  });
};

export default task;
