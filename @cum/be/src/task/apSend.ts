import { Task } from 'graphile-worker';
import { store } from '../config.js';
import { Guard } from '../util/guard.js';
import { Type } from '@sinclair/typebox';
import mm from 'micromatch';

const guard = Guard(
  Type.Object({
    userId: Type.String(),
    domain: Type.String(),
  }),
);

const task: Task = async (payload, helpers) => {
  const { logger: log } = helpers;

  if (!store.config.federating) {
    return;
  }

  if (!guard(payload)) {
    return log.warn('delivery failed to parse', { payload });
  }

  const blocked = store.domainBlocked.map((x) => x.pattern);

  if (mm.isMatch(payload.domain, blocked)) {
    return log.debug(`blocked domain ${payload.domain}`);
  }

  log.info(`Hello, world`);
};

export default task;

// import type { Task } from "graphile-worker";
//
// import http from "http";
// import https from "https";
// import { Type } from "@sinclair/typebox";
// import { Guard } from "../util/schema.js";
// import { sign, createDigest } from "../util/crypto";
// import { AP_URL, SSL_ENABLED } from "@cum/config";
// import { inspect } from "util";
//
// const fetch = SSL_ENABLED ? https : http;
//
// const createOptions = (uri: string, headers: any) => {
//   // FIXME: pathname does not include query parameters
//   const { hostname, pathname } = new URL(uri);
//
//   return {
//     host: hostname,
//     path: pathname,
//     port: SSL_ENABLED ? 443 : 80,
//     method: "POST",
//     headers,
//   };
// };
//
// const guard = Guard(
//   Type.Object({
//     userId: Type.String(),
//     http: Type.String(),
//     body: Type.String(),
//     digest: Type.String(),
//     privkey: Type.String(),
//   })
// );
//
// const task: Task = async (payload, { logger: log }) => {
//   if (!guard(payload, log)) return;
//
//   const { userId, http, body, digest, privkey } = payload;
//
//   const headers = createOptions(http, {
//     Accept: "application/activity+json, application/json",
//     "Content-Type": "application/activity+json",
//     "Content-Length": Buffer.byteLength(body),
//     Digest: `SHA-256=${digest}`,
//   });
//
//   const req = fetch.request(headers, (res) => {
//     let data = "";
//
//     res.on("data", (d) => {
//       data += d;
//     });
//
//     res.on("end", () => {
//       log.info(`code: ${res.statusCode}, result: ${data}`);
//     });
//   });
//
//   sign(userId, privkey, req);
//
//   log.info(inspect(JSON.parse(body)), { depth: Infinity });
//
//   req.write(body);
//   req.end();
// };
//
// export default task;
//
