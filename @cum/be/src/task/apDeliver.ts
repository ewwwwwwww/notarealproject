import { Task } from 'graphile-worker';
import { PluginKeys } from '@cum/check';
import type { Plugin } from '../activity/index.js';
import { store } from '../config.js';
import { Guard } from '../util/guard.js';
import { Type } from '@sinclair/typebox';

import * as plugins from '../activity/index.js';

type PluginMap = Partial<{
  [K in PluginKeys]: Plugin<any, any>;
}>;

const guard = Guard(
  Type.Object({
    type: Type.String(),
    userId: Type.String(),
  }),
);

const task: Task = async (payload, helpers) => {
  const { logger: log } = helpers;

  if (!store.config.federating) {
    return;
  }

  if (!guard(payload)) {
    return log.warn('prepare failed to parse', { payload });
  }

  const { userId } = payload;

  log.info(`Hello, world`);
};

export default task;

// import type { Task } from "graphile-worker";
//
// import * as uuid from "uuid";
// import fastJson from "fast-json-stringify";
// import { Type } from "@sinclair/typebox";
// import { schema as ActivitySchema } from "@cum/check";
// import { Guard } from "../util/schema.js";
// import { isProduction, AP_CONTEXT, AP_ROOT_URL } from "@cum/config";
// import { userPrivKeyById, insertActivity } from "@cum/queries";
// import { createDigest } from "../util/crypto";
// import { hash } from "blake3";
// import { fetchInboxFromActor } from "../util/fetch";
//
// import type { ActivityPlugin, Deliver, Deliverable } from "../activity";
// import * as Plugins from "../activity";
//
// interface PluginMap {
//   deliver: Deliver<any, any>;
//   guard: ReturnType<typeof Guard>;
//   stringify: (activity: any) => string;
//   afterSave?: Plugins.AfterSave<any>;
// }
//
// const pluginBuilder = (plugins: ActivityPlugin<any, any>[]) => {
//   const map = new Map<string, PluginMap>();
//
//   for (const plugin of plugins) {
//     const { name, deliver, deliverGuard, afterSave } = plugin;
//     const guard = Guard(deliverGuard);
//
//     if (!(name in ActivitySchema)) {
//       throw new Error(`missing json schema for ${name}`);
//     }
//
//     // @ts-ignore
//     const stringify = fastJson(ActivitySchema[name]);
//
//     map.set(name, { deliver, guard, stringify, afterSave });
//   }
//
//   return map;
// };
//
// const plugins = pluginBuilder(Object.values(Plugins));
//
// const guard = Guard(
//   Type.Object({
//     userId: Type.String(),
//     type: Type.String(),
//   })
// );
//
// function dateJitter(secs: number) {
//   const nowMs = Date.now();
//   const jitterSecs = Math.random() * (secs + 1);
//
//   return new Date(nowMs + 1000 * jitterSecs);
// }
//
// import { inspect } from "util";
//
// const task: Task = async (payload, helpers) => {
//   helpers.logger.info(inspect(payload, { depth: Infinity }));
//   if (!guard(payload, helpers.logger)) return;
//
//   const { logger: log, withPgClient } = helpers;
//   const { userId, type } = payload;
//   const plugin = plugins.get(type);
//
//   if (!plugin) return log.info(`unimplemented activity task: ${type}`);
//   if (!plugin.guard(payload, log, `invalid task payload ${type}`)) return;
//
//   const activityId = uuid.v4();
//   const activity = await plugin.deliver(payload, helpers, activityId, userId);
//
//   if (!activity) return log.warn(`empty activity ${type}`);
//
//   activity["id"] = `${AP_ROOT_URL}/activity/${activityId}`;
//   activity["@context"] = AP_CONTEXT;
//
//   if (!isProduction) {
//     log.debug(`activity ${type}: ${inspect(activity, { depth: 3 })}`);
//   }
//
//   const body = plugin.stringify(activity);
//   // TODO: check if we can use http digest hash instead of blake3
//   const blake3 = hash(body).toString("hex");
//   const digest = createDigest(body);
//   const uri = `${AP_ROOT_URL}/activity/${activityId}`;
//
//   const user = await withPgClient(async (client) => {
//     try {
//       await insertActivity.run(
//         { activityId, uri, body, blake3, remote: false },
//         client
//       );
//     } catch (err: any) {
//       log.error(err);
//     }
//
//     return userPrivKeyById.run({ userId }, client);
//   });
//
//   if (!user.length) throw new Error(`user missing privkey userId:${userId}`);
//
//   const { privkey } = user[0];
//   const to = await withPgClient((client) => {
//     return Promise.all(
//       activity.to.map((o: string) => fetchInboxFromActor(o, client))
//     );
//   });
//
//   for (const http of to) {
//     helpers.addJob(
//       "activitySend",
//       {
//         userId,
//         http,
//         body,
//         digest,
//         privkey,
//       },
//       {
//         runAt: dateJitter(3),
//       }
//     );
//   }
//
//   if (plugin.afterSave) {
//     await plugin.afterSave(payload, helpers, activityId);
//   }
// };
//
// export default task;
//
