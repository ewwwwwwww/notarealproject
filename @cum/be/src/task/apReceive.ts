import { Task } from 'graphile-worker';

const task: Task = async (payload, helpers) => {
  const { logger: log, withPgClient } = helpers;
};

// import { Task } from 'graphile-worker';
// import { PluginKeys, transmogrify } from '@cum/check';
// import schema from '@cum/check/schema';
// import type { Plugin } from '../activity/index.js';
// import { store } from '../config.js';
// import { Guard } from '../util/guard.js';
// import { Type } from '@sinclair/typebox';
// import mm from 'micromatch';
// import fastJson from 'fast-json-stringify';
// import { v1 } from 'uuid';
// import { hash } from 'blake3';
// import { activityInsert } from '@cum/db/query/activity';
// import { setCurrentUser } from '@cum/db/query/db';
// import { actorUserId } from '@cum/db/query/actor';
//
// import * as plugins from '../activity/index.js';
//
// function isInSchema(type: string): type is keyof typeof schema {
//   return type in schema;
// }
//
// function isPlugin(type: string): type is keyof typeof plugins {
//   return type in plugins;
// }
//
// type PluginEntry = {
//   plugin: Plugin<any, any>;
//   stringify: any;
// };
//
// const pluginMap = new Map<string, PluginEntry>();
//
// for (const [key, plugin] of Object.entries(plugins)) {
//   if (!isInSchema(key)) throw new Error('misisng schema definition');
//
//   const stringify = fastJson(schema[key] as fastJson.Schema);
//
//   pluginMap.set(key, { plugin, stringify });
// }
//
// const guard = Guard(
//   Type.Object({
//     domain: Type.String(),
//     body: Type.Object({}),
//     headers: Type.Object({
//       keyId: Type.String(),
//     }),
//   }),
// );
//
// const task: Task = async (payload, helpers) => {
//   const { logger: log, withPgClient } = helpers;
//
//   if (!store.config.federating) {
//     return;
//   }
//
//   if (!guard(payload)) {
//     return log.warn('receive failed to parse', { payload });
//   }
//
//   const blocked = store.domainBlocked.map((x) => x.pattern);
//
//   if (mm.isMatch(payload.domain, blocked)) {
//     return;
//   }
//
//   const { body } = payload;
//   const { name, doc } = transmogrify(body) ?? {};
//
//   if (!doc) {
//     return log.warn('could not parse activity', body);
//   }
//
//   if (!doc.actor) {
//     return log.warn('activity has no actor', body);
//   }
//
//   const verified = await withPgClient((client) => {
//     return Promise.resolve(false);
//   });
//
//   if (!verified) {
//     return log.warn('headers could not be verified', payload.headers);
//   }
//
//   const plugin = pluginMap.get(name ?? '');
//
//   if (!plugin) {
//     return log.warn('unimplemented activity', body);
//   }
//
//   log.debug('parsed activity', body);
//
//   const activityId = v1();
//   const raw = plugin.stringify(body);
//   const blake3 = hash(raw).toString('hex');
//
//   withPgClient(async (client) => {
//     let userId: string;
//
//     try {
//       const row = await actorUserId.run({ actor: doc.actor }, client);
//
//       userId = row[0].userId;
//     } catch (err) {
//       return log.error('could not find actor id', { actor: doc.actor });
//     }
//
//     try {
//       await setCurrentUser.run({ userId }, client);
//     } catch (err) {
//       return log.error('could not set role', { userId });
//     }
//
//     try {
//       await activityInsert.run(
//         {
//           userId,
//           activityId,
//           uri: doc.id,
//           body: raw,
//           blake3,
//           remote: true,
//         },
//         client,
//       );
//     } catch (err) {
//       return log.error('problems inserting activity', { err });
//     }
//
//     plugin.plugin.receive(doc, {
//       userId,
//       activityId,
//       client,
//       log,
//     });
//   });
// };

export default task;
