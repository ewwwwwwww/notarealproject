import type { FastifyPluginAsync, RouteShorthandOptions } from 'fastify';

import { Static, Type } from '@sinclair/typebox';
import fp from 'fastify-plugin';
import { Collection, CollectionPage } from '@cum/check';
import schema from '@cum/check/schema';
import { activityUrl } from '@cum/config';

const route: FastifyPluginAsync = async (fastify) => {
  const params = Type.Object({
    userId: Type.String({ format: 'uuid' }),
    page: Type.Optional(Type.Number()),
  });

  // TODO: add json schema
  const opts: RouteShorthandOptions = {
    schema: {
      params,
      // response: {
      //   200: schema['Collection'],
      // },
    },
  };

  type Schema = {
    Params: Static<typeof params>;
    Reply: Collection;
  };

  // TODO: fetch from db
  fastify.get<Schema>(
    activityUrl.featured(':userId', false),
    opts,
    async (request, reply) => {
      const { userId } = request.params;
      const client = await fastify.pg.connect();

      client.release();

      const body: Collection = {
        '@context': activityUrl.context,
        type: 'OrderedCollection',
        id: activityUrl.featured(userId),
        orderedItems: [],
        totalItems: 0,
      };

      reply
        .code(200)
        .header('Content-Type', 'application/activity+json; charset=utf-8')
        .send(body);
    },
  );

  // TODO: fetch from db
  fastify.get<Schema>(
    activityUrl.featured(':userId', false) + '/:page',
    opts,
    async (request, reply) => {
      const { userId } = request.params;
      const client = await fastify.pg.connect();

      client.release();

      const body: Collection = {
        '@context': activityUrl.context,
        type: 'OrderedCollection',
        id: activityUrl.featured(userId),
        orderedItems: [],
        totalItems: 0,
      };

      reply
        .code(200)
        .header('Content-Type', 'application/activity+json; charset=utf-8')
        .send(body);
    },
  );
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres'],
});
