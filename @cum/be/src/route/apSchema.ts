import { activityUrl } from '@cum/config';
import type { FastifyPluginAsync } from 'fastify';
import fp from 'fastify-plugin';

const body = JSON.stringify({
  '@context': [
    'https://www.w3.org/ns/activitystreams',
    'https://w3id.org/security/v1',
    {
      Emoji: 'toot:Emoji',
      Hashtag: 'as:Hashtag',
      PropertyValue: 'schema:PropertyValue',
      atomUri: 'ostatus:atomUri',
      conversation: {
        '@id': 'ostatus:conversation',
        '@type': '@id',
      },
      discoverable: 'toot:discoverable',
      manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
      capabilities: 'litepub:capabilities',
      ostatus: 'http://ostatus.org#',
      schema: 'http://schema.org#',
      toot: 'http://joinmastodon.org/ns#',
      value: 'schema:value',
      sensitive: 'as:sensitive',
      litepub: 'http://litepub.social/ns#',
      invisible: 'litepub:invisible',
      directMessage: 'litepub:directMessage',
      listMessage: {
        '@id': 'litepub:listMessage',
        '@type': '@id',
      },
      oauthRegistrationEndpoint: {
        '@id': 'litepub:oauthRegistrationEndpoint',
        '@type': '@id',
      },
      EmojiReact: 'litepub:EmojiReact',
      ChatMessage: 'litepub:ChatMessage',
      alsoKnownAs: {
        '@id': 'as:alsoKnownAs',
        '@type': '@id',
      },
    },
  ],
});

const route: FastifyPluginAsync = async (f) => {
  f.get(activityUrl.schema(false), async (_, reply) => {
    reply
      .code(200)
      .header('Content-Type', 'application/ld+json; charset=utf-8')
      .send(body);
  });
};

export default fp.default(route);
