import type { FastifyPluginAsync, RouteShorthandOptions } from 'fastify';

import {
  HttpError,
  NotFoundError,
  BadRequestError,
} from 'http-errors-enhanced';
import { Static, Type } from '@sinclair/typebox';
import fp from 'fastify-plugin';
import { config, activityUrl, constants } from '@cum/config';
import { userIdLocal } from '@cum/db/query/user';

const querystring = Type.Object({
  resource: Type.String(),
});

const reply = Type.Object({
  subject: Type.String(),
  aliases: Type.Optional(Type.Array(Type.String())),
  links: Type.Array(
    Type.Object({
      rel: Type.String(),
      type: Type.String(),
      href: Type.String(),
    }),
  ),
});

const opts: RouteShorthandOptions = {
  schema: {
    querystring,
    response: {
      200: reply,
    },
  },
};

type Schema = {
  Querystring: Static<typeof querystring>;
  Reply: Static<typeof reply> | HttpError;
};

const route: FastifyPluginAsync = async (f) => {
  f.get<Schema>(constants.webfinger(false), opts, async (request, reply) => {
    const { query } = request;

    if (query.resource.startsWith('acc:')) {
      return reply.send(new BadRequestError('resource must start with acct:'));
    }

    const resource = query.resource.startsWith('acct:')
      ? query.resource.slice(5).split('@')
      : query.resource.split('@');

    if (resource.length !== 2) {
      return reply.send(
        new BadRequestError('account name must be of format name@domain'),
      );
    }

    const [name, domain] = resource;

    if (!name.length || !domain.length) {
      return reply.send(
        new BadRequestError('account name must be of format name@domain'),
      );
    }

    if (name[0] === '@') {
      return reply.send(
        new BadRequestError('name must not start with @ symbol'),
      );
    }

    if (domain !== config.core.domain) {
      return reply.send(new BadRequestError('requesting non-local actor'));
    }

    const client = await f.pg.connect();
    const user = await userIdLocal.run({ name }, client);

    client.release();

    if (!user.length) {
      return reply.send(new NotFoundError(`unknown user ${name}`));
    }

    const { user_id } = user[0];

    const body = {
      subject: query.resource,
      links: [
        {
          rel: 'http://webfinger.net/rel/profile-page',
          type: 'text/html',
          // TODO: custom profile pages for differernt frontends
          href: activityUrl.actor(user_id),
        },

        {
          rel: 'self',
          type: 'application/activity+json',
          href: activityUrl.actor(user_id),
        },

        // TODO: ostatus support
        // {
        //   rel: "http://ostatus.org/schema/1.0/subscribe",
        //   template: `https://{domain}/ostatus_subscribe?acct={{uri}}`,
        // }
      ],
    };

    reply
      .code(200)
      .header('Content-Type', 'application/jrd+json; charset=utf-8')
      .send(body);
  });
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres'],
});
