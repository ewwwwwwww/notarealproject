import type { FastifyPluginAsync, RouteShorthandOptions } from 'fastify';
import fp from 'fastify-plugin';
import { Static, Type } from '@sinclair/typebox';
import { HttpError, NotFoundError } from 'http-errors-enhanced';
import { activityUrl, config } from '@cum/config';

const reply = Type.Object({
  links: Type.Array(
    Type.Object({
      rel: Type.String(),
      href: Type.String(),
    }),
  ),
});

const opts: RouteShorthandOptions = {
  schema: {
    response: {
      200: reply,
    },
  },
};

const versionParams = Type.Object({
  version: Type.String(),
});

const versionReply = Type.Object({
  metadata: Type.Object({}),
  openRegistrations: Type.Boolean(),
  protocols: Type.Array(Type.String()),
  services: Type.Object({
    inbound: Type.Array(Type.String()),
    outbound: Type.Array(Type.String()),
  }),
  software: Type.Object({
    name: Type.String(),
    // repository: Type.Optional(Type.String()),
    // version: Type.String(),
  }),
  usage: Type.Object({
    localPosts: Type.Number(),
    users: Type.Object({
      total: Type.Number(),
    }),
  }),
  version: Type.String(),
});

const versionOpts: RouteShorthandOptions = {
  schema: {
    params: versionParams,
    response: {
      200: versionReply,
    },
  },
};

type Schema = {
  Reply: Static<typeof reply>;
};

type VersionSchema = {
  Params: Static<typeof versionParams>;
  Reply: Static<typeof versionReply> | HttpError;
};

const nodeinfo = () => {
  return {
    links: [
      {
        rel: 'http://nodeinfo.diaspora.software/ns/schema/2.1',
        href: activityUrl.nodeinfo(2.1),
      },
    ],
  };
};

// TODO: get metadata
const nodeinfo_2_1 = async (): Promise<Static<typeof versionReply>> => ({
  metadata: {},

  openRegistrations: true,
  protocols: ['activitypub'],

  services: {
    inbound: [],
    outbound: [],
  },

  software: {
    name: 'cumbe',
    // repository: CUMFE_REPOSITORY,
    // version: CUMFE_REV || "unknown-develop",
  },

  usage: {
    localPosts: 0, // TODO: get user posts

    users: {
      total: 0, // TODO: get user count
    },
  },

  version: '2.1',
});

// TODO: get metadata
const nodeinfo_2_0 = async (): Promise<Static<typeof versionReply>> => ({
  metadata: {},

  openRegistrations: true,
  protocols: ['activitypub'],

  services: {
    inbound: [],
    outbound: [],
  },

  software: {
    name: 'cumbe',
    // version: CUMFE_REV || 'unknown-develop',
  },

  usage: {
    localPosts: 0, // TODO: get user posts

    users: {
      total: 0, // TODO: get user count
    },
  },

  version: '2.0',
});

const route: FastifyPluginAsync = async (fastify) => {
  fastify.get<Schema>(
    activityUrl.nodeinfo(undefined, false),
    opts,
    (_, reply) => {
      reply
        .code(200)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send(nodeinfo());
    },
  );

  fastify.get<VersionSchema>(
    '/nodeinfo/:version',
    versionOpts,
    async (request, reply) => {
      const { version } = request.params;

      if (version === '2.1.json' || version === '2.1') {
        return reply
          .code(200)
          .header(
            'Content-Type',
            'application/json; profile="http://nodeinfo.diaspora.software/ns/schema/2.1#"',
          )
          .send(await nodeinfo_2_1());
      }

      if (version === '2.0.json' || version === '2.0') {
        return reply
          .code(200)
          .header(
            'Content-Type',
            'application/json; profile="http://nodeinfo.diaspora.software/ns/schema/2.0#"',
          )
          .send(await nodeinfo_2_0());
      }

      reply.send(new NotFoundError());
    },
  );
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres'],
});
