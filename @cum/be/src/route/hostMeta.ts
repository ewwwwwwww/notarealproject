import type { FastifyPluginAsync } from 'fastify';

import { constants } from '@cum/config';
import fp from 'fastify-plugin';

const body = JSON.stringify({
  links: [
    {
      rel: 'lrdd',
      type: 'application/json',
      template: `${constants.webfinger}?resource={{uri}}`,
    },
  ],
});

// TODO: format xml from json
const body_xml = `<?xml version="1.0" encoding="UTF-8"?><XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0"><Link rel="lrdd" template="${constants.webfinger()}?resource={uri}" type="application/xrd+xml" /></XRD>`;

// TODO: handle json/xml header requests
const route: FastifyPluginAsync = async (fastify) => {
  fastify.get(constants.hostmeta, (_, reply) => {
    // reply
    //   .code(200)
    //   .header("Content-Type", "application/json; charset=utf-8")
    //   .send(body);
    reply
      .code(200)
      .header('Content-Type', 'application/xrd+xml; charset=utf-8')
      .send(body_xml);
  });
};

export default fp.default(route);
