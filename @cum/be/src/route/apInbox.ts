import type { FastifyPluginAsync, RouteShorthandOptions } from 'fastify';

import { Static, Type } from '@sinclair/typebox';
import fp from 'fastify-plugin';
import { activityUrl } from '@cum/config';

// @ts-ignore
import httpSignature from '@peertube/http-signature';

const params = Type.Object({
  userId: Type.String({ format: 'uuid' }),
});

const opts: RouteShorthandOptions = {
  schema: {
    params,
  },
};

type Schema = {
  Params: Static<typeof params>;
};

const route: FastifyPluginAsync = async (fastify) => {
  fastify.post(activityUrl.inbox('', false), async (request, reply) => {
    const headers = httpSignature.parseRequest(request);

    const { body } = request;

    fastify.worker.addJob('activityReceive', {
      headers,
      body,
    });

    reply
      .code(200)
      .header('Content-Type', 'application/activity+json; charset=utf-8');
  });

  fastify.post<Schema>(
    activityUrl.inbox(':userId', false),
    opts,
    async (request, reply) => {
      const headers = httpSignature.parseRequest(request);

      const { userId } = request.params;
      const { body } = request;

      fastify.worker.addJob('activityReceive', {
        userId,
        headers,
        body,
      });

      reply
        .code(200)
        .header('Content-Type', 'application/activity+json; charset=utf-8');
    },
  );
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres', 'worker'],
});
