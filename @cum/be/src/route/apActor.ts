import type { FastifyPluginAsync, RouteShorthandOptions } from 'fastify';
import { NotFoundError, HttpError } from 'http-errors-enhanced';
import { Static, Type } from '@sinclair/typebox';
import fp from 'fastify-plugin';
import { TPerson } from '@cum/check/plugins';
import schema from '@cum/check/schema';
import { activityUrl } from '@cum/config';
import { actorDataById, IActorDataByIdResult } from '@cum/db/query/actor';

const params = Type.Object({
  userId: Type.String({ format: 'uuid' }),
});

const opts: RouteShorthandOptions = {
  schema: {
    params,
    response: {
      200: schema['Person'],
    },
  },
};

type Schema = {
  Params: Static<typeof params>;
  Reply: TPerson | HttpError;
};

function fromData(userId: string, person: IActorDataByIdResult): TPerson {
  const { nick, name: preferredUsername, pubkey: publicKeyPem } = person;
  const nickname = nick || preferredUsername;

  if (!publicKeyPem) {
    throw new Error(`public key has not been generated yet for ${userId}`);
  }

  const actor = activityUrl.actor(userId);

  return {
    '@context': activityUrl.context,
    type: 'Person',
    id: actor,
    name: nickname,
    preferredUsername,

    inbox: activityUrl.inbox(userId),
    outbox: activityUrl.outbox(userId),
    followers: activityUrl.followers(userId),
    following: activityUrl.following(userId),
    featured: activityUrl.featured(userId),

    endpoints: {
      sharedInbox: activityUrl.inbox(''),
    },

    publicKey: {
      id: actor + '#main-key',
      owner: actor,
      publicKeyPem,
    },

    alsoKnownAs: [],
    attachment: [],
    capabilities: {
      acceptsChatMessages: true,
      // @ts-ignore
      oauthAuthorizationEndpoint: 'http://pleroma.local/oauth/authorize',
      oauthRegistrationEndpoint: 'http://pleroma.local/api/v1/apps',
      oauthTokenEndpoint: 'http://pleroma.local/oauth/token',
      uploadMedia: 'http://pleroma.local/api/ap/upload_media',
    },
    discoverable: false,
    manuallyApprovesFollowers: false,
    summary: 'hello world',
    tag: [],
    url: activityUrl.inbox(userId),
  };
}

const route: FastifyPluginAsync = async (fastify) => {
  fastify.get<Schema>(
    activityUrl.actor(':userId', false),
    opts,
    async (request, reply) => {
      const { userId } = request.params;
      const client = await fastify.pg.connect();
      const user = await actorDataById.run({ userId }, client);

      client.release();

      if (!user?.length) {
        return reply.send(new NotFoundError(`unknown actor ${userId}`));
      }

      const body = fromData(userId, user[0]);

      reply
        .code(200)
        .header('Content-Type', 'application/activity+json; charset=utf-8')
        .send(body);
    },
  );
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres'],
});
