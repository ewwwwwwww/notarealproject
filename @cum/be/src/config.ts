import { syncedStore, getYjsValue } from '@syncedstore/core';
import { log } from './logger.js';

interface Config {
  federating: boolean;
}

interface DomainEntry {
  pattern: string;
  reason?: string;
}

export const store = syncedStore({
  config: {} as Config,
  domainMediaRemoval: [] as DomainEntry[],
  domainUnlisted: [] as DomainEntry[],
  domainBlocked: [] as DomainEntry[],
});

store.config.federating = true;
store.domainBlocked.push({
  pattern: 'google.com',
});

log.info(store.config, 'config initialized');
log.info(store.domainBlocked, 'blocking domains');
