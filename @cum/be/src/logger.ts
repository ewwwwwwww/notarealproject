import { pino } from 'pino';

export const log = pino({
  name: '@cum/be',
  level: process.env.LOG_LEVEL || 'info',
  sync: false,
});

export type Logger = typeof log;
