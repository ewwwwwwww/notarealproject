/**
 * This plugin treats any table that doesn't have an `@omit` comment as if it
 * had `@omit create,update,delete` (thereby disabling mutations).
 *
 * Override it by adding a smart comment to the table. To restore all
 * mutations, do `COMMENT ON my_table IS E'@omit :';` (the `:` is special
 * syntax for "nothing").
 */

export default function PgOmitMutationsPlugin(builder: any) {
  builder.hook('build', (build: any) => {
    const { pgIntrospectionResultsByKind } = build;
    pgIntrospectionResultsByKind.class
      .filter((table: any) => table.isSelectable && table.namespace)
      .forEach((table: any) => {
        if (!('omit' in table.tags)) {
          table.tags.omit = 'create,update,delete';
        }
      });
    return build;
  });
}
