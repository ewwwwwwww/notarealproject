import type { FastifyPluginAsync } from 'fastify';

import fp from 'fastify-plugin';

const plugin: FastifyPluginAsync = async (fastify) => {
  fastify.addContentTypeParser(
    /^application\/.+\+json$/,
    { parseAs: 'string' },
    fastify.getDefaultJsonParser('error', 'ignore'),
  );
};

export default fp.default(plugin, {
  name: 'activityContentType',
  fastify: '4.x',
});
