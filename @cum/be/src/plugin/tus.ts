import type { FastifyPluginAsync, FastifyReply } from 'fastify';

import fp from 'fastify-plugin';
import tus, { EVENTS, IFile } from 'tus-node-server';
import { IncomingMessage } from 'http';
import path, { resolve } from 'path';
import { createReadStream, existsSync, mkdirSync } from 'fs';
import { createHash } from 'blake3';
import {
  fileAdd,
  userAttachmentAdd,
  userUpdateAvatar,
  userUpdateBanner,
} from '@cum/db/query/file';
import { randomBytes } from 'crypto';
import { rename } from 'fs/promises';
import { Type } from '@sinclair/typebox';
import { TypeCompiler } from '@sinclair/typebox/compiler/compiler.js';
import { UnauthorizedError } from 'http-errors-enhanced';
import { FastifyRequest } from 'fastify';
import cookies from 'cookie';
import jwt from 'jsonwebtoken';
import { JwtToken } from './postgraphile.js';
import { config } from '@cum/config';
import { setCurrentUser } from '@cum/db/query/db';

const Meta = TypeCompiler.Compile(
  Type.Object({
    action: Type.String(),
    relativePath: Type.String(),
    name: Type.Optional(Type.String()),
    type: Type.String(),
    filetype: Type.String(),
    filename: Type.String(),
    userId: Type.String(),
    caption: Type.Optional(Type.String()),
  }),
);

const parseMeta = (str: string) => {
  const map = Object.fromEntries(
    str
      .split(',')
      .map((x) => x.split(' '))
      .map(([key, value]) => [key, atob(value)]),
  );

  return Meta.Check(map) ? map : null;
};

const getPath = (hash: string) => {
  const dir = hash.slice(0, 2);

  return path.resolve('../../uploads/file', dir, hash);
};

const jwtGuard = (req: IncomingMessage): boolean => {
  if (typeof req.headers['cookie'] !== 'string') return false;

  const cookie = cookies.parse(req.headers['cookie']);

  if (!cookie.auth_token) return false;

  const [type, token] = cookie.auth_token.split(' ');

  if (type !== 'Bearer') return false;
  if (!token) return false;

  const claims = jwt.verify(token, config.credentials.jwt) as
    | JwtToken
    | undefined;

  if (!claims?.user_id) return false;

  req.headers['upload-metadata'] += ',userId ' + btoa(claims.user_id);

  return true;
};

for (let i = 0x0; i <= 0xff; i++) {
  const prefix = i.toString(16);
  const dir = resolve(
    '../../uploads/file',
    prefix.length === 1 ? '0' + prefix : prefix,
  );

  if (!existsSync(dir)) {
    mkdirSync(dir, { recursive: true });
  }
}

const tmpDir = resolve('../../uploads', 'tmp');

if (!existsSync(tmpDir)) {
  mkdirSync(tmpDir);
}

const route: FastifyPluginAsync = async (fastify) => {
  const tusServer = new tus.Server({
    path: '/file-tmp',
  });

  const hasher = (path: string): Promise<string> => {
    return new Promise((ok, reject) => {
      createReadStream(path)
        .pipe(createHash())
        .on('data', (hash) => ok(hash.toString('hex')));
    });
  };

  tusServer.datastore = new tus.FileStore({ directory: '../../uploads/tmp' });

  tusServer.on(EVENTS.EVENT_UPLOAD_COMPLETE, async (event: { file: IFile }) => {
    if (!event.file) {
      return fastify.log.error(`missing file`);
    }

    const oldPath = resolve(tmpDir, event.file.id);
    const meta = parseMeta(event.file.upload_metadata);

    if (!meta) {
      return;
    }

    console.log(meta);

    const { action, userId } = meta;
    const hash = await hasher(oldPath);
    const newPath = getPath(hash);

    await rename(oldPath, newPath);

    const client = await fastify.pg.connect();
    // FIXME: permission denied
    // await setCurrentUser.run({ userId: meta.userId }, client);

    await fileAdd.run(
      {
        blake3: hash,
        size: parseInt(event.file.upload_length),
      },
      client,
    );

    console.log(action);

    if (action === 'AVATAR') {
      const res = await userAttachmentAdd.run(
        {
          userId: userId,
          blake3: hash,
        },
        client,
      );

      console.log(res);

      await userUpdateAvatar.run(
        {
          // @ts-ignore
          avatarId: res[0].attachment_id,
          userId,
        },
        client,
      );
    } else if (action === 'BANNER') {
      const res = await userAttachmentAdd.run(
        {
          userId: userId,
          blake3: hash,
        },
        client,
      );

      await userUpdateBanner.run(
        {
          // @ts-ignore
          bannerId: res[0].attachment_id,
          userId,
        },
        client,
      );
    } else if (action === 'NOTE') {
    }
  });

  const handle = (req: FastifyRequest, res: FastifyReply) => {
    tusServer.handle(req.raw, res.raw);
  };

  const handleAuth = (req: FastifyRequest, res: FastifyReply) => {
    res.raw.setHeader('Access-Control-Allow-Credentials', 'true');
    if (!jwtGuard(req.raw)) return new UnauthorizedError();
    tusServer.handle(req.raw, res.raw);
  };

  fastify.addContentTypeParser(
    'application/offset+octet-stream',
    (request, payload, done) => done(null),
  );

  fastify.get('/file-tmp', handle);
  fastify.post('/file-tmp', handleAuth);
  fastify.patch('/file-tmp', handleAuth);
  fastify.delete('/file-tmp', handleAuth);
  fastify.options('/file-tmp', handleAuth);

  fastify.get('/file-tmp/*', handle);
  fastify.post('/file-tmp/*', handleAuth);
  fastify.patch('/file-tmp/*', handleAuth);
  fastify.delete('/file-tmp/*', handleAuth);
  fastify.options('/file-tmp/*', handleAuth);
};

export default fp.default(route, {
  dependencies: ['@fastify/postgres', 'worker'],
});
