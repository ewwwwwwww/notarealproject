import { makeAddPgTableConditionPlugin } from 'postgraphile';
import { makePluginByCombiningPlugins } from 'graphile-utils';

function isString(x: any): x is string {
  return typeof x === 'string';
}

// TODO: rename descriptions

const fullText = makeAddPgTableConditionPlugin(
  'app_public',
  'user',
  'autocomplete',
  (build) => {
    const { GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    if (!isString(value)) return;
    if (value.length < 3) return;

    const { sql, sqlTableAlias, queryBuilder } = helpers;

    return sql.fragment`${sqlTableAlias}.name_full LIKE ${sql.value(
      value + '%',
    )}`;
  },
);

export default makePluginByCombiningPlugins(fullText);
