import type { FastifyPluginAsync, FastifyRequest, FastifyReply } from 'fastify';

import { config, connectionString } from '@cum/config';
import fp from 'fastify-plugin';

import {
  postgraphile,
  PostGraphileOptions,
  PostGraphileResponseFastify3,
  PostGraphileResponse,
  makeWrapResolversPlugin,
  makePluginHook,
} from 'postgraphile';

import PgConnectionFilterPlugin from 'postgraphile-plugin-connection-filter';
import PgSimplifyInflectorPlugin from '@graphile-contrib/pg-simplify-inflector';
import PgOmitMutationsPlugin from './pgOmitMutations.js';
import PgTimelineFilters from './pgTimelineFilters.js';
import PgUserFilters from './pgUserFilters.js';
import PgPubSub from '@graphile/pg-pubsub';

const pluginHook = makePluginHook([PgPubSub.default]);

import { fileURLToPath } from 'url';
import { dirname } from 'path';
import jwt from 'jsonwebtoken';
import cookies from 'cookie';
import { IncomingMessage } from 'http';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Converts a PostGraphile route handler into a Fastify request handler.
const convert = (handler: (res: PostGraphileResponse) => Promise<void>) => {
  return (request: FastifyRequest, reply: FastifyReply) => {
    return handler(new PostGraphileResponseFastify3(request, reply));
  };
};

export type JwtToken = {
  user_id: string;
  role: string;
  iat: number;
  exp: number;
  aud: string;
  iss: string;
};

const pgSettings = async (req: IncomingMessage) => {
  if (req.headers['authorization']) return {};

  const cookie = cookies.parse(req.headers['cookie'] ?? '');
  const bearer = cookie?.auth_token ?? '';

  if (bearer === '') return {};

  const [type, token] = bearer.split(' ');

  if (type !== 'Bearer') {
    throw new Error(
      'Authorization header is not of the correct bearer scheme format',
    );
  }

  if (token) {
    const claims = jwt.verify(token, config.credentials.jwt) as
      | JwtToken
      | undefined;

    if (!claims) return {};

    return {
      'jwt.claims.user_id': claims.user_id,
      'jwt.claims.role': claims.role,
      'jwt.claims.iat': claims.iat,
      'jwt.claims.exp': claims.exp,
      'jwt.claims.aud': claims.aud,
      'jwt.claims.iss': claims.iss,
    };
  }

  return {};
};

// TODO: types
// TODO: expiration
const PgHttpOnlyCookiePlugin = makeWrapResolversPlugin({
  Mutation: {
    userLogin: {
      async resolve(resolver, source, args, context, info) {
        const result = await resolver(source, args, context, info);

        if (result.data) {
          const data: JwtToken = {
            ...result.data,
            aud: 'postgraphile',
            iss: 'postgraphile',
          };

          context.setCookie(jwt.sign(data, config.credentials.jwt));
        }

        return result;
      },
    },
  },
});

// TODO: persisted queries (relay)
// TODO: persisted queries from file (so other clients can gen their own)
const plugin: FastifyPluginAsync = async (fastify, { pgPool }: any) => {
  const options: PostGraphileOptions = {
    pluginHook,
    pgSettings,

    additionalGraphQLContextFromRequest: async (req, res) => {
      return {
        setCookie: (token: any) => {
          const ext = config.core.ssl ? 'HttpOnly;Secure' : 'HttpOnly';

          res.setHeader('Set-Cookie', `auth_token=Bearer ${token};${ext}`);
        },
      };
    },

    appendPlugins: [
      PgHttpOnlyCookiePlugin,
      PgSimplifyInflectorPlugin.default,
      PgTimelineFilters,
      PgUserFilters,
    ],

    ignoreIndexes: false,
    watchPg: true,
    graphiql: true,
    enhanceGraphiql: true,
    subscriptions: true,
    simpleSubscriptions: true,
    dynamicJson: true,
    jwtSecret: config.credentials.jwt,
    jwtPgTypeIdentifier: 'app_hidden.jwt_token',
    setofFunctionsContainNulls: false,
    ignoreRBAC: false,
    showErrorStack: 'json',
    extendedErrors: ['hint', 'detail', 'errcode'],
    allowExplain: true,
    legacyRelations: 'omit',
    exportGqlSchemaPath: `${__dirname}/../../../../db/data/schema.graphql`,
    sortExport: true,
    classicIds: true,

    graphileBuildOptions: {
      pgSubscriptionPrefix: 'pg:',
    },
  };

  const {
    graphqlRoute,
    graphqlRouteHandler,
    graphiqlRoute,
    graphiqlRouteHandler,
    faviconRouteHandler,
    eventStreamRoute,
    eventStreamRouteHandler,
    options: middleware,
  } = postgraphile(pgPool, ['app_public'], options);

  fastify.options(
    graphqlRoute,
    { logLevel: 'fatal' },
    convert(graphqlRouteHandler),
  );

  fastify.post(
    graphqlRoute,
    // { logLevel: 'fatal' },
    convert(graphqlRouteHandler),
  );
  fastify.get(
    graphqlRoute,
    // { logLevel: 'fatal' },
    convert(graphqlRouteHandler),
  );

  // GraphiQL
  if (middleware.graphiql) {
    if (graphiqlRouteHandler) {
      fastify.head(graphiqlRoute, convert(graphiqlRouteHandler));
      fastify.get(
        graphiqlRoute,
        // { logLevel: 'fatal' },
        convert(graphiqlRouteHandler),
      );
    }

    if (faviconRouteHandler) {
      fastify.get('/favicon.ico', convert(faviconRouteHandler));
    }
  }

  // Watch mode
  if (middleware.watchPg && eventStreamRouteHandler) {
    fastify.options(eventStreamRoute, convert(eventStreamRouteHandler));
    fastify.get(
      eventStreamRoute,
      // { logLevel: 'fatal' },
      convert(eventStreamRouteHandler),
    );
  }
};

export default fp.default(plugin, {
  name: 'postgraphile',
  fastify: '4.x',
  dependencies: [],
});
