import type { FastifyPluginAsync } from 'fastify';

import fp from 'fastify-plugin';
import { makeWorkerUtils, run, Logger } from 'graphile-worker';
import { pino } from 'pino';

import * as taskList from '../task/index.js';

const log = pino({
  name: '@cum/be/worker',
  level: process.env.LOG_LEVEL || 'info',
  sync: false,
});

function logFactory(scope: any) {
  return (level: any, message: any, meta: any) => {
    switch (level) {
      case 'error':
        return log.error(meta, message);
      case 'warning':
        return log.warn(meta, message);
      case 'debug':
        return log.debug(meta, message);
      case 'info':
      default:
        return log.info(meta, message);
    }
  };
}

const logger = new Logger(logFactory);

// TODO: move into own package
export async function startWorker(pgPool: any) {
  const runner = await run({
    pgPool,
    concurrency: 5,
    noHandleSignals: false,
    pollInterval: 1000,
    taskList,
    // taskDirectory: `${__dirname}/tasks`,
  });
}

const plugin: FastifyPluginAsync = async (fastify, { pgPool }: any) => {
  const utils = await makeWorkerUtils({
    pgPool,
  });

  fastify.addHook('onClose', async () => {
    await utils.release();
  });

  fastify.decorate('worker', utils);
};

export type { WorkerUtils } from 'graphile-worker';

export default fp.default(plugin, {
  name: 'worker',
  fastify: '4.x',
  dependencies: [],
});
