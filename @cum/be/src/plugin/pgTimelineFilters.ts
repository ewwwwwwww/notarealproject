import { makeAddPgTableConditionPlugin } from 'postgraphile';
import { makePluginByCombiningPlugins } from 'graphile-utils';

function isString(x: any): x is string {
  return typeof x === 'string';
}

// TODO: rename descriptions

const userByName = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'userByName',
  (build) => {
    const { GraphQLList, GraphQLNonNull, GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    if (!isString(value)) return;

    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.user_id = (select user_id from app_public.user where name = ${sql.value(
      value,
    )} and remote = false)`;
  },
);

const userByFqn = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'userByFqn',
  (build) => {
    const { GraphQLList, GraphQLNonNull, GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    if (!isString(value)) return;

    const { sql, sqlTableAlias } = helpers;
    const values = value.split('@');

    if (values.length === 1) {
      const name = values[0];

      return sql.fragment`${sqlTableAlias}.user_id = (select user_id from app_public.user where name = ${sql.value(
        name,
      )} and remote = false)`;
    } else if (values.length === 2) {
      const [name, domain] = values;

      return sql.fragment`${sqlTableAlias}.user_id = (select user_id from app_public.user where name = ${sql.value(
        name,
      )} and domain_id = (select domain_id from app_public.domain where name = ${sql.value(
        domain,
      )}))`;
    }
  },
);

const domain = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'domainName',
  (build) => {
    const { GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.domain_id = (select domain_id from app_public.domain where name = ${sql.value(
      value,
    )} limit 1)`;
  },
);

const onlyFollows = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'onlyFollows',
  (build) => {
    const { GraphQLBoolean } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLBoolean,
    };
  },
  (value, helpers, build) => {
    if (!value) return;

    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.user_id in (select followed_id from app_public.follow where follower_id = app_hidden.current_user_id())`;
  },
);

const hideBlocks = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'hideBlocks',
  (build) => {
    const { GraphQLBoolean } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLBoolean,
    };
  },
  (value, helpers, build) => {
    if (!value) return;

    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.user_id not in (select blocked_id from app_public.block where blocker_id = app_hidden.current_user_id())`;
  },
);

const dateStart = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'dateStart',
  (build) => {
    const { GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.ctime >= ${sql.value(
      value,
    )}::timestamptz`;
  },
);

const dateEnd = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'dateEnd',
  (build) => {
    const { GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.ctime <= ${sql.value(
      value,
    )}::timestamptz`;
  },
);

const hasTags = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'hasTags',
  (build) => {
    const { GraphQLList, GraphQLNonNull, GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: new GraphQLList(new GraphQLNonNull(GraphQLString)),
    };
  },
  (value, helpers, build) => {
    const { sql, sqlTableAlias } = helpers;

    return sql.fragment`${sqlTableAlias}.tags @> (${sql.value(value)}::text[])`;
  },
);

const fullText = makeAddPgTableConditionPlugin(
  'app_public',
  'note',
  'fullText',
  (build) => {
    const { GraphQLString } = build.graphql;
    return {
      description: 'Filters to records matching one of these ids',
      type: GraphQLString,
    };
  },
  (value, helpers, build) => {
    const { sql, sqlTableAlias, queryBuilder } = helpers;

    queryBuilder.orderBy(
      sql.fragment`(ts_rank_cd(${sqlTableAlias}.weighted_keywords, websearch_to_tsquery('simple', ${sql.value(
        value,
      )})))`,
      false,
      false,
    );

    return sql.fragment`${sqlTableAlias}.weighted_keywords @@ websearch_to_tsquery('simple', ${sql.value(
      value,
    )})`;
  },
);

export default makePluginByCombiningPlugins(
  userByName,
  userByFqn,
  domain,
  onlyFollows,
  hideBlocks,
  dateStart,
  dateEnd,
  hasTags,
  fullText,
);
