import { TFollowAccept as FollowAccept } from '@cum/check/plugins';
import { Receive, Deliver, Cleanup } from './index.js';
import { Type } from '@sinclair/typebox';

export const receive: Receive<FollowAccept> = (doc, helpers) => {};

export const deliverGuard = Type.Object({
  followerId: Type.String(),
  followedId: Type.String(),
});

export const deliver: Deliver<typeof deliverGuard, FollowAccept> = (
  doc,
  { client, log, activityId, userId },
) => {};

// const deliver: Deliver<typeof deliverGuard, FollowAccept> = async (
//   { followerId, followedId, activityUrl },
//   { logger: log, withPgClient }
// ) => {
//   const followerActor = await withPgClient((client) => {
//     return fetchActorFromId(followerId, client);
//   });
//
//   if (!followerActor) {
//     throw new Error("invalid actor");
//   }
//
//   const followedActor = `${AP_URL.ACTOR}/${followedId}`;
//
//   return {
//     type: "Accept",
//     to: [followerActor],
//     cc: [],
//     actor: followedActor,
//     object: {
//       type: "Follow",
//       id: activityUrl,
//       actor: followerActor,
//       object: followedActor,
//     },
//   };
// };

export const cleanup: Cleanup<FollowAccept> = () => {};

// const afterSave: AfterSave<typeof deliverGuard> = async (
//   { followerId, followedId },
//   { withPgClient },
//   activityId
// ) => {
//   await withPgClient((client) =>
//     updateFollowActivity.run(
//       {
//         followerId,
//         followedId,
//         activityId,
//       },
//       client
//     )
//   );
// };
