import { TFollow as Follow } from '@cum/check/plugins';
import { Receive, Deliver, Cleanup } from './index.js';
import { Type } from '@sinclair/typebox';
import { isLocalUri } from '../util/uri.js';

const getDirection = (follower: string, followed: string) => {
  const lhs = isLocalUri(follower);
  const rhs = isLocalUri(followed);

  if (lhs && rhs) {
    return 'local';
  } else if (!lhs && !rhs) {
    return 'remote';
  } else if (!lhs) {
    return 'inbound';
  } else {
    return 'outbound';
  }
};

export const receive: Receive<Follow> = (doc, helpers) => {};

// const execute: Execute<Follow> = async (
//   doc,
//   { logger: log, withPgClient },
//   activityId
// ) => {
//   if (!doc.to.length) {
//     return log.warn("follow `to` field empty");
//   }
//
//   await withPgClient(async (client) => {
//     const followerId = await fetchIdFromActor(doc.actor, client);
//
//     for (const to of doc.to) {
//       const direction = getDirection(doc.actor, to);
//       const followedId = await fetchIdFromActor(to, client);
//
//       try {
//         return insertFollow.run(
//           {
//             followerId,
//             followedId,
//             activityId,
//             direction,
//           },
//           client
//         );
//       } catch (err) {
//         log.error(JSON.stringify(err));
//       }
//     }
//   });
// };

export const deliverGuard = Type.Object({
  followerId: Type.String(),
  followedId: Type.String(),
});

export const deliver: Deliver<typeof deliverGuard, Follow> = (
  doc,
  { client, log, activityId, userId },
) => {};

// const deliver: Deliver<typeof deliverGuard, Follow> = async (
//   { followerId, followedId },
//   { logger: log, withPgClient }
// ) => {
//   const followerActor = `${AP_URL.ACTOR}/${followerId}`;
//   const followedActor = await withPgClient((client) => {
//     return fetchActorFromId(followedId, client);
//   });
//
//   if (!followedActor) {
//     throw new Error("invalid actor");
//   }
//
//   return {
//     type: "Follow",
//     to: [followedActor],
//     actor: followerActor,
//     object: followedActor,
//   };
// };

export const cleanup: Cleanup<Follow> = () => {};

// const afterSave: AfterSave<typeof deliverGuard> = async (
//   { followerId, followedId },
//   { withPgClient },
//   activityId
// ) => {};
