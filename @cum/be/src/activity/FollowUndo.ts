import { TFollowUndo as FollowUndo } from '@cum/check/plugins';
import { Receive, Deliver, Cleanup } from './index.js';
import { Type } from '@sinclair/typebox';

export const receive: Receive<FollowUndo> = (doc, helpers) => {};

export const deliverGuard = Type.Object({});

export const deliver: Deliver<typeof deliverGuard, FollowUndo> = (
  doc,
  { client, log, activityId, userId },
) => {};

export const cleanup: Cleanup<FollowUndo> = () => {};
