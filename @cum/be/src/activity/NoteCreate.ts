import { TNoteCreate as NoteCreate } from '@cum/check/plugins';
import { Receive, Deliver, Cleanup } from './index.js';
import { Type } from '@sinclair/typebox';

export const receive: Receive<NoteCreate> = (doc, helpers) => {};

// const execute: Execute<NoteCreate> = async (
//   doc,
//   { logger: log, withPgClient },
//   activityId,
//   userId
// ) => {
//   if (!userId) {
//     return log.warn("missing userid for activity NoteCreate");
//   }
//
//   try {
//     await withPgClient((client) => {
//       return insertNote.run(
//         {
//           activityId,
//           userId,
//           scope: "public",
//           body: doc.object.content,
//           remote: true,
//           reply: null,
//         },
//         client
//       );
//     });
//   } catch (err: any) {
//     return log.error(err);
//   }
// };

export const deliverGuard = Type.Object({
  userId: Type.String(),
  noteId: Type.String(),
});

export const deliver: Deliver<typeof deliverGuard, NoteCreate> = (
  doc,
  { client, log, activityId, userId },
) => {};

// const deliver: Deliver<typeof deliverGuard, NoteCreate> = async (
//   { userId, noteId },
//   { logger: log, withPgClient }
// ) => {
//   const actor = `${AP_URL.ACTOR}/${userId}`;
//   const id = `${AP_URL.ACTIVITY}/${noteId}`;
//   const followers = `${AP_URL.FOLLOWERS}/${userId}`;
//   const content = "";
//
//   return {
//     type: "Create",
//     id,
//     actor,
//     to: ["https://www.w3.org/ns/activitystreams#Public"],
//     cc: [followers],
//     object: {
//       type: "Note",
//       actor,
//       id,
//       to: ["https://www.w3.org/ns/activitystreams#Public"],
//       content,
//     },
//   };
// };

export const cleanup: Cleanup<NoteCreate> = () => {};
