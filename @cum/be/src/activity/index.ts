import { Activity } from '@cum/check';
import { TSchema, Static } from '@sinclair/typebox';
import { JobHelpers } from 'graphile-worker';
import { PoolClient } from 'pg';

export * as Follow from './Follow.js';
export * as FollowAccept from './FollowAccept.js';
export * as FollowUndo from './FollowUndo.js';
export * as NoteCreate from './NoteCreate.js';

export type Helpers = {
  userId: string;
  activityId?: string;
  log: JobHelpers['logger'];
  client: PoolClient;
};

export type Receive<A extends Activity> = (doc: A, helpers: Helpers) => void;

export type Deliver<T extends TSchema, A extends Activity> = (
  doc: Static<T>,
  helpers: Helpers,
) => A | void;

export type Cleanup<A extends Activity> = (doc: A, helpers: Helpers) => void;

export type Plugin<T extends TSchema, A extends Activity> = {
  receive: Receive<A>;
  deliver: Deliver<T, A>;
  deliverGuard: T;
  cleanup?: Cleanup<A>;
};
