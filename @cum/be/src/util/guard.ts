import { TSchema, Static } from '@sinclair/typebox';
import { TypeCompiler } from '@sinclair/typebox/compiler/index.js';

export const Guard = <T extends TSchema>(schema: T) => {
  const C = TypeCompiler.Compile(schema);

  return (payload: any): payload is Static<T> => {
    return typeof payload === 'object' && C.Check(payload);
  };
};
