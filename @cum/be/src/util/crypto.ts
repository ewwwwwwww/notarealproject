import { PoolClient } from 'pg';
import { actorPubKey } from '@cum/db/query/actor';
import { userPubKeyById, userPrivKeyById } from '@cum/db/query/user';
import { isLocalUri, stripAnchor, convertUUID } from './uri.js';
/* import { fetchActor } from './fetch.js'; */
import { ClientRequest } from 'http';
import { createHash } from 'crypto';
import { activityUrl } from '@cum/config';

// @ts-ignore
import httpSignature from '@peertube/http-signature';

const verifyLocal = async (actor: string, headers: any, client: PoolClient) => {
  const userId = convertUUID(stripAnchor(actor));
  const user = await userPubKeyById.run({ userId }, client);

  if (!user.length) {
    return false;
  }

  return httpSignature.verifySignature(headers, user[0].pubkey);
};

export const verify = async (
  actor: string,
  headers: any,
  client: PoolClient,
): Promise<boolean> => {
  if (isLocalUri(actor)) {
    return verifyLocal(actor, headers, client);
  }

  const http = stripAnchor(actor);
  const user = await actorPubKey.run({ actor: http }, client);

  let pubkey;

  // FIXME: blah
  if (!user.length) {
    return false;
    /* const data = await fetchActor(http, client); */
    /*  */
    /* if (!data || !data.publicKey.publicKeyPem) { */
    /*   return false; */
    /* } */
    /*  */
    /* pubkey = data.publicKey.publicKeyPem; */
  } else {
    pubkey = user[0].pubkey;
  }

  return httpSignature.verify(headers, pubkey);
};

export const createDigest = (body: string) => {
  return createHash('sha256').update(body).digest('base64');
};

export const sign = (
  userId: string,
  privkey: string,
  request: ClientRequest,
): Promise<any> => {
  if (userId.length !== 36) {
    throw new Error(`invalid userId uuid: ${userId}`);
  }

  const options = {
    key: privkey,
    keyId: activityUrl.actor(userId) + '#main-key',
    authorizationHeaderName: 'Signature',
    // hideAlgorithm: true,
    headers: [
      '(request-target)',
      'content-length',
      'date',
      'digest',
      'host',
      'accept',
    ],
  };

  return httpSignature.sign(request, options);
};
