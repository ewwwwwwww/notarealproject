import { config } from '@cum/config';

const RE_DOMAIN = new RegExp(`^http?://${config.core.domain}`);

export const isLocalUri = (uri: string) => {
  return RE_DOMAIN.test(uri);
};

export const convertUUID = (uri: string) => {
  const uuid = uri.slice(-36);

  if (uuid.length !== 36) {
    throw new Error(`cannot parse uuid from uri: ${uri}`);
  }

  return uuid;
};

export const stripAnchor = (uri: string) => {
  return uri.replace('#main-key', ''); // TODO: generic strip
};
