// import got from 'got';
// import { PoolClient } from 'pg';
// import { TPerson } from '@cum/check/plugins';
// import { transmogrify } from '@cum/check';
// import { lru, LRU } from 'tiny-lru';
// import { actorUserId, actorInbox } from '@cum/db/query/actor';
// import { userRegisterRemote, userInboxById } from '@cum/db/query/user';
// import { isLocalUri, convertUUID } from './uri.js';
//
// const httpOptions = {
//   headers: {
//     accept: 'application/activity+json',
//   },
// };
//
// export const fetchActor = async (
//   actor: string,
//   client: PoolClient,
// ): Promise<(TPerson & { userId: string }) | undefined> => {
//   const resp = await got.get(actor, httpOptions).json();
//   const { hostname } = new URL(actor);
//   const { name, doc } = transmogrify<TPerson>(resp as {}) ?? {};
//
//   if (!name || !doc) {
//     throw new Error(`invalid actor ${name} ${actor}`);
//   }
//
//   const user = await userRegisterRemote.run(
//     {
//       name: doc.name,
//       domain: hostname,
//       id: doc.id,
//       inbox: doc.inbox,
//       pubkey: doc.publicKey.publicKeyPem,
//     },
//     client,
//   );
//
//   if (!user.length || !user[0].userId) {
//     throw new Error(`could not register actor ${doc.name}@${hostname}`);
//   }
//
//   const { userId } = user[0];
//
//   // set LRU caches
//   actorIdCache.set(actor, userId);
//   actorInboxCache.set(actor, doc.inbox);
//   idActorCache.set(userId, actor);
//
//   return { ...doc, userId };
// };
//
// const actorIdCache = lru<string>(1000);
//
// export const fetchIdFromActor = async (
//   actor: string,
//   client: PoolClient,
// ): Promise<string | undefined> => {
//   if (isLocalUri(actor)) return convertUUID(actor);
//
//   let res = actorIdCache.get(actor);
//
//   if (!res) {
//     const user = await actorUserId.run({ actor }, client);
//
//     if (!user.length) {
//       const user = await fetchActor(actor, client);
//
//       if (!user) return;
//
//       res = user.userId;
//     } else {
//       res = user[0].userId;
//       actorIdCache.set(actor, res);
//     }
//   }
//
//   return res;
// };
//
// const actorInboxCache = lru<string>(1000);
//
// export const fetchInboxFromActor = async (
//   actor: string,
//   client: PoolClient,
// ): Promise<string | undefined> => {
//   let res = actorInboxCache.get(actor);
//
//   if (!res) {
//     const user = await actorInbox.run({ actor }, client);
//
//     if (!user.length) {
//       const user = await fetchActor(actor, client);
//
//       if (!user) return;
//
//       res = user.inbox;
//     } else {
//       res = user[0].inbox;
//       actorInboxCache.set(actor, res);
//     }
//   }
//
//   return res;
// };
//
// const idActorCache = lru<string>(1000);
//
// export const fetchActorFromId = async (
//   userId: string,
//   client: PoolClient,
// ): Promise<string | undefined> => {
//   let res = idActorCache.get(userId);
//
//   if (!res) {
//     const user = await userInboxById.run({ userId }, client);
//
//     if (!user.length) {
//       throw new Error('invalid user id');
//     }
//
//     res = user[0].actor;
//     idActorCache.set(userId, user[0].actor);
//   }
//
//   return res;
// };
//
