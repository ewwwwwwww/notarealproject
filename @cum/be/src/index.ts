import type Pg from 'pg';

import Fastify from 'fastify';
import postgres, { PostgresDb } from '@fastify/postgres';

import worker, { startWorker, WorkerUtils } from './plugin/worker.js';
import jsonActivity from './plugin/activityContentType.js';
import postgraphile from './plugin/postgraphile.js';
import tus from './plugin/tus.js';
import { createPool } from './pgpool.js';
import { config, connectionString } from '@cum/config';
import cors from '@fastify/cors';
import cookie from '@fastify/cookie';
import serveStatic from '@fastify/static';

import * as routes from './route/index.js';
import { resolve } from 'path';

type FastifyPostgresRouteOptions = {
  transact: boolean | string;
};

declare module 'fastify' {
  export interface FastifyInstance {
    pg: PostgresDb & Record<string, PostgresDb>;
    worker: WorkerUtils;
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface FastifyInstance {}

  export interface FastifyRequest {
    pg?: Pg.PoolClient;
  }

  export interface RouteShorthandOptions {
    pg?: FastifyPostgresRouteOptions;
  }
}

// @ts-ignore
const fastify = Fastify({
  logger: true,
});

const start = async () => {
  const pgPool = createPool();

  await fastify.register(cookie, {
    secret: 'my-secret', // for cookies signature
    hook: 'onRequest', // set to false to disable cookie autoparsing or set autoparsing on any of the following hooks: 'onRequest', 'preParsing', 'preHandler', 'preValidation'. default: 'onRequest'
    parseOptions: {}, // options for parsing cookies
  });

  await fastify.register(cors, {
    // FIXME: cors
    origin: true,
    credentials: true,
  });

  fastify.register(postgres, { connectionString });
  fastify.register(worker, { pgPool });
  fastify.register(tus);
  fastify.register(jsonActivity);
  fastify.register(postgraphile, { pgPool });

  fastify.register(serveStatic, {
    root: resolve('../../uploads/file'),
    prefix: '/file/', // optional: default '/'
  });

  for (const route of Object.values(routes)) {
    fastify.register(route);
  }

  try {
    await startWorker(pgPool);
    await fastify.listen({ port: config.backend.port });
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
