import pg from 'pg';
import { config } from '@cum/config';

const { Pool } = pg;

// TODO: env option to tweak connection pool settings
export const createPool = () => {
  return new Pool({
    database: config.database.name,
    user: config.database.user,
    password: config.database.pass,
    host: config.database.host,
    port: config.database.port,
    ssl: config.database.ssl,
    max: 20, // set pool max size to 20
    idleTimeoutMillis: 1000, // close idle clients after 1 second
    connectionTimeoutMillis: 1000, // return an error after 1 second if connection could not be established
    maxUses: 7500, // close (and replace) a connection after it has been used 7500 times (see below for discussion)
  });
};
