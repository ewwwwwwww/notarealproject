import schema from './__generated__/schema.js';
import Ajv from 'ajv';
import addFormats from 'ajv-formats';

// @ts-ignore
export const ajv = new Ajv({
  removeAdditional: true,
  // @ts-ignore
  allErrors: process?.env?.NODE_ENV === 'test',
  // coerceTypes: true,
  allowUnionTypes: true,
});

// @ts-ignore
addFormats(ajv, { mode: 'fast' });

for (const [key, value] of Object.entries(schema)) {
  ajv.addSchema(value, key);
}
