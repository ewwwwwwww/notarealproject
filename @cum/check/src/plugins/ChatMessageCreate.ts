import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface ChatMessageCreate extends Activity {
  type: 'Create';
  object: {
    type: 'ChatMessage';
    id: string;
    content: string;
  };
}

const guard: Guard = (doc) => {
  return doc.type === 'Create';
};

const subguard: Guard = (doc) => {
  return doc.type === 'ChatMessage';
};

export interface Greedy extends Activity {
  type: 'Create';
  object: {
    type: 'ChatMessage';
    id: string;
    content: string;
  };
}

const Greedy: Parse<Greedy, ChatMessageCreate> = (doc) => doc;

export const plugin: ParserPlugin<ChatMessageCreate> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Create';

  const activity: ChatMessageCreate = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: {
      type: 'ChatMessage',
      id: randObjectUrl(),
      content: 'blah',
    },
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      id: 'https://social.tld.com/activities/207b1efa-a3b3-4ec4-885f-610c59a4d7ef',
      object: {
        actor: 'https://social.tld.com/users/test3',
        attachment: null,
        attributedTo: 'https://social.tld.com/users/test3',
        content: 'test',
        conversation: null,
        id: 'https://social.tld.com/objects/2bdede01-783c-40c6-bb1c-7f7087fb3c6f',
        published: '2021-11-21T18:34:13.679206Z',
        tag: [
          {
            href: 'https://dev.social/ap/actor/smeek',
            name: '@smeek@dev.social',
            type: 'Mention',
          },
        ],
        to: ['https://dev.social/ap/actor/smeek'],
        type: 'ChatMessage',
      },
      to: ['https://dev.social/ap/actor/smeek'],
      type: 'Create',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<ChatMessageCreate>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('ChatMessageCreate', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
