import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface PersonUpdate extends Activity {
  type: 'Update';
}

const guard: Guard = (doc) => {
  return doc.type === 'Update';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Person';
};

export interface Greedy extends Activity {
  type: 'Update';
}

const Greedy: Parse<Greedy, PersonUpdate> = (doc) => doc;

export const plugin: ParserPlugin<PersonUpdate> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Update';

  const activity: PersonUpdate = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test7',
      cc: [],
      id: 'https://social.tld.com/activities/d8fe3e82-0ef2-4b80-a9f7-c1d159135054',
      object: {
        alsoKnownAs: [],
        attachment: [],
        capabilities: {
          acceptsChatMessages: true,
        },
        discoverable: false,
        endpoints: {
          oauthAuthorizationEndpoint: 'https://social.tld.com/oauth/authorize',
          oauthRegistrationEndpoint: 'https://social.tld.com/api/v1/apps',
          oauthTokenEndpoint: 'https://social.tld.com/oauth/token',
          sharedInbox: 'https://social.tld.com/inbox',
          uploadMedia: 'https://social.tld.com/api/ap/upload_media',
        },
        featured: 'https://social.tld.com/users/test7/collections/featured',
        followers: 'https://social.tld.com/users/test7/followers',
        following: 'https://social.tld.com/users/test7/following',
        icon: {
          type: 'Image',
          url: 'https://social.tld.com/media/5f43ae5318923a52b9a944c00f44bca07832344d9408f9c5d13c6da01ba3b88e.jpg',
        },
        id: 'https://social.tld.com/users/test7',
        inbox: 'https://social.tld.com/users/test7/inbox',
        manuallyApprovesFollowers: false,
        name: 'test',
        outbox: 'https://social.tld.com/users/test7/outbox',
        preferredUsername: 'test7',
        publicKey: {
          id: 'https://social.tld.com/users/test7#main-key',
          owner: 'https://social.tld.com/users/test7',
          publicKeyPem:
            '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyr1qAHd5UYWtQsSjKzC0\nc9bHCOt8W+Nn6PCvnqoG592OUQgrD5Y3lpVp8V0Eb3PzZWwq9Fb/geYaV4SzH1ct\nLB12oAKBgVdp+Y2LtTdvTIS1slw3hLxGT9uvTfVr7fSU25VJH1G9wT6sQI4Gc6oc\ntGMQ8ddIBmcDJOw9m4ywrMGpGcnkfQWJXtqDf5I4ofWBR445gt0hivYlbppoMISJ\n/5MPxDFVs04jfM9q5JpwEoQ0EoDBybE7ZKpWccIVMhNHvjAiBwtRHZE9usoCEnIs\nDXNax5gBBjORp7P5Z30t4yrKpCbEA+EMnrsSHeKyLIgMOx6ZrLVHtNoKdukTuJXA\n0wIDAQAB\n-----END PUBLIC KEY-----\n\n',
        },
        summary: '',
        tag: [],
        type: 'Person',
        url: 'https://social.tld.com/users/test7',
      },
      to: [
        'https://social.tld.com/users/test7/followers',
        'https://www.w3.org/ns/activitystreams#Public',
      ],
      type: 'Update',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<PersonUpdate>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('PersonUpdate', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
