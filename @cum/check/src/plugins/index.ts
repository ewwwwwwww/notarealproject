export * as Announce from './Announce.js';
export type { Announce as TAnnounce } from './Announce.js';
export * as ChatMessageCreate from './ChatMessageCreate.js';
export type { ChatMessageCreate as TChatMessageCreate } from './ChatMessageCreate.js';
export * as Delete from './Delete.js';
export type { Delete as TDelete } from './Delete.js';
export * as EmojiReact from './EmojiReact.js';
export type { EmojiReact as TEmojiReact } from './EmojiReact.js';
export * as Follow from './Follow.js';
export type { Follow as TFollow } from './Follow.js';
export * as FollowAccept from './FollowAccept.js';
export type { FollowAccept as TFollowAccept } from './FollowAccept.js';
export * as FollowReject from './FollowReject.js';
export type { FollowReject as TFollowReject } from './FollowReject.js';
export * as FollowUndo from './FollowUndo.js';
export type { FollowUndo as TFollowUndo } from './FollowUndo.js';
export * as Like from './Like.js';
export type { Like as TLike } from './Like.js';
export * as LikeUndo from './LikeUndo.js';
export type { LikeUndo as TLikeUndo } from './LikeUndo.js';
export * as NoteCreate from './NoteCreate.js';
export type { NoteCreate as TNoteCreate } from './NoteCreate.js';
export * as Person from './Person.js';
export type { Person as TPerson } from './Person.js';
export * as PersonUpdate from './PersonUpdate.js';
export type { PersonUpdate as TPersonUpdate } from './PersonUpdate.js';

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { globby } = await import('globby');
  const { resolve, basename } = await import('path');
  const { default: schema } = await import('../__generated__/schema.js');

  const dir = resolve(__dirname, './*.ts');
  const paths = await globby([dir, '!**/index.ts']);
  const files = paths.map((f) => basename(f));
  const modules = Object.keys(module.exports);

  const startsWithUpper = (word: string) => {
    return word.charAt(0) === word.charAt(0).toUpperCase();
  };

  it.concurrent('exporting all plugins', async () => {
    for (const file of files.map((f) => f.replace('.ts', ''))) {
      expect(module.exports).toHaveProperty(file);
    }
  });

  it.concurrent('plugins have a root plugin export', async () => {
    for (const file of paths) {
      const plugin = await import(file);

      expect(plugin).toHaveProperty('plugin');
      expect(Object.keys(plugin).length).toBe(1);
    }
  });

  it.concurrent('plugins export has a guard and parsers', async () => {
    for (const file of paths) {
      const { plugin } = await import(file);

      expect(plugin).toBeInstanceOf(Object);
      expect(plugin).toHaveProperty('guard');
      expect(plugin).toHaveProperty('parsers');
      expect(Object.keys(plugin.parsers).length).toBeGreaterThan(0);
    }
  });

  it.concurrent('plugins have a generated schema', async () => {
    for (const file of files) {
      const { plugin } = await import('./' + file);

      for (const parser of Object.keys(plugin.parsers)) {
        expect(schema).toHaveProperty(`${file.replace('.ts', '')}:${parser}`);
      }
    }
  });

  it.concurrent('root export must start with uppercase letter', async () => {
    for (const name of modules) {
      expect(startsWithUpper(name)).toBe(true);
    }
  });

  it.concurrent('filenames must start with an uppercase letter', async () => {
    for (const file of paths) {
      expect(startsWithUpper(file)).toBe(true);
    }
  });
}
