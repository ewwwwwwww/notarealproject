import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface Person extends Activity {
  type: 'Person';

  /**
   * The temporary nickname of the person
   */
  name: string;

  /**
   * The permanent name of the person
   */
  preferredUsername: string;

  /**
   * The person's inbox for receiving activities
   * @TJS-format uri
   */
  inbox: string;

  /**
   * The person's outbox for displaying activities
   * @TJS-format uri
   */
  outbox: string;

  /**
   * The person's lists of followers
   * @TJS-format uri
   */
  followers: string;

  /**
   * The person's lists of following
   * @TJS-format uri
   */
  following: string;

  /**
   * The person's featured posts (i.e. stickies)
   * @TJS-format uri
   */
  featured: string;

  /**
   * List of endpoints
   */
  endpoints?: {
    /**
     * The shared inbox of the person's instance
     * @TJS-format uri
     */
    sharedInbox?: string;
  };

  /**
   * Public key validation for signature signing
   */
  publicKey: {
    /**
     * Actor's key location
     * @TJS-format uri
     */
    id: string;

    /**
     * Actor's location
     * @TJS-format uri
     */
    owner: string;

    /**
     * Pubkey
     */
    publicKeyPem: string;
  };

  /**
   * Person requires approval for follow requests
   */
  manuallyApprovesFollowers?: boolean;

  /**
   * Person's profile description
   */
  summary?: string;

  /**
   * TODO: write this
   */
  tag?: string[];

  /**
   * TODO: write this
   */
  attachment?: any[];

  /**
   * TODO: write this
   */
  alsoKnownAs?: any[];

  /**
   * TODO: write this
   */
  capabilities?: {
    /**
     * TODO: write this
     */
    acceptsChatMessages?: boolean;
  };

  /**
   * TODO: write this
   */
  discoverable?: boolean;
}

const guard: Guard = (doc) => {
  return doc.type === 'Person';
};

export interface Greedy extends Person {
  type: 'Person';
}

const Greedy: Parse<Greedy, Person> = (doc) => doc;

export const plugin: ParserPlugin<Person> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Person';

  const activity: Person = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
    name: '',
    preferredUsername: '',
    inbox: '',
    outbox: '',
    followers: '',
    following: '',
    featured: '',
    publicKey: {
      id: '',
      owner: '',
      publicKeyPem: '',
    },
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      alsoKnownAs: [],
      attachment: [],
      capabilities: {
        acceptsChatMessages: true,
      },
      discoverable: false,
      endpoints: {
        oauthAuthorizationEndpoint: 'https://social.tld.com/oauth/authorize',
        oauthRegistrationEndpoint: 'https://social.tld.com/api/v1/apps',
        oauthTokenEndpoint: 'https://social.tld.com/oauth/token',
        sharedInbox: 'https://social.tld.com/inbox',
        uploadMedia: 'https://social.tld.com/api/ap/upload_media',
      },
      featured: 'https://social.tld.com/users/test7/collections/featured',
      followers: 'https://social.tld.com/users/test7/followers',
      following: 'https://social.tld.com/users/test7/following',
      icon: {
        type: 'Image',
        url: 'https://social.tld.com/media/5f43ae5318923a52b9a944c00f44bca07832344d9408f9c5d13c6da01ba3b88e.jpg',
      },
      id: 'https://social.tld.com/users/test7',
      inbox: 'https://social.tld.com/users/test7/inbox',
      manuallyApprovesFollowers: false,
      name: 'test',
      outbox: 'https://social.tld.com/users/test7/outbox',
      preferredUsername: 'test7',
      publicKey: {
        id: 'https://social.tld.com/users/test7#main-key',
        owner: 'https://social.tld.com/users/test7',
        publicKeyPem:
          '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyr1qAHd5UYWtQsSjKzC0\nc9bHCOt8W+Nn6PCvnqoG592OUQgrD5Y3lpVp8V0Eb3PzZWwq9Fb/geYaV4SzH1ct\nLB12oAKBgVdp+Y2LtTdvTIS1slw3hLxGT9uvTfVr7fSU25VJH1G9wT6sQI4Gc6oc\ntGMQ8ddIBmcDJOw9m4ywrMGpGcnkfQWJXtqDf5I4ofWBR445gt0hivYlbppoMISJ\n/5MPxDFVs04jfM9q5JpwEoQ0EoDBybE7ZKpWccIVMhNHvjAiBwtRHZE9usoCEnIs\nDXNax5gBBjORp7P5Z30t4yrKpCbEA+EMnrsSHeKyLIgMOx6ZrLVHtNoKdukTuJXA\n0wIDAQAB\n-----END PUBLIC KEY-----\n\n',
      },
      summary: '',
      tag: [],
      type: 'Person',
      url: 'https://social.tld.com/users/test7',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Person>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Person', doc)).toBe(true);
  });

  it.concurrent('evals misskey', () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://w3id.org/security/v1',
        {
          manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
          sensitive: 'as:sensitive',
          Hashtag: 'as:Hashtag',
          quoteUrl: 'as:quoteUrl',
          toot: 'http://joinmastodon.org/ns#',
          Emoji: 'toot:Emoji',
          featured: 'toot:featured',
          discoverable: 'toot:discoverable',
          schema: 'http://schema.org#',
          PropertyValue: 'schema:PropertyValue',
          value: 'schema:value',
          misskey: 'https://misskey-hub.net/ns#',
          _misskey_content: 'misskey:_misskey_content',
          _misskey_quote: 'misskey:_misskey_quote',
          _misskey_reaction: 'misskey:_misskey_reaction',
          _misskey_votes: 'misskey:_misskey_votes',
          _misskey_talk: 'misskey:_misskey_talk',
          isCat: 'misskey:isCat',
          vcard: 'http://www.w3.org/2006/vcard/ns#',
        },
      ],
      type: 'Person',
      id: 'https://niscii.xyz/users/8sk42i63km',
      inbox: 'https://niscii.xyz/users/8sk42i63km/inbox',
      outbox: 'https://niscii.xyz/users/8sk42i63km/outbox',
      followers: 'https://niscii.xyz/users/8sk42i63km/followers',
      following: 'https://niscii.xyz/users/8sk42i63km/following',
      featured: 'https://niscii.xyz/users/8sk42i63km/collections/featured',
      sharedInbox: 'https://niscii.xyz/inbox',
      endpoints: { sharedInbox: 'https://niscii.xyz/inbox' },
      url: 'https://niscii.xyz/@emma',
      preferredUsername: 'emma',
      name: '恵真 :tweakercat:',
      summary:
        "<p><i>​:fatcat:​</i><span><br><br>i'm just playing around with misskey<br>here i ramble a bit about video games, cats and japanese language.<br>also a lot of </span>​:ffxiv:​<span> (spoiler/ネタバレ注意)<br><br>たまに日本語で投稿します。<br>ヘンテコ日本語で申し訳ないですm(＿ ＿)m<br><br></span>​:windowsflag:​<span> windows is my favourite OS</span></p>",
      icon: {
        type: 'Image',
        url: 'https://niscii001.s3.us-east-2.amazonaws.com/misskey_obj/webpublic-27565ddf-5cbd-4c09-bd1d-98556ddc6157.png',
        sensitive: false,
        name: null,
      },
      image: {
        type: 'Image',
        url: 'https://niscii001.s3.us-east-2.amazonaws.com/misskey_obj/webpublic-583693ca-6a9d-4229-8af4-0aa4996e8266.png',
        sensitive: false,
        name: null,
      },
      tag: [
        {
          id: 'https://niscii.xyz/emojis/tweakercat',
          type: 'Emoji',
          name: ':tweakercat:',
          updated: '2022-05-01T23:46:04.062Z',
          icon: {
            type: 'Image',
            mediaType: 'image/apng',
            url: 'https://niscii.xyz/files/9cd7252f-29f6-4615-bdb4-1d4e3e0e7d82',
          },
        },
        {
          id: 'https://niscii.xyz/emojis/fatcat',
          type: 'Emoji',
          name: ':fatcat:',
          updated: '2022-01-15T22:43:06.771Z',
          icon: {
            type: 'Image',
            mediaType: 'image/png',
            url: 'https://niscii.xyz/files/d5c48947-c347-4098-a146-c9c1b7b40705',
          },
        },
        {
          id: 'https://niscii.xyz/emojis/ffxiv',
          type: 'Emoji',
          name: ':ffxiv:',
          updated: '2022-01-13T12:00:43.246Z',
          icon: {
            type: 'Image',
            mediaType: 'image/png',
            url: 'https://niscii.xyz/files/097275f1-98b5-470a-8676-c06a7b38797b',
          },
        },
        {
          id: 'https://niscii.xyz/emojis/windowsflag',
          type: 'Emoji',
          name: ':windowsflag:',
          updated: '2022-05-01T23:46:51.750Z',
          icon: {
            type: 'Image',
            mediaType: 'image/png',
            url: 'https://niscii.xyz/files/webpublic-8a5f0ddf-0c4f-4844-97e0-6185120254b1',
          },
        },
      ],
      manuallyApprovesFollowers: false,
      discoverable: true,
      publicKey: {
        id: 'https://niscii.xyz/users/8sk42i63km#main-key',
        type: 'Key',
        owner: 'https://niscii.xyz/users/8sk42i63km',
        publicKeyPem:
          '-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAw3b9BnfAhX6uuvPWkVYJ\ncD1UZQGBPmh0t5BY/3AbDZw977pWerlensXQ4FsXENt7oAQcXO4/EeZFm1dI0w1e\nIgy93lkBMWzUynMUGNX2qNDAuVCKaxe4REfeRkPFXK04rjTI4p8wllaWu1SMJUiO\nB5vsoBKZFNuMc5W03KhZDMtOve+idEe/gLYhgDRucSQsoFbZHxNYpFUpIdUOMy/i\nyvMo3hLIIrBM+mH4f82ThwjlvtaIgpIvg/XBCzrhWzWSo4W8qX1op31efzNiZ0kp\nJf/ojL/niK1x3a7IjxRMZxLnuymz9SlLZyhy1YaGBDE9wsPAp5CRFI0VLpnSxgMx\nSAa3lPsV44f34kQFAIrXiLFEPdsD9CIa5yRTgzpF8055K6a02Hy7cdU/drU3OzRH\n2rc1Dq9ARCm0S213Kv6dNTDoEMuKQl/Nwd9ddwtzgCdOAPiC4+LiGTH4c//uwCJC\nAJnmH6Ff0Ri4bCyixE3EYOPmz8EfnkYbe54DInqhUGNbiRrcUYWAWm6/G+wq9zK1\nFAH5c5dPA3x/GLDbONOOGiPcs4qEYMCvXY8m9LbLestupmRFT6lEOkYTAIdBV5oY\nJnJ1+HJXBbzOKUPG75Fec8yvSpaKz0jFUssIwR1s8JsRS3kPShcuiE5Bxr1305MF\n/uKn7kigPkEVIyqDli2BVxcCAwEAAQ==\n-----END PUBLIC KEY-----\n',
      },
      isCat: false,
      attachment: [
        {
          type: 'PropertyValue',
          name: 'サブアカウント',
          value: '@elezen@ffxiv-mastodon.com',
        },
        {
          type: 'PropertyValue',
          name: 'if my instance dies look for me here',
          value: '@emma@poa.st',
        },
      ],
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Person>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Person', doc)).toBe(true);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://w3id.org/security/v1',
        {
          manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
          toot: 'http://joinmastodon.org/ns#',
          featured: { '@id': 'toot:featured', '@type': '@id' },
          featuredTags: { '@id': 'toot:featuredTags', '@type': '@id' },
          alsoKnownAs: { '@id': 'as:alsoKnownAs', '@type': '@id' },
          movedTo: { '@id': 'as:movedTo', '@type': '@id' },
          schema: 'http://schema.org#',
          PropertyValue: 'schema:PropertyValue',
          value: 'schema:value',
          discoverable: 'toot:discoverable',
          Device: 'toot:Device',
          Ed25519Signature: 'toot:Ed25519Signature',
          Ed25519Key: 'toot:Ed25519Key',
          Curve25519Key: 'toot:Curve25519Key',
          EncryptedMessage: 'toot:EncryptedMessage',
          publicKeyBase64: 'toot:publicKeyBase64',
          deviceId: 'toot:deviceId',
          claim: { '@type': '@id', '@id': 'toot:claim' },
          fingerprintKey: { '@type': '@id', '@id': 'toot:fingerprintKey' },
          identityKey: { '@type': '@id', '@id': 'toot:identityKey' },
          devices: { '@type': '@id', '@id': 'toot:devices' },
          messageFranking: 'toot:messageFranking',
          messageType: 'toot:messageType',
          cipherText: 'toot:cipherText',
          suspended: 'toot:suspended',
          focalPoint: { '@container': '@list', '@id': 'toot:focalPoint' },
        },
      ],
      id: 'https://sperg.city/users/dick',
      type: 'Person',
      following: 'https://sperg.city/users/dick/following',
      followers: 'https://sperg.city/users/dick/followers',
      inbox: 'https://sperg.city/users/dick/inbox',
      outbox: 'https://sperg.city/users/dick/outbox',
      featured: 'https://sperg.city/users/dick/collections/featured',
      featuredTags: 'https://sperg.city/users/dick/collections/tags',
      preferredUsername: 'dick',
      name: 'Dick Masterson',
      summary: '\u003cp\u003eAmerica\u0026#39;s Wingman.\u003c/p\u003e',
      url: 'https://sperg.city/@dick',
      manuallyApprovesFollowers: false,
      discoverable: false,
      published: '2022-09-10T00:00:00Z',
      devices: 'https://sperg.city/users/dick/collections/devices',
      publicKey: {
        id: 'https://sperg.city/users/dick#main-key',
        owner: 'https://sperg.city/users/dick',
        publicKeyPem:
          '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAylDzSc06Zk9K64HBDENo\nHozay/7eLTJUtuIXcrzxPLzoDlUFYXxrKiQbtHdopgCcb5yzIL1iGTAgYtp1Y0oi\nb4omQgNwgWvX7d3d3+ZF8Sxn3p1bUAOXEmQ4Upo6HZolKyYFj0X8h/ZGprYM7KFM\n+je7qCxBGKjTr4IpuBn8QhDxAuPeY2i3wLGuklQrElydQic4rKtcwW7U41ICIEqO\np1QJ/nNIlfYIPI22rZgOc8oFzar+SdHls7oeQX6k1JQ1aBIQQFUEz6P4KruxHA39\n8G2hX1NcenWjStUpu+ny2lavj5mbsUOFBIrZbXPAY7E/IHuNP7kGYKaK2evTYzgf\nQQIDAQAB\n-----END PUBLIC KEY-----\n',
      },
      tag: [],
      attachment: [],
      endpoints: { sharedInbox: 'https://sperg.city/inbox' },
      icon: {
        type: 'Image',
        mediaType: 'image/jpeg',
        url: 'https://cdn.masto.host/spergcity/accounts/avatars/108/975/786/540/205/760/original/79e7119836d2e3b1.jpg',
      },
      image: {
        type: 'Image',
        mediaType: 'image/png',
        url: 'https://cdn.masto.host/spergcity/accounts/headers/108/975/786/540/205/760/original/c289ea4a26521bd6.png',
      },
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Person>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Person', doc)).toBe(true);
  });
}
