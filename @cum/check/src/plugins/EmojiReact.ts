import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface EmojiReact extends Activity {
  type: 'EmojiReact';
}

const guard: Guard = (doc) => {
  return doc.type === 'EmojiReact';
};

export interface Greedy extends Activity {
  type: 'EmojiReact';
}

const Greedy: Parse<Greedy, EmojiReact> = (doc) => doc;

export const plugin: ParserPlugin<EmojiReact> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'EmojiReact';

  const activity: EmojiReact = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      cc: ['https://www.w3.org/ns/activitystreams#Public'],
      content: 'xf0x9fxaax80',
      context:
        'https://social.tld.com/contexts/1b0b0c9d-9e2d-40b4-a879-5240425ac815',
      id: 'https://social.tld.com/activities/7f98b3ed-0a6d-4c5c-b8b0-91e0d1c251a3',
      object:
        'https://social.tld.com/objects/7870e97a-cb4f-49c9-ab2b-c2fe528fbc81',
      to: ['https://social.tld.com/users/test3/followers'],
      type: 'EmojiReact',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<EmojiReact>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('EmojiReact', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
