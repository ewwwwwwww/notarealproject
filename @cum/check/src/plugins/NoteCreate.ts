import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface NoteCreate extends Activity {
  type: 'Create';
  object: {
    type: 'Note';
    id: string;
    content: string;
  };
}

const guard: Guard = (doc) => {
  return doc.type === 'Create';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Note';
};

export interface Greedy extends Activity {
  type: 'Create';
  object: {
    type: 'Note';
    id: string;
    content: string;
  };
}

const Greedy: Parse<Greedy, NoteCreate> = (doc) => doc;

export const plugin: ParserPlugin<NoteCreate> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Create';

  const activity: NoteCreate = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: {
      type: 'Note',
      id: randObjectUrl(),
      content: 'blah',
    },
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://pleroma.local/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://pleroma.local/users/a',
      cc: ['https://pleroma.local/users/a/followers'],
      context:
        'https://pleroma.local/contexts/2ddfca59-53bb-48e0-9101-dee7a4aca993',
      context_id: 124,
      directMessage: false,
      id: 'https://pleroma.local/activities/ddfec888-e799-4a45-8804-f4bde3d8a7ec',
      object: {
        actor: 'https://pleroma.local/users/a',
        attachment: [],
        attributedTo: 'https://pleroma.local/users/a',
        cc: ['https://pleroma.local/users/a/followers'],
        content: 'this is an example comment',
        context:
          'https://pleroma.local/contexts/2ddfca59-53bb-48e0-9101-dee7a4aca993',
        conversation:
          'https://pleroma.local/contexts/2ddfca59-53bb-48e0-9101-dee7a4aca993',
        id: 'https://pleroma.local/objects/39508ad4-b20f-4684-8f59-1899c4e13e14',
        published: '2021-11-21T20:12:57.559765Z',
        sensitive: null,
        source: 'this is an example comment',
        summary: '',
        tag: [],
        to: ['https://www.w3.org/ns/activitystreams#Public'],
        type: 'Note',
      },
      published: '2021-11-21T20:12:57.559096Z',
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      type: 'Create',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<NoteCreate>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('NoteCreate', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
