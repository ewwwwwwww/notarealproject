import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export type TFollowState = 'pending' | 'cancelled' | 'accept' | 'reject';

export interface Follow extends Activity {
  type: 'Follow';
}

const guard: Guard = (doc) => {
  return doc.type === 'Follow';
};

export interface Greedy extends Activity {
  type: 'Follow';
}

const Greedy: Parse<Greedy, Follow> = (doc) => doc;

export const plugin: ParserPlugin<Follow> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Follow';

  const activity: Follow = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://pleroma.local/schemas/litepub-0.1.jsonld',
        { '@language': 'und' },
      ],
      type,
      actor: 'http://pleroma.local/users/a',
      cc: [],
      id: 'https://pleroma.local/activities/f3c483e9-c10b-4223-8c7a-89e244caa8d0',
      object: 'https://pleroma.local/ap/actor/b',
      state: 'pending',
      to: ['https://pleroma.local/ap/actor/b'],
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Follow>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Follow', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
