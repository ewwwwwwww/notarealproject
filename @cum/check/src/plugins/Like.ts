import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface Like extends Activity {
  type: 'Like';
}

const guard: Guard = (doc) => {
  return doc.type === 'Like';
};

export interface Greedy extends Activity {
  type: 'Like';
}

const Greedy: Parse<Greedy, Like> = (doc) => doc;

export const plugin: ParserPlugin<Like> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Like';

  const activity: Like = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      cc: [
        'https://dev.social/ap/actor/smeek',
        'https://www.w3.org/ns/activitystreams#Public',
      ],
      context:
        'https://social.tld.com/contexts/ed2b9d67-39c8-477c-aa3b-5726495a0be7',
      id: 'https://social.tld.com/activities/877ef938-9196-4159-9447-66bce1323ba7',
      object:
        'https://social.tld.com/objects/0f340d65-9452-48e3-a2cb-162535942535',
      to: ['https://social.tld.com/users/test3/followers'],
      type: 'Like',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Like>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Like', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
