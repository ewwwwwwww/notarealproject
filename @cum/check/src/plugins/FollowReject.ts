import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface FollowReject extends Activity {
  type: 'Reject';
}

const guard: Guard = (doc) => {
  return doc.type === 'Reject';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Follow';
};

export interface Greedy extends Activity {
  type: 'Reject';
}

const Greedy: Parse<Greedy, FollowReject> = (doc) => doc;

export const plugin: ParserPlugin<FollowReject> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Reject';

  const activity: FollowReject = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/asdf',
      cc: [],
      id: 'https://social.tld.com/activities/b53f6c1a-e38c-49cc-9982-74ae22daf270',
      object: {
        actor: 'https://dev.social/ap/actor/smeek',
        id: 'https://dev.social/ap/objects/6',
        object: 'https://social.tld.com/users/asdf',
        type: 'Follow',
      },
      to: ['https://dev.social/ap/actor/smeek'],
      type: 'Reject',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<FollowReject>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('FollowReject', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
