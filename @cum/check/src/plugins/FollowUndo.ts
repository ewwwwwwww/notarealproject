import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface FollowUndo extends Activity {
  type: 'Undo';
}

const guard: Guard = (doc) => {
  return doc.type === 'Undo';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Follow';
};

export interface Greedy extends Activity {
  type: 'Undo';
}

const Greedy: Parse<Greedy, FollowUndo> = (doc) => doc;

export const plugin: ParserPlugin<FollowUndo> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Undo';

  const activity: FollowUndo = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://pleroma.local/schemas/litepub-0.1.jsonld',
        { '@language': 'und' },
      ],
      actor: 'http://pleroma.local/users/admin',
      context:
        'https://pleroma.local/contexts/4d2928ec-fb09-489d-83d0-ff9bc6ae2301',
      id: 'https://pleroma.local/activities/2c30b3bb-f9e7-4e7a-be05-8e80a5486a78',
      object: {
        actor: 'http://pleroma.local/users/admin',
        cc: [],
        context:
          'https://pleroma.local/contexts/4d2928ec-fb09-489d-83d0-ff9bc6ae2301',
        context_id: 71,
        id: 'https://pleroma.local/activities/8a12870d-6a07-4747-b5d7-423eee5dfe90',
        object: 'https://cumfe.local/ap/actor/test4',
        published: '2022-08-23T10:41:51.963906Z',
        state: 'cancelled',
        to: ['https://cumfe.local/ap/actor/test4'],
        type: 'Follow',
      },
      published: '2022-08-23T10:41:51.963899Z',
      to: ['https://cumfe.local/ap/actor/test4'],
      type: 'Undo',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<FollowUndo>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('FollowUndo', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
