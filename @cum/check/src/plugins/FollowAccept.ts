import { Activity, Guard, Parse, ParserPlugin } from '../types.js';
import { TFollowState } from './Follow.js';

export interface FollowAccept extends Activity {
  type: 'Accept';
}

const guard: Guard = (doc) => {
  return doc.type === 'Accept';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Follow';
};

export interface Greedy extends Activity {
  type: 'Accept';
}

const Greedy: Parse<Greedy, FollowAccept> = (doc) => doc;

export const plugin: ParserPlugin<FollowAccept> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Accept';

  const activity: FollowAccept = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test4',
      cc: [],
      id: 'https://social.tld.com/activities/094c0cf5-d159-49f2-a89c-911986182950',
      object: {
        actor: 'https://dev.social/ap/actor/smeek',
        id: 'https://dev.social/ap/objects/5',
        object: 'https://social.tld.com/users/test4',
        type: 'Follow',
      },
      to: ['https://dev.social/ap/actor/smeek'],
      type: 'Accept',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<FollowAccept>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('FollowAccept', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
