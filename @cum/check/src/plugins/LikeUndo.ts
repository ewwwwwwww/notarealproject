import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface LikeUndo extends Activity {
  type: 'Undo';
}

const guard: Guard = (doc) => {
  return doc.type === 'Undo';
};

const subguard: Guard = (doc) => {
  return doc.type === 'Like';
};

export interface Greedy extends Activity {
  type: 'Undo';
}

const Greedy: Parse<Greedy, LikeUndo> = (doc) => doc;

export const plugin: ParserPlugin<LikeUndo> = {
  guard,
  subguard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Undo';

  const activity: LikeUndo = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      cc: ['https://www.w3.org/ns/activitystreams#Public'],
      id: 'https://social.tld.com/activities/06d640cc-db5c-4dfe-923f-23af4ab9496f',
      object:
        'https://social.tld.com/activities/dbf425fb-7cea-46e1-866c-15741a06e787',
      to: ['https://social.tld.com/users/test3/followers'],
      type: 'Undo',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<LikeUndo>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('LikeUndo', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
