import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface Announce extends Activity {
  type: 'Announce';
}

const guard: Guard = (doc) => {
  return doc.type === 'Announce';
};

export interface Greedy extends Activity {
  type: 'Announce';
}

const Greedy: Parse<Greedy, Announce> = (doc) => doc;

export const plugin: ParserPlugin<Announce> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Announce';

  const activity: Announce = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      cc: [],
      context:
        'https://social.tld.com/contexts/cae469fc-4c4e-4ab4-b169-b58c976e585f',
      id: 'https://social.tld.com/activities/9a335c67-4b83-4642-be06-dd4ecbbe56d3',
      object:
        'https://social.tld.com/objects/7d09184c-7252-4be8-ba22-459f6d86a388',
      published: '2021-11-21T19:55:09.216027Z',
      to: [
        'https://social.tld.com/users/test3/followers',
        'https://www.w3.org/ns/activitystreams#Public',
      ],
      type: 'Announce',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Announce>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Announce', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
