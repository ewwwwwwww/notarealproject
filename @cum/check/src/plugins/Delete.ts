import { Activity, Guard, Parse, ParserPlugin } from '../types.js';

export interface Delete extends Activity {
  type: 'Delete';
}

const guard: Guard = (doc) => {
  return doc.type === 'Delete';
};

export interface Greedy extends Activity {
  type: 'Delete';
}

const Greedy: Parse<Greedy, Delete> = (doc) => doc;

export const plugin: ParserPlugin<Delete> = {
  guard,
  parsers: {
    Greedy,
  },
};

/**
 * TESTS
 */

if (import.meta.vitest) {
  const { it, expect } = import.meta.vitest;
  const { randActivityUrl, randActorUrl, randObjectUrl } = await import(
    '@cum/fixtures'
  );
  const { commonTests, validate } = await import('../tests.js');
  const { transmogrify } = await import('../index.js');

  const type = 'Delete';

  const activity: Delete = {
    type,
    id: randActivityUrl(),
    actor: randActorUrl(),
    object: randObjectUrl(),
  };

  commonTests(type, activity, guard, it);

  it.concurrent('evals pleroma', async () => {
    const ctx = {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://social.tld.com/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://social.tld.com/users/test3',
      cc: [],
      id: 'https://social.tld.com/activities/1f3e9fc7-8905-4c01-8627-fb7e5055065c',
      object:
        'https://social.tld.com/objects/b3d0b8db-1b4e-4446-bb44-0422cbe8263f',
      to: [
        'https://social.tld.com/users/test3/followers',
        'https://www.w3.org/ns/activitystreams#Public',
      ],
      type: 'Delete',
    };

    expect(guard(ctx)).toBe(true);

    const { name, doc } = transmogrify<Delete>(ctx, type) ?? {};

    expect(name).toBe(type);
    expect(validate('Delete', doc)).toBe(true);
  });

  // TODO: misskey tests
  it.concurrent('evals misskey', () => {
    expect(true).toBe(false);
  });

  // TODO: mastodon tests
  it.concurrent('evals mastodon', () => {
    expect(true).toBe(false);
  });
}
