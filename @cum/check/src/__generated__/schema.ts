export default {
  Announce: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Announce' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Announce:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Announce' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  ChatMessageCreate: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Create' ]
      },
      object: {
        description: 'The subject object or person',
        type: 'object',
        properties: {
          type: { type: 'string', enum: [ 'ChatMessage' ] },
          id: { type: 'string' },
          content: { type: 'string' }
        },
        required: [ 'content', 'id', 'type' ]
      },
      '@context': {},
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'object', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'ChatMessageCreate:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Create' ]
      },
      object: {
        description: 'The subject object or person',
        type: 'object',
        properties: {
          type: { type: 'string', enum: [ 'ChatMessage' ] },
          id: { type: 'string' },
          content: { type: 'string' }
        },
        required: [ 'content', 'id', 'type' ]
      },
      '@context': {},
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'object', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  Delete: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Delete' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Delete:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Delete' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  EmojiReact: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'EmojiReact' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'EmojiReact:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'EmojiReact' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  Follow: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Follow' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Follow:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Follow' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Follow:TFollowState': {
    enum: [ 'accept', 'cancelled', 'pending', 'reject' ],
    type: 'string',
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  FollowAccept: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Accept' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'FollowAccept:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Accept' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  FollowReject: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Reject' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'FollowReject:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Reject' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  FollowUndo: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Undo' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'FollowUndo:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Undo' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  Like: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Like' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Like:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Like' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  LikeUndo: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Undo' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'LikeUndo:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Undo' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  NoteCreate: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Create' ]
      },
      object: {
        description: 'The subject object or person',
        type: 'object',
        properties: {
          type: { type: 'string', enum: [ 'Note' ] },
          id: { type: 'string' },
          content: { type: 'string' }
        },
        required: [ 'content', 'id', 'type' ]
      },
      '@context': {},
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'object', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'NoteCreate:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Create' ]
      },
      object: {
        description: 'The subject object or person',
        type: 'object',
        properties: {
          type: { type: 'string', enum: [ 'Note' ] },
          id: { type: 'string' },
          content: { type: 'string' }
        },
        required: [ 'content', 'id', 'type' ]
      },
      '@context': {},
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'object', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  Person: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Person' ]
      },
      name: {
        description: 'The temporary nickname of the person',
        type: 'string'
      },
      preferredUsername: {
        description: 'The permanent name of the person',
        type: 'string'
      },
      inbox: {
        description: "The person's inbox for receiving activities",
        format: 'uri',
        type: 'string'
      },
      outbox: {
        description: "The person's outbox for displaying activities",
        format: 'uri',
        type: 'string'
      },
      followers: {
        description: "The person's lists of followers",
        format: 'uri',
        type: 'string'
      },
      following: {
        description: "The person's lists of following",
        format: 'uri',
        type: 'string'
      },
      featured: {
        description: "The person's featured posts (i.e. stickies)",
        format: 'uri',
        type: 'string'
      },
      endpoints: {
        description: 'List of endpoints',
        type: 'object',
        properties: {
          sharedInbox: {
            description: "The shared inbox of the person's instance",
            format: 'uri',
            type: 'string'
          }
        }
      },
      publicKey: {
        description: 'Public key validation for signature signing',
        type: 'object',
        properties: {
          id: {
            description: "Actor's key location",
            format: 'uri',
            type: 'string'
          },
          owner: {
            description: "Actor's location",
            format: 'uri',
            type: 'string'
          },
          publicKeyPem: { description: 'Pubkey', type: 'string' }
        },
        required: [ 'id', 'owner', 'publicKeyPem' ]
      },
      manuallyApprovesFollowers: {
        description: 'Person requires approval for follow requests',
        type: 'boolean'
      },
      summary: { description: "Person's profile description", type: 'string' },
      tag: {
        description: 'TODO: write this',
        type: 'array',
        items: { type: 'string' }
      },
      attachment: { description: 'TODO: write this', type: 'array', items: {} },
      alsoKnownAs: { description: 'TODO: write this', type: 'array', items: {} },
      capabilities: {
        description: 'TODO: write this',
        type: 'object',
        properties: {
          acceptsChatMessages: { description: 'TODO: write this', type: 'boolean' }
        }
      },
      discoverable: { description: 'TODO: write this', type: 'boolean' },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [
      'featured',
      'followers',
      'following',
      'id',
      'inbox',
      'name',
      'outbox',
      'preferredUsername',
      'publicKey',
      'type'
    ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'Person:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Person' ]
      },
      name: {
        description: 'The temporary nickname of the person',
        type: 'string'
      },
      preferredUsername: {
        description: 'The permanent name of the person',
        type: 'string'
      },
      inbox: {
        description: "The person's inbox for receiving activities",
        format: 'uri',
        type: 'string'
      },
      outbox: {
        description: "The person's outbox for displaying activities",
        format: 'uri',
        type: 'string'
      },
      followers: {
        description: "The person's lists of followers",
        format: 'uri',
        type: 'string'
      },
      following: {
        description: "The person's lists of following",
        format: 'uri',
        type: 'string'
      },
      featured: {
        description: "The person's featured posts (i.e. stickies)",
        format: 'uri',
        type: 'string'
      },
      endpoints: {
        description: 'List of endpoints',
        type: 'object',
        properties: {
          sharedInbox: {
            description: "The shared inbox of the person's instance",
            format: 'uri',
            type: 'string'
          }
        }
      },
      publicKey: {
        description: 'Public key validation for signature signing',
        type: 'object',
        properties: {
          id: {
            description: "Actor's key location",
            format: 'uri',
            type: 'string'
          },
          owner: {
            description: "Actor's location",
            format: 'uri',
            type: 'string'
          },
          publicKeyPem: { description: 'Pubkey', type: 'string' }
        },
        required: [ 'id', 'owner', 'publicKeyPem' ]
      },
      manuallyApprovesFollowers: {
        description: 'Person requires approval for follow requests',
        type: 'boolean'
      },
      summary: { description: "Person's profile description", type: 'string' },
      tag: {
        description: 'TODO: write this',
        type: 'array',
        items: { type: 'string' }
      },
      attachment: { description: 'TODO: write this', type: 'array', items: {} },
      alsoKnownAs: { description: 'TODO: write this', type: 'array', items: {} },
      capabilities: {
        description: 'TODO: write this',
        type: 'object',
        properties: {
          acceptsChatMessages: { description: 'TODO: write this', type: 'boolean' }
        }
      },
      discoverable: { description: 'TODO: write this', type: 'boolean' },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [
      'featured',
      'followers',
      'following',
      'id',
      'inbox',
      'name',
      'outbox',
      'preferredUsername',
      'publicKey',
      'type'
    ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  PersonUpdate: {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Update' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  },
  'PersonUpdate:Greedy': {
    type: 'object',
    properties: {
      type: {
        description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
        type: 'string',
        enum: [ 'Update' ]
      },
      '@context': {},
      object: {
        description: 'The subject object or person',
        anyOf: [
          {
            type: 'object',
            properties: {
              type: {
                description: "The type of the activity or object (e.g. 'Follow', 'Undo')",
                type: 'string'
              },
              id: {
                description: 'Fully qualified url pointing to the activity id',
                format: 'uri',
                type: 'string'
              },
              actor: {
                description: 'The person who sent the activity',
                format: 'uri',
                type: 'string'
              },
              to: {
                description: 'The list of actors that this is for',
                type: 'array',
                items: { type: 'string' }
              },
              cc: {
                description: 'Extra actors that should also be sent this activity',
                type: 'array',
                items: { type: 'string' }
              },
              published: {
                description: 'Date the object or activity was created',
                format: 'date-time',
                type: 'string'
              }
            },
            required: [ 'id', 'type' ]
          },
          { type: 'string' }
        ]
      },
      id: {
        description: 'Fully qualified url pointing to the activity id',
        format: 'uri',
        type: 'string'
      },
      actor: {
        description: 'The person who sent the activity',
        format: 'uri',
        type: 'string'
      },
      to: {
        description: 'The list of actors that this is for',
        type: 'array',
        items: { type: 'string' }
      },
      cc: {
        description: 'Extra actors that should also be sent this activity',
        type: 'array',
        items: { type: 'string' }
      },
      published: {
        description: 'Date the object or activity was created',
        format: 'date-time',
        type: 'string'
      }
    },
    required: [ 'id', 'type' ],
    '$schema': 'http://json-schema.org/draft-07/schema#'
  }
}