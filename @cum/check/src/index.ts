import {
  Activity,
  Parse,
  isActivity,
  isCollection,
  isCollectionPage,
  Collection,
  CollectionPage,
} from './types.js';
import { ajv } from './ajv.js';
import { dumpUnique } from './dump.js';
import { log } from './logger.js';

import * as plugins from './plugins/index.js';

export * from './types.js';
export { ajv } from './ajv.js';
export type PluginKeys = keyof typeof plugins;

log.trace(
  Object.keys(plugins),
  `loaded ${Object.keys(plugins).length} parsers`,
);

const pluginIter = Object.entries(plugins).map(([k, v]) => {
  if (!v.plugin) {
    throw new Error(`plugin '${k}' is missing 'plugin' export`);
  }

  const { guard, parsers, subguard } = v.plugin;

  if (!guard) {
    throw new Error(`plugin '${k}' is missing 'plugin.guard' export`);
  }

  if (!parsers) {
    throw new Error(`plugin '${k}' is missing 'plugin.parsers' export`);
  }

  // schemas are prefixed to prevent collisions
  const mparsers: [string, Parse<any, any>][] = Object.entries(parsers).map(
    ([key, val]) => {
      return k === key ? [key, val] : [`${k}:${key}`, val];
    },
  );

  return {
    key: k as PluginKeys,
    guard,
    subguard,
    parsers: mparsers,
  };
});

const frequency: Map<string, number> = new Map();
let freqTicks = 0;

for (const plugin of pluginIter) {
  frequency.set(plugin.key, 0);
}

const sortPlugins = () => {
  pluginIter.sort((a, b) => {
    return (frequency.get(b.key) as number) - (frequency.get(a.key) as number);
  });

  for (const plugin of pluginIter) {
    frequency.set(plugin.key, 0);
  }

  freqTicks = 0;
};

type TransmogCallbackMap = {
  [A in keyof typeof plugins]: (doc: Activity) => any;
};

// TODO: constraint should be defined when A is set
export function transmogrify<A extends Activity>(
  doc: Object,
  constraint?: string,
  cb?: TransmogCallbackMap,
): { doc: A; name: keyof typeof plugins } | undefined {
  if (!isActivity(doc)) {
    log.warn(doc, 'doc is not an activity');

    return;
  }

  if (constraint && doc.type !== constraint) {
    log.warn(
      doc,
      `document type '${doc.type}' violated constraint '${constraint}.'`,
    );

    return;
  }

  for (const { key, guard, subguard, parsers } of pluginIter) {
    if (!guard(doc)) continue;

    // objects can be either undefined, string, or a subtype
    if (
      subguard &&
      doc.object &&
      typeof doc.object !== 'string' &&
      !subguard(doc.object)
    )
      continue;

    for (const [name, parser] of parsers) {
      const validate = ajv.getSchema(name);

      if (!validate) {
        throw new Error(
          `missing schema for '${key}.${name}'. is the parser name the same as the interface?`,
        );
      }

      if (!validate(doc)) continue;

      log.trace(doc, `parsed as type '${key}.${name}'`);

      frequency.set(key, (frequency.get(key) as number) + 1);

      if (freqTicks++ > 300) {
        sortPlugins();
      }

      if (cb && cb[key]) cb[key](doc);

      return { name: key as keyof typeof plugins, doc: parser(doc) as A };
    }
  }

  log.trace(doc, `failed to find a parser for doc`);

  if (process.env.DUMP_ACTIVITIES) {
    dumpUnique(doc);
  }
}

export function collection(doc: any, cb: TransmogCallbackMap): Collection {
  if (!isCollection(doc)) {
    log.warn(doc, 'collection is not valid');
  }

  return {
    type: doc.type,
    id: doc.id,
    totalItems: doc.totalItems,
    orderedItems: doc.orderedItems.map((x: any) => {
      return transmogrify(x, undefined, cb);
    }),
  };
}

const featured = {
  '@context': [
    'https://www.w3.org/ns/activitystreams',
    'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
    { '@language': 'und' },
  ],
  id: 'https://shigusegubu.club/users/hj/collections/featured',
  orderedItems: [
    {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
        { '@language': 'und' },
      ],
      actor: 'https://shigusegubu.club/users/hj',
      attachment: [],
      attributedTo: 'https://shigusegubu.club/users/hj',
      cc: ['https://shigusegubu.club/users/hj/followers'],
      content: 'why would anyone want to bin a bost?',
      context:
        'https://shigusegubu.club/contexts/9278a041-f79a-4116-aac3-7c24d861a66b',
      conversation:
        'https://shigusegubu.club/contexts/9278a041-f79a-4116-aac3-7c24d861a66b',
      id: 'https://shigusegubu.club/objects/9f27db2e-29c5-4f1f-a2b6-133b488512e9',
      published: '2019-04-28T15:50:28.509182Z',
      repliesCount: 2,
      summary: null,
      tag: [],
      to: ['https://www.w3.org/ns/activitystreams#Public'],
      type: 'Note',
    },
  ],
  totalItems: 1,
  type: 'OrderedCollection',
};

const followrs = {
  '@context': [
    'https://www.w3.org/ns/activitystreams',
    'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
    { '@language': 'und' },
  ],
  first: {
    id: 'https://shigusegubu.club/users/hj/followers?page=1',
    next: 'https://shigusegubu.club/users/hj/followers?page=2',
    orderedItems: [
      'https://shitposter.club/users/LukeAlmighty',
      'https://fedi.absturztau.be/users/allison',
      'https://neckbeard.xyz/users/worm',
      'https://pleroma.pandanet.moe/users/dentropy',
      'https://cybre.club/users/Talloran',
      'https://pl.firechicken.net/users/uelen',
      'https://mastodon.online/users/a723811549',
      'https://gleasonator.dev/users/snakeAIDS',
      'https://blob.cat/users/NeikoCat',
      'https://noagendasocial.com/users/Arwalk',
    ],
    partOf: 'https://shigusegubu.club/users/hj/followers',
    totalItems: 2077,
    type: 'OrderedCollectionPage',
  },
  id: 'https://shigusegubu.club/users/hj/followers',
  totalItems: 2077,
  type: 'OrderedCollection',
};

const outbox = {
  '@context': [
    'https://www.w3.org/ns/activitystreams',
    'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
    { '@language': 'und' },
  ],
  first: 'https://shigusegubu.club/users/hj/outbox?page=true',
  id: 'https://shigusegubu.club/users/hj/outbox',
  type: 'OrderedCollection',
};

const outboxp2 = {
  '@context': [
    'https://www.w3.org/ns/activitystreams',
    'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
    {
      '@language': 'und',
    },
  ],
  id: 'https://shigusegubu.club/users/hj/outbox?page=true',
  next: 'https://shigusegubu.club/users/hj/outbox?max_id=AOGKGNqN5pVmU0e1IW&nickname=hj&page=true',
  orderedItems: [
    {
      '@context': [
        'https://www.w3.org/ns/activitystreams',
        'https://shigusegubu.club/schemas/litepub-0.1.jsonld',
        {
          '@language': 'und',
        },
      ],
      actor: 'https://shigusegubu.club/users/hj',
      bto: [],
      cc: [],
      context:
        'https://udongein.xyz/contexts/bb1e032a-034e-45b7-83d9-a049fda23753',
      id: 'https://shigusegubu.club/activities/aced4ac2-bc9f-4f00-b6bc-71b08d9cfee2',
      object:
        'https://udongein.xyz/objects/10b58364-5d0b-4718-a527-ac9ddd8732d9',
      published: '2022-10-05T21:05:44.979330Z',
      to: [
        'https://shigusegubu.club/users/hj/followers',
        'https://udongein.xyz/users/duponin',
        'https://www.w3.org/ns/activitystreams#Public',
      ],
      type: 'Announce',
    },
  ],
  partOf: 'https://shigusegubu.club/users/hj/outbox',
  prev: 'https://shigusegubu.club/users/hj/outbox?min_id=AOGk9ambBWJpOVInAm&nickname=hj&page=true',
  type: 'OrderedCollectionPage',
};
