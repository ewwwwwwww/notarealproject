import { pino } from 'pino';

export const log = pino({
  name: '@cum/check',
  level: process.env.LOG_LEVEL || 'info',
  sync: false,
});
