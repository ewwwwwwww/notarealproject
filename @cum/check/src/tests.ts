import { ajv } from './ajv.js';
import { expect, TestAPI } from 'vitest';
import { Guard } from './types.js';

export const validate = (key: string, doc: any) => ajv.getSchema(key)(doc);

export const commonTests = (
  type: string,
  activity: any,
  guard: Guard,
  it: TestAPI,
) => {
  it.concurrent('guard matches', () => {
    expect(guard(activity)).toBe(true);
  });

  it.concurrent('guard does not match', () => {
    expect(guard({ ...activity, type: type + 'Accept' })).toBe(false);
    expect(guard({ ...activity, type: type.substring(0, -2) })).toBe(false);
  });
};
