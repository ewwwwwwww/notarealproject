import { writeFileSync, existsSync, mkdirSync } from 'fs';
import { createHash } from 'crypto';
import { resolve } from 'path';

export const propertiesToArray = (obj: {}): string[] => {
  const isObject = (val: any) =>
    val && typeof val === 'object' && !Array.isArray(val);

  const addDelimiter = (a: string, b: string) => (a ? `${a}.${b}` : b);

  // @ts-ignore
  const paths = (obj = {}, head = '') => {
    // @ts-ignore
    return Object.entries(obj).reduce((product, [key, value]) => {
      let fullPath = addDelimiter(head, key);
      return isObject(value)
        ? // @ts-ignore
          product.concat(paths(value, fullPath))
        : // @ts-ignore
          product.concat(fullPath);
    }, []);
  };

  return paths(obj);
};

export const dumpUnique = (doc: Record<string, any>) => {
  if (!doc.type) return;

  /* const parameters = propertiesToArray(doc).flatMap((e) => [ */
  /*   e, */
  /*   typeof get(doc, e), */
  /* ]); */
  const parameters = propertiesToArray(doc);

  parameters.sort();

  const next = [doc.type, ...parameters].join(':');

  const hash = createHash('md5').update(next).digest('hex');
  const dir = resolve(`./dumps`);

  if (!existsSync(dir)) {
    mkdirSync(dir);
  }

  const filepath = resolve(
    dir,
    `${doc.type.slice(0, 20).toLowerCase()}_${hash}.json`,
  );

  if (existsSync(filepath)) return;

  writeFileSync(filepath, JSON.stringify(doc, null, 2));
};
