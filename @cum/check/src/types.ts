export interface Object {
  /**
   * The type of the activity or object (e.g. 'Follow', 'Undo')
   */
  type: string;

  /**
   * Fully qualified url pointing to the activity id
   * @TJS-format uri
   */
  id: string;

  /**
   * The person who sent the activity
   * @TJS-format uri
   */
  actor?: string;

  /**
   * The list of actors that this is for
   */
  to?: string[];

  /**
   * Extra actors that should also be sent this activity
   */
  cc?: string[];

  /**
   * Date the object or activity was created
   * @TJS-format date-time
   */
  published?: string;
}

export interface Activity extends Object {
  '@context'?: any;

  /**
   * The subject object or person
   */
  object?: string | Object;
}

export type Guard<Doc = Activity> = (doc: Doc) => boolean;
export type Parse<From extends Activity, Into extends Activity> = (
  doc: From,
) => Into;

export type ParserPlugin<Into extends Activity> = {
  guard: Guard;
  subguard?: Guard;
  parsers: {
    [key: string]: Parse<any, Into>;
  };
};

export type Collection = Activity & {
  type: 'Collection' | 'OrderedCollection';
} & (
    | {
        totalItems: number;
        orderedItems: Activity[];
      }
    | {
        first: string;
      }
  );

export interface CollectionPage extends Activity {
  type: 'CollectionPage' | 'OrderedCollectionPage';
}

export function isActivity(doc: any): doc is Activity {
  return doc.type && typeof doc.type === 'string';
}

export function isCollection(doc: any): doc is Collection {
  return (
    doc.type && (doc.type === 'OrderedCollection' || doc.type === 'Collection')
  );
}

export function isCollectionPage(doc: any): doc is CollectionPage {
  return (
    doc.type &&
    (doc.type === 'OrderedCollectionPage' || doc.type === 'CollectionPage')
  );
}
