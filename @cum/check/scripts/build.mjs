import { resolve } from 'path';
import * as TJS from 'typescript-json-schema';
import { writeFileSync, readdirSync } from 'fs';
import { inspect } from 'util';
import * as path from 'path';
import * as plugins from '../dist/src/plugins/index.js';

const basePath = './';
const compilerOptions = {};

const files = (() => {
  const files = readdirSync(path.join(basePath, 'src', 'plugins'));
  return files.map((file) => path.join(basePath, 'src', 'plugins', file));
})();

const program = TJS.getProgramFromFiles(files, compilerOptions, basePath);

const settings = {
  required: true,
  ref: false,
  excludePrivate: true,
  ignoreErrors: true,
  uniqueNames: true,
};

let pluginExports = new Set();
let pluginExportsDone = new Set();

pluginExports.add('Activity');

for (const key of Object.entries(plugins)) {
  pluginExports.add(key[0]);
}

const generator = TJS.buildGenerator(program, settings);

const map = {};

const firstNames = [...pluginExports].join(', ');

console.log(`:: building activity json schema`);

for (const symb of generator.getSymbols()) {
  if (symb.fullyQualifiedName.includes('src/plugins/')) {
    const filepath = symb.symbol.parent.escapedName.replaceAll('"', '');
    const base = path.basename(filepath);
    const suffix = symb.name.slice(0, -9);

    try {
      const schema = generator.getSchemaForSymbol(symb.name);

      if (suffix === base) {
        map[suffix] = schema;
      } else {
        map[base + ':' + suffix] = schema;
      }
    } catch (e) {
      console.log(e);
    }
  }
}

console.log(
  `${firstNames}... (${Object.keys(map).length - pluginExports.size} more)`,
);

const ordered = Object.keys(map)
  .sort()
  .reduce((obj, key) => {
    obj[key] = map[key];
    return obj;
  }, {});

writeFileSync(
  './src/__generated__/schema.ts',
  ('export default ' + inspect(ordered, { depth: Infinity })).replaceAll(
    '#/definitions/',
    '',
  ),
);
