/** Types generated for queries found in "src/sql/follow.sql" */
import { PreparedQuery } from '@pgtyped/query';

export type activity_direction = 'inbound' | 'local' | 'outbound' | 'remote';

/** 'InsertFollow' parameters type */
export interface IInsertFollowParams {
  direction: activity_direction | null | void;
  followedId: string | null | void;
  followerId: string | null | void;
}

/** 'InsertFollow' return type */
export type IInsertFollowResult = void;

/** 'InsertFollow' query type */
export interface IInsertFollowQuery {
  params: IInsertFollowParams;
  result: IInsertFollowResult;
}

const insertFollowIR: any = {"usedParamSet":{"followerId":true,"followedId":true,"direction":true},"params":[{"name":"followerId","required":false,"transform":{"type":"scalar"},"locs":[{"a":79,"b":89}]},{"name":"followedId","required":false,"transform":{"type":"scalar"},"locs":[{"a":94,"b":104}]},{"name":"direction","required":false,"transform":{"type":"scalar"},"locs":[{"a":109,"b":118}]}],"statement":"insert into app_public.follow (follower_id, followed_id, direction)\nvalues (\n  :followerId,\n  :followedId,\n  :direction\n)"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.follow (follower_id, followed_id, direction)
 * values (
 *   :followerId,
 *   :followedId,
 *   :direction
 * )
 * ```
 */
export const insertFollow = new PreparedQuery<IInsertFollowParams,IInsertFollowResult>(insertFollowIR);


