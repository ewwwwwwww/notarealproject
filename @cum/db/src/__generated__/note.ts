/** Types generated for queries found in "src/sql/note.sql" */
import { PreparedQuery } from '@pgtyped/query';

export type scope = 'direct' | 'local' | 'private' | 'public' | 'unlisted';

/** 'NoteInsert' parameters type */
export interface INoteInsertParams {
  body: string | null | void;
  remote: boolean | null | void;
  reply: string | null | void;
  scope: scope | null | void;
  userId: string | null | void;
}

/** 'NoteInsert' return type */
export interface INoteInsertResult {
  note_id: string;
}

/** 'NoteInsert' query type */
export interface INoteInsertQuery {
  params: INoteInsertParams;
  result: INoteInsertResult;
}

const noteInsertIR: any = {"usedParamSet":{"userId":true,"scope":true,"reply":true,"body":true,"remote":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":93,"b":99},{"a":161,"b":167}]},{"name":"scope","required":false,"transform":{"type":"scalar"},"locs":[{"a":175,"b":180}]},{"name":"reply","required":false,"transform":{"type":"scalar"},"locs":[{"a":187,"b":192}]},{"name":"body","required":false,"transform":{"type":"scalar"},"locs":[{"a":199,"b":203}]},{"name":"remote","required":false,"transform":{"type":"scalar"},"locs":[{"a":210,"b":216}]}],"statement":"insert into app_public.note (user_id, domain_id, scope, reply_id, body, remote) values (\n    :userId,\n    (select domain_id from app_public.user where user_id = :userId),\n    :scope,\n    :reply,\n    :body,\n    :remote\n) returning note_id"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.note (user_id, domain_id, scope, reply_id, body, remote) values (
 *     :userId,
 *     (select domain_id from app_public.user where user_id = :userId),
 *     :scope,
 *     :reply,
 *     :body,
 *     :remote
 * ) returning note_id
 * ```
 */
export const noteInsert = new PreparedQuery<INoteInsertParams,INoteInsertResult>(noteInsertIR);


/** 'NoteById' parameters type */
export interface INoteByIdParams {
  noteId: string | null | void;
}

/** 'NoteById' return type */
export interface INoteByIdResult {
  body: string | null;
  remote: boolean;
  reply_id: string | null;
  scope: scope;
  user_id: string;
}

/** 'NoteById' query type */
export interface INoteByIdQuery {
  params: INoteByIdParams;
  result: INoteByIdResult;
}

const noteByIdIR: any = {"usedParamSet":{"noteId":true},"params":[{"name":"noteId","required":false,"transform":{"type":"scalar"},"locs":[{"a":81,"b":87}]}],"statement":"select user_id, scope, reply_id, body, remote from app_public.note where note_id=:noteId"};

/**
 * Query generated from SQL:
 * ```
 * select user_id, scope, reply_id, body, remote from app_public.note where note_id=:noteId
 * ```
 */
export const noteById = new PreparedQuery<INoteByIdParams,INoteByIdResult>(noteByIdIR);


