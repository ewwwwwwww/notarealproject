/** Types generated for queries found in "src/sql/user.sql" */
import { PreparedQuery } from '@pgtyped/query';

/** 'UserIdLocal' parameters type */
export interface IUserIdLocalParams {
  name: string | null | void;
}

/** 'UserIdLocal' return type */
export interface IUserIdLocalResult {
  /** Unique identifier for the user. */
  user_id: string;
}

/** 'UserIdLocal' query type */
export interface IUserIdLocalQuery {
  params: IUserIdLocalParams;
  result: IUserIdLocalResult;
}

const userIdLocalIR: any = {"usedParamSet":{"name":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":49,"b":53}]}],"statement":"select user_id from app_public.user\n  where name=:name and remote=false\n  limit 1"};

/**
 * Query generated from SQL:
 * ```
 * select user_id from app_public.user
 *   where name=:name and remote=false
 *   limit 1
 * ```
 */
export const userIdLocal = new PreparedQuery<IUserIdLocalParams,IUserIdLocalResult>(userIdLocalIR);


/** 'UserPubKeyUpdate' parameters type */
export interface IUserPubKeyUpdateParams {
  id: string | null | void;
  pubkey: string | null | void;
}

/** 'UserPubKeyUpdate' return type */
export type IUserPubKeyUpdateResult = void;

/** 'UserPubKeyUpdate' query type */
export interface IUserPubKeyUpdateQuery {
  params: IUserPubKeyUpdateParams;
  result: IUserPubKeyUpdateResult;
}

const userPubKeyUpdateIR: any = {"usedParamSet":{"pubkey":true,"id":true},"params":[{"name":"pubkey","required":false,"transform":{"type":"scalar"},"locs":[{"a":51,"b":57}]},{"name":"id","required":false,"transform":{"type":"scalar"},"locs":[{"a":74,"b":76}]}],"statement":"update app_hidden.user set pubkey=coalesce(pubkey, :pubkey) where user_id=:id"};

/**
 * Query generated from SQL:
 * ```
 * update app_hidden.user set pubkey=coalesce(pubkey, :pubkey) where user_id=:id
 * ```
 */
export const userPubKeyUpdate = new PreparedQuery<IUserPubKeyUpdateParams,IUserPubKeyUpdateResult>(userPubKeyUpdateIR);


/** 'UserPubKeyById' parameters type */
export interface IUserPubKeyByIdParams {
  userId: string | null | void;
}

/** 'UserPubKeyById' return type */
export interface IUserPubKeyByIdResult {
  /** Pubkey for verifying activity pub messages */
  pubkey: string | null;
}

/** 'UserPubKeyById' query type */
export interface IUserPubKeyByIdQuery {
  params: IUserPubKeyByIdParams;
  result: IUserPubKeyByIdResult;
}

const userPubKeyByIdIR: any = {"usedParamSet":{"userId":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":49,"b":55}]}],"statement":"select pubkey from app_hidden.user where user_id=:userId\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select pubkey from app_hidden.user where user_id=:userId
 * limit 1
 * ```
 */
export const userPubKeyById = new PreparedQuery<IUserPubKeyByIdParams,IUserPubKeyByIdResult>(userPubKeyByIdIR);


/** 'UserPrivKeyUpdate' parameters type */
export interface IUserPrivKeyUpdateParams {
  id: string | null | void;
  privkey: string | null | void;
}

/** 'UserPrivKeyUpdate' return type */
export type IUserPrivKeyUpdateResult = void;

/** 'UserPrivKeyUpdate' query type */
export interface IUserPrivKeyUpdateQuery {
  params: IUserPrivKeyUpdateParams;
  result: IUserPrivKeyUpdateResult;
}

const userPrivKeyUpdateIR: any = {"usedParamSet":{"privkey":true,"id":true},"params":[{"name":"privkey","required":false,"transform":{"type":"scalar"},"locs":[{"a":54,"b":61}]},{"name":"id","required":false,"transform":{"type":"scalar"},"locs":[{"a":78,"b":80}]}],"statement":"update app_private.user set privkey=coalesce(privkey, :privkey) where user_id=:id"};

/**
 * Query generated from SQL:
 * ```
 * update app_private.user set privkey=coalesce(privkey, :privkey) where user_id=:id
 * ```
 */
export const userPrivKeyUpdate = new PreparedQuery<IUserPrivKeyUpdateParams,IUserPrivKeyUpdateResult>(userPrivKeyUpdateIR);


/** 'UserPrivKeyById' parameters type */
export interface IUserPrivKeyByIdParams {
  userId: string | null | void;
}

/** 'UserPrivKeyById' return type */
export interface IUserPrivKeyByIdResult {
  /** Private key for signing activity pub messages */
  privkey: string | null;
}

/** 'UserPrivKeyById' query type */
export interface IUserPrivKeyByIdQuery {
  params: IUserPrivKeyByIdParams;
  result: IUserPrivKeyByIdResult;
}

const userPrivKeyByIdIR: any = {"usedParamSet":{"userId":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":53,"b":59}]}],"statement":"select privkey from app_private.user\n  where user_id=:userId\n  limit 1"};

/**
 * Query generated from SQL:
 * ```
 * select privkey from app_private.user
 *   where user_id=:userId
 *   limit 1
 * ```
 */
export const userPrivKeyById = new PreparedQuery<IUserPrivKeyByIdParams,IUserPrivKeyByIdResult>(userPrivKeyByIdIR);


/** 'UserRegisterRemote' parameters type */
export interface IUserRegisterRemoteParams {
  domain: string | null | void;
  id: string | null | void;
  inbox: string | null | void;
  name: string | null | void;
  pubkey: string | null | void;
}

/** 'UserRegisterRemote' return type */
export interface IUserRegisterRemoteResult {
  user_id: string | null;
}

/** 'UserRegisterRemote' query type */
export interface IUserRegisterRemoteQuery {
  params: IUserRegisterRemoteParams;
  result: IUserRegisterRemoteResult;
}

const userRegisterRemoteIR: any = {"usedParamSet":{"name":true,"domain":true,"id":true,"inbox":true,"pubkey":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":53,"b":57}]},{"name":"domain","required":false,"transform":{"type":"scalar"},"locs":[{"a":60,"b":66}]},{"name":"id","required":false,"transform":{"type":"scalar"},"locs":[{"a":69,"b":71}]},{"name":"inbox","required":false,"transform":{"type":"scalar"},"locs":[{"a":74,"b":79}]},{"name":"pubkey","required":false,"transform":{"type":"scalar"},"locs":[{"a":82,"b":88}]}],"statement":"select user_id from app_private.user_register_remote(:name, :domain, :id, :inbox, :pubkey)"};

/**
 * Query generated from SQL:
 * ```
 * select user_id from app_private.user_register_remote(:name, :domain, :id, :inbox, :pubkey)
 * ```
 */
export const userRegisterRemote = new PreparedQuery<IUserRegisterRemoteParams,IUserRegisterRemoteResult>(userRegisterRemoteIR);


/** 'UserInboxById' parameters type */
export interface IUserInboxByIdParams {
  userId: string | null | void;
}

/** 'UserInboxById' return type */
export interface IUserInboxByIdResult {
  actor: string | null;
  inbox: string | null;
}

/** 'UserInboxById' query type */
export interface IUserInboxByIdQuery {
  params: IUserInboxByIdParams;
  result: IUserInboxByIdResult;
}

const userInboxByIdIR: any = {"usedParamSet":{"userId":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":55,"b":61}]}],"statement":"select actor, inbox from app_hidden.user where user_id=:userId\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select actor, inbox from app_hidden.user where user_id=:userId
 * limit 1
 * ```
 */
export const userInboxById = new PreparedQuery<IUserInboxByIdParams,IUserInboxByIdResult>(userInboxByIdIR);


/** 'UserNamePrivKeyById' parameters type */
export interface IUserNamePrivKeyByIdParams {
  id: string | null | void;
}

/** 'UserNamePrivKeyById' return type */
export interface IUserNamePrivKeyByIdResult {
  /** Unique username that never changes */
  name: string;
  /** Private key for signing activity pub messages */
  privkey: string | null;
}

/** 'UserNamePrivKeyById' query type */
export interface IUserNamePrivKeyByIdQuery {
  params: IUserNamePrivKeyByIdParams;
  result: IUserNamePrivKeyByIdResult;
}

const userNamePrivKeyByIdIR: any = {"usedParamSet":{"id":true},"params":[{"name":"id","required":false,"transform":{"type":"scalar"},"locs":[{"a":149,"b":151}]}],"statement":"select name, privkey from app_public.user, app_private.user\n  where app_public.user.user_id=app_private.user.user_id\n  and app_public.user.user_id = :id\n  limit 1"};

/**
 * Query generated from SQL:
 * ```
 * select name, privkey from app_public.user, app_private.user
 *   where app_public.user.user_id=app_private.user.user_id
 *   and app_public.user.user_id = :id
 *   limit 1
 * ```
 */
export const userNamePrivKeyById = new PreparedQuery<IUserNamePrivKeyByIdParams,IUserNamePrivKeyByIdResult>(userNamePrivKeyByIdIR);


