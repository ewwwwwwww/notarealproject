/** Types generated for queries found in "src/sql/file.sql" */
import { PreparedQuery } from '@pgtyped/query';

/** 'FileAdd' parameters type */
export interface IFileAddParams {
  blake3: string | null | void;
  size: number | null | void;
}

/** 'FileAdd' return type */
export type IFileAddResult = void;

/** 'FileAdd' query type */
export interface IFileAddQuery {
  params: IFileAddParams;
  result: IFileAddResult;
}

const fileAddIR: any = {"usedParamSet":{"blake3":true,"size":true},"params":[{"name":"blake3","required":false,"transform":{"type":"scalar"},"locs":[{"a":51,"b":57}]},{"name":"size","required":false,"transform":{"type":"scalar"},"locs":[{"a":60,"b":64}]}],"statement":"insert into app_public.file (blake3, size) values (:blake3, :size) on conflict do nothing"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.file (blake3, size) values (:blake3, :size) on conflict do nothing
 * ```
 */
export const fileAdd = new PreparedQuery<IFileAddParams,IFileAddResult>(fileAddIR);


/** 'NoteAttachmentAdd' parameters type */
export interface INoteAttachmentAddParams {
  blake3: string | null | void;
  noteId: string | null | void;
}

/** 'NoteAttachmentAdd' return type */
export interface INoteAttachmentAddResult {
  attachment_id: string;
}

/** 'NoteAttachmentAdd' query type */
export interface INoteAttachmentAddQuery {
  params: INoteAttachmentAddParams;
  result: INoteAttachmentAddResult;
}

const noteAttachmentAddIR: any = {"usedParamSet":{"noteId":true,"blake3":true},"params":[{"name":"noteId","required":false,"transform":{"type":"scalar"},"locs":[{"a":71,"b":77}]},{"name":"blake3","required":false,"transform":{"type":"scalar"},"locs":[{"a":80,"b":86}]}],"statement":"insert into app_public.note_attachment (note_id, file_id) values (\n    :noteId, :blake3\n) on conflict do nothing returning attachment_id"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.note_attachment (note_id, file_id) values (
 *     :noteId, :blake3
 * ) on conflict do nothing returning attachment_id
 * ```
 */
export const noteAttachmentAdd = new PreparedQuery<INoteAttachmentAddParams,INoteAttachmentAddResult>(noteAttachmentAddIR);


/** 'UserAttachmentAdd' parameters type */
export interface IUserAttachmentAddParams {
  blake3: string | null | void;
  userId: string | null | void;
}

/** 'UserAttachmentAdd' return type */
export interface IUserAttachmentAddResult {
  attachment_id: string;
}

/** 'UserAttachmentAdd' query type */
export interface IUserAttachmentAddQuery {
  params: IUserAttachmentAddParams;
  result: IUserAttachmentAddResult;
}

const userAttachmentAddIR: any = {"usedParamSet":{"userId":true,"blake3":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":71,"b":77}]},{"name":"blake3","required":false,"transform":{"type":"scalar"},"locs":[{"a":80,"b":86}]}],"statement":"insert into app_public.user_attachment (user_id, file_id) values (\n    :userId, :blake3\n) on conflict do nothing returning attachment_id"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.user_attachment (user_id, file_id) values (
 *     :userId, :blake3
 * ) on conflict do nothing returning attachment_id
 * ```
 */
export const userAttachmentAdd = new PreparedQuery<IUserAttachmentAddParams,IUserAttachmentAddResult>(userAttachmentAddIR);


/** 'UserUpdateAvatar' parameters type */
export interface IUserUpdateAvatarParams {
  avatarId: string | null | void;
  userId: string | null | void;
}

/** 'UserUpdateAvatar' return type */
export type IUserUpdateAvatarResult = void;

/** 'UserUpdateAvatar' query type */
export interface IUserUpdateAvatarQuery {
  params: IUserUpdateAvatarParams;
  result: IUserUpdateAvatarResult;
}

const userUpdateAvatarIR: any = {"usedParamSet":{"avatarId":true,"userId":true},"params":[{"name":"avatarId","required":false,"transform":{"type":"scalar"},"locs":[{"a":43,"b":51}]},{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":73,"b":79}]}],"statement":"update app_public.user\n    set avatar_id = :avatarId\n    where user_id = :userId"};

/**
 * Query generated from SQL:
 * ```
 * update app_public.user
 *     set avatar_id = :avatarId
 *     where user_id = :userId
 * ```
 */
export const userUpdateAvatar = new PreparedQuery<IUserUpdateAvatarParams,IUserUpdateAvatarResult>(userUpdateAvatarIR);


/** 'UserUpdateBanner' parameters type */
export interface IUserUpdateBannerParams {
  bannerId: string | null | void;
  userId: string | null | void;
}

/** 'UserUpdateBanner' return type */
export type IUserUpdateBannerResult = void;

/** 'UserUpdateBanner' query type */
export interface IUserUpdateBannerQuery {
  params: IUserUpdateBannerParams;
  result: IUserUpdateBannerResult;
}

const userUpdateBannerIR: any = {"usedParamSet":{"bannerId":true,"userId":true},"params":[{"name":"bannerId","required":false,"transform":{"type":"scalar"},"locs":[{"a":43,"b":51}]},{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":73,"b":79}]}],"statement":"update app_public.user\n    set banner_id = :bannerId\n    where user_id = :userId"};

/**
 * Query generated from SQL:
 * ```
 * update app_public.user
 *     set banner_id = :bannerId
 *     where user_id = :userId
 * ```
 */
export const userUpdateBanner = new PreparedQuery<IUserUpdateBannerParams,IUserUpdateBannerResult>(userUpdateBannerIR);


