/** Types generated for queries found in "src/sql/mock.sql" */
import { PreparedQuery } from '@pgtyped/query';

export type activity_direction = 'inbound' | 'local' | 'outbound' | 'remote';

export type activity_type = 'follow' | 'follow_undo' | 'note_create';

export type scope = 'direct' | 'local' | 'private' | 'public' | 'unlisted';

export type tribool = 'false' | 'maybe' | 'true';

export type stringArray = (string)[];

/** 'InsertDomain' parameters type */
export interface IInsertDomainParams {
  name: string | null | void;
}

/** 'InsertDomain' return type */
export interface IInsertDomainResult {
  /** creation time */
  ctime: Date;
  /** unique surrogate key for domain name */
  domain_id: string;
  /** domain name i.e. example.com */
  host: string | null;
  /** modified time */
  mtime: Date | null;
  name: string | null;
  open_registrations: boolean | null;
}

/** 'InsertDomain' query type */
export interface IInsertDomainQuery {
  params: IInsertDomainParams;
  result: IInsertDomainResult;
}

const insertDomainIR: any = {"usedParamSet":{"name":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":51,"b":55},{"a":64,"b":68}]}],"statement":"insert into app_public.domain (host, name) values (:name, upper(:name)) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.domain (host, name) values (:name, upper(:name)) returning *
 * ```
 */
export const insertDomain = new PreparedQuery<IInsertDomainParams,IInsertDomainResult>(insertDomainIR);


/** 'InsertActivity' parameters type */
export interface IInsertActivityParams {
  data: string | null | void;
  remote: boolean | null | void;
  type: activity_type | null | void;
  uri: string | null | void;
}

/** 'InsertActivity' return type */
export interface IInsertActivityResult {
  /** Primary surrogate uuid key */
  activity_id: string | null;
  /** Creation time */
  ctime: Date;
  data: string;
  /** Modified time */
  mtime: Date | null;
  remote: boolean;
  type: activity_type;
  uri: string | null;
}

/** 'InsertActivity' query type */
export interface IInsertActivityQuery {
  params: IInsertActivityParams;
  result: IInsertActivityResult;
}

const insertActivityIR: any = {"usedParamSet":{"type":true,"data":true,"uri":true,"remote":true},"params":[{"name":"type","required":false,"transform":{"type":"scalar"},"locs":[{"a":71,"b":75}]},{"name":"data","required":false,"transform":{"type":"scalar"},"locs":[{"a":82,"b":86}]},{"name":"uri","required":false,"transform":{"type":"scalar"},"locs":[{"a":93,"b":96}]},{"name":"remote","required":false,"transform":{"type":"scalar"},"locs":[{"a":103,"b":109}]}],"statement":"insert into app_hidden.activity (type, data, uri, remote) values (\n    :type,\n    :data,\n    :uri,\n    :remote\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_hidden.activity (type, data, uri, remote) values (
 *     :type,
 *     :data,
 *     :uri,
 *     :remote
 * ) returning *
 * ```
 */
export const insertActivity = new PreparedQuery<IInsertActivityParams,IInsertActivityResult>(insertActivityIR);


/** 'InsertUser' parameters type */
export interface IInsertUserParams {
  body: string | null | void;
  caption: string | null | void;
  name: string | null | void;
  nick: string | null | void;
}

/** 'InsertUser' return type */
export interface IInsertUserResult {
  avatar_id: string | null;
  banner_id: string | null;
  body: string | null;
  caption: string | null;
  count_blockers: number;
  count_blocking: number;
  count_followers: number;
  count_following: number;
  count_notes: number;
  count_notifications: number;
  count_notifications_unread: number;
  /** Creation time */
  ctime: Date;
  dead: boolean | null;
  /** Domain the user originates from */
  domain_id: string;
  manually_approves_followers: boolean;
  /** Modified time */
  mtime: Date | null;
  /** Unique username that never changes */
  name: string;
  name_full: string;
  /** Custom nickname that can change */
  nick: string | null;
  /** Whether or not the user comes from here or a foreign instance */
  remote: boolean;
  /** Unique identifier for the user. */
  user_id: string;
}

/** 'InsertUser' query type */
export interface IInsertUserQuery {
  params: IInsertUserParams;
  result: IInsertUserResult;
}

const insertUserIR: any = {"usedParamSet":{"name":true,"nick":true,"body":true,"caption":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":110,"b":114}]},{"name":"nick","required":false,"transform":{"type":"scalar"},"locs":[{"a":194,"b":198}]},{"name":"body","required":false,"transform":{"type":"scalar"},"locs":[{"a":205,"b":209}]},{"name":"caption","required":false,"transform":{"type":"scalar"},"locs":[{"a":216,"b":223}]}],"statement":"insert into app_public.user (name, domain_id, nick, body, caption, remote, avatar_id, banner_id) values (\n    :name,\n    (select domain_id from app_public.domain order by random() limit 1),\n    :nick,\n    :body,\n    :caption,\n    true,\n    (select attachment_id from app_public.user_attachment order by random() limit 1),\n    (select attachment_id from app_public.user_attachment order by random() limit 1)\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.user (name, domain_id, nick, body, caption, remote, avatar_id, banner_id) values (
 *     :name,
 *     (select domain_id from app_public.domain order by random() limit 1),
 *     :nick,
 *     :body,
 *     :caption,
 *     true,
 *     (select attachment_id from app_public.user_attachment order by random() limit 1),
 *     (select attachment_id from app_public.user_attachment order by random() limit 1)
 * ) returning *
 * ```
 */
export const insertUser = new PreparedQuery<IInsertUserParams,IInsertUserResult>(insertUserIR);


/** 'InsertAttachment' parameters type */
export interface IInsertAttachmentParams {
  blake3: string | null | void;
  remote: boolean | null | void;
}

/** 'InsertAttachment' return type */
export interface IInsertAttachmentResult {
  blake3: string;
  blurhash: string | null;
  ctime: Date;
  height: number | null;
  mtime: Date | null;
  ref_count: number;
  remote: boolean;
  size: number;
  width: number | null;
}

/** 'InsertAttachment' query type */
export interface IInsertAttachmentQuery {
  params: IInsertAttachmentParams;
  result: IInsertAttachmentResult;
}

const insertAttachmentIR: any = {"usedParamSet":{"blake3":true,"remote":true},"params":[{"name":"blake3","required":false,"transform":{"type":"scalar"},"locs":[{"a":58,"b":64}]},{"name":"remote","required":false,"transform":{"type":"scalar"},"locs":[{"a":71,"b":77}]}],"statement":"insert into app_public.file (blake3, remote) values (\n    :blake3,\n    :remote\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.file (blake3, remote) values (
 *     :blake3,
 *     :remote
 * ) returning *
 * ```
 */
export const insertAttachment = new PreparedQuery<IInsertAttachmentParams,IInsertAttachmentResult>(insertAttachmentIR);


/** 'InsertNote' parameters type */
export interface IInsertNoteParams {
  body: string | null | void;
  scope: scope | null | void;
}

/** 'InsertNote' return type */
export interface IInsertNoteResult {
  announce: boolean;
  body: string | null;
  count_attachments: number;
  count_bookmarks: number;
  count_likes: number;
  count_reacts: number;
  count_repeats: number;
  count_replies: number;
  count_thread: number;
  ctime: Date;
  domain_id: string;
  mtime: Date | null;
  note_id: string;
  remote: boolean;
  reply_id: string | null;
  scope: scope;
  sensitive: boolean | null;
  tags: stringArray | null;
  thread_id: string | null;
  title: string | null;
  user_id: string;
  weighted_keywords: string | null;
}

/** 'InsertNote' query type */
export interface IInsertNoteQuery {
  params: IInsertNoteParams;
  result: IInsertNoteResult;
}

const insertNoteIR: any = {"usedParamSet":{"scope":true,"body":true},"params":[{"name":"scope","required":false,"transform":{"type":"scalar"},"locs":[{"a":232,"b":237}]},{"name":"body","required":false,"transform":{"type":"scalar"},"locs":[{"a":244,"b":248}]}],"statement":"insert into app_public.note (user_id, domain_id, scope, body, reply_id, ctime) values (\n    (select user_id from app_public.user order by random() limit 1),\n    (select domain_id from app_public.user order by random() limit 1),\n    :scope,\n    :body,\n    case when random() > 0.5\n        then (select note_id from app_public.note order by random() limit 1)\n        else null end,\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.note (user_id, domain_id, scope, body, reply_id, ctime) values (
 *     (select user_id from app_public.user order by random() limit 1),
 *     (select domain_id from app_public.user order by random() limit 1),
 *     :scope,
 *     :body,
 *     case when random() > 0.5
 *         then (select note_id from app_public.note order by random() limit 1)
 *         else null end,
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const insertNote = new PreparedQuery<IInsertNoteParams,IInsertNoteResult>(insertNoteIR);


/** 'AddAttachment' parameters type */
export type IAddAttachmentParams = void;

/** 'AddAttachment' return type */
export interface IAddAttachmentResult {
  attachment_id: string;
  ctime: Date;
  file_id: string | null;
  mtime: Date | null;
  note_id: string;
  src: string | null;
}

/** 'AddAttachment' query type */
export interface IAddAttachmentQuery {
  params: IAddAttachmentParams;
  result: IAddAttachmentResult;
}

const addAttachmentIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.note_attachment (note_id, file_id) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select blake3 from app_public.file order by random() limit 1)\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.note_attachment (note_id, file_id) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select blake3 from app_public.file order by random() limit 1)
 * ) returning *
 * ```
 */
export const addAttachment = new PreparedQuery<IAddAttachmentParams,IAddAttachmentResult>(addAttachmentIR);


/** 'AddNoteReact' parameters type */
export interface IAddNoteReactParams {
  attachmentId: string | null | void;
}

/** 'AddNoteReact' return type */
export interface IAddNoteReactResult {
  ctime: Date;
  file_id: string | null;
  mtime: Date | null;
  note_id: string;
  react_id: string;
  src: string | null;
  user_id: string;
}

/** 'AddNoteReact' query type */
export interface IAddNoteReactQuery {
  params: IAddNoteReactParams;
  result: IAddNoteReactResult;
}

const addNoteReactIR: any = {"usedParamSet":{"attachmentId":true},"params":[{"name":"attachmentId","required":false,"transform":{"type":"scalar"},"locs":[{"a":215,"b":227}]}],"statement":"insert into app_public.react (note_id, user_id, file_id, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    :attachmentId,\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.react (note_id, user_id, file_id, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     :attachmentId,
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addNoteReact = new PreparedQuery<IAddNoteReactParams,IAddNoteReactResult>(addNoteReactIR);


/** 'AddNoteReactEmoji' parameters type */
export interface IAddNoteReactEmojiParams {
  emoji: string | null | void;
}

/** 'AddNoteReactEmoji' return type */
export interface IAddNoteReactEmojiResult {
  ctime: Date;
  file_id: string | null;
  mtime: Date | null;
  note_id: string;
  react_id: string;
  src: string | null;
  user_id: string;
}

/** 'AddNoteReactEmoji' query type */
export interface IAddNoteReactEmojiQuery {
  params: IAddNoteReactEmojiParams;
  result: IAddNoteReactEmojiResult;
}

const addNoteReactEmojiIR: any = {"usedParamSet":{"emoji":true},"params":[{"name":"emoji","required":false,"transform":{"type":"scalar"},"locs":[{"a":211,"b":216}]}],"statement":"insert into app_public.react (note_id, user_id, src, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    :emoji,\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.react (note_id, user_id, src, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     :emoji,
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addNoteReactEmoji = new PreparedQuery<IAddNoteReactEmojiParams,IAddNoteReactEmojiResult>(addNoteReactEmojiIR);


/** 'AddLike' parameters type */
export type IAddLikeParams = void;

/** 'AddLike' return type */
export interface IAddLikeResult {
  ctime: Date;
  like_id: string;
  mtime: Date | null;
  note_id: string;
  remote: boolean;
  user_id: string;
}

/** 'AddLike' query type */
export interface IAddLikeQuery {
  params: IAddLikeParams;
  result: IAddLikeResult;
}

const addLikeIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.like (note_id, user_id, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.like (note_id, user_id, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addLike = new PreparedQuery<IAddLikeParams,IAddLikeResult>(addLikeIR);


/** 'AddFollow' parameters type */
export type IAddFollowParams = void;

/** 'AddFollow' return type */
export interface IAddFollowResult {
  approved: tribool | null;
  ctime: Date;
  dead: boolean | null;
  direction: activity_direction;
  follow_id: string;
  followed_id: string;
  follower_id: string;
  mtime: Date | null;
  remote: boolean;
}

/** 'AddFollow' query type */
export interface IAddFollowQuery {
  params: IAddFollowParams;
  result: IAddFollowResult;
}

const addFollowIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.follow (followed_id, follower_id, ctime) values (\n    (select user_id from app_public.user order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.follow (followed_id, follower_id, ctime) values (
 *     (select user_id from app_public.user order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addFollow = new PreparedQuery<IAddFollowParams,IAddFollowResult>(addFollowIR);


/** 'AddRepeat' parameters type */
export type IAddRepeatParams = void;

/** 'AddRepeat' return type */
export interface IAddRepeatResult {
  ctime: Date;
  mtime: Date | null;
  note_id: string;
  remote: boolean;
  repeat_id: string;
  user_id: string;
}

/** 'AddRepeat' query type */
export interface IAddRepeatQuery {
  params: IAddRepeatParams;
  result: IAddRepeatResult;
}

const addRepeatIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.repeat (note_id, user_id, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.repeat (note_id, user_id, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addRepeat = new PreparedQuery<IAddRepeatParams,IAddRepeatResult>(addRepeatIR);


/** 'AddBlock' parameters type */
export type IAddBlockParams = void;

/** 'AddBlock' return type */
export interface IAddBlockResult {
  block_id: string;
  blocked_id: string;
  blocker_id: string;
  ctime: Date;
  expires: Date | null;
  mtime: Date | null;
}

/** 'AddBlock' query type */
export interface IAddBlockQuery {
  params: IAddBlockParams;
  result: IAddBlockResult;
}

const addBlockIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.block (blocker_id, blocked_id, ctime) values (\n    (select user_id from app_public.user order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.block (blocker_id, blocked_id, ctime) values (
 *     (select user_id from app_public.user order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addBlock = new PreparedQuery<IAddBlockParams,IAddBlockResult>(addBlockIR);


/** 'AddBookmark' parameters type */
export type IAddBookmarkParams = void;

/** 'AddBookmark' return type */
export interface IAddBookmarkResult {
  ctime: Date;
  mtime: Date | null;
  note_id: string;
  user_id: string;
}

/** 'AddBookmark' query type */
export interface IAddBookmarkQuery {
  params: IAddBookmarkParams;
  result: IAddBookmarkResult;
}

const addBookmarkIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.bookmark (note_id, user_id, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.bookmark (note_id, user_id, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addBookmark = new PreparedQuery<IAddBookmarkParams,IAddBookmarkResult>(addBookmarkIR);


/** 'AddField' parameters type */
export interface IAddFieldParams {
  body: string | null | void;
  label: string | null | void;
}

/** 'AddField' return type */
export interface IAddFieldResult {
  body: string;
  ctime: Date;
  label: string;
  mtime: Date | null;
  user_id: string;
}

/** 'AddField' query type */
export interface IAddFieldQuery {
  params: IAddFieldParams;
  result: IAddFieldResult;
}

const addFieldIR: any = {"usedParamSet":{"label":true,"body":true},"params":[{"name":"label","required":false,"transform":{"type":"scalar"},"locs":[{"a":141,"b":146}]},{"name":"body","required":false,"transform":{"type":"scalar"},"locs":[{"a":153,"b":157}]}],"statement":"insert into app_public.field (user_id, label, body, ctime) values (\n    (select user_id from app_public.user order by random() limit 1),\n    :label,\n    :body,\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.field (user_id, label, body, ctime) values (
 *     (select user_id from app_public.user order by random() limit 1),
 *     :label,
 *     :body,
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addField = new PreparedQuery<IAddFieldParams,IAddFieldResult>(addFieldIR);


/** 'AddMention' parameters type */
export type IAddMentionParams = void;

/** 'AddMention' return type */
export interface IAddMentionResult {
  ctime: Date;
  mention_id: string;
  mtime: Date | null;
  note_id: string;
  remote: boolean;
  reply: boolean;
  user_id: string;
}

/** 'AddMention' query type */
export interface IAddMentionQuery {
  params: IAddMentionParams;
  result: IAddMentionResult;
}

const addMentionIR: any = {"usedParamSet":{},"params":[],"statement":"insert into app_public.mention (note_id, user_id, ctime) values (\n    (select note_id from app_public.note order by random() limit 1),\n    (select user_id from app_public.user order by random() limit 1),\n    NOW() - (random() * INTERVAL '2 days')\n) returning *"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.mention (note_id, user_id, ctime) values (
 *     (select note_id from app_public.note order by random() limit 1),
 *     (select user_id from app_public.user order by random() limit 1),
 *     NOW() - (random() * INTERVAL '2 days')
 * ) returning *
 * ```
 */
export const addMention = new PreparedQuery<IAddMentionParams,IAddMentionResult>(addMentionIR);


