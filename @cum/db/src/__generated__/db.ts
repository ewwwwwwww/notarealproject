/** Types generated for queries found in "src/sql/db.sql" */
import { PreparedQuery } from '@pgtyped/query';

/** 'SetCurrentUser' parameters type */
export interface ISetCurrentUserParams {
  userId: string | null | void;
}

/** 'SetCurrentUser' return type */
export interface ISetCurrentUserResult {
  set_current_user: undefined | null;
}

/** 'SetCurrentUser' query type */
export interface ISetCurrentUserQuery {
  params: ISetCurrentUserParams;
  result: ISetCurrentUserResult;
}

const setCurrentUserIR: any = {"usedParamSet":{"userId":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":36,"b":42}]}],"statement":"select app_private.set_current_user(:userId)"};

/**
 * Query generated from SQL:
 * ```
 * select app_private.set_current_user(:userId)
 * ```
 */
export const setCurrentUser = new PreparedQuery<ISetCurrentUserParams,ISetCurrentUserResult>(setCurrentUserIR);


/** 'InsertDomain' parameters type */
export interface IInsertDomainParams {
  name: string | null | void;
}

/** 'InsertDomain' return type */
export type IInsertDomainResult = void;

/** 'InsertDomain' query type */
export interface IInsertDomainQuery {
  params: IInsertDomainParams;
  result: IInsertDomainResult;
}

const insertDomainIR: any = {"usedParamSet":{"name":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":51,"b":55},{"a":64,"b":68}]}],"statement":"insert into app_public.domain (host, name) values (:name, upper(:name))\n  on conflict (host) do nothing"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_public.domain (host, name) values (:name, upper(:name))
 *   on conflict (host) do nothing
 * ```
 */
export const insertDomain = new PreparedQuery<IInsertDomainParams,IInsertDomainResult>(insertDomainIR);


/** 'Truncate' parameters type */
export type ITruncateParams = void;

/** 'Truncate' return type */
export type ITruncateResult = void;

/** 'Truncate' query type */
export interface ITruncateQuery {
  params: ITruncateParams;
  result: ITruncateResult;
}

const truncateIR: any = {"usedParamSet":{},"params":[],"statement":"truncate table\n    app_public.block,\n    app_public.bookmark,\n    app_public.field,\n    app_public.follow,\n    app_public.like,\n    app_public.mention,\n    app_public.note,\n    app_public.note_attachment,\n    app_public.note_history,\n    app_public.notification,\n    app_public.react,\n    app_public.react_aggr,\n    app_public.user,\n    app_public.user_attachment,\n    app_hidden.activity,\n    app_public.domain restart identity cascade"};

/**
 * Query generated from SQL:
 * ```
 * truncate table
 *     app_public.block,
 *     app_public.bookmark,
 *     app_public.field,
 *     app_public.follow,
 *     app_public.like,
 *     app_public.mention,
 *     app_public.note,
 *     app_public.note_attachment,
 *     app_public.note_history,
 *     app_public.notification,
 *     app_public.react,
 *     app_public.react_aggr,
 *     app_public.user,
 *     app_public.user_attachment,
 *     app_hidden.activity,
 *     app_public.domain restart identity cascade
 * ```
 */
export const truncate = new PreparedQuery<ITruncateParams,ITruncateResult>(truncateIR);


/** 'DropJobs' parameters type */
export type IDropJobsParams = void;

/** 'DropJobs' return type */
export type IDropJobsResult = void;

/** 'DropJobs' query type */
export interface IDropJobsQuery {
  params: IDropJobsParams;
  result: IDropJobsResult;
}

const dropJobsIR: any = {"usedParamSet":{},"params":[],"statement":"truncate table graphile_worker.jobs cascade"};

/**
 * Query generated from SQL:
 * ```
 * truncate table graphile_worker.jobs cascade
 * ```
 */
export const dropJobs = new PreparedQuery<IDropJobsParams,IDropJobsResult>(dropJobsIR);


