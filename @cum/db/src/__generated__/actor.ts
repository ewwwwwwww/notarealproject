/** Types generated for queries found in "src/sql/actor.sql" */
import { PreparedQuery } from '@pgtyped/query';

/** 'ActorDataById' parameters type */
export interface IActorDataByIdParams {
  userId: string | null | void;
}

/** 'ActorDataById' return type */
export interface IActorDataByIdResult {
  /** Unique username that never changes */
  name: string;
  /** Custom nickname that can change */
  nick: string | null;
  /** Pubkey for verifying activity pub messages */
  pubkey: string | null;
}

/** 'ActorDataById' query type */
export interface IActorDataByIdQuery {
  params: IActorDataByIdParams;
  result: IActorDataByIdResult;
}

const actorDataByIdIR: any = {"usedParamSet":{"userId":true},"params":[{"name":"userId","required":false,"transform":{"type":"scalar"},"locs":[{"a":157,"b":163}]}],"statement":"select name, nick, pubkey from app_public.user\ninner join app_hidden.user\non app_public.user.user_id=app_hidden.user.user_id\nwhere app_public.user.user_id = :userId and remote = false\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select name, nick, pubkey from app_public.user
 * inner join app_hidden.user
 * on app_public.user.user_id=app_hidden.user.user_id
 * where app_public.user.user_id = :userId and remote = false
 * limit 1
 * ```
 */
export const actorDataById = new PreparedQuery<IActorDataByIdParams,IActorDataByIdResult>(actorDataByIdIR);


/** 'ActorDataByName' parameters type */
export interface IActorDataByNameParams {
  name: string | null | void;
}

/** 'ActorDataByName' return type */
export interface IActorDataByNameResult {
  /** Custom nickname that can change */
  nick: string | null;
  /** Pubkey for verifying activity pub messages */
  pubkey: string | null;
}

/** 'ActorDataByName' query type */
export interface IActorDataByNameQuery {
  params: IActorDataByNameParams;
  result: IActorDataByNameResult;
}

const actorDataByNameIR: any = {"usedParamSet":{"name":true},"params":[{"name":"name","required":false,"transform":{"type":"scalar"},"locs":[{"a":132,"b":136}]}],"statement":"select nick, pubkey from app_public.user\ninner join app_hidden.user\non app_public.user.user_id=app_hidden.user.user_id\nwhere name = :name and remote = false\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select nick, pubkey from app_public.user
 * inner join app_hidden.user
 * on app_public.user.user_id=app_hidden.user.user_id
 * where name = :name and remote = false
 * limit 1
 * ```
 */
export const actorDataByName = new PreparedQuery<IActorDataByNameParams,IActorDataByNameResult>(actorDataByNameIR);


/** 'ActorFromFollow' parameters type */
export interface IActorFromFollowParams {
  followedId: string | null | void;
  followerId: string | null | void;
}

/** 'ActorFromFollow' return type */
export interface IActorFromFollowResult {
  actor: string | null;
  inbox: string | null;
}

/** 'ActorFromFollow' query type */
export interface IActorFromFollowQuery {
  params: IActorFromFollowParams;
  result: IActorFromFollowResult;
}

const actorFromFollowIR: any = {"usedParamSet":{"followerId":true,"followedId":true},"params":[{"name":"followerId","required":false,"transform":{"type":"scalar"},"locs":[{"a":59,"b":69}]},{"name":"followedId","required":false,"transform":{"type":"scalar"},"locs":[{"a":72,"b":82}]}],"statement":"select actor, inbox from app_hidden.user where user_id in (:followerId, :followedId)"};

/**
 * Query generated from SQL:
 * ```
 * select actor, inbox from app_hidden.user where user_id in (:followerId, :followedId)
 * ```
 */
export const actorFromFollow = new PreparedQuery<IActorFromFollowParams,IActorFromFollowResult>(actorFromFollowIR);


/** 'ActorInbox' parameters type */
export interface IActorInboxParams {
  actor: string | null | void;
}

/** 'ActorInbox' return type */
export interface IActorInboxResult {
  inbox: string | null;
}

/** 'ActorInbox' query type */
export interface IActorInboxQuery {
  params: IActorInboxParams;
  result: IActorInboxResult;
}

const actorInboxIR: any = {"usedParamSet":{"actor":true},"params":[{"name":"actor","required":false,"transform":{"type":"scalar"},"locs":[{"a":46,"b":51}]}],"statement":"select inbox from app_hidden.user where actor=:actor\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select inbox from app_hidden.user where actor=:actor
 * limit 1
 * ```
 */
export const actorInbox = new PreparedQuery<IActorInboxParams,IActorInboxResult>(actorInboxIR);


/** 'ActorPubKey' parameters type */
export interface IActorPubKeyParams {
  actor: string | null | void;
}

/** 'ActorPubKey' return type */
export interface IActorPubKeyResult {
  /** Pubkey for verifying activity pub messages */
  pubkey: string | null;
}

/** 'ActorPubKey' query type */
export interface IActorPubKeyQuery {
  params: IActorPubKeyParams;
  result: IActorPubKeyResult;
}

const actorPubKeyIR: any = {"usedParamSet":{"actor":true},"params":[{"name":"actor","required":false,"transform":{"type":"scalar"},"locs":[{"a":47,"b":52}]}],"statement":"select pubkey from app_hidden.user where actor=:actor\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select pubkey from app_hidden.user where actor=:actor
 * limit 1
 * ```
 */
export const actorPubKey = new PreparedQuery<IActorPubKeyParams,IActorPubKeyResult>(actorPubKeyIR);


/** 'ActorPrivKey' parameters type */
export interface IActorPrivKeyParams {
  actor: string | null | void;
}

/** 'ActorPrivKey' return type */
export interface IActorPrivKeyResult {
  /** Private key for signing activity pub messages */
  privkey: string | null;
}

/** 'ActorPrivKey' query type */
export interface IActorPrivKeyQuery {
  params: IActorPrivKeyParams;
  result: IActorPrivKeyResult;
}

const actorPrivKeyIR: any = {"usedParamSet":{"actor":true},"params":[{"name":"actor","required":false,"transform":{"type":"scalar"},"locs":[{"a":107,"b":112}]}],"statement":"select privkey from app_private.user\n  where user_id=(\n    select user_id from app_hidden.user where actor=:actor\n  )\n  limit 1"};

/**
 * Query generated from SQL:
 * ```
 * select privkey from app_private.user
 *   where user_id=(
 *     select user_id from app_hidden.user where actor=:actor
 *   )
 *   limit 1
 * ```
 */
export const actorPrivKey = new PreparedQuery<IActorPrivKeyParams,IActorPrivKeyResult>(actorPrivKeyIR);


/** 'ActorUserId' parameters type */
export interface IActorUserIdParams {
  actor: string | null | void;
}

/** 'ActorUserId' return type */
export interface IActorUserIdResult {
  /** Reference to user_id */
  user_id: string;
}

/** 'ActorUserId' query type */
export interface IActorUserIdQuery {
  params: IActorUserIdParams;
  result: IActorUserIdResult;
}

const actorUserIdIR: any = {"usedParamSet":{"actor":true},"params":[{"name":"actor","required":false,"transform":{"type":"scalar"},"locs":[{"a":48,"b":53}]}],"statement":"select user_id from app_hidden.user where actor=:actor\nlimit 1"};

/**
 * Query generated from SQL:
 * ```
 * select user_id from app_hidden.user where actor=:actor
 * limit 1
 * ```
 */
export const actorUserId = new PreparedQuery<IActorUserIdParams,IActorUserIdResult>(actorUserIdIR);


/** 'ActorsInboxes' parameters type */
export interface IActorsInboxesParams {
  actors: readonly (string | null | void)[];
}

/** 'ActorsInboxes' return type */
export interface IActorsInboxesResult {
  inbox: string | null;
}

/** 'ActorsInboxes' query type */
export interface IActorsInboxesQuery {
  params: IActorsInboxesParams;
  result: IActorsInboxesResult;
}

const actorsInboxesIR: any = {"usedParamSet":{"actors":true},"params":[{"name":"actors","required":false,"transform":{"type":"array_spread"},"locs":[{"a":51,"b":57}]}],"statement":"select inbox from app_hidden.user\n  where actor in :actors"};

/**
 * Query generated from SQL:
 * ```
 * select inbox from app_hidden.user
 *   where actor in :actors
 * ```
 */
export const actorsInboxes = new PreparedQuery<IActorsInboxesParams,IActorsInboxesResult>(actorsInboxesIR);


