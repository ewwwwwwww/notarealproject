/** Types generated for queries found in "src/sql/activity.sql" */
import { PreparedQuery } from '@pgtyped/query';

export type activity_type = 'follow' | 'follow_undo' | 'note_create';

/** 'ActivityInsert' parameters type */
export interface IActivityInsertParams {
  data: string | null | void;
  remote: boolean | null | void;
  type: activity_type | null | void;
  uri: string | null | void;
}

/** 'ActivityInsert' return type */
export type IActivityInsertResult = void;

/** 'ActivityInsert' query type */
export interface IActivityInsertQuery {
  params: IActivityInsertParams;
  result: IActivityInsertResult;
}

const activityInsertIR: any = {"usedParamSet":{"type":true,"data":true,"uri":true,"remote":true},"params":[{"name":"type","required":false,"transform":{"type":"scalar"},"locs":[{"a":71,"b":75}]},{"name":"data","required":false,"transform":{"type":"scalar"},"locs":[{"a":82,"b":86}]},{"name":"uri","required":false,"transform":{"type":"scalar"},"locs":[{"a":93,"b":96}]},{"name":"remote","required":false,"transform":{"type":"scalar"},"locs":[{"a":103,"b":109}]}],"statement":"insert into app_hidden.activity (type, data, uri, remote) values (\n    :type,\n    :data,\n    :uri,\n    :remote\n)"};

/**
 * Query generated from SQL:
 * ```
 * insert into app_hidden.activity (type, data, uri, remote) values (
 *     :type,
 *     :data,
 *     :uri,
 *     :remote
 * )
 * ```
 */
export const activityInsert = new PreparedQuery<IActivityInsertParams,IActivityInsertResult>(activityInsertIR);


