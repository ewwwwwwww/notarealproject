/* @name insertFollow */

insert into app_public.follow (follower_id, followed_id, direction)
values (
  :followerId,
  :followedId,
  :direction
);
