/* @name ActorDataById */
select name, nick, pubkey from app_public.user
inner join app_hidden.user
on app_public.user.user_id=app_hidden.user.user_id
where app_public.user.user_id = :userId and remote = false
limit 1;

/* @name ActorDataByName */
select nick, pubkey from app_public.user
inner join app_hidden.user
on app_public.user.user_id=app_hidden.user.user_id
where name = :name and remote = false
limit 1;

/* @name ActorFromFollow */
select actor, inbox from app_hidden.user where user_id in (:followerId, :followedId);

/* @name ActorInbox */
select inbox from app_hidden.user where actor=:actor
limit 1;

/* @name ActorPubKey */
select pubkey from app_hidden.user where actor=:actor
limit 1;

/* @name ActorPrivKey */
select privkey from app_private.user
  where user_id=(
    select user_id from app_hidden.user where actor=:actor
  )
  limit 1;

/* @name ActorUserId */
select user_id from app_hidden.user where actor=:actor
limit 1;

/*
  @name ActorsInboxes
  @param actors -> (...)
*/
select inbox from app_hidden.user
  where actor in :actors;
