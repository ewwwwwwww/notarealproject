/* @name NoteInsert */
insert into app_public.note (user_id, domain_id, scope, reply_id, body, remote) values (
    :userId,
    (select domain_id from app_public.user where user_id = :userId),
    :scope,
    :reply,
    :body,
    :remote
) returning note_id;

/* @name NoteById */
select user_id, scope, reply_id, body, remote from app_public.note where note_id=:noteId;
