/* @name ActivityInsert */
insert into app_hidden.activity (type, data, uri, remote) values (
    :type,
    :data,
    :uri,
    :remote
);