/* @name SetCurrentUser */
select app_private.set_current_user(:userId);

/* @name InsertDomain */
insert into app_public.domain (host, name) values (:name, upper(:name))
  on conflict (host) do nothing;

/* @name Truncate */
truncate table
    app_public.block,
    app_public.bookmark,
    app_public.field,
    app_public.follow,
    app_public.like,
    app_public.mention,
    app_public.note,
    app_public.note_attachment,
    app_public.note_history,
    app_public.notification,
    app_public.react,
    app_public.react_aggr,
    app_public.user,
    app_public.user_attachment,
    app_hidden.activity,
    app_public.domain restart identity cascade;
    
/* @name DropJobs */
truncate table graphile_worker.jobs cascade;