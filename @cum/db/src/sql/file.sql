/* @name FileAdd */
insert into app_public.file (blake3, size) values (:blake3, :size) on conflict do nothing;

/* @name NoteAttachmentAdd */
insert into app_public.note_attachment (note_id, file_id) values (
    :noteId, :blake3
) on conflict do nothing returning attachment_id;

/* @name UserAttachmentAdd */
insert into app_public.user_attachment (user_id, file_id) values (
    :userId, :blake3
) on conflict do nothing returning attachment_id;

/* @name UserUpdateAvatar */
update app_public.user
    set avatar_id = :avatarId
    where user_id = :userId;

/* @name UserUpdateBanner */
update app_public.user
    set banner_id = :bannerId
    where user_id = :userId;