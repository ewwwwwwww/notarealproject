/* @name UserIdLocal */
select user_id from app_public.user
  where name=:name and remote=false
  limit 1;

/* @name UserPubKeyUpdate */
update app_hidden.user set pubkey=coalesce(pubkey, :pubkey) where user_id=:id;

/* @name UserPubKeyById */
select pubkey from app_hidden.user where user_id=:userId
limit 1;

/* @name UserPrivKeyUpdate */
update app_private.user set privkey=coalesce(privkey, :privkey) where user_id=:id;

/* @name UserPrivKeyById */
select privkey from app_private.user
  where user_id=:userId
  limit 1;

/* @name UserRegisterRemote */
select user_id from app_private.user_register_remote(:name, :domain, :id, :inbox, :pubkey);

/* @name UserInboxById */
select actor, inbox from app_hidden.user where user_id=:userId
limit 1;

/* @name UserNamePrivKeyById */
select name, privkey from app_public.user, app_private.user
  where app_public.user.user_id=app_private.user.user_id
  and app_public.user.user_id = :id
  limit 1;