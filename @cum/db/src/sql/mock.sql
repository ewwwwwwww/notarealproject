/* @name InsertDomain */
insert into app_public.domain (host, name) values (:name, upper(:name)) returning *;

/* @name InsertActivity */
insert into app_hidden.activity (type, data, uri, remote) values (
    :type,
    :data,
    :uri,
    :remote
) returning *;

/* @name InsertUser */
insert into app_public.user (name, domain_id, nick, body, caption, remote, avatar_id, banner_id) values (
    :name,
    (select domain_id from app_public.domain order by random() limit 1),
    :nick,
    :body,
    :caption,
    true,
    (select attachment_id from app_public.user_attachment order by random() limit 1),
    (select attachment_id from app_public.user_attachment order by random() limit 1)
) returning *;

/* @name InsertAttachment */
insert into app_public.file (blake3, remote) values (
    :blake3,
    :remote
) returning *;

/* @name InsertNote */
insert into app_public.note (user_id, domain_id, scope, body, reply_id, ctime) values (
    (select user_id from app_public.user order by random() limit 1),
    (select domain_id from app_public.user order by random() limit 1),
    :scope,
    :body,
    case when random() > 0.5
        then (select note_id from app_public.note order by random() limit 1)
        else null end,
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddAttachment */
insert into app_public.note_attachment (note_id, file_id) values (
    (select note_id from app_public.note order by random() limit 1),
    (select blake3 from app_public.file order by random() limit 1)
) returning *;

/* @name AddNoteReact */
insert into app_public.react (note_id, user_id, file_id, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    :attachmentId,
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddNoteReactEmoji */
insert into app_public.react (note_id, user_id, src, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    :emoji,
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddLike */
insert into app_public.like (note_id, user_id, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddFollow */
insert into app_public.follow (followed_id, follower_id, ctime) values (
    (select user_id from app_public.user order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddRepeat */
insert into app_public.repeat (note_id, user_id, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddBlock */
insert into app_public.block (blocker_id, blocked_id, ctime) values (
    (select user_id from app_public.user order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddBookmark */
insert into app_public.bookmark (note_id, user_id, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddField */
insert into app_public.field (user_id, label, body, ctime) values (
    (select user_id from app_public.user order by random() limit 1),
    :label,
    :body,
    NOW() - (random() * INTERVAL '2 days')
) returning *;

/* @name AddMention */
insert into app_public.mention (note_id, user_id, ctime) values (
    (select note_id from app_public.note order by random() limit 1),
    (select user_id from app_public.user order by random() limit 1),
    NOW() - (random() * INTERVAL '2 days')
) returning *;