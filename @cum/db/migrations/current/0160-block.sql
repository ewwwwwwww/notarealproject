create table app_public.block (
  block_id uuid primary key default fn_uuid_time_ordered(),
  blocker_id uuid not null references app_public.user (user_id),
  blocked_id uuid not null references app_public.user (user_id),
  ctime timestamptz not null default now(),
  mtime timestamptz,
  expires timestamptz,

  constraint prevent_block_self check (blocker_id != blocked_id)
);

create index on app_public.block (expires) where expires is not null;
create unique index on app_public.block (blocker_id, blocked_id);
create unique index on app_public.block (blocked_id, blocker_id);

comment on constraint "block_blocked_id_fkey" on app_public.block is
  E'@foreignFieldName blocking';

comment on constraint "block_blocker_id_fkey" on app_public.block is
  E'@foreignFieldName blockers';

create function app_public.user_is_blocking_you(person app_public.user) returns boolean as $$
  select coalesce((
    select true from app_public.block where app_public.block.blocker_id = person.user_id
      and app_public.block.blocked_id = app_hidden.current_user_id()
  ), false)
$$ language sql stable;

comment on function app_public.user_is_blocking_you(person app_public.user) is e'@nonNull';

create function app_public.user_is_blocked_by_you(person app_public.user) returns boolean as $$
  select coalesce((
    select true from app_public.block where app_public.block.blocked_id = person.user_id
      and app_public.block.blocker_id = app_hidden.current_user_id()
  ), false)
$$ language sql stable;

comment on function app_public.user_is_blocked_by_you(person app_public.user) is e'@nonNull';

create function app_public.tg_050_deliver_block() returns trigger as $$
begin
  perform graphile_worker.add_job('activityDeliver', json_build_object(
    'type', 'Block',
    'userId', new.blocker_id,
    'blockedId', new.blocked_id,
    'blockerId', new.blocker_id
  ));

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_block after insert on app_public.block for each row
  execute procedure app_public.tg_050_deliver_block();


create function app_public.tg_050_user_blocks_up() returns trigger as $$
begin
  update app_public.user
    set count_blocking = count_blocking + 1
    where user_id = new.blocker_id;

  update app_public.user
    set count_blockers = count_blockers + 1
    where user_id = new.blocked_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_blocks_up after insert
  on app_public.block
  for each row
  execute procedure app_public.tg_050_user_blocks_up();

create function app_public.tg_050_user_blocks_down() returns trigger as $$
begin
  update app_public.user
    set count_blocking = greatest(0, count_blocking - 1)
    where user_id = old.blocker_id;

  update app_public.user
    set count_blockers = greatest(0, count_blockers - 1)
    where user_id = old.blocked_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_blocks_down after delete
  on app_public.block
  for each row
  execute procedure app_public.tg_050_user_blocks_down();