create table app_public.repeat (
  repeat_id uuid primary key default fn_uuid_time_ordered(),
  note_id uuid not null references app_public.note (note_id),
  user_id uuid not null references app_public.user (user_id),
  remote boolean not null default false,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create unique index on app_public.repeat (note_id, user_id);
create unique index on app_public.repeat (user_id, note_id);

create trigger sync_mtime before update on app_public.repeat
  for each row execute procedure sync_mtime();

create function app_public.tg_050_deliver_announce() returns trigger as $$
begin
  if not new.remote then
    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'Announce',
      'userId', new.user_id,
      'noteId', new.note_id
    ));
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_announce after insert
  on app_public.repeat
  for each row
  execute procedure app_public.tg_050_deliver_announce();

create function app_public.tg_050_note_repeat_up() returns trigger as $$
begin
  update app_public.note
    set count_repeats = count_repeats + 1
    where note_id = new.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_repeat_up after insert
  on app_public.repeat
  for each row
  execute procedure app_public.tg_050_note_repeat_up();

create function app_public.tg_050_note_repeat_down() returns trigger as $$
begin
  update app_public.note
    set count_repeats = greatest(0, count_repeats - 1)
    where note_id = old.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_repeat_down after delete
  on app_public.repeat
  for each row
  execute procedure app_public.tg_050_note_repeat_down();

create function app_public.note_set_repeat(
  note_id uuid,
  state boolean
) returns app_public.note as $$
declare
  note app_public.note;
begin
  if state then
    insert into app_public.repeat (note_id, user_id)
      values (note_id, app_hidden.current_user_id())
      on conflict do nothing;
  else
    delete from app_public.repeat where app_public.repeat.note_id = $1 and user_id = app_hidden.current_user_id();
  end if;

  select * from app_public.note into note
    where app_public.note.note_id = $1
    limit 1;
  
  return note;
end;
$$ language plpgsql;