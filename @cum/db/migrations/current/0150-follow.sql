create table app_public.follow (
  follow_id uuid primary key default fn_uuid_time_ordered(),
  followed_id uuid not null references app_public.user (user_id),
  follower_id uuid not null references app_public.user (user_id),
  remote boolean not null default false,
  direction activity_direction not null default 'local',
  approved tribool,
  dead boolean default false,
  ctime timestamptz not null default now(),
  mtime timestamptz,

  constraint prevent_follow_self check (follower_id != followed_id),
  constraint follower_followed unique (follower_id, followed_id)
);

create index on app_public.follow (approved) where approved = 'maybe';
create unique index on app_public.follow (followed_id, follower_id);

comment on constraint "follow_followed_id_fkey" on app_public.follow is
  E'@foreignFieldName following';

comment on constraint "follow_follower_id_fkey" on app_public.follow is
  E'@foreignFieldName followers';

create trigger sync_mtime before update on app_public.follow
  for each row execute procedure sync_mtime();

create function app_public.user_is_following_you(person app_public.user) returns tribool as $$
  select coalesce((
    select approved from app_public.follow where app_public.follow.follower_id = person.user_id
      and app_public.follow.followed_id = app_hidden.current_user_id()
  ), 'false')
$$ language sql stable;

create function app_public.user_is_followed_by_you(person app_public.user) returns tribool as $$
  select coalesce((
    select approved from app_public.follow where app_public.follow.followed_id = person.user_id
      and app_public.follow.follower_id = app_hidden.current_user_id()
  ), 'false')
$$ language sql stable;

create function app_public.tg_050_follow_default_approved() returns trigger as $$
declare
  v_manually bool;
  v_remote bool;
begin
  if new.approved is null then
    select manually_approves_followers, remote
      into v_manually, v_remote
      from app_public.user
      where user_id = new.followed_id;
      
    if v_remote then
      new.approved = 'maybe';
    elsif v_manually then
      new.approved = 'maybe';
    else
      new.approved = 'true';
    end if;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger follow_default_approved before insert or update on app_public.follow for each row
  execute procedure app_public.tg_050_follow_default_approved();


create function app_public.tg_050_deliver_follow() returns trigger as $$
begin
  if (new.direction = 'inbound') then
    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'FollowAccept',
      'userId', new.followed_id,
      'followedId', new.followed_id,
      'followerId', new.follower_id
    ));
  elsif (new.direction = 'outbound') then
    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'Follow',
      'userId', new.follower_id,
      'followedId', new.followed_id,
      'followerId', new.follower_id
    ));
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_follow after insert on app_public.follow for each row
  execute procedure app_public.tg_050_deliver_follow();

create function app_public.tg_050_user_follows_up() returns trigger as $$
begin
  update app_public.user
    set count_following = count_following + 1
    where user_id = new.follower_id;

  update app_public.user
    set count_followers = count_followers + 1
    where user_id = new.followed_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_follows_up after insert
  on app_public.follow
  for each row
  execute procedure app_public.tg_050_user_follows_up();

create function app_public.tg_050_user_follows_down() returns trigger as $$
begin
  update app_public.user
    set count_following = greatest(0, count_following - 1)
    where user_id = old.follower_id;

  update app_public.user
    set count_followers = greatest(0, count_followers - 1)
    where user_id = old.followed_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_follows_down after delete
  on app_public.follow
  for each row
  execute procedure app_public.tg_050_user_follows_down();