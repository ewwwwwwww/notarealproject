create table app_public.note (
  note_id uuid primary key default fn_uuid_time_ordered(),
  user_id uuid not null references app_public.user (user_id),
  domain_id uuid not null references app_public.domain (domain_id) default app_public.domain_local_id(),
  thread_id uuid references app_public.note (note_id),
  reply_id uuid references app_public.note (note_id),

  announce boolean not null default false,
  remote boolean not null default false,
  scope scope not null default 'public',
  sensitive boolean default false,
  body text,
  title text,
  tags text[],

  count_likes int not null default 0,
  count_reacts int not null default 0,
  count_repeats int not null default 0,
  count_replies int not null default 0,
  count_thread int not null default 0,
  count_attachments int not null default 0,
  count_bookmarks int not null default 0,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

alter table app_public.note add column if not exists weighted_keywords tsvector generated always as (
  setweight(to_tsvector('simple', coalesce(title, '')), 'A') ||
  setweight(to_tsvector('simple', coalesce(body, '')), 'B')
) stored;

create index on app_public.note using rum (weighted_keywords);

create trigger sync_mtime before update on app_public.note
  for each row execute procedure sync_mtime();

create index on app_public.note (user_id);
create index on app_public.note (reply_id);
create index on app_public.note (ctime);
create index on app_public.note (remote);
create index on app_public.note (sensitive);
create index on app_public.note (domain_id);
create index on app_public.note (thread_id);
create index on app_public.note (count_attachments);

create function app_public.tg_050_note_default_thread_id() returns trigger as $$
declare
begin
  if new.thread_id is null then
    new.thread_id = new.note_id;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_default_thread_id before insert or update on app_public.note for each row
  execute procedure app_public.tg_050_note_default_thread_id();

create function app_public.tg_050_deliver_note() returns trigger as $$
  declare uid uuid;
begin
  if not new.remote then
    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'NoteCreate',
      'userId', new.user_id,
      'noteId', new.note_id
    ));
  end if;
  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_note after insert on app_public.note for each row
  execute procedure app_public.tg_050_deliver_note();

create function app_public.tg_050_notify_note_created() returns trigger as $$
  declare uid uuid;
begin
  if new.scope = 'public' or new.scope = 'unlisted' or new.scope = 'local' then
    perform pg_notify('pg:note:created', json_build_object(
      '__node__', json_build_array('notes', new.note_id)
    )::text);
  elseif new.scope = 'private' then
    for uid in (select follower_id from app_public.follow where followed_id=new.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:created:', uid), json_build_object(
        '__node__', json_build_array('notes', new.note_id)
      )::text);
    end loop;
  elseif new.scope = 'direct' then
    for uid in (select user_id from app_public.mention where user_id=new.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:created:', uid), json_build_object(
        '__node__', json_build_array('notes', new.note_id)
      )::text);
    end loop;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger notify_note_created after insert on app_public.note for each row
  execute procedure app_public.tg_050_notify_note_created();

create function app_public.tg_050_notify_note_updated_body() returns trigger as $$
  declare uid uuid;
begin
  if old.body = new.body and old.title = new.title then
    return new;
  elseif new.scope = 'public' or new.scope = 'unlisted' or new.scope = 'local' then
    perform pg_notify('pg:note:updated:body', json_build_object(
      '__node__', json_build_array('notes', new.note_id)
    )::text);
  elseif new.scope = 'private' then
    for uid in (select follower_id from app_public.follow where followed_id=new.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:updated:body:', uid), json_build_object(
        '__node__', json_build_array('notes', new.note_id)
      )::text);
    end loop;
  elseif new.scope = 'direct' then
    for uid in (select user_id from app_public.mention where user_id=new.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:updated:body:', uid), json_build_object(
        '__node__', json_build_array('notes', new.note_id)
      )::text);
    end loop;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger notify_note_updated_body after update on app_public.note for each row
  execute procedure app_public.tg_050_notify_note_updated_body();

create function app_public.tg_050_notify_note_deleted() returns trigger as $$
  declare uid uuid;
begin
  if old.scope = 'public' or old.scope = 'unlisted' or old.scope = 'local' then
    perform pg_notify('pg:note:deleted', json_build_object(
      '__node__', json_build_array('notes', old.note_id)
    )::text);
  elseif old.scope = 'private' then
    for uid in (select follower_id from app_public.follow where followed_id=old.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:deleted:', uid), json_build_object(
        '__node__', json_build_array('notes', old.note_id)
      )::text);
    end loop;
  elseif old.scope = 'direct' then
    for uid in (select user_id from app_public.mention where user_id=old.user_id and remote=false) loop 
      perform pg_notify(concat('pg:note:deleted:', uid), json_build_object(
        '__node__', json_build_array('notes', old.note_id)
      )::text);
    end loop;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger notify_note_deleted after delete on app_public.note for each row
  execute procedure app_public.tg_050_notify_note_deleted();

create function app_public.tg_050_note_replies_up() returns trigger as $$
begin
  update app_public.note
    set count_replies = count_replies + 1
    where note_id = new.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_replies_up after insert
  on app_public.note
  for each row
  execute procedure app_public.tg_050_note_replies_up();

create function app_public.tg_050_note_replies_down() returns trigger as $$
begin
  update app_public.note
    set count_replies = greatest(0, count_replies - 1)
    where note_id = old.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_replies_down after delete
  on app_public.note
  for each row
  execute procedure app_public.tg_050_note_replies_down();

create function app_public.tg_050_note_thread_up() returns trigger as $$
begin
  if new.thread_id is not null then
    update app_public.note
      set count_thread = count_thread + 1
      where note_id = new.thread_id;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_thread_up after insert
  on app_public.note
  for each row
  execute procedure app_public.tg_050_note_thread_up();

create function app_public.tg_050_note_thread_down() returns trigger as $$
begin
  if old.thread_id is not null then
    update app_public.note
      set count_thread = greatest(0, count_thread - 1)
      where note_id = old.thread_id;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_thread_down after delete
  on app_public.note
  for each row
  execute procedure app_public.tg_050_note_thread_down();

create function app_public.tg_050_user_notes_up() returns trigger as $$
begin
  update app_public.user
    set count_notes = count_notes + 1
    where user_id = new.user_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_notes_up after insert
  on app_public.note
  for each row
  execute procedure app_public.tg_050_user_notes_up();

create function app_public.tg_050_user_notes_down() returns trigger as $$
begin
  update app_public.user
    set count_notes = greatest(0, count_notes - 1)
    where user_id = old.user_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger user_notes_down after delete
  on app_public.note
  for each row
  execute procedure app_public.tg_050_user_notes_down();