create table app_public.field (
  user_id uuid references app_public.user (user_id),
  label text not null,
  body text not null,

  ctime timestamptz not null default now(),
  mtime timestamptz,

  primary key (user_id, label)
);

create index on app_public.field (user_id);

create trigger sync_mtime before update on app_public.field
  for each row execute procedure sync_mtime();