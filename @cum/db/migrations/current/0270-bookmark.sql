create table app_public.bookmark (
  note_id uuid not null references app_public.note (note_id),
  user_id uuid not null references app_public.user (user_id),

  ctime timestamptz not null default now(),
  mtime timestamptz,

  primary key (note_id, user_id)
);

create trigger sync_mtime before update on app_public.bookmark
  for each row execute procedure sync_mtime();

create index on app_public.bookmark (user_id);

create function app_public.tg_050_note_bookmark_up() returns trigger as $$
begin
  update app_public.note
    set count_bookmarks = count_bookmarks + 1
    where note_id = new.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_bookmark_up after insert
  on app_public.bookmark
  for each row
  execute procedure app_public.tg_050_note_bookmark_up();

create function app_public.tg_050_note_bookmark_down() returns trigger as $$
begin
  update app_public.note
    set count_bookmarks = greatest(0, count_bookmarks - 1)
    where note_id = old.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_bookmark_down after delete
  on app_public.bookmark
  for each row
  execute procedure app_public.tg_050_note_bookmark_down();

create function app_public.note_set_bookmark(
  note_id uuid,
  state boolean
) returns app_public.note as $$
declare
  note app_public.note;
begin
  if state then
    insert into app_public.bookmark (note_id, user_id)
      values (note_id, app_hidden.current_user_id())
      on conflict do nothing;
  else
    delete from app_public.bookmark where app_public.bookmark.note_id = $1 and user_id = app_hidden.current_user_id();
  end if;

  select * from app_public.note into note
    where app_public.note.note_id = $1
    limit 1;
  
  return note;
end;
$$ language plpgsql;