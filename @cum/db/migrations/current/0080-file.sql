create table app_public.file (
  blake3 text primary key,
  remote boolean not null default false,
  ref_count int not null default 0,
  size int not null,
  width int,
  height int,
  blurhash text,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create index on app_public.file (ref_count) where ref_count = 0;

create trigger sync_mtime before update on app_public.file
  for each row execute procedure sync_mtime();
