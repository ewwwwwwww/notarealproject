drop function if exists app_private.set_current_user;

create function app_private.set_current_user(user_id text) returns void as $$
begin
  set role :DATABASE_AUTH;
  perform set_config('jwt.claims.user_id', user_id, false);

  return;
end;
$$ language plpgsql;

drop function if exists sync_mtime;

create function sync_mtime() returns trigger as $$
begin
  new.mtime := now();

  return new;
end;
$$ language plpgsql;