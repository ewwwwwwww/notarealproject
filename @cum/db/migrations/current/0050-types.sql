create collation if not exists case_insensitive (
  provider = icu,
  locale = 'und-u-ks-level2',
  deterministic = false
);

drop type if exists app_hidden.jwt_token;

create type app_hidden.jwt_token as (
  role text,
  user_id uuid
);

drop type if exists scope;

create type scope as enum (
  'public',
  'private',
  'local',
  'unlisted',
  'direct'
);

drop type if exists attachment_type;

create type attachment_type as enum (
  'image',
  'video',
  'animated',
  'other'
);

drop type if exists activity_direction;

create type activity_direction as enum (
  'local',
  'remote',
  'inbound',
  'outbound'
);

drop type if exists notification_type;

create type notification_type as enum (
  'block',
  'follow',
  'like',
  'mention',
  'react',
  'repeat'
);

drop type if exists tribool;

create type tribool as enum (
  'true',
  'false',
  'maybe'
);

drop type if exists activity_type;

create type activity_type as enum (
  'note_create',
  'follow',
  'follow_undo'
);