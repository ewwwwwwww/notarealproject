create table app_public.notification (
  notification_id uuid primary key default fn_uuid_time_ordered(),
  user_id uuid not null references app_public.user (user_id),

  type notification_type not null,
  read boolean not null default false,
  undo boolean not null default false,

  block_id uuid references app_public.block (block_id) on delete cascade,
  follow_id uuid references app_public.follow (follow_id) on delete cascade,
  like_id uuid references app_public.like (like_id) on delete cascade,
  mention_id uuid references app_public.mention (mention_id) on delete cascade,
  react_id uuid references app_public.react (react_id) on delete cascade,
  repeat_id uuid references app_public.repeat (repeat_id) on delete cascade,
  
  constraint prevent_dangling_ref check (
    type = 'block' and block_id is not null or
    type = 'follow' and follow_id is not null or
    type = 'like' and like_id is not null or
    type = 'mention' and mention_id is not null or
    type = 'react' and react_id is not null or
    type = 'repeat' and repeat_id is not null
  ),

  ctime timestamptz not null,
  mtime timestamptz
);

create unique index on app_public.notification (user_id, ctime, type);
create index on app_public.notification (read);
create index on app_public.notification (block_id);
create index on app_public.notification (follow_id);
create index on app_public.notification (like_id);
create index on app_public.notification (mention_id);
create index on app_public.notification (react_id);
create index on app_public.notification (repeat_id);
create index on app_public.notification (ctime);

create trigger sync_mtime before update on app_public.notification
  for each row execute procedure sync_mtime();

create function app_public.tg_050_notification_count_up() returns trigger as $$
begin
  update app_public.user
    set count_notifications = count_notifications + 1,
        count_notifications_unread = count_notifications_unread + 1
    where user_id = new.user_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_up after insert
  on app_public.notification
  for each row
  execute procedure app_public.tg_050_notification_count_up();

create function app_public.tg_050_notification_count_down() returns trigger as $$
begin
  if old.read then
    update app_public.user
      set count_notifications = greatest(0, count_notifications - 1)
      where user_id = old.user_id;
  else
    update app_public.user
      set count_notifications = greatest(0, count_notifications - 1),
          count_notifications_unread = greatest(0, count_notifications_unread - 1)
      where user_id = old.user_id;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_down after delete
  on app_public.notification
  for each row
  execute procedure app_public.tg_050_notification_count_down();

create function app_public.tg_050_notify_notification_created() returns trigger as $$
begin
  perform pg_notify(concat('pg:noti:created:', new.user_id), json_build_object(
    '__node__', json_build_array('notifications', new.notification_id)
  )::text);

  return new;
end;
$$ language plpgsql volatile;

create trigger notify_notification_created after insert on app_public.notification for each row
  execute procedure app_public.tg_050_notify_notification_created();