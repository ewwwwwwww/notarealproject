create table app_public.react_aggr (
  note_id uuid not null references app_public.note (note_id),

  blake3 text references app_public.file (blake3),
  src text,

  count int not null default 1,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create unique index on app_public.react_aggr (note_id, blake3);
create unique index on app_public.react_aggr (note_id, src);
create index on app_public.react_aggr (blake3);
create index on app_public.react_aggr (note_id);
create index on app_public.react_aggr (count);

create trigger sync_mtime before update on app_public.react_aggr
  for each row execute procedure sync_mtime();

create function app_public.tg_050_react_aggr_up() returns trigger as $$
begin
  if (new.blake3 is not null) then
    insert into app_public.react_aggr (note_id, blake3)
      values (new.note_id, new.blake3)
      on conflict (note_id, blake3) do
      update set count = app_public.react_aggr.count + 1;
  else
    insert into app_public.react_aggr (note_id, src)
      values (new.note_id, new.src)
      on conflict (note_id, src) do
      update set count = app_public.react_aggr.count + 1;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger react_aggr_up after insert
  on app_public.react
  for each row
  execute procedure app_public.tg_050_react_aggr_up();

create function app_public.tg_050_react_aggr_down() returns trigger as $$
begin
  if (old.blake3 is not null) then
    update app_public.react_aggr
      set count = count - 1
      where note_id = old.note_id and blake3 = old.blake3;
  else
    update app_public.react_aggr
      set count = count - 1
      where note_id = old.note_id and src = old.src;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger react_aggr_down after delete
  on app_public.react
  for each row
  execute procedure app_public.tg_050_react_aggr_down();