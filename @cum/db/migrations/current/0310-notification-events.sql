
create function app_public.tg_050_notification_block() returns trigger as $$
begin
  insert into app_public.notification (type, block_id, user_id, ctime) values (
    'block',
    new.block_id,
    new.blocked_id,
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_block after insert on app_public.block for each row
  execute procedure app_public.tg_050_notification_block();

create function app_public.tg_050_notification_follow() returns trigger as $$
begin
  insert into app_public.notification (type, follow_id, user_id, ctime) values (
    'follow',
    new.follow_id,
    new.followed_id,
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_follow after insert on app_public.follow for each row
  execute procedure app_public.tg_050_notification_follow();

create function app_public.tg_050_notification_like() returns trigger as $$
begin
  insert into app_public.notification (type, like_id, user_id, ctime) values (
    'like',
    new.like_id,
    (select user_id from app_public.note where note_id = new.note_id),
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_like after insert on app_public.like for each row
  execute procedure app_public.tg_050_notification_like();

create function app_public.tg_050_notification_mention() returns trigger as $$
begin
  insert into app_public.notification (type, mention_id, user_id, ctime) values (
    'mention',
    new.mention_id,
    new.user_id,
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_mention after insert on app_public.mention for each row
  execute procedure app_public.tg_050_notification_mention();

create function app_public.tg_050_notification_react() returns trigger as $$
begin
  insert into app_public.notification (type, react_id, user_id, ctime) values (
    'react',
    new.react_id,
    (select user_id from app_public.note where note_id = new.note_id),
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_react after insert on app_public.react for each row
  execute procedure app_public.tg_050_notification_react();

create function app_public.tg_050_notification_repeat() returns trigger as $$
begin
  insert into app_public.notification (type, repeat_id, user_id, ctime) values (
    'repeat',
    new.repeat_id,
    (select user_id from app_public.note where note_id = new.note_id),
    new.ctime
  );

  return new;
end;
$$ language plpgsql volatile;

create trigger notification_repeat after insert on app_public.repeat for each row
  execute procedure app_public.tg_050_notification_repeat();