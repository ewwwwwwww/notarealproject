create table app_public.user_attachment (
  attachment_id uuid primary key default fn_uuid_time_ordered(),
  user_id uuid not null references app_public.user (user_id),
  file_id text references app_public.file (blake3) on delete restrict,
  src text,
  
  constraint prevent_empty check (
    file_id is not null or src is not null
  ),

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create trigger sync_mtime before update on app_public.user_attachment
  for each row execute procedure sync_mtime();

create index on app_public.user_attachment (attachment_id);
create index on app_public.user_attachment (user_id);
create index on app_public.user_attachment (file_id);

alter table app_public.user
  add column avatar_id uuid references app_public.user_attachment (attachment_id),
  add column banner_id uuid references app_public.user_attachment (attachment_id);
  
create index on app_public.user (avatar_id);
create index on app_public.user (banner_id);