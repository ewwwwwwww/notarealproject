create function app_public.now() returns timestamptz as $$
  select now();
$$ language sql stable;