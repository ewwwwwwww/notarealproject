create table app_hidden.email (
  user_id uuid primary key references app_public.user (user_id),

  email text not null check (email ~ '[^@]+@[^@]+\.[^@]+'),
  ctime timestamptz not null default now(),
  mtime timestamptz
);

create trigger sync_mtime before update on app_hidden.email
  for each row execute procedure sync_mtime();