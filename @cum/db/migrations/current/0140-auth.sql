create function app_public.user_login(
  name text,
  pass text
) returns app_hidden.jwt_token as $$
  select (':DATABASE_AUTH', user_id)::app_hidden.jwt_token from app_private.user
    where
      user_id = (select user_id from app_public.user where name=$1)
      and phash = crypt($2, phash);
$$ language sql strict security definer;

comment on function app_public.user_login(text, text) is 'Creates a JWT auth token if pass hashes match';

create function app_public.me() returns app_public.user as $$
  select * from app_public.user
    where user_id = current_setting('jwt.claims.user_id', true)::uuid
$$ language sql stable;

comment on function app_public.me() is 'Gets the user who was identified by our JWT.';

create function app_hidden.current_user_id() returns uuid as $$
  select user_id from app_public.me()
$$ language sql stable;