create table app_public.user (
  user_id uuid primary key default fn_uuid_time_ordered(),
  domain_id uuid not null references app_public.domain (domain_id),

  name text collate "case_insensitive" not null,
  name_full text not null,
  nick text,
  dead bool default false,
  remote bool not null default false,
  body text,
  caption text,

  count_followers int not null default 0,
  count_following int not null default 0,
  count_blockers int not null default 0,
  count_blocking int not null default 0,
  count_notifications int not null default 0,
  count_notifications_unread int not null default 0,
  count_notes int not null default 0,
  
  manually_approves_followers boolean not null default false,

  ctime timestamptz not null default now(),
  mtime timestamptz,

  constraint name_domain unique (name, domain_id)
);

create unique index on app_public.user (name_full text_pattern_ops);
create unique index on app_public.user (name, remote) where remote is false;

create index on app_public.user (domain_id);

comment on column app_public.user.nick is
  E'Custom nickname that can change';
comment on column app_public.user.name is
  E'Unique username that never changes';
comment on column app_public.user.domain_id is
  E'Domain the user originates from';
comment on column app_public.user.remote is
  E'Whether or not the user comes from here or a foreign instance';
comment on column app_public.user.ctime is
  E'Creation time';
comment on column app_public.user.mtime is
  E'Modified time';
comment on column app_public.user.user_id is
  E'Unique identifier for the user.';

create table app_hidden.user (
  user_id uuid primary key references app_public.user (user_id),

  pubkey text,
  actor text unique,
  inbox text,
  following text,
  followers text,
  featured text,
  sharedInbox text,
  ctime timestamptz not null default now(),
  mtime timestamptz
);

alter table app_public.user enable row level security;
create policy select_all on app_public.user for select using (true);
grant select on app_public.user to :DATABASE_VISITOR;
grant select on app_public.user to :DATABASE_AUTH;

comment on column app_hidden.user.pubkey is
  E'Pubkey for verifying activity pub messages';
comment on column app_hidden.user.ctime is
  E'Creation time';
comment on column app_hidden.user.mtime is
  E'Modified time';
comment on column app_hidden.user.user_id is
  E'Reference to user_id';

create table app_private.user (
  user_id uuid primary key references app_public.user (user_id),

  phash text not null,
  privkey text,
  ctime timestamptz not null default now(),
  mtime timestamptz
);

comment on column app_private.user.phash is
  E'Password hash';
comment on column app_private.user.privkey is
  E'Private key for signing activity pub messages';
comment on column app_private.user.ctime is
  E'Creation time';
comment on column app_private.user.mtime is
  E'Modified time';
comment on column app_private.user.user_id is
  E'Reference to user_id';

create trigger sync_mtime before update on app_public.user
  for each row execute procedure sync_mtime();

create trigger sync_mtime before update on app_hidden.user
  for each row execute procedure sync_mtime();

create trigger sync_mtime before update on app_private.user
  for each row execute procedure sync_mtime();

create function app_public.user_by_name(
  name text,
  domain text
) returns app_public.user as $$
  select * from app_public.user where name=$1
  and case
    when domain is not null then domain_id = (select domain_id from app_public.domain where app_public.domain.host = $2)
    else remote = false
  end limit 1
$$ language sql stable;