create function app_public.tg_050_revive_dead() returns trigger as $$
begin
  if not (select dead from app_public.user where user_id = new.user_id) then
    update app_public.user set dead = false where user_id = new.user_id;
    update app_public.follow set dead = false where followed_id = new.user_id;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_follow after insert on app_public.note for each row
  execute procedure app_public.tg_050_revive_dead();
