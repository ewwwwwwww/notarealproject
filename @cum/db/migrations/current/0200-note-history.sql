create table app_public.note_history (
  note_id uuid primary key default fn_uuid_time_ordered(),
  body text,
  ctime timestamptz not null default now(),
  mtime timestamptz
);

create trigger sync_mtime before update on app_public.note_history
  for each row execute procedure sync_mtime();

create function app_public.tg_050_deliver_note_edit() returns trigger as $$
begin
  if old.body != new.body then
    insert into app_public.note_history (note_id, body)
      values (old.note_id, old.body);

    new.note_id = fn_uuid_time_ordered();

    if new.remote then
      return new;
    end if;

    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'NoteEdit',
      'userId', new.user_id,
      'noteId', new.note_id
    ));
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_note_edit after update on app_public.note for each row
  execute procedure app_public.tg_050_deliver_note_edit();