create function app_public.note_is_liked_by_you(note app_public.note) returns boolean as $$
  select coalesce((
    select true from app_public.like
    where app_public.like.note_id = note.note_id
      and app_public.like.user_id = app_hidden.current_user_id()
  ), false)
$$ language sql stable;

create function app_public.note_is_bookmarked_by_you(note app_public.note) returns boolean as $$
  select coalesce((
    select true from app_public.bookmark
    where app_public.bookmark.note_id = note.note_id
      and app_public.bookmark.user_id = app_hidden.current_user_id()
  ), false)
$$ language sql stable;

create function app_public.note_is_repeated_by_you(note app_public.note) returns boolean as $$
  select coalesce((
    select true from app_public.repeat
    where app_public.repeat.note_id = note.note_id
      and app_public.repeat.user_id = app_hidden.current_user_id()
  ), false)
$$ language sql stable;