create table app_public.mention (
  mention_id uuid primary key default fn_uuid_time_ordered(),
  user_id uuid not null references app_public.user (user_id),
  note_id uuid not null references app_public.note (note_id),
  remote boolean not null default false,
  reply boolean not null default false,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create unique index on app_public.mention (note_id, user_id);
create index on app_public.mention (user_id);

create trigger sync_mtime before update on app_public.mention
  for each row execute procedure sync_mtime();