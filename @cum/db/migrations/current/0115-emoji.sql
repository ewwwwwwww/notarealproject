create table app_public.emoji (
  user_id uuid not null references app_public.user (user_id),
  file_id text not null references app_public.file (blake3),
  
  src text,
  shortcode text not null,
  
  primary key (user_id, file_id),
  
  ctime timestamptz not null default now(),
  mtime timestamptz
);

create trigger sync_mtime before update on app_public.emoji
  for each row execute procedure sync_mtime();

create index on app_public.emoji (file_id);