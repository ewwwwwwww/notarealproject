create table app_hidden.activity (
  activity_id uuid default fn_uuid_time_ordered(),

  type activity_type not null,
  data uuid not null,

  uri text,

  remote boolean not null default false,
  ctime timestamptz not null default now(),
  mtime timestamptz
);

comment on column app_hidden.activity.ctime is
  E'Creation time';
comment on column app_hidden.activity.mtime is
  E'Modified time';
comment on column app_hidden.activity.activity_id is
  E'Primary surrogate uuid key';

create trigger sync_mtime before update on app_hidden.activity
  for each row execute procedure sync_mtime();

create index on app_hidden.activity (activity_id);
create index on app_hidden.activity (uri);