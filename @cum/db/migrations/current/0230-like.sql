create table app_public.like (
  like_id uuid primary key default fn_uuid_time_ordered(),
  note_id uuid not null references app_public.note (note_id),
  user_id uuid not null references app_public.user (user_id),
  remote boolean not null default false,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create unique index on app_public.like (note_id, user_id);
create index on app_public.like (user_id);

create trigger sync_mtime before update on app_public.like
  for each row execute procedure sync_mtime();

create function app_public.tg_050_deliver_like() returns trigger as $$
begin
  if not new.remote then
    perform graphile_worker.add_job('activityDeliver', json_build_object(
      'type', 'Like',
      'userId', new.user_id,
      'noteId', new.note_id
    ));
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_like after insert
  on app_public.like
  for each row
  execute procedure app_public.tg_050_deliver_like();

create function app_public.tg_050_note_like_up() returns trigger as $$
begin
  update app_public.note
    set count_likes = count_likes + 1
    where note_id = new.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_like_up after insert
  on app_public.like
  for each row
  execute procedure app_public.tg_050_note_like_up();

create function app_public.tg_050_note_like_down() returns trigger as $$
begin
  update app_public.note
    set count_likes = greatest(0, count_likes - 1)
    where note_id = old.note_id;

  return new;
end;
$$ language plpgsql volatile;

create trigger note_like_down after delete
  on app_public.like
  for each row
  execute procedure app_public.tg_050_note_like_down();

create function app_public.note_set_like(
  note_id uuid,
  state boolean
) returns app_public.note as $$
declare
  note app_public.note;
begin
  if state then
    insert into app_public.like (note_id, user_id)
      values (note_id, app_hidden.current_user_id())
      on conflict do nothing;
  else
    delete from app_public.like where app_public.like.note_id = $1 and user_id = app_hidden.current_user_id();
  end if;

  select * from app_public.note into note
    where app_public.note.note_id = $1
    limit 1;
  
  return note;
end;
$$ language plpgsql;