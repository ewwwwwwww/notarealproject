create function app_public.user_register(
  name text,
  pass text
) returns app_public.user as $$
declare
  person app_public.user;
  ldomain app_public.domain;
begin
  if not (select open_registrations from app_public.instance()) then
    raise exception 'Registrations are closed';
  end if; 

  if exists (select from app_public.user where app_public.user.name=$1 and remote=false) then
    raise exception 'Username taken';
  end if; 
  
  select * into ldomain from app_public.domain_local();

  insert into app_public.user (name, name_full, domain_id) values (
    $1,
    $1 || '@' || ldomain.host,
    ldomain.domain_id
  ) returning * into person;

  insert into app_hidden.user (user_id) values (
    person.user_id
  );

  insert into app_private.user (user_id, phash) values
    (person.user_id, crypt($2, gen_salt('bf')));

  perform graphile_worker.add_job('genUserKeys', json_build_object('userId', person.user_id));

  return person;
end;
$$ language plpgsql strict security definer;

comment on function app_public.user_register(text, text) is 'Registers a single user and creates an account.';

create function app_private.user_register_remote(
  name text,
  domain_name text,
  actor_url text,
  inbox_url text,
  pub_key text
) returns app_public.user as $$
declare
  person app_public.user;
  did uuid;
begin
  select domain_id into did from app_public.domain_get($2);

  if exists (select from app_public.user where app_public.user.name=$1 and domain_id=did) then
    raise exception 'Username exists';
  end if; 

  insert into app_public.user (name, name_full, domain_id, remote) values (
    $1,
    $1 || '@' || domain_name,
    did,
    true
  ) returning * into person;

  insert into app_hidden.user (user_id, actor, inbox, pubkey)
    values (person.user_id, $3, $4, $5);

  return person;
end;
$$ language plpgsql strict security definer;