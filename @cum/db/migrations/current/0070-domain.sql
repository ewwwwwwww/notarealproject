create table app_public.domain (
  domain_id uuid primary key default fn_uuid_time_ordered(),

  host text collate "case_insensitive" unique,
  name text,
  
  open_registrations bool,

  ctime timestamptz not null default now(),
  mtime timestamptz
);

comment on column app_public.domain.domain_id
  is E'unique surrogate key for domain name';

comment on column app_public.domain.host
  is E'domain name i.e. example.com';

comment on column app_public.domain.ctime
  is E'creation time';

comment on column app_public.domain.mtime
  is E'modified time';

drop function if exists app_public.domain_local;

create function app_public.domain_local() returns app_public.domain as $$
  select * from app_public.domain where host = ':DOMAIN';
$$ language sql stable;

comment on function app_public.domain_local
  is E'returns localhosts domain row';

drop function if exists app_public.domain_local_id;

create function app_public.domain_local_id() returns uuid as $$
  select domain_id from app_public.domain where host = ':DOMAIN';
$$ language sql stable;

comment on function app_public.domain_local_id
  is E'returns localhosts domain id';

create trigger sync_mtime before update on app_public.domain
  for each row execute procedure sync_mtime();

insert into app_public.domain (host, name, open_registrations) values (
  ':DOMAIN',
  upper(':DOMAIN'),
  true
) on conflict (host) do nothing;

drop function if exists app_public.domain_get;

create function app_public.domain_get(
  domain text
) returns app_public.domain as $$
  with cte as (
     insert into app_public.domain (name)
     values ($1)
     on conflict (name) do nothing
     returning *
  )
  select * from cte
  union all
  select * from app_public.domain
  where name = $1 and not exists (select 1 from cte);
$$ language sql volatile;

comment on function app_public.domain_get
  is E'finds a domain or upserts it';

drop function if exists app_public.tg_050_new_domain;

create function app_public.tg_050_new_domain() returns trigger as $$
begin
  perform graphile_worker.add_job('newDomain', json_build_object(
    'domain', new.name,
    'domainId', new.domain_id
  ));

  return new;
end;
$$ language plpgsql volatile;

create trigger tg_050_new_domain after insert
  on app_public.domain
  for each row
  execute procedure app_public.tg_050_new_domain();

create function app_public.instance() returns app_public.domain as $$
  select * from app_public.domain where name = ':DOMAIN';
$$ language sql stable;