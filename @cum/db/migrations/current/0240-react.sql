create table app_public.react (
  react_id uuid primary key default fn_uuid_time_ordered(),
  note_id uuid not null references app_public.note (note_id),
  user_id uuid not null references app_public.user (user_id),

  file_id text references app_public.file (blake3) on delete restrict,
  src text,

  constraint prevent_either check (
    src is null and file_id is not null or
    src is not null and file_id is null
  ),

  ctime timestamptz not null default now(),
  mtime timestamptz
);

create index on app_public.react (file_id);
create unique index on app_public.react (note_id, file_id);
create unique index on app_public.react (note_id, src);

create trigger sync_mtime before update on app_public.react
  for each row execute procedure sync_mtime();

create index on app_public.react (user_id);

create function app_public.tg_050_deliver_react() returns trigger as $$
begin
  if (select remote from app_public.user where user_id = new.user_id) then
    if (new.file_id is not null) then
      perform graphile_worker.add_job('activityDeliver', json_build_object(
        'type', 'ReactEmoji',
        'userId', new.user_id,
        'noteId', new.note_id
      ));
    else
      perform graphile_worker.add_job('activityDeliver', json_build_object(
        'type', 'ReactUnicode',
        'userId', new.user_id,
        'noteId', new.note_id
      ));
    end if;
  end if;

  return new;
end;
$$ language plpgsql volatile;

create trigger deliver_react after insert
  on app_public.react
  for each row
  execute procedure app_public.tg_050_deliver_react();