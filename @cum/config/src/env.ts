import { config } from './index.js';

const { env } = process;
const { user, host, pass, port, name } = config.database;

env.PGURI = `postgres://${user}:${pass}@${host}:${port}/${name}`;
env.DATABASE_URL = process.env.PGURI;
env.DATABASE_VISITOR = config.database.visitor.user;
env.DATABASE_AUTH = config.database.auth.user;
env.DOMAIN = config.core.domain;

const {
  user: ruser,
  pass: rpass,
  name: rname,
} = config.database.superuser ?? {};

env.SHADOW_DATABASE_URL = process.env.PGURI + '_shadow';
env.ROOT_DATABASE_URL = `postgres://${ruser}:${rpass}@${host}:${port}/${rname}`;
