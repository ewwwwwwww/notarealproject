import { parse } from '@iarna/toml';
import { readFileSync } from 'node:fs';
import { resolve } from 'node:path';
import URL from 'node:url';

import { Boolean, Number, String, Record, Optional, Static } from 'runtypes';

const paths = [
  './config.toml',
  './cumfe.toml',
  './cumbe.toml',
  '../config.toml',
  '../../config.toml',
  './docker/cumfe/config.toml',
  '../../docker/cumfe/config.toml',
];

let data;

for (const path of paths) {
  try {
    const str = readFileSync(resolve(path), 'utf8');

    data = parse(str);

    break;
  } catch (err) {}
}

if (!data) {
  throw new Error('failed to find and parse valid toml config file');
}

const Config = Record({
  core: Record({
    domain: String,
    ssl: Boolean,
  }),

  backend: Record({
    port: Number,
  }),

  frontend: Record({
    port: Number,
  }),

  proxy: Record({
    port: Number,
  }),

  admin: Record({
    port: Number,
  }),

  worker: Record({
    port: Number,
  }),

  database: Record({
    host: String,
    port: Number,
    user: String,
    pass: String,
    name: String,
    ssl: Boolean,
    auth: Record({
      user: String,
      pass: String,
    }),
    visitor: Record({
      user: String,
    }),
    superuser: Optional(
      Record({
        user: String,
        pass: String,
        name: String,
      }),
    ),
  }),

  credentials: Record({
    jwt: String,
  }),
});

export type Config = Static<typeof Config>;

export const config = Config.check(data);

const { user, pass, host, port, name } = config.database;

export const connectionString = `postgres://${user}:${pass}@${host}:${port}/${name}`;

export const isProd = () => {
  return process.env.NODE_ENV === 'production';
};

const url = URL.parse(
  `${config.core.ssl ? 'https' : 'http'}://${config.core.domain}`,
);

export const constants = {
  url,

  webfinger(full = true) {
    return `${full ? url.href : '/'}.well-known/webfinger`;
  },

  hostmeta: `/.well-known/host-meta`,
};

const root = `${constants.url.href}ap`;

export const activityUrl = {
  root,
  context: [
    'https://www.w3.org/ns/activitystreams',
    // `${root}/schemas/litepub-0.1.jsonld`,
  ],

  nodeinfo(version?: number, full = true) {
    return `${full ? root : '/ap'}/nodeinfo/${version}`;
  },

  schema(full = true) {
    return `${full ? root : '/ap'}/schemas/litepub-0.1.jsonld`;
  },

  actor(uuid: string, full = true) {
    return `${full ? root : '/ap'}/actor/${uuid}`;
  },

  inbox(uuid?: string, full = true) {
    return `${full ? root : '/ap'}/inbox/${uuid}`;
  },

  outbox(uuid?: string, full = true) {
    return `${full ? root : '/ap'}/outbox/${uuid}`;
  },

  following(uuid: string, full = true) {
    return `${full ? root : '/ap'}/following/${uuid}`;
  },

  followers(uuid: string, full = true) {
    return `${full ? root : '/ap'}/followers/${uuid}`;
  },

  featured(uuid: string, full = true) {
    return `${full ? root : '/ap'}/featured/${uuid}`;
  },

  activity(uuid: string, full = true) {
    return `${full ? root : '/ap'}/featured/${uuid}`;
  },
};
