## Current Status

Activity pub handling is on hold until a functioning micro-blogger is done.

The main work right now is around `@cum/client/components` and `@cum/server/migrations/current.sql`

## Getting Started

1\. **Install Deps**

```bash
apt install ... # need watchman, postgres, yarn
yarn install
```

1\. **Setup DB**

```bash
yarn prepare
yarn setup
```

2\. **Run watch script**

```bash
yarn watch
```

3\. **View in your browser**

[localhost:5000](localhost:5000)

> react components with cosmos

[localhost:3036/graphiql](localhost:3036/graphiql)

> graphiql inspector

[localhost:3000](localhost:3000)

> nextjs frontend (does nothing right now)

## Monorepo Structure

```
├── @cum
│   ├── admin               # adminJS (todo)
│   ├── check               # activity pub validation / parser
│   ├── client              # frontend
│   ├── commander           # cli client (todo)
│   ├── config              # shared env vars
│   ├── graphql             # graphql schema
│   ├── proxy               # image proxy resize server (todo)
│   ├── queries             # typed sql queries
│   ├── server
│   │   ├── migrations
│   │   │   └── current.sql # database schema
│   │   ├── src
│   │   │   ├── activity    # activity routing
│   │   │   ├── plugin      # fastify plugins
│   │   │   ├── route       # http routes
│   │   │   ├── task        # graphile job tasks
│   └── worker              # job tasks as standalone server (todo)
```
