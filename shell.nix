{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell rec {
  name = "cumfe-shell";

  buildInputs = with pkgs; with nodePackages; [
    yarn
    # watchman
    # postgresql
    (postgresql.withPackages (p: [
      p.timescaledb
      p.rum
    ]))
    # nodejs-18_x
    # typescript
    # eslint
    # prettier
  ];

  postPhases = ''
    corepack enable
  '';

  shellHook = ''
    export PGDATA="$PWD/.tmp/db"
    export SOCKET_DIRECTORIES="$PWD/.tmp/sockets"
    mkdir $SOCKET_DIRECTORIES
    initdb

    echo "unix_socket_directories = '$SOCKET_DIRECTORIES'" >> $PGDATA/postgresql.conf
    echo "shared_preload_libraries = 'timescaledb'" >> $PGDATA/postgresql.conf

    pg_ctl -l $PGDATA/logfile start
    createuser postgres --createdb -h localhost
    function end {
      echo "Shutting down the database..."
      pg_ctl stop
      echo "Removing directories..."
      rm -rf $PGDATA $SOCKET_DIRECTORIES
    }
    trap end EXIT
  '';
}
